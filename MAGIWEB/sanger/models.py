"""    *** ****  ****  **** ****     18 FEBRUARY 2022    """ """ """  """ """ """

AUTHOR: ING. MUHARREM DAJA

CONTACT: REMIDAJA@OUTLOOK.COM || MUHARREM.DAJA@FTI.EDU.AL

""" """ """  """ """ """  """ """ """  """ """ """  """ """ """  """ """ """  """
from django.db import models
from django.core.files.storage import FileSystemStorage
from django.contrib.auth.models import User

from simple_history.models import HistoricalRecords

import os
# Create your models here.

import datetime
#now = datetime.datetime(1900,01,01) #.strftime("%D/%M/%Y")
YEAR = datetime.datetime.today().year

NGS = 'NGS'
SANGER = 'SANGER'
MLPA = 'MLPA'
MICROSATELLITI = 'MICROSATELLITI'
Lavorazione = 'In Lavorazione'
PCR_eluizione_sequenze = 'PCR_eluizione_sequenze'
RIPETERE = 'RIPETERE'
NOSANGER = 'NOSANGER'
DROPOUT = 'DROPOUT'
concluso = 'Concluso'

B = 'Benign'
LB = 'Likely Benign'
LP = 'Likely Pathogenic'
P = 'Pathogenic'

VUS = 'Uncertain Significance'
UNK = 'UNKNOWN'

SI = 'SI'
NO = 'NO'

CHOICES_PRESELEZIONE = (
        ('PRESELEZIONATO', 'PRESELEZIONATO'),
        ('SELEZIONATO', 'SELEZIONATO'),
        ('ESCLUSO', 'ESCLUSO'),
)

CHOICES_SINO = (
    (None, '---',),
    (SI, 'SI'),
    (NO, 'NO'),
)

CHOICES_METHOD = (
    (None, '---'),
    (NGS, 'NGS'),
    (SANGER, 'SANGER'),
    (MLPA, 'MLPA'),
    (MICROSATELLITI, 'MICROSATELLITI'),
)

# CHOICES_RILEVANZA = (
#     ('Patogenetica', 'Patogenetica'),
#     ('Verosimilmente Patogenetica', 'Verosimilmente Patogenetica'),
#     ('VUS', 'VUS'),
#     ('Non Determinata', 'Non Determinata'),
#     ('Verosimilmente Benigna', 'Verosimilmente Benigna'),
#     ('Benigna', 'Benigna'),
#     ('Non Determinata', 'Non Determinata'),
# )

CHOICES_RILEVANZA = (
    (B, 'Benign'),
    (LB, 'Likely Benign'),
    (LP, 'Likely Pathogenic'),
    (P, 'Pathogenic'),
    (VUS, 'Uncertain Significance'),
    (UNK, 'UNKNOWN')
)

CHOICES_ORIGINE = (
        ('Non Determinata', 'Non Determinata'),
        ('Materna', 'Materna'),
        ('Paterna', 'Paterna'),
        ('De Novo', 'De Novo'),
        ('Biparentale', 'Biparentale'),
)

CHOICES_EREDITARIETA = (
        ('AR', 'AR'),
        ('AD', 'AD'),
        ('XL', 'XL'),
        ('YL', 'YL'),
        ('XLR', 'XLR'),
        ('XLD', 'XLD'),
        ('AD/AR', 'AD/AR'),
        ('AR/DIG', 'AR/DIG'),
        ('SOMATICO', 'SOMATICO'),
        ('Non Determinata', 'Non Determinata'),
)


CHOICES_RILEVANZASOMATICA = (
	('Tier I','Tier I'),
	('Tier II', 'Tier II'),
	('Tier III','Tier III'),
	('Tier IV','Tier IV'),
)

CHOICES_LAVORAZIONE = (
                (Lavorazione, 'In Lavorazione'),
                (PCR_eluizione_sequenze, 'PCR_eluizione_sequenze'),
                (concluso, 'Concluso'),
                (NOSANGER, 'NOSANGER'),
                ('ESCLUSO', 'ESCLUSO'),
)

CHOICES_CONFERMA = (
                (Lavorazione, 'In Lavorazione'),
                (SI, 'SI'),
                (NO, 'NO'),
                (NOSANGER, 'NOSANGER'),
                (RIPETERE, 'RIPETERE'),
                (DROPOUT, 'DROPOUT'),
                ('ESCLUSO', 'ESCLUSO'),
)

CHOICES_RISULTATO = (
                ('Primary', 'Primary'),
                ('Secondary', 'Secondary'),
)

# CHOICES_VUSSTATUS = (
#         ('SCONOSCIUTO', 'SCONOSCIUTO'),
#         ('CALDA', 'CALDA'),
#         ('TIEPIDA', 'TIEPIDA'),
#         ('FREDDA', 'FREDDA'),
# )

CHOICES_VUSSTATUS = (
        ('Cold', 'Cold'),
        ('Middle', 'Middle'),
        ('Hot', 'Hot'),
        ('Unknown', 'Unknown'),
)


class AllVariation(models.Model):
    id_interno = models.CharField(max_length=25, null=True, blank=True)
    select = models.BooleanField(default=False)
    preselezione = models.CharField(max_length=20, choices=CHOICES_PRESELEZIONE,
                                        default='SELEZIONATO', null=True, blank=True)
    familiare = models.CharField(max_length=20, choices=CHOICES_SINO, default=NO,
                                    null=True, blank=True)
    malattia_text = models.CharField(max_length=400, null=True, blank=True)
    pannello_text = models.CharField(max_length=40, null=True, blank=True)
    sample_id = models.CharField(max_length=50, null=True, blank=True)
    riferimento = models.CharField(max_length=50, null=True, blank=True)
    tecnologia = models.CharField(max_length=20, choices=CHOICES_METHOD,
                                    default='', null=True, blank=True)
    hgvs = models.CharField(max_length=500, null=True, blank=True)
    annotazione = models.CharField(max_length=50, null=True, blank=True)
    timestamp_sanger = models.DateTimeField(auto_now_add=True, auto_now=False,
                                                null=True, blank=True)
    updated_sanger = models.DateTimeField(auto_now_add=False, auto_now=True,
                                            null=True, blank=True)
    protocollo = models.CharField(max_length=100, null=True, blank=True)
    note = models.TextField(null=True, blank=True)
    note_date = models.DateField(verbose_name = 'Note date', null=True, blank=True)
    stato = models.CharField(max_length=30, choices=CHOICES_LAVORAZIONE,
                                default='In Lavorazione', null=True, blank=True)
    stato_date = models.DateField(verbose_name='Stato Date', null=True, blank=True)
    geno = models.CharField(max_length=100, null=True, blank=True)
    gene = models.CharField(max_length=300, null=True, blank=True)
    refseq = models.CharField(max_length=50, null=True, blank=True)
    exone = models.CharField(max_length=10, null=True, blank=True, verbose_name='Esone')
    strand = models.IntegerField(default=1)
    tipo = models.CharField(max_length=300, null=True, blank=True)
    variazione = models.CharField(max_length=500, null=True, blank=True)
    # primer = models.ForeignKey(Primer, on_delete=models.CASCADE, null=True, blank=True)
    conferma = models.CharField(max_length=300, choices=CHOICES_CONFERMA,
                                    default='In Lavorazione', null=True, blank=True)
    conferma_date = models.DateField(verbose_name = "Conferma Date", null=True, blank=True)
    genetista = models.ForeignKey(User, on_delete=models.CASCADE,related_name='genetista2',
                                    limit_choices_to={'groups__name': "GENETISTA"},
                                    null=True, blank=True)
    operatore = models.ForeignKey(User, on_delete=models.CASCADE, related_name='operatore2',
                                        limit_choices_to={'groups__name': "OPERATORE"},
                                        null=True, blank=True)
    info1 = models.IntegerField(default=0)
    info2 = models.IntegerField(default=0)
    selectunique = models.BooleanField(default=False)
    selectcontrollo = models.BooleanField(default=False)
    rilevanza = models.CharField(max_length=50, choices=CHOICES_RILEVANZA,
                                    null=True, blank=True)
    origine = models.CharField(max_length=50, choices=CHOICES_ORIGINE,
                                null=True, blank=True)
    ereditarieta = models.CharField(max_length=50, choices=CHOICES_EREDITARIETA,
                                        null=True, blank=True)
    importanza = models.CharField(max_length=50, choices=CHOICES_RISULTATO,
                                    null=True, blank=True)
    rilevanza_somatica = models.CharField(max_length=50, choices=CHOICES_RILEVANZASOMATICA,
                                      null=True,blank=True)
    genetista_name = models.CharField(max_length=20, null=True, blank=True)
    operatore_name = models.CharField(max_length=20, null=True, blank=True)
    tablename = models.CharField(max_length=15, null=True, blank=True)
    referto = models.CharField(max_length=10, null=True, blank=True)
    lab = models.CharField(max_length=10, null=True, blank=True)
    report = models.TextField(null=True,blank=True)
    vusstatus = models.CharField(max_length=20, choices=CHOICES_VUSSTATUS,
                                    default='Unknown', null=True, blank=True)

    history = HistoricalRecords()

    class Meta:
            unique_together = ('id_interno', 'hgvs')
            permissions = (
                ("read_sanger", "Can read sanger"),
            )
            verbose_name = 'Validazione Varianti'
            verbose_name_plural = 'Validazione Varianti'

    def __unicode__(self):
        return ('%s %s %s %s') % (self.sample_id, self.hgvs, self.pannello_text)

    def __str__(self):
        return self.id_interno + ' - ('+self.hgvs+')' + ' - ' + self.sample_id + ' - ( ' + self.pannello_text + ' )'
