"""    *** ****  ****  **** ****     18 FEBRUARY 2022    """ """ """  """ """ """

AUTHOR: ING. MUHARREM DAJA

CONTACT: REMIDAJA@OUTLOOK.COM || MUHARREM.DAJA@FTI.EDU.AL

""" """ """  """ """ """  """ """ """  """ """ """  """ """ """  """ """ """  """
from django.contrib import admin

# Register your models here.
from sanger.models import AllVariation
from simple_history.admin import SimpleHistoryAdmin

admin.site.register(AllVariation, SimpleHistoryAdmin)
