from django.apps import AppConfig


class SangerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'sanger'
