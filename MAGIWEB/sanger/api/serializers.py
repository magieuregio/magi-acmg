"""    *** ****  ****  **** ****     18 FEBRUARY 2022    """ """ """  """ """ """

AUTHOR: ING. MUHARREM DAJA

CONTACT: REMIDAJA@OUTLOOK.COM || MUHARREM.DAJA@FTI.EDU.AL

""" """ """  """ """ """  """ """ """  """ """ """  """ """ """  """ """ """  """



from rest_framework import serializers
from sanger.models import AllVariation
import re


class AllVariationSerializer(serializers.ModelSerializer):

    # timestamp_sanger = serializers.DateTimeField(format="%d-%m-%Y")
    # updated_sanger = serializers.DateTimeField(format="%d-%m-%Y")

    class Meta:
        model = AllVariation
        fields = ('pk', 'select', 'preselezione', 'familiare', 'malattia_text',
                    'sample_id', 'riferimento', 'tecnologia', 'hgvs', #'timestamp_sanger',
                    #'updated_sanger',
                    'protocollo', 'note', 'note_date', 'stato',
                    'stato_date', 'geno', 'gene', 'refseq', 'exone', 'strand', 'tipo',
                    'variazione', 'annotazione', 'conferma', 'conferma_date',
                    'genetista', 'operatore', 'info1', 'info2', 'selectunique',
                    'selectcontrollo', 'rilevanza', 'origine', 'ereditarieta',
                    'importanza', 'genetista_name', 'operatore_name', 'tablename',
                    'referto', 'lab', 'rilevanza_somatica', 'report', 'vusstatus',
                    'pannello_text', 'id_interno')

    def validate_sample_id(self, value):
        regex = re.compile(r'^\w{0,10}\d+[.]\d{4,4}$')
        if value == '':
            raise serializers.ValidationError("Sample ID non puo essere vuoto")
        elif not (re.match(regex, value)):
            raise serializers.ValidationError("Attenzione, Sample ID deve essere in un\' altro formato!")
        return value

    def validate_hgvs(self, value):
        if self.instance and self.instance.hgvs != value:
            raise serializers.ValidationError("You are not ALLOWED to edit HGVS field")
        return value
