"""    *** ****  ****  **** ****     18 FEBRUARY 2022    """ """ """  """ """ """

AUTHOR: ING. MUHARREM DAJA

CONTACT: REMIDAJA@OUTLOOK.COM || MUHARREM.DAJA@FTI.EDU.AL

""" """ """  """ """ """  """ """ """  """ """ """  """ """ """  """ """ """  """

from rest_framework import mixins
from rest_framework import generics
from rest_framework import viewsets
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from rest_framework import status


from sanger.models import AllVariation
from sanger.api.serializers import AllVariationSerializer

from rest_framework.filters import SearchFilter
from rest_framework import filters
from django_filters.rest_framework import DjangoFilterBackend

from rest_framework import permissions,renderers,authentication
from rest_framework.permissions import IsAuthenticated,AllowAny
from rest_framework.decorators import api_view
from rest_framework.decorators import authentication_classes, permission_classes
from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework.authentication import BaseAuthentication,SessionAuthentication, BasicAuthentication, TokenAuthentication
from django.shortcuts import render
from sanger.api.permissions import IsOwnerOrReadOnly
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token

from django.shortcuts import get_object_or_404
from rest_framework.decorators import action
from django.http import JsonResponse
from django.http import HttpResponseRedirect
from django.shortcuts import redirect

import io
from rest_framework.parsers import JSONParser
from rest_framework.renderers import JSONRenderer
from rest_framework import serializers
from io import BytesIO


class AllVariationViewSet(ModelViewSet):
    serializer_class = AllVariationSerializer
    # authentication_classes = (BasicAuthentication, SessionAuthentication,)
    # permission_classes = (IsAuthenticated,)

    # filter_backends = [SearchFilter]
    # search_field = ["sample_id"]
    filter_backends = (filters.SearchFilter, DjangoFilterBackend, filters.OrderingFilter,)
    search_fields = ('$sample_id', '$hgvs', '$genetista_name', '$pannello_text', '$id_interno',)
    filter_fields = ('sample_id', 'hgvs', 'genetista_name', 'pannello_text', 'id_interno',)

    # def get(self, request):
    #     return self.list(request)

    def get_queryset(self):
        allvariations = AllVariation.objects.all()
        # allvariations = AllVariation.objects.distinct('sample_id')
        # allvariations = AllVariation.objects.exclude(conferma='NO')
        return allvariations

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers = headers)

    def retrieve(self, request, *args, **kwargs):
        allvariation = self.get_object()
        serializer = self.get_serializer(allvariation)
        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        allvariation = self.get_object()
        self.perform_destroy(allvariation)
        return Response({"message": "It was deleted"}, status=status.HTTP_204_NO_CONTENT)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        allvariation = self.get_object()

        if 'chooseimportance' in request.headers['name']:

            json  = JSONRenderer().render(request.data)
            stream = io.BytesIO(json)
            data = JSONParser().parse(stream)


            serializer = AllVariationSerializer(allvariation, data=data, partial=partial)
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)

            if getattr(allvariation, '_prefetched_objects_cache', None):
                # If 'prefetch_related' has been applied to a queryset, we need to
                # forcibly invalidate the prefetch cache on the instance.
                allvariation._prefetched_objects_cache = {}

            return Response(serializer.data)
