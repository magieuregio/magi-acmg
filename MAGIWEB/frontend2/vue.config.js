// """    *** ****  ****  **** ****     18 FEBRUARY 2022    """ """ """  """ """ """
//
// AUTHOR: ING. MUHARREM DAJA
//
// CONTACT: REMIDAJA@OUTLOOK.COM || MUHARREM.DAJA@FTI.EDU.AL
//
// """ """ """  """ """ """  """ """ """  """ """ """  """ """ """  """ """ """  """
const BundleTracker = require("webpack-bundle-tracker");

module.exports = {
  publicPath: "http://192.168.1.48:8089/",
  outputDir: './dist/',

  chainWebpack: config => {

    config.optimization
      .splitChunks(false)

    config
      .plugin('BundleTracker')
      .use(BundleTracker, [{filename: '../frontend2/webpack-stats.json'}])

    config.resolve.alias
      .set('__STATIC__', 'static')

    config.devServer
      .public('http://192.168.1.48:8089')
      .host('192.168.1.48')
      .port(8089)
      .hotOnly(true)
      .watchOptions({poll: 1000})
      .https(false)
      .headers({"Access-Control-Allow-Origin": ["\*"]})
  }
};
