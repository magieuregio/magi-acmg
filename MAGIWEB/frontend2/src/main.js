// """    *** ****  ****  **** ****     18 FEBRUARY 2022    """ """ """  """ """ """
//
// AUTHOR: ING. MUHARREM DAJA
//
// CONTACT: REMIDAJA@OUTLOOK.COM || MUHARREM.DAJA@FTI.EDU.AL
//
// """ """ """  """ """ """  """ """ """  """ """ """  """ """ """  """ """ """  """
import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import axios from 'axios'

import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { far } from '@fortawesome/free-regular-svg-icons'

library.add(fas, fab, far);


axios.defaults.baseURL = 'http://192.168.1.48:8800'

createApp(App).component('fa', FontAwesomeIcon).use(store).use(router, axios).mount('#app')
