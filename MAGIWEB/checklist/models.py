"""    *** ****  ****  **** ****     18 FEBRUARY 2022    """ """ """  """ """ """

AUTHOR: ING. MUHARREM DAJA

CONTACT: REMIDAJA@OUTLOOK.COM || MUHARREM.DAJA@FTI.EDU.AL

""" """ """  """ """ """  """ """ """  """ """ """  """ """ """  """ """ """  """




from django.db import models
from simple_history.models import HistoricalRecords

# Create your models here.

B = 'Benign'
LB = 'Likely Benign'
LP = 'Likely Pathogenic'
P = 'Pathogenic'

ST = 'Stand Alone'
VS = 'Very Strong'
S = 'Strong'
SP = 'Supporting'
M = 'Moderate'

VUS = 'Uncertain Significance'
UNK = 'UNKNOWN'



CHOICES_INTENSITA_BENIGNITA = (
    (S, 'Strong'),
    (SP, 'Supporting')
)

CHOICES_INTENSITA_PATHOGENETIC = (
    (VS, 'Very Strong'),
    (S, 'Strong'),
    (M, 'Moderate'),
    (SP, 'Supporting')
)

CHOICES_INTENSITY_BA1 = (
    (ST, 'Stand Alone'),
    (S, 'Strong'),
    (SP, 'Supporting')
)

CHOICES_INTENSITY_BS1 = (
    (VS, 'Very Strong'),
    (S, 'Strong'),
    (SP, 'Supporting')
)

CHOICES_INTERPRETER = (
    (B, 'Benign'),
    (LB, 'Likely Benign'),
    (LP, 'Likely Pathogenic'),
    (P, 'Pathogenic'),
    (VUS, 'Uncertain Significance'),
    (UNK, 'UNKNOWN')
)


CHOICES_INTERPRETER_FINAL = (
    (B, 'Benign'),
    (LB, 'Likely Benign'),
    (LP, 'Likely Pathogenic'),
    (P, 'Pathogenic'),
    (VUS, 'Uncertain Significance'),
    (UNK, 'UNKNOWN')
)

# CHOICES_VUSSTATUS = (
#         ('NON NOTO', 'NON NOTO'),
#         ('CALDA', 'CALDA'),
#         ('TIEPIDA', 'TIEPIDA'),
#         ('FREDDA', 'FREDDA'),
# )

CHOICES_VUSSTATUS = (
        ('Cold', 'Cold'),
        ('Middle', 'Middle'),
        ('Hot', 'Hot'),
        ('Unknown', 'Unknown'),
)

CHOICES_RISULTATO = (
                ('Primary', 'Primary'),
                ('Secondary', 'Secondary'),
)

class ResultInterpretation(models.Model):
    id_interno = models.CharField(max_length=25, null=True, blank=True)
    sample_id = models.CharField(max_length=25, null=True, blank=True)
    hgvs = models.CharField(max_length=115, null=True, blank=True)
    codice_pannello = models.CharField(max_length=50, null=True, blank=True)
    timestamp_sample = models.DateTimeField(auto_now_add = True, auto_now = False, null=True, blank=True)
    updated_sample = models.DateTimeField(auto_now_add = False, auto_now = True, null = True, blank = True)
    annotazione = models.CharField(max_length=50, null=True, blank=True)
    BA1_final = models.BooleanField(default=False, null=True, blank=True)
    BA1_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITY_BA1,
                                            default=ST, null=True, blank=True)
    BA1_cause_final = models.CharField(max_length=400, null=True, blank=True,
                                            default='Allele frequency is >5% in Exome Sequencing Project, 1000 Genomes Project, or Exome Aggregation Consortium.', verbose_name='BA1_cause_final')
    BS1_final = models.BooleanField(default=False, null=True, blank=True)
    BS1_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITY_BS1,
                                            default=S, null=True, blank=True)
    BS1_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Allele frequency is greater than expected for disorder.')
    BS2_final = models.BooleanField(default=False, null=True, blank=True)
    BS2_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_BENIGNITA,
                                            default=S, null=True, blank=True)
    BS2_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Observed in a healthy adult individual for a recessive (homozygous), dominant (heterozygous), or X-linked (hemizygous)\
 disorder, with full penetrance expected at an early age.')
    BS3_final = models.BooleanField(default=False, null=True, blank=True)
    BS3_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_BENIGNITA,
                                            default=S, null=True, blank=True)
    BS3_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Well-established in vitro or in vivo functional studies show no damaging effect on protein function or splicing.')
    BS4_final = models.BooleanField(default=False, null=True)
    BS4_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_BENIGNITA,
                                            default=S, null=True, blank=True)
    BS4_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Lack of segregation in affected members of a family.')
    BP1_final = models.BooleanField(default=False, null=True)
    BP1_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_BENIGNITA,
                                            default=SP, null=True, blank=True)
    BP1_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Missense variant in a gene for which primarily truncating variants are known to cause disease.')
    BP2_final = models.BooleanField(default=False, null=True)
    BP2_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_BENIGNITA,
                                            default=SP, null=True, blank=True)
    BP2_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Observed in trans with a pathogenic variant for a fully penetrant dominant gene/disorder or observed in cis with a\
 pathogenic variant in any inheritance pattern.')
    BP3_final = models.BooleanField(default=False, null=True)
    BP3_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_BENIGNITA,
                                            default=SP, null=True, blank=True)
    BP3_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='In-frame deletions/insertions in a repetitive region without a known function.')
    BP4_final = models.BooleanField(default=False, null=True)
    BP4_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_BENIGNITA,
                                            default=SP, null=True, blank=True)
    BP4_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Multiple lines of computational evidence suggest no impact on gene or gene product (conservation, evolutionary,\
 splicing impact, etc).')
    BP5_final = models.BooleanField(default=False, null=True)
    BP5_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_BENIGNITA,
                                            default=SP, null=True, blank=True)
    BP5_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Variant found in a case with an alternate molecular basis for disease.')
    BP6_final = models.BooleanField(default=False, null=True)
    BP6_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_BENIGNITA,
                                            default=SP, null=True, blank=True)
    BP6_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Reputable source recently reports variant as benign, but the evidence is not available to the laboratory to\
 perform an independent evaluation.')
    BP7_final = models.BooleanField(default=False, null=True)
    BP7_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_BENIGNITA,
                                            default=SP, null=True, blank=True)
    BP7_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='A synonymous (silent) variant for which splicing prediction algorithms predict no impact to the splice consensus\
 sequence nor the creation of a new splice site AND the nucleotide is not highly conserved.')
    PVS1_final = models.BooleanField(default=False, null=True)
    PVS1_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_PATHOGENETIC,
                                                default=S, null=True, blank=True)
    PVS1_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Null variant (nonsense, frameshift, canonical ±1 or 2 splice sites, initiation codon, single or multiexon deletion)\
 in a gene where LOF is a known mechanism of disease.')
    PS1_final = models.BooleanField(default=False, null=True)
    PS1_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_PATHOGENETIC,
                                                default=S, null=True, blank=True)
    PS1_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Same amino acid change as a previously established pathogenic variant regardless of nucleotide change.')
    PS2_final = models.BooleanField(default=False, null=True)
    PS2_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_PATHOGENETIC,
                                                default=S, null=True, blank=True)
    PS2_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='De novo (both maternity and paternity confirmed) in a patient with the disease and no family history.')
    PS3_final = models.BooleanField(default=False, null=True)
    PS3_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_PATHOGENETIC,
                                                default=S, null=True, blank=True)
    PS3_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Well-established in vitro or in vivo functional studies supportive of a damaging effect on the gene or gene product.')
    PS4_final = models.BooleanField(default=False, null=True)
    PS4_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_PATHOGENETIC,
                                                default=S, null=True, blank=True)
    PS4_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='The prevalence of the variant in affected individuals is significantly increased compared with the prevalence in controls.')
    PM1_final = models.BooleanField(default=False, null=True)
    PM1_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_PATHOGENETIC,
                                                default=M, null=True, blank=True)
    PM1_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Located in a mutational hot spot and/or critical and well-established functional domain (e.g., active site of an enzyme)\
 without benign variation.')
    PM2_final = models.BooleanField(default=False, null=True)
    PM2_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_PATHOGENETIC,
                                                default=SP, null=True, blank=True)
    PM2_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Absent from controls (or at extremely low frequency if recessive) in Exome Sequencing Project, 1000 Genomes Project, or\
 Exome Aggregation Consortium.')
    PM3_final = models.BooleanField(default=False, null=True)
    PM3_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_PATHOGENETIC,
                                                default=M, null=True, blank=True)
    PM3_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='For recessive disorders, detected in trans with a pathogenic variant.')
    PM4_final = models.BooleanField(default=False, null=True)
    PM4_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_PATHOGENETIC,
                                                default=M, null=True, blank=True)
    PM4_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Protein length changes as a result of in-frame deletions/insertions in a non-repeat region or stop-loss variants.')
    PM5_final = models.BooleanField(default=False, null=True)
    PM5_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_PATHOGENETIC,
                                                default=M, null=True, blank=True)
    PM5_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Novel missense change at an amino acid residue where a different missense change determined to be pathogenic has been seen\
 before.')
    PM6_final = models.BooleanField(default=False, null=True)
    PM6_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_PATHOGENETIC,
                                                default=M, null=True, blank=True)
    PM6_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Assumed de novo, but without confirmation of paternity and maternity.')
    PP1_final = models.BooleanField(default=False, null=True)
    PP1_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_PATHOGENETIC,
                                                default=SP, null=True, blank=True)
    PP1_cause_final = models.CharField(max_length=500,null=True, blank=True,
                                            default='Cosegregation with disease in multiple affected family members in a gene definitively known to cause the disease.')
    PP2_final = models.BooleanField(default=False, null=True)
    PP2_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_PATHOGENETIC,
                                                default=SP, null=True, blank=True)
    PP2_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Missense variant in a gene that has a low rate of benign missense variation and in which missense variants are a common\
 mechanism of disease.')
    PP3_final = models.BooleanField(default=False, null=True)
    PP3_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_PATHOGENETIC,
                                                default=SP, null=True, blank=True)
    PP3_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Multiple lines of computational evidence support a deleterious effect on the gene or gene product (conservation, evolutionary,\
 splicing impact, etc.).')
    PP4_final = models.BooleanField(default=False, null=True)
    PP4_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_PATHOGENETIC,
                                                default=SP, null=True, blank=True)
    PP4_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Patient’s phenotype or family history is highly specific for a disease with a single genetic etiology.')
    PP5_final = models.BooleanField(default=False, null=True)
    PP5_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_PATHOGENETIC,
                                                default=SP, null=True, blank=True)
    PP5_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Reputable source recently reports variant as pathogenic, but the evidence is not available to the laboratory to perform an\
 independent evaluation.')
    choice_interpretation = models.CharField(max_length=25,
                                                choices=CHOICES_INTERPRETER,
                                                null=True, blank=True)
    choice_interpretation_final = models.CharField(max_length=25,
                                                choices=CHOICES_INTERPRETER_FINAL,
                                                null=True, blank=True)
    operatoreGENETISTA = models.CharField(max_length=30, null=True, blank=True, verbose_name='Genetista')
    operatoreGENETISTA_date = models.DateField(null=True, blank=True, verbose_name='Genetista Date')
    PMPOT_final = models.BooleanField(default=False, null=True)
    PMPOT_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_PATHOGENETIC,
                                                default=M, null=True, blank=True)
    PMPOT_cause_final = models.CharField(max_length=500, null=True, blank=True, default='')
    PPPOT_final = models.BooleanField(default=False, null=True)
    PPPOT_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_PATHOGENETIC,
                                                default=SP, null=True, blank=True)
    PPPOT_cause_final = models.CharField(max_length=500, null=True, blank=True, default='Potenziale PM3-PM6-PP1 per eterozigosita\' composta o in Omozigosi o De Novo')
    consequence = models.CharField(max_length=100, null=True, blank=True)
    ada_score = models.FloatField(default=-999)
    rf_score = models.FloatField(default=-999)
    revel_score = models.FloatField(default=-999)
    cadd_score = models.FloatField(default=-999)
    sift_score = models.FloatField(default=-999)
    polyphen2_hdiv_score = models.FloatField(default=-999)
    polyphen2_hvar_score = models.FloatField(default=-999)
    autopvs1 = models.CharField(max_length=25, null=True, blank=True)
    spliceAI = models.FloatField(default=-999)
    vusstatus = models.CharField(max_length=20, choices=CHOICES_VUSSTATUS,
                                    default='Unknown', null=True, blank=True)
    importanza = models.CharField(max_length=50, choices=CHOICES_RISULTATO, null=True, blank=True)
    var_on_gene = models.IntegerField(default=0)
    zigosita = models.CharField(max_length=10, null=True, blank=True)
    allinheritance = models.CharField(max_length=40, null=True, blank=True)

    history = HistoricalRecords()

    class Meta:

        unique_together = ('id_interno', 'hgvs')
        verbose_name = 'Result Interpretation'
        verbose_name_plural = 'Result Interpretation'

    def __unicode__(self):
        return '%s - %s - (%s) - (%s)' %s (self.sample_id,self.hgvs)

    def __str__(self):
        return self.id_interno+' - ('+self.hgvs+')' + ' - ' + self.sample_id + ' -( ' + self.codice_pannello + ' )'



class ResultInterpretationRestore(models.Model):
    id_interno = models.CharField(max_length=25, null=True, blank=True)
    sample_id = models.CharField(max_length=25, null=True, blank=True)
    hgvs = models.CharField(max_length=115, null=True, blank=True)
    codice_pannello = models.CharField(max_length=50, null=True, blank=True)
    timestamp_sample = models.DateTimeField(auto_now_add = True, auto_now = False, null=True, blank=True)
    updated_sample = models.DateTimeField(auto_now_add = False, auto_now = True, null = True, blank = True)
    annotazione = models.CharField(max_length=50, null=True, blank=True)
    BA1_final = models.BooleanField(default=False, null=True, blank=True)
    BA1_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITY_BA1,
                                            default=ST, null=True, blank=True)
    BA1_cause_final = models.CharField(max_length=400, null=True, blank=True,
                                            default='Allele frequency is >5% in Exome Sequencing Project, 1000 Genomes Project, or Exome Aggregation Consortium.', verbose_name='BA1_cause_final')
    BS1_final = models.BooleanField(default=False, null=True, blank=True)
    BS1_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITY_BS1,
                                            default=S, null=True, blank=True)
    BS1_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Allele frequency is greater than expected for disorder.')
    BS2_final = models.BooleanField(default=False, null=True, blank=True)
    BS2_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_BENIGNITA,
                                            default=S, null=True, blank=True)
    BS2_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Observed in a healthy adult individual for a recessive (homozygous), dominant (heterozygous), or X-linked (hemizygous)\
 disorder, with full penetrance expected at an early age.')
    BS3_final = models.BooleanField(default=False, null=True, blank=True)
    BS3_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_BENIGNITA,
                                            default=S, null=True, blank=True)
    BS3_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Well-established in vitro or in vivo functional studies show no damaging effect on protein function or splicing.')
    BS4_final = models.BooleanField(default=False, null=True)
    BS4_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_BENIGNITA,
                                            default=S, null=True, blank=True)
    BS4_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Lack of segregation in affected members of a family.')
    BP1_final = models.BooleanField(default=False, null=True)
    BP1_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_BENIGNITA,
                                            default=SP, null=True, blank=True)
    BP1_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Missense variant in a gene for which primarily truncating variants are known to cause disease.')
    BP2_final = models.BooleanField(default=False, null=True)
    BP2_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_BENIGNITA,
                                            default=SP, null=True, blank=True)
    BP2_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Observed in trans with a pathogenic variant for a fully penetrant dominant gene/disorder or observed in cis with a\
 pathogenic variant in any inheritance pattern.')
    BP3_final = models.BooleanField(default=False, null=True)
    BP3_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_BENIGNITA,
                                            default=SP, null=True, blank=True)
    BP3_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='In-frame deletions/insertions in a repetitive region without a known function.')
    BP4_final = models.BooleanField(default=False, null=True)
    BP4_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_BENIGNITA,
                                            default=SP, null=True, blank=True)
    BP4_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Multiple lines of computational evidence suggest no impact on gene or gene product (conservation, evolutionary,\
 splicing impact, etc).')
    BP5_final = models.BooleanField(default=False, null=True)
    BP5_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_BENIGNITA,
                                            default=SP, null=True, blank=True)
    BP5_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Variant found in a case with an alternate molecular basis for disease.')
    BP6_final = models.BooleanField(default=False, null=True)
    BP6_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_BENIGNITA,
                                            default=SP, null=True, blank=True)
    BP6_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Reputable source recently reports variant as benign, but the evidence is not available to the laboratory to\
 perform an independent evaluation.')
    BP7_final = models.BooleanField(default=False, null=True)
    BP7_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_BENIGNITA,
                                            default=SP, null=True, blank=True)
    BP7_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='A synonymous (silent) variant for which splicing prediction algorithms predict no impact to the splice consensus\
 sequence nor the creation of a new splice site AND the nucleotide is not highly conserved.')
    PVS1_final = models.BooleanField(default=False, null=True)
    PVS1_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_PATHOGENETIC,
                                                default=S, null=True, blank=True)
    PVS1_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Null variant (nonsense, frameshift, canonical ±1 or 2 splice sites, initiation codon, single or multiexon deletion)\
 in a gene where LOF is a known mechanism of disease.')
    PS1_final = models.BooleanField(default=False, null=True)
    PS1_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_PATHOGENETIC,
                                                default=S, null=True, blank=True)
    PS1_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Same amino acid change as a previously established pathogenic variant regardless of nucleotide change.')
    PS2_final = models.BooleanField(default=False, null=True)
    PS2_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_PATHOGENETIC,
                                                default=S, null=True, blank=True)
    PS2_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='De novo (both maternity and paternity confirmed) in a patient with the disease and no family history.')
    PS3_final = models.BooleanField(default=False, null=True)
    PS3_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_PATHOGENETIC,
                                                default=S, null=True, blank=True)
    PS3_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Well-established in vitro or in vivo functional studies supportive of a damaging effect on the gene or gene product.')
    PS4_final = models.BooleanField(default=False, null=True)
    PS4_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_PATHOGENETIC,
                                                default=S, null=True, blank=True)
    PS4_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='The prevalence of the variant in affected individuals is significantly increased compared with the prevalence in controls.')
    PM1_final = models.BooleanField(default=False, null=True)
    PM1_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_PATHOGENETIC,
                                                default=M, null=True, blank=True)
    PM1_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Located in a mutational hot spot and/or critical and well-established functional domain (e.g., active site of an enzyme)\
 without benign variation.')
    PM2_final = models.BooleanField(default=False, null=True)
    PM2_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_PATHOGENETIC,
                                                default=SP, null=True, blank=True)
    PM2_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Absent from controls (or at extremely low frequency if recessive) in Exome Sequencing Project, 1000 Genomes Project, or\
 Exome Aggregation Consortium.')
    PM3_final = models.BooleanField(default=False, null=True)
    PM3_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_PATHOGENETIC,
                                                default=M, null=True, blank=True)
    PM3_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='For recessive disorders, detected in trans with a pathogenic variant.')
    PM4_final = models.BooleanField(default=False, null=True)
    PM4_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_PATHOGENETIC,
                                                default=M, null=True, blank=True)
    PM4_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Protein length changes as a result of in-frame deletions/insertions in a non-repeat region or stop-loss variants.')
    PM5_final = models.BooleanField(default=False, null=True)
    PM5_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_PATHOGENETIC,
                                                default=M, null=True, blank=True)
    PM5_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Novel missense change at an amino acid residue where a different missense change determined to be pathogenic has been seen\
 before.')
    PM6_final = models.BooleanField(default=False, null=True)
    PM6_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_PATHOGENETIC,
                                                default=M, null=True, blank=True)
    PM6_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Assumed de novo, but without confirmation of paternity and maternity.')
    PP1_final = models.BooleanField(default=False, null=True)
    PP1_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_PATHOGENETIC,
                                                default=SP, null=True, blank=True)
    PP1_cause_final = models.CharField(max_length=500,null=True, blank=True,
                                            default='Cosegregation with disease in multiple affected family members in a gene definitively known to cause the disease.')
    PP2_final = models.BooleanField(default=False, null=True)
    PP2_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_PATHOGENETIC,
                                                default=SP, null=True, blank=True)
    PP2_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Missense variant in a gene that has a low rate of benign missense variation and in which missense variants are a common\
 mechanism of disease.')
    PP3_final = models.BooleanField(default=False, null=True)
    PP3_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_PATHOGENETIC,
                                                default=SP, null=True, blank=True)
    PP3_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Multiple lines of computational evidence support a deleterious effect on the gene or gene product (conservation, evolutionary,\
 splicing impact, etc.).')
    PP4_final = models.BooleanField(default=False, null=True)
    PP4_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_PATHOGENETIC,
                                                default=SP, null=True, blank=True)
    PP4_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Patient’s phenotype or family history is highly specific for a disease with a single genetic etiology.')
    PP5_final = models.BooleanField(default=False, null=True)
    PP5_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_PATHOGENETIC,
                                                default=SP, null=True, blank=True)
    PP5_cause_final = models.CharField(max_length=500, null=True, blank=True,
                                            default='Reputable source recently reports variant as pathogenic, but the evidence is not available to the laboratory to perform an\
 independent evaluation.')
    choice_interpretation = models.CharField(max_length=25,
                                                choices=CHOICES_INTERPRETER,
                                                null=True, blank=True)
    choice_interpretation_final = models.CharField(max_length=25,
                                                choices=CHOICES_INTERPRETER_FINAL,
                                                null=True, blank=True)
    operatoreGENETISTA = models.CharField(max_length=30, null=True, blank=True, verbose_name='Genetista')
    operatoreGENETISTA_date = models.DateField(null=True, blank=True, verbose_name='Genetista Date')
    PMPOT_final = models.BooleanField(default=False, null=True)
    PMPOT_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_PATHOGENETIC,
                                                default=M, null=True, blank=True)
    PMPOT_cause_final = models.CharField(max_length=500, null=True, blank=True, default='')
    PPPOT_final = models.BooleanField(default=False, null=True)
    PPPOT_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_PATHOGENETIC,
                                                default=SP, null=True, blank=True)
    PPPOT_cause_final = models.CharField(max_length=500, null=True, blank=True, default='')
    consequence = models.CharField(max_length=100, null=True, blank=True)
    ada_score = models.FloatField(default=-999)
    rf_score = models.FloatField(default=-999)
    revel_score = models.FloatField(default=-999)
    cadd_score = models.FloatField(default=-999)
    sift_score = models.FloatField(default=-999)
    polyphen2_hdiv_score = models.FloatField(default=-999)
    polyphen2_hvar_score = models.FloatField(default=-999)
    autopvs1 = models.CharField(max_length=25, null=True, blank=True)
    spliceAI = models.FloatField(default=-999)
    vusstatus = models.CharField(max_length=20, choices=CHOICES_VUSSTATUS,
                                    default='Unkown', null=True, blank=True)
    importanza = models.CharField(max_length=50, choices=CHOICES_RISULTATO, null=True, blank=True)
    var_on_gene = models.IntegerField(default=0)
    zigosita = models.CharField(max_length=10, null=True, blank=True)
    allinheritance = models.CharField(max_length=40, null=True, blank=True)

    history = HistoricalRecords()

    class Meta:

        unique_together = ('id_interno', 'hgvs')
        verbose_name = 'Result Interpretation Restore'
        verbose_name_plural = 'Result Interpretation Restore'

    def __unicode__(self):
        return '%s - %s - (%s) - (%s)' %s (self.sample_id,self.hgvs)

    def __str__(self):
        return self.id_interno+' - ('+self.hgvs+')' + ' - ' + self.sample_id + ' -( ' + self.codice_pannello + ' )'
