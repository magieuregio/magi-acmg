"""    *** ****  ****  **** ****     18 FEBRUARY 2022    """ """ """  """ """ """

AUTHOR: ING. MUHARREM DAJA

CONTACT: REMIDAJA@OUTLOOK.COM || MUHARREM.DAJA@FTI.EDU.AL

""" """ """  """ """ """  """ """ """  """ """ """  """ """ """  """ """ """  """

from rest_framework import viewsets,mixins,generics,permissions,renderers,authentication,status
from rest_framework.permissions import IsAuthenticated,AllowAny
from rest_framework.decorators import api_view
from rest_framework.decorators import authentication_classes, permission_classes
from rest_framework.viewsets import ModelViewSet,ReadOnlyModelViewSet
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.authentication import BaseAuthentication,SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework import filters
from rest_framework.decorators import action
from rest_framework.response import Response
from django.http import JsonResponse
from django.http import HttpResponseRedirect

from rest_framework.filters import SearchFilter
from rest_framework import filters
from django_filters.rest_framework import DjangoFilterBackend

import io
from rest_framework.parsers import JSONParser
from rest_framework.renderers import JSONRenderer
from rest_framework import serializers
from io import BytesIO

from rest_framework.viewsets import ModelViewSet,ReadOnlyModelViewSet

from django.shortcuts import get_object_or_404
from django.shortcuts import redirect

from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache

from checklist.models import ResultInterpretation, ResultInterpretationRestore
from checklist.api.serializers import ResultInterpretationSerializer, ResultInterpretationSerializer_NOTCAUSE
from checklist.api.serializers import ResultInterpretationRestoreSerializer


from checklist.api.variant_interpretation import variant_interpretation
from sanger.models import AllVariation

import pandas as pd

@method_decorator(never_cache,name='dispatch')
class ResultInterpretationViewSet(ModelViewSet):
	serializer_class = ResultInterpretationSerializer_NOTCAUSE
	# authentication_classes = (BasicAuthentication, SessionAuthentication,)
	# permission_classes = (IsAuthenticated,)
	filter_backends = (filters.SearchFilter, DjangoFilterBackend, filters.OrderingFilter,)
	search_fields = ('$sample_id', '$hgvs', '$codice_pannello', '$id_interno',)
	filter_fields = ('sample_id', 'hgvs', 'codice_pannello', 'id_interno',)

	def get_queryset(self):
		risultati_interpretati = ResultInterpretation.objects.all()
		return risultati_interpretati

	def create(self, request, *args, **kwargs):
		serializer = self.get_serializer(data=request.data)
		serializer.is_valid(raise_exception=True)
		self.perform_create(serializer)
		headers = self.get_success_headers(serializer.data)
		return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

	def retrieve(self, request, *args, **kwargs):
		risultato = self.get_object()
		serializer = self.get_serializer(risultato)
		return Response(serializer.data)


	def destroy(self, request, *args, **kwargs):
		risultato = self.get_object()
		self.perform_destroy(risultato)
		return Response({"message": "It was deleted"}, status=status.HTTP_204_NO_CONTENT)

	def update(self, request, *args, **kwargs):
		partial = kwargs.pop('partial', False)
		results = self.get_object()

		if 'runinterpretation' in request.headers['name']:

			data1 = request.data
			data2 = pd.Series(data1).to_frame()
			sample = pd.DataFrame(data2.T)

			for index, row in sample.iterrows():
				print('BA1', row.BA1_final, row.BA1_intensita_final)
				print('BS1', row.BS1_final, row.BS1_intensita_final)
				print('BS2', row.BS2_final, row.BS2_intensita_final)
				print('BS3', row.BS3_final, row.BS3_intensita_final)
				print('BS4', row.BS4_final, row.BS4_intensita_final)
				print('BP1', row.BP1_final, row.BP1_intensita_final)
				print('BP2', row.BP2_final, row.BP2_intensita_final)
				print('BP3', row.BP3_final, row.BP3_intensita_final)
				print('BP4', row.BP4_final, row.BP4_intensita_final)
				print('BP5', row.BP5_final, row.BP5_intensita_final)
				print('BP6', row.BP6_final, row.BP6_intensita_final)
				print('BP7', row.BP7_final, row.BP7_intensita_final)
				print('PVS1', row.PVS1_final, row.PVS1_intensita_final)
				print('PS1', row.PS1_final, row.PS1_intensita_final)
				print('PS2', row.PS2_final, row.PS2_intensita_final)
				print('PS3', row.PS3_final, row.PS3_intensita_final)
				print('PS4', row.PS4_final, row.PS4_intensita_final)
				print('PM1', row.PM1_final, row.PM1_intensita_final)
				print('PM2', row.PM2_final, row.PM2_intensita_final)
				print('PM3', row.PM3_final, row.PM3_intensita_final)
				print('PM4 ',row.PM4_final, row.PM4_intensita_final)
				print('PM5', row.PM5_final, row.PM5_intensita_final)
				print('PM6', row.PM6_final, row.PM6_intensita_final)
				print('PP1', row.PP1_final, row.PP1_intensita_final)
				print('PP2 ',row.PP2_final, row.PP2_intensita_final)
				print('PP3', row.PP3_final, row.PP3_intensita_final)
				print('PP4', row.PP4_final, row.PP4_intensita_final)
				print('PP5', row.PP5_final, row.PP5_intensita_final)
				print('PMPOT', row.PMPOT_final, row.PMPOT_intensita_final)
				print('PPPOT', row.PPPOT_final, row.PPPOT_intensita_final)

			result = variant_interpretation(sample)

			json = JSONRenderer().render(request.data)
			stream = io.BytesIO(json)
			data = JSONParser().parse(stream)

			# data['choice_interpretation_final'] = result

			data['choice_interpretation_final'] = result[0]
			data['vusstatus'] = result[1]
			data['PMPOT_final'] = result[2]
			data['PMPOT_intensita_final'] = result[3]
			data['PMPOT_cause_final'] = result[4]
			data['PPPOT_final'] = result[5]
			data['PPPOT_intensita_final'] = result[6]
			data['PPPOT_cause_final'] = result[7]

			print('RESULT IS', result[0], result[1])

			# print('RESULT IS', result)

			serializer = ResultInterpretationSerializer_NOTCAUSE(results ,data=data, partial=partial)
			serializer.is_valid(raise_exception=True)
			self.perform_update(serializer)

			if getattr(results, '_prefetched_objects_cache', None):
				# If 'prefetch_related' has been applied to a queryset, we need to
				# forcibly invalidate the prefetch cache on the instance.
				results._prefetched_objects_cache = {}

			interpretation = serializer.data['choice_interpretation_final']
			vusstato = serializer.data['vusstatus']
			pk = serializer.data['pk']

			AllVariation.objects.filter(id_interno=row.id_interno, hgvs=row.hgvs).update(rilevanza=data['choice_interpretation_final'],
											vusstatus=data['vusstatus'])

			interpretation_final = {"key": pk,  "interpretation": interpretation, "vusstato": vusstato}
			return Response(interpretation_final)


class ResultInterpretationRestoreViewSet(ModelViewSet):
	serializer_class = ResultInterpretationRestoreSerializer
	# authentication_classes = (BasicAuthentication, SessionAuthentication,)
	# permission_classes = (IsAuthenticated,)
	filter_backends = (filters.SearchFilter, DjangoFilterBackend, filters.OrderingFilter,)
	search_fields = ('$sample_id', '$hgvs', '$codice_pannello',)
	filter_fields = ('sample_id', 'hgvs', 'codice_pannello',)

	def get_queryset(self):
		risultati_interpretati = ResultInterpretationRestore.objects.all()
		return risultati_interpretati

	def create(self, request, *args, **kwargs):
		serializer = self.get_serializer(data=request.data)
		serializer.is_valid(raise_exception=True)
		self.perform_create(serializer)
		headers = self.get_success_headers(serializer.data)
		return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

	def retrieve(self, request, *args, **kwargs):
		risultato = self.get_object()
		serializer = self.get_serializer(risultato)
		return Response(serializer.data)


	def destroy(self, request, *args, **kwargs):
		risultato = self.get_object()
		self.perform_destroy(risultato)
		return Response({"message": "It was deleted"}, status=status.HTTP_204_NO_CONTENT)

	def update(self, request, *args, **kwargs):
		partial = kwargs.pop('partial', False)
		results = self.get_object()

		if 'runinterpretation' in request.headers['name']:

			data1 = request.data
			data2 = pd.Series(data1).to_frame()
			sample = pd.DataFrame(data2.T)

			for index, row in sample.iterrows():
				print('BA1', row.BA1_final, row.BA1_intensita_final)
				print('BS1', row.BS1_final, row.BS1_intensita_final)
				print('BS2', row.BS2_final, row.BS2_intensita_final)
				print('BS3', row.BS3_final, row.BS3_intensita_final)
				print('BS4', row.BS4_final, row.BS4_intensita_final)
				print('BP1', row.BP1_final, row.BP1_intensita_final)
				print('BP2', row.BP2_final, row.BP2_intensita_final)
				print('BP3', row.BP3_final, row.BP3_intensita_final)
				print('BP4', row.BP4_final, row.BP4_intensita_final)
				print('BP5', row.BP5_final, row.BP5_intensita_final)
				print('BP6', row.BP6_final, row.BP6_intensita_final)
				print('BP7', row.BP7_final, row.BP7_intensita_final)
				print('PVS1', row.PVS1_final, row.PVS1_intensita_final)
				print('PS1', row.PS1_final, row.PS1_intensita_final)
				print('PS2', row.PS2_final, row.PS2_intensita_final)
				print('PS3', row.PS3_final, row.PS3_intensita_final)
				print('PS4', row.PS4_final, row.PS4_intensita_final)
				print('PM1', row.PM1_final, row.PM1_intensita_final)
				print('PM2', row.PM2_final, row.PM2_intensita_final)
				print('PM3', row.PM3_final, row.PM3_intensita_final)
				print('PM4 ',row.PM4_final, row.PM4_intensita_final)
				print('PM5', row.PM5_final, row.PM5_intensita_final)
				print('PM6', row.PM6_final, row.PM6_intensita_final)
				print('PP1', row.PP1_final, row.PP1_intensita_final)
				print('PP2 ',row.PP2_final, row.PP2_intensita_final)
				print('PP3', row.PP3_final, row.PP3_intensita_final)
				print('PP4', row.PP4_final, row.PP4_intensita_final)
				print('PP5', row.PP5_final, row.PP5_intensita_final)
				print('PMPOT', row.PMPOT_final, row.PMPOT_intensita_final)
				print('PPPOT', row.PPPOT_final, row.PPPOT_intensita_final)

			result = variant_interpretation(sample)

			json = JSONRenderer().render(request.data)
			stream = io.BytesIO(json)
			data = JSONParser().parse(stream)

			# data['choice_interpretation_final'] = result

			data['choice_interpretation_final'] = result[0]
			data['vusstatus'] = result[1]
			data['PMPOT_final'] = result[2]
			data['PMPOT_intensita_final'] = result[3]
			data['PMPOT_cause_final'] = result[4]
			data['PPPOT_final'] = result[5]
			data['PPPOT_intensita_final'] = result[6]
			data['PPPOT_cause_final'] = result[7]

			print('RESULT IS', result[0], result[1])

			# print('RESULT IS', result)

			serializer = ResultInterpretationRestoreSerializer(results ,data=data, partial=partial)
			serializer.is_valid(raise_exception=True)
			self.perform_update(serializer)

			if getattr(results, '_prefetched_objects_cache', None):
				# If 'prefetch_related' has been applied to a queryset, we need to
				# forcibly invalidate the prefetch cache on the instance.
				results._prefetched_objects_cache = {}

			interpretation = serializer.data['choice_interpretation_final']
			vusstato = serializer.data['vusstatus']
			pk = serializer.data['pk']

			interpretation_final = {"key": pk,  "interpretation": interpretation, "vusstato": vusstato}
			return Response(interpretation_final)
