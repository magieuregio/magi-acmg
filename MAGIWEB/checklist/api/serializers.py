"""    *** ****  ****  **** ****     18 FEBRUARY 2022    """ """ """  """ """ """

AUTHOR: ING. MUHARREM DAJA

CONTACT: REMIDAJA@OUTLOOK.COM || MUHARREM.DAJA@FTI.EDU.AL

""" """ """  """ """ """  """ """ """  """ """ """  """ """ """  """ """ """  """


from rest_framework import serializers
from checklist.models import ResultInterpretation, ResultInterpretationRestore
import re

ST = 'Stand Alone'
VS = 'Very Strong'
S = 'Strong'
SP = 'Supporting'
M = 'Moderate'


class ResultInterpretationSerializer(serializers.ModelSerializer):
	BA1_cause_final = serializers.CharField(initial='Allele frequency is >5% in Exome Sequencing Project, 1000 Genomes Project, or Exome Aggregation Consortium.')
	BS1_cause_final = serializers.CharField(initial='Allele frequency is greater than expected for disorder.')
	BS2_cause_final = serializers.CharField(initial='Observed in a healthy adult individual for a recessive (homozygous), dominant (heterozygous), or X-linked (hemizygous)\
 disorder, with full penetrance expected at an early age.')
	BS3_cause_final = serializers.CharField(initial='Well-established in vitro or in vivo functional studies show no damaging effect on protein function or splicing.')
	BS4_cause_final = serializers.CharField(initial='Lack of segregation in affected members of a family.')
	BP1_cause_final = serializers.CharField(initial='Missense variant in a gene for which primarily truncating variants are known to cause disease.')
	BP2_cause_final = serializers.CharField(initial='Observed in trans with a pathogenic variant for a fully penetrant dominant gene/disorder or observed in cis with a\
 pathogenic variant in any inheritance pattern.')
	BP3_cause_final = serializers.CharField(initial='In-frame deletions/insertions in a repetitive region without a known function.')
	BP4_cause_final = serializers.CharField(initial='Multiple lines of computational evidence suggest no impact on gene or gene product (conservation, evolutionary,\
 splicing impact, etc).')
	BP5_cause_final = serializers.CharField(initial='Variant found in a case with an alternate molecular basis for disease.')
	BP6_cause_final = serializers.CharField(initial='Reputable source recently reports variant as benign, but the evidence is not available to the laboratory to\
 perform an independent evaluation.')
	BP7_cause_final = serializers.CharField(initial='A synonymous (silent) variant for which splicing prediction algorithms predict no impact to the splice consensus\
 sequence nor the creation of a new splice site AND the nucleotide is not highly conserved.')
	PVS1_cause_final = serializers.CharField(initial='Null variant (nonsense, frameshift, canonical ±1 or 2 splice sites, initiation codon, single or multiexon deletion)\
 in a gene where LOF is a known mechanism of disease.')
	PS1_cause_final = serializers.CharField(initial='Same amino acid change as a previously established pathogenic variant regardless of nucleotide change.')
	PS2_cause_final = serializers.CharField(initial='De novo (both maternity and paternity confirmed) in a patient with the disease and no family history.')
	PS3_cause_final = serializers.CharField(initial='Well-established in vitro or in vivo functional studies supportive of a damaging effect on the gene or gene product.')
	PS4_cause_final = serializers.CharField(initial='The prevalence of the variant in affected individuals is significantly increased compared with the prevalence in controls.')
	PM1_cause_final = serializers.CharField(initial='Located in a mutational hot spot and/or critical and well-established functional domain (e.g., active site of an enzyme)\
 without benign variation.')
	PM2_cause_final = serializers.CharField(initial='Absent from controls (or at extremely low frequency if recessive) in Exome Sequencing Project, 1000 Genomes Project, or\
 Exome Aggregation Consortium.')
	PM3_cause_final = serializers.CharField(initial='For recessive disorders, detected in trans with a pathogenic variant.')
	PM4_cause_final = serializers.CharField(initial='Protein length changes as a result of in-frame deletions/insertions in a non-repeat region or stop-loss variants.')
	PM5_cause_final = serializers.CharField(initial='Novel missense change at an amino acid residue where a different missense change determined to be pathogenic has been seen\
 before.')
	PM6_cause_final = serializers.CharField(initial='Assumed de novo, but without confirmation of paternity and maternity.')
	PP1_cause_final = serializers.CharField(initial='Cosegregation with disease in multiple affected family members in a gene definitively known to cause the disease.')
	PP2_cause_final = serializers.CharField(initial='Missense variant in a gene that has a low rate of benign missense variation and in which missense variants are a common\
 mechanism of disease.')
	PP3_cause_final = serializers.CharField(initial='Multiple lines of computational evidence support a deleterious effect on the gene or gene product (conservation, evolutionary,\
 splicing impact, etc.).')
	PP4_cause_final = serializers.CharField(initial='Patient’s phenotype or family history is highly specific for a disease with a single genetic etiology.')
	PP5_cause_final = serializers.CharField(initial='Reputable source recently reports variant as pathogenic, but the evidence is not available to the laboratory to perform an\
 independent evaluation.')


	class Meta:
		model = ResultInterpretation
		fields = ('pk','sample_id', 'hgvs', 'BA1_final', 'BA1_intensita_final', 'BA1_cause_final', 'BS1_final',
				'BS1_intensita_final', 'BS1_cause_final', 'BS2_final', 'BS2_intensita_final',
				'BS2_cause_final', 'BS3_final', 'BS3_intensita_final', 'BS3_cause_final', 'BS4_final',
				'BS4_intensita_final', 'BS4_cause_final', 'BP1_final', 'BP1_intensita_final',
				'BP1_cause_final', 'BP2_final', 'BP2_intensita_final', 'BP2_cause_final', 'BP3_final',
				'BP3_intensita_final', 'BP3_cause_final', 'BP4_final', 'BP4_intensita_final',
				'BP4_cause_final', 'BP5_final', 'BP5_intensita_final', 'BP5_cause_final', 'BP6_final',
				'BP6_intensita_final', 'BP6_cause_final', 'BP7_final', 'BP7_intensita_final',
				'BP7_cause_final', 'PVS1_final', 'PVS1_intensita_final', 'PVS1_cause_final', 'PS1_final',
				'PS1_intensita_final', 'PS1_cause_final', 'PS2_final', 'PS2_intensita_final',
				'PS2_cause_final', 'PS3_final', 'PS3_intensita_final', 'PS3_cause_final', 'PS4_final',
				'PS4_intensita_final', 'PS4_cause_final', 'PM1_final', 'PM1_intensita_final',
				'PM1_cause_final', 'PM2_final', 'PM2_intensita_final', 'PM2_cause_final', 'PM3_final',
				'PM3_intensita_final', 'PM3_cause_final', 'PM4_final', 'PM4_intensita_final',
				'PM4_cause_final' ,'PM5_final', 'PM5_intensita_final', 'PM5_cause_final','PM6_final',
				'PM6_intensita_final', 'PM6_cause_final','PP1_final', 'PP1_intensita_final', 'PP1_cause_final',
				'PP2_final', 'PP2_intensita_final', 'PP2_cause_final' ,'PP3_final',
				'PP3_intensita_final', 'PP3_cause_final', 'PP4_final', 'PP4_intensita_final',
				'PP4_cause_final', 'PP5_final', 'PP5_intensita_final', 'PP5_cause_final',
				'choice_interpretation', 'choice_interpretation_final', 'operatoreGENETISTA',
				'operatoreGENETISTA_date', 'PMPOT_final', 'PMPOT_intensita_final', 'PMPOT_cause_final',
				'PPPOT_final', 'PPPOT_intensita_final', 'PPPOT_cause_final', 'consequence', 'ada_score',
				'rf_score', 'revel_score', 'autopvs1', 'spliceAI', 'vusstatus', 'importanza', 'annotazione',
				'var_on_gene', 'zigosita', 'allinheritance', 'codice_pannello', 'id_interno', 'cadd_score',
				'polyphen2_hdiv_score', 'polyphen2_hvar_score', 'sift_score')


	def __init__(self, *args,**kwargs):
		super(ResultInterpretationSerializer,self).__init__(*args, **kwargs)
		self.fields['BA1_intensita_final'].initial = ST
		self.fields['BS1_intensita_final'].initial = S
		self.fields['BS2_intensita_final'].initial = S
		self.fields['BS3_intensita_final'].initial = S
		self.fields['BS4_intensita_final'].initial = S
		self.fields['BP1_intensita_final'].initial = SP
		self.fields['BP2_intensita_final'].initial = SP
		self.fields['BP3_intensita_final'].initial = SP
		self.fields['BP4_intensita_final'].initial = SP
		self.fields['BP5_intensita_final'].initial = SP
		self.fields['BP6_intensita_final'].initial = SP
		self.fields['BP7_intensita_final'].initial = SP
		self.fields['PVS1_intensita_final'].initial =S
		self.fields['PS1_intensita_final'].initial = S
		self.fields['PS2_intensita_final'].initial = S
		self.fields['PS3_intensita_final'].initial = S
		self.fields['PS4_intensita_final'].initial = S
		self.fields['PM1_intensita_final'].initial = M
		self.fields['PM2_intensita_final'].initial = SP
		self.fields['PM3_intensita_final'].initial = M
		self.fields['PM4_intensita_final'].initial = M
		self.fields['PM5_intensita_final'].initial = M
		self.fields['PM6_intensita_final'].initial = M
		self.fields['PP1_intensita_final'].initial = SP
		self.fields['PP2_intensita_final'].initial = SP
		self.fields['PP3_intensita_final'].initial = SP
		self.fields['PP4_intensita_final'].initial = SP
		self.fields['PP5_intensita_final'].initial = SP
		self.fields['PMPOT_intensita_final'].initial = SP
		self.fields['PPPOT_intensita_final'].initial = SP


class ResultInterpretationSerializer_NOTCAUSE(serializers.ModelSerializer):

	# timestamp_sample = serializers.DateTimeField(format="%d-%m-%Y")
	# updated_sample = serializers.DateTimeField(format = "%d-%m-%Y")

	class Meta:
		model = ResultInterpretation
		fields = ('pk','sample_id', 'hgvs', 'BA1_final', 'BA1_intensita_final', 'BA1_cause_final', 'BS1_final',
				'BS1_intensita_final', 'BS1_cause_final', 'BS2_final', 'BS2_intensita_final',
				'BS2_cause_final', 'BS3_final', 'BS3_intensita_final', 'BS3_cause_final', 'BS4_final',
				'BS4_intensita_final', 'BS4_cause_final', 'BP1_final', 'BP1_intensita_final',
				'BP1_cause_final', 'BP2_final', 'BP2_intensita_final', 'BP2_cause_final', 'BP3_final',
				'BP3_intensita_final', 'BP3_cause_final', 'BP4_final', 'BP4_intensita_final',
				'BP4_cause_final', 'BP5_final', 'BP5_intensita_final', 'BP5_cause_final', 'BP6_final',
				'BP6_intensita_final', 'BP6_cause_final', 'BP7_final', 'BP7_intensita_final',
				'BP7_cause_final', 'PVS1_final', 'PVS1_intensita_final', 'PVS1_cause_final', 'PS1_final',
				'PS1_intensita_final', 'PS1_cause_final', 'PS2_final', 'PS2_intensita_final',
				'PS2_cause_final', 'PS3_final', 'PS3_intensita_final', 'PS3_cause_final', 'PS4_final',
				'PS4_intensita_final', 'PS4_cause_final', 'PM1_final', 'PM1_intensita_final',
				'PM1_cause_final', 'PM2_final', 'PM2_intensita_final', 'PM2_cause_final', 'PM3_final',
				'PM3_intensita_final', 'PM3_cause_final', 'PM4_final', 'PM4_intensita_final',
				'PM4_cause_final' ,'PM5_final', 'PM5_intensita_final', 'PM5_cause_final','PM6_final',
				'PM6_intensita_final', 'PM6_cause_final','PP1_final', 'PP1_intensita_final', 'PP1_cause_final',
				'PP2_final', 'PP2_intensita_final', 'PP2_cause_final' ,'PP3_final',
				'PP3_intensita_final', 'PP3_cause_final', 'PP4_final', 'PP4_intensita_final',
				'PP4_cause_final', 'PP5_final', 'PP5_intensita_final', 'PP5_cause_final',
				'choice_interpretation', 'choice_interpretation_final', 'operatoreGENETISTA',
				'operatoreGENETISTA_date', 'PMPOT_final', 'PMPOT_intensita_final', 'PMPOT_cause_final',
				'PMPOT_final', 'PPPOT_intensita_final', 'PPPOT_cause_final', 'consequence', 'ada_score',
				'rf_score', 'revel_score', 'autopvs1', 'spliceAI', 'vusstatus', 'importanza',
				'annotazione', 'PPPOT_final', 'var_on_gene', 'zigosita', 'allinheritance', 'codice_pannello',
				'id_interno', 'cadd_score', 'polyphen2_hdiv_score', 'polyphen2_hvar_score', 'sift_score')


	def validate_sample_id(self, value):
		regex = re.compile(r'^\w{0,10}\d+[.]\d{4,4}$')
		if value == '':
			raise serializers.ValidationError("Sample ID non puo essere vuoto")
		elif not (re.match(regex, value)):
			raise serializers.ValidationError("Attenzione, Sample ID deve essere in un\' altro formato")
		return value

	def validate_hgvs(self, value):
		if self.instance and self.instance.hgvs != value:
			raise serializers.ValidationError("You are not ALLOWED to edit HGVS field")
		return value

	def validate_BA1_cause_final(self, value):
		return self.instance.BA1_cause_final

	def validate_BS1_cause_final(self, value):
		return self.instance.BS1_cause_final

	def validate_BS2_cause_final(self, value):
		return self.instance.BS2_cause_final

	def validate_BS3_cause_final(self, value):
		return self.instance.BS3_cause_final

	def validate_BS4_cause_final(self, value):
		return self.instance.BS4_cause_final

	def validate_BP1_cause_final(self, value):
		return self.instance.BP1_cause_final

	def validate_BP2_cause_final(self, value):
		return self.instance.BP2_cause_final

	def validate_BP3_cause_final(self, value):
		return self.instance.BP3_cause_final

	def validate_BP4_cause_final(self, value):
		return self.instance.BP4_cause_final

	def validate_BP5_cause_final(self, value):
		return self.instance.BP5_cause_final

	def validate_BP6_cause_final(self, value):
		return self.instance.BP6_cause_final

	def validate_BP7_cause_final(self, value):
		return self.instance.BP7_cause_final

	def validate_PVS1_cause_final(self, value):
		return self.instance.PVS1_cause_final

	def validate_PS1_cause_final(self, value):
		return self.instance.PS1_cause_final

	def validate_PS2_cause_final(self, value):
		return self.instance.PS2_cause_final

	def validate_PS3_cause_final(self, value):
		return self.instance.PS3_cause_final

	def validate_PS4_cause_final(self, value):
		return self.instance.PS4_cause_final

	def validate_PM1_cause_final(self, value):
		return self.instance.PM1_cause_final

	def validate_PM2_cause_final(self, value):
		return self.instance.PM2_cause_final

	def validate_PM3_cause_final(self, value):
		return self.instance.PM3_cause_final

	def validate_PM4_cause_final(self, value):
		return self.instance.PM4_cause_final

	def validate_PM5_cause_final(self, value):
		return self.instance.PM5_cause_final

	def validate_PM6_cause_final(self, value):
		return self.instance.PM6_cause_final

	def validate_PP1_cause_final(self, value):
		return self.instance.PP1_cause_final

	def validate_PP2_cause_final(self, value):
		return self.instance.PP2_cause_final

	def validate_PP3_cause_final(self, value):
		return self.instance.PP3_cause_final

	def validate_PP4_cause_final(self, value):
		return self.instance.PP4_cause_final

	def validate_PP5_cause_final(self, value):
		return self.instance.PP5_cause_final

	def __init__(self, *args,**kwargs):
		super(ResultInterpretationSerializer_NOTCAUSE,self).__init__(*args, **kwargs)
		self.fields['BA1_intensita_final'].initial = ST
		self.fields['BS1_intensita_final'].initial = S
		self.fields['BS2_intensita_final'].initial = S
		self.fields['BS3_intensita_final'].initial = S
		self.fields['BS4_intensita_final'].initial = S
		self.fields['BP1_intensita_final'].initial = SP
		self.fields['BP2_intensita_final'].initial = SP
		self.fields['BP3_intensita_final'].initial = SP
		self.fields['BP4_intensita_final'].initial = SP
		self.fields['BP5_intensita_final'].initial = SP
		self.fields['BP6_intensita_final'].initial = SP
		self.fields['BP7_intensita_final'].initial = SP
		self.fields['PVS1_intensita_final'].initial =S
		self.fields['PS1_intensita_final'].initial = S
		self.fields['PS2_intensita_final'].initial = S
		self.fields['PS3_intensita_final'].initial = S
		self.fields['PS4_intensita_final'].initial = S
		self.fields['PM1_intensita_final'].initial = M
		self.fields['PM2_intensita_final'].initial = SP
		self.fields['PM3_intensita_final'].initial = M
		self.fields['PM4_intensita_final'].initial = M
		self.fields['PM5_intensita_final'].initial = M
		self.fields['PM6_intensita_final'].initial = M
		self.fields['PP1_intensita_final'].initial = SP
		self.fields['PP2_intensita_final'].initial = SP
		self.fields['PP3_intensita_final'].initial = SP
		self.fields['PP4_intensita_final'].initial = SP
		self.fields['PP5_intensita_final'].initial = SP
		self.fields['PMPOT_intensita_final'].initial = SP
		self.fields['PPPOT_intensita_final'].initial = SP



class ResultInterpretationRestoreSerializer(serializers.ModelSerializer):

	# timestamp_sample = serializers.DateTimeField(format="%d-%m-%Y")
	# updated_sample = serializers.DateTimeField(format = "%d-%m-%Y")

	class Meta:
		model = ResultInterpretationRestore
		fields = ('pk','sample_id', 'hgvs', 'BA1_final', 'BA1_intensita_final', 'BA1_cause_final', 'BS1_final',
				'BS1_intensita_final', 'BS1_cause_final', 'BS2_final', 'BS2_intensita_final',
				'BS2_cause_final', 'BS3_final', 'BS3_intensita_final', 'BS3_cause_final', 'BS4_final',
				'BS4_intensita_final', 'BS4_cause_final', 'BP1_final', 'BP1_intensita_final',
				'BP1_cause_final', 'BP2_final', 'BP2_intensita_final', 'BP2_cause_final', 'BP3_final',
				'BP3_intensita_final', 'BP3_cause_final', 'BP4_final', 'BP4_intensita_final',
				'BP4_cause_final', 'BP5_final', 'BP5_intensita_final', 'BP5_cause_final', 'BP6_final',
				'BP6_intensita_final', 'BP6_cause_final', 'BP7_final', 'BP7_intensita_final',
				'BP7_cause_final', 'PVS1_final', 'PVS1_intensita_final', 'PVS1_cause_final', 'PS1_final',
				'PS1_intensita_final', 'PS1_cause_final', 'PS2_final', 'PS2_intensita_final',
				'PS2_cause_final', 'PS3_final', 'PS3_intensita_final', 'PS3_cause_final', 'PS4_final',
				'PS4_intensita_final', 'PS4_cause_final', 'PM1_final', 'PM1_intensita_final',
				'PM1_cause_final', 'PM2_final', 'PM2_intensita_final', 'PM2_cause_final', 'PM3_final',
				'PM3_intensita_final', 'PM3_cause_final', 'PM4_final', 'PM4_intensita_final',
				'PM4_cause_final' ,'PM5_final', 'PM5_intensita_final', 'PM5_cause_final','PM6_final',
				'PM6_intensita_final', 'PM6_cause_final','PP1_final', 'PP1_intensita_final', 'PP1_cause_final',
				'PP2_final', 'PP2_intensita_final', 'PP2_cause_final' ,'PP3_final',
				'PP3_intensita_final', 'PP3_cause_final', 'PP4_final', 'PP4_intensita_final',
				'PP4_cause_final', 'PP5_final', 'PP5_intensita_final', 'PP5_cause_final',
				'choice_interpretation', 'choice_interpretation_final', 'operatoreGENETISTA',
				'operatoreGENETISTA_date', 'PMPOT_final', 'PMPOT_intensita_final', 'PMPOT_cause_final',
				'PMPOT_final', 'PPPOT_intensita_final', 'PPPOT_cause_final', 'consequence', 'ada_score',
				'rf_score', 'revel_score', 'autopvs1', 'spliceAI', 'vusstatus', 'importanza',
				'annotazione', 'PPPOT_final', 'var_on_gene', 'zigosita', 'allinheritance', 'codice_pannello',
				'id_interno', 'cadd_score', 'polyphen2_hdiv_score', 'polyphen2_hvar_score', 'sift_score')



	def validate_sample_id(self, value):
		regex = re.compile(r'^\w{0,10}\d+[.]\d{4,4}$')
		if value == '':
			raise serializers.ValidationError("Sample ID non puo essere vuoto")
		elif not (re.match(regex, value)):
			raise serializers.ValidationError("Attenzione, Sample ID deve essere in un\' altro formato")
		return value


	def validate_hgvs(self, value):
		if self.instance and self.instance.hgvs != value:
			raise serializers.ValidationError("You are not ALLOWED to edit HGVS field")
		return value

	def validate_BA1_cause_final(self, value):
		return self.instance.BA1_cause_final

	def validate_BS1_cause_final(self, value):
		return self.instance.BS1_cause_final

	def validate_BS2_cause_final(self, value):
		return self.instance.BS2_cause_final

	def validate_BS3_cause_final(self, value):
		return self.instance.BS3_cause_final

	def validate_BS4_cause_final(self, value):
		return self.instance.BS4_cause_final

	def validate_BP1_cause_final(self, value):
		return self.instance.BP1_cause_final

	def validate_BP2_cause_final(self, value):
		return self.instance.BP2_cause_final

	def validate_BP3_cause_final(self, value):
		return self.instance.BP3_cause_final

	def validate_BP4_cause_final(self, value):
		return self.instance.BP4_cause_final

	def validate_BP5_cause_final(self, value):
		return self.instance.BP5_cause_final

	def validate_BP6_cause_final(self, value):
		return self.instance.BP6_cause_final

	def validate_BP7_cause_final(self, value):
		return self.instance.BP7_cause_final

	def validate_PVS1_cause_final(self, value):
		return self.instance.PVS1_cause_final

	def validate_PS1_cause_final(self, value):
		return self.instance.PS1_cause_final

	def validate_PS2_cause_final(self, value):
		return self.instance.PS2_cause_final

	def validate_PS3_cause_final(self, value):
		return self.instance.PS3_cause_final

	def validate_PS4_cause_final(self, value):
		return self.instance.PS4_cause_final

	def validate_PM1_cause_final(self, value):
		return self.instance.PM1_cause_final

	def validate_PM2_cause_final(self, value):
		return self.instance.PM2_cause_final

	def validate_PM3_cause_final(self, value):
		return self.instance.PM3_cause_final

	def validate_PM4_cause_final(self, value):
		return self.instance.PM4_cause_final

	def validate_PM5_cause_final(self, value):
		return self.instance.PM5_cause_final

	def validate_PM6_cause_final(self, value):
		return self.instance.PM6_cause_final

	def validate_PP1_cause_final(self, value):
		return self.instance.PP1_cause_final

	def validate_PP2_cause_final(self, value):
		return self.instance.PP2_cause_final

	def validate_PP3_cause_final(self, value):
		return self.instance.PP3_cause_final

	def validate_PP4_cause_final(self, value):
		return self.instance.PP4_cause_final

	def validate_PP5_cause_final(self, value):
		return self.instance.PP5_cause_final

	def __init__(self, *args,**kwargs):
		super(ResultInterpretationRestoreSerializer,self).__init__(*args, **kwargs)
		self.fields['BA1_intensita_final'].initial = ST
		self.fields['BS1_intensita_final'].initial = S
		self.fields['BS2_intensita_final'].initial = S
		self.fields['BS3_intensita_final'].initial = S
		self.fields['BS4_intensita_final'].initial = S
		self.fields['BP1_intensita_final'].initial = SP
		self.fields['BP2_intensita_final'].initial = SP
		self.fields['BP3_intensita_final'].initial = SP
		self.fields['BP4_intensita_final'].initial = SP
		self.fields['BP5_intensita_final'].initial = SP
		self.fields['BP6_intensita_final'].initial = SP
		self.fields['BP7_intensita_final'].initial = SP
		self.fields['PVS1_intensita_final'].initial =S
		self.fields['PS1_intensita_final'].initial = S
		self.fields['PS2_intensita_final'].initial = S
		self.fields['PS3_intensita_final'].initial = S
		self.fields['PS4_intensita_final'].initial = S
		self.fields['PM1_intensita_final'].initial = M
		self.fields['PM2_intensita_final'].initial = SP
		self.fields['PM3_intensita_final'].initial = M
		self.fields['PM4_intensita_final'].initial = M
		self.fields['PM5_intensita_final'].initial = M
		self.fields['PM6_intensita_final'].initial = M
		self.fields['PP1_intensita_final'].initial = SP
		self.fields['PP2_intensita_final'].initial = SP
		self.fields['PP3_intensita_final'].initial = SP
		self.fields['PP4_intensita_final'].initial = SP
		self.fields['PP5_intensita_final'].initial = SP
		self.fields['PMPOT_intensita_final'].initial = SP
		self.fields['PPPOT_intensita_final'].initial = SP
