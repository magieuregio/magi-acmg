#!/home/bioinfo/VIRTUAL38/bin/python3.8

import os,re,glob,csv,argparse,datetime,sys,subprocess
from os.path import isfile, join

import numpy as np
import pandas as pd
from varsome_api.client import VarSomeAPIClient,VarSomeAPIException
from varsome_api.models.variant import AnnotatedVariant
import sqlite3
from os import listdir, system
import requests
from requests.structures import CaseInsensitiveDict
from requests.auth import HTTPBasicAuth
from pandas.io.json import json_normalize
import json
import ast

path = os.getcwd()

pd.options.display.float_format = '{:,.5f}'.format


strength_dict={'BA1':'Stand Alone','BS1':'Strong','BS2':'Strong','BS3':'Strong',
		'BS4':'Strong','BP1':'Supporting','BP2':'Supporting',
		'BP3':'Supporting','BP4':'Supporting','BP5':'Supporting',
		'BP6':'Supporting','BP7':'Supporting','PVS1':'Very Strong','PS1':'Strong',
		'PS2':'Strong','PS3':'Strong','PS4':'Strong','PM1':'Moderate','PM2':'Moderate','PM3':'Moderate',
		'PM4':'Moderate','PM5':'Moderate','PM6':'Moderate','PP1':'Supporting','PP2':'Supporting',
		'PP3':'Supporting','PP4':'Supporting','PP5':'Supporting','PMPOT': 'Moderate','PPPOT':'Supporting'}

def variant_interpretation(variant):
	variant['BA1_intensita_final'].fillna(strength_dict['BA1'],inplace=True)
	variant['BS1_intensita_final'].fillna(strength_dict['BS1'],inplace=True)
	variant['BS2_intensita_final'].fillna(strength_dict['BS2'],inplace=True)
	variant['BS3_intensita_final'].fillna(strength_dict['BS3'],inplace=True)
	variant['BS4_intensita_final'].fillna(strength_dict['BS4'],inplace=True)
	variant['BP1_intensita_final'].fillna(strength_dict['BP1'],inplace=True)
	variant['BP2_intensita_final'].fillna(strength_dict['BP2'],inplace=True)
	variant['BP3_intensita_final'].fillna(strength_dict['BP3'],inplace=True)
	variant['BP4_intensita_final'].fillna(strength_dict['BP4'],inplace=True)
	variant['BP5_intensita_final'].fillna(strength_dict['BP5'],inplace=True)
	variant['BP6_intensita_final'].fillna(strength_dict['BP6'],inplace=True)
	variant['BP7_intensita_final'].fillna(strength_dict['BP7'],inplace=True)
	variant['PVS1_intensita_final'].fillna(strength_dict['PVS1'],inplace=True)
	variant['PS1_intensita_final'].fillna(strength_dict['PS1'],inplace=True)
	variant['PS2_intensita_final'].fillna(strength_dict['PS2'],inplace=True)
	variant['PS3_intensita_final'].fillna(strength_dict['PS3'],inplace=True)
	variant['PS4_intensita_final'].fillna(strength_dict['PS4'],inplace=True)
	variant['PM1_intensita_final'].fillna(strength_dict['PM1'],inplace=True)
	variant['PM2_intensita_final'].fillna(strength_dict['PM2'],inplace=True)
	variant['PM3_intensita_final'].fillna(strength_dict['PM3'],inplace=True)
	variant['PM4_intensita_final'].fillna(strength_dict['PM4'],inplace=True)
	variant['PM5_intensita_final'].fillna(strength_dict['PM5'],inplace=True)
	variant['PM6_intensita_final'].fillna(strength_dict['PM6'],inplace=True)
	variant['PP1_intensita_final'].fillna(strength_dict['PP1'],inplace=True)
	variant['PP2_intensita_final'].fillna(strength_dict['PP2'],inplace=True)
	variant['PP3_intensita_final'].fillna(strength_dict['PP3'],inplace=True)
	variant['PP4_intensita_final'].fillna(strength_dict['PP4'],inplace=True)
	variant['PP5_intensita_final'].fillna(strength_dict['PP5'],inplace=True)
	variant['PMPOT_intensita_final'].fillna(strength_dict['PMPOT'],inplace=True)
	variant['PPPOT_intensita_final'].fillna(strength_dict['PPPOT'],inplace=True)

	variant['BA1_final'].fillna(0,inplace=True)
	variant['BS1_final'].fillna(0,inplace=True)
	variant['BS2_final'].fillna(0,inplace=True)
	variant['BS3_final'].fillna(0,inplace=True)
	variant['BS4_final'].fillna(0,inplace=True)
	variant['BP1_final'].fillna(0,inplace=True)
	variant['BP2_final'].fillna(0,inplace=True)
	variant['BP3_final'].fillna(0,inplace=True)
	variant['BP4_final'].fillna(0,inplace=True)
	variant['BP5_final'].fillna(0,inplace=True)
	variant['BP6_final'].fillna(0,inplace=True)
	variant['BP7_final'].fillna(0,inplace=True)
	variant['PVS1_final'].fillna(0,inplace=True)
	variant['PS2_final'].fillna(0,inplace=True)
	variant['PS3_final'].fillna(0,inplace=True)
	variant['PS1_final'].fillna(0,inplace=True)
	variant['PS4_final'].fillna(0,inplace=True)
	variant['PM1_final'].fillna(0,inplace=True)
	variant['PM2_final'].fillna(0,inplace=True)
	variant['PM3_final'].fillna(0,inplace=True)
	variant['PM4_final'].fillna(0,inplace=True)
	variant['PM5_final'].fillna(0,inplace=True)
	variant['PM6_final'].fillna(0,inplace=True)
	variant['PP1_final'].fillna(0,inplace=True)
	variant['PP2_final'].fillna(0,inplace=True)
	variant['PP3_final'].fillna(0,inplace=True)
	variant['PP4_final'].fillna(0,inplace=True)
	variant['PP5_final'].fillna(0,inplace=True)
	variant['PMPOT_final'].fillna(0,inplace=True)
	variant['PPPOT_final'].fillna(0,inplace=True)

	benign_subscore = 'Uncertain Significance'
	pathogenic_subscore = 'Uncertain Significance'
	verdict = 'Uncertain Significance'
	punti_pat = ['PVS1', 'PS1', 'PS2', 'PS3', 'PS4', 'PM1', 'PM2', 'PM3', 'PM4', 'PM5', 'PM6', 'PP1', 'PP2', 'PP3', 'PP4', 'PP5','PMPOT','PPPOT']
	punti_ben = ['BA1', 'BS1', 'BS2', 'BS3', 'BS4', 'BP1', 'BP2', 'BP3', 'BP4', 'BP5', 'BP6', 'BP7']

	n_PM = 0
	n_PP = 0
	n_BP = 0
	pat_model = {}
	index = variant.index[0]
	#dataframe of pathogenicity, rows the criteria and cols the 3 characteristics
	for p in punti_pat:
		if (variant.loc[index, p+'_intensita_final'] == 'Moderate') & (variant.loc[index, p+'_final'] == 1) & ((p == 'PM1') | (p == 'PM3') | (p == 'PM4') | (p == 'PM5') | (p == 'PM6')):
			n_PM = n_PM + 1
		if (variant.loc[index, p+'_intensita_final'] == 'Supporting') & (variant.loc[index, p+'_final'] == 1) & ((p == 'PP1') | (p == 'PP2') | (p == 'PP4') | (p == 'PP5') | (p == 'PP6')):
			n_PP = n_PP + 1
		pat_model[p] = {'Presenza': variant.loc[index, p+'_final'],'Intensita': variant.loc[index, p+'_intensita_final']}
	pat_model = pd.DataFrame.from_dict(pat_model, orient='index')

	if len(pat_model.loc[(pat_model['Intensita'].astype(str) != 'Very Strong') & (pat_model['Intensita'].astype(str) != 'Strong') & (pat_model['Intensita'].astype(str) != 'Moderate') & (pat_model['Intensita'].astype(str) != 'Supporting')]) != 0:
		benign_subscore = 'UNKNOWN'
		pathogenic_subscore = 'UNKNOWN'
		verdict = 'UNKNOWN'
	#dataframe of benignity, rows the criteria and cols the 3 characteristics
	ben_model = {}
	for p in punti_ben:
		if (variant.loc[index, p+'_intensita_final'] == 'Supporting') &  (variant.loc[index, p+'_final'] == 1) & ((p == 'BP1') | (p == 'BP2') | (p == 'BP3') | (p == 'BP4') | (p == 'BP5') | (p == 'BP6')):
			n_BP = n_BP + 1
		#gestisco i diversi modi di scrivere Stand Alone
		if (p == 'BA1') & ((variant.loc[index, p+'_intensita_final'] == 'Stand-Alone') | (variant.loc[index, p+'_intensita_final'] == 'Stand Alone') | (variant.loc[index, p+'_intensita_final'] == 'Stand-alone') | (variant.loc[index, p+'_intensita_final'] == 'Stand alone')):
			ben_model[p] = {'Presenza': variant.loc[index, p+'_final'], 'Intensita': 'Stand Alone'}
		else:
			ben_model[p] = {'Presenza': variant.loc[index, p+'_final'], 'Intensita': variant.loc[index, p+'_intensita_final']}
	ben_model = pd.DataFrame.from_dict(ben_model, orient='index')
	if len(ben_model.loc[((ben_model['Intensita'].astype(str) != 'Stand Alone')) & (ben_model['Intensita'].astype(str) != 'Strong')  & (ben_model['Intensita'].astype(str) != 'Moderate')  & (ben_model['Intensita'].astype(str) != 'Supporting')]) != 0:
		benign_subscore = 'UNKNOWN'
		pathogenic_subscore = 'UNKNOWN'
		verdict = 'UNKNOWN'
##################################################################################################
##################################################################################################
	present_pat = pat_model[pat_model['Presenza']==1]
	pat_intensities = present_pat['Intensita'].value_counts()
	if 'Very Strong' not in pat_intensities:
		pat_intensities['Very Strong'] = 0
	if 'Strong' not in pat_intensities:
		pat_intensities['Strong'] = 0
	if 'Moderate' not in pat_intensities:
		pat_intensities['Moderate'] = 0
	if 'Supporting' not in pat_intensities:
		pat_intensities['Supporting'] = 0

	present_ben = ben_model[ben_model['Presenza']==1]
	ben_intensities = present_ben['Intensita'].value_counts()
	if 'Stand Alone' not in ben_intensities:
		ben_intensities['Stand Alone'] = 0
	if 'Strong' not in ben_intensities:
		ben_intensities['Strong'] = 0
	if 'Supporting' not in ben_intensities:
		ben_intensities['Supporting'] = 0

	#print ('AAAAAAAAAAA:',pat_intensities,'pat intesity')
	###############Algorithm Varsome Interpratation#############################
	if pathogenic_subscore != 'UNKNOWN':
		if (pat_intensities['Very Strong'] >= 2):
			pathogenic_subscore = 'Pathogenic'
		elif (pat_intensities['Very Strong'] >= 1):
			if pat_intensities['Strong'] >= 1:
				pathogenic_subscore = 'Pathogenic'
			elif pat_intensities['Moderate'] >= 2:
				pathogenic_subscore = 'Pathogenic'
			elif (pat_intensities['Moderate'] ==1) & (pat_intensities['Supporting'] >=1):
				pathogenic_subscore = 'Pathogenic'
			elif pat_intensities['Supporting'] >=2:
				pathogenic_subscore = 'Pathogenic'
			elif pat_intensities['Moderate'] ==1:
				pathogenic_subscore = 'Likely Pathogenic'
		elif ((pat_intensities['Strong'] >= 2) & (pat_intensities['Very Strong'] == 0)):
			#print ('PATHOGENICCCCCCCCCCCCC')
			pathogenic_subscore = 'Pathogenic'
		elif ((pat_intensities['Strong'] == 1) & (pat_intensities['Very Strong'] == 0)):
			if (pat_intensities['Moderate'] == 0) & (pat_intensities['Supporting'] >= 2): #modificato da >=1 supporting a >=2
				pathogenic_subscore = 'Likely Pathogenic'
			elif (((pat_intensities['Moderate'] == 1) | (pat_intensities['Moderate'] == 2)) & (pat_intensities['Supporting'] == 0)):
				pathogenic_subscore = 'Likely Pathogenic'
			elif (pat_intensities['Moderate'] == 1) & (pat_intensities['Supporting'] >= 4):#modificato da >=3 supporting a >=4
				pathogenic_subscore = 'Pathogenic'
			elif (pat_intensities['Moderate'] == 1) & (pat_intensities['Supporting'] < 4):#mod da <3 a <4
				pathogenic_subscore = 'Likely Pathogenic'
			elif (pat_intensities['Moderate'] == 2) & (pat_intensities['Supporting'] >= 2):
				pathogenic_subscore = 'Pathogenic'
			elif (pat_intensities['Moderate'] == 2) & (pat_intensities['Supporting'] < 2):
				pathogenic_subscore = 'Likely Pathogenic'
			elif pat_intensities['Moderate'] >= 3:
				pathogenic_subscore = 'Pathogenic'
		elif ((pat_intensities['Moderate'] == 1) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0)):
			if pat_intensities['Supporting'] >= 4: # 25022022 modificato per nuovo algoritmo #sul web 4
				pathogenic_subscore = 'Likely Pathogenic'
			elif pat_intensities['Supporting'] < 4: # 25022022 modificato per nuovo algoritmo #sul web 4
				pathogenic_subscore = 'Uncertain Significance'
		elif ((pat_intensities['Moderate'] >= 3) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0)):
			pathogenic_subscore =  'Likely Pathogenic'
		elif ((pat_intensities['Moderate'] >= 2) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0)):
			if pat_intensities['Supporting'] == 1:
				pathogenic_subscore = 'Uncertain Significance'
			elif (pat_intensities['Supporting'] == 2):
				pathogenic_subscore =  'Likely Pathogenic'
			elif pat_intensities['Supporting'] >= 3:
				pathogenic_subscore =  'Likely Pathogenic'

	if benign_subscore != 'UNKNOWN':
		if ben_intensities['Stand Alone'] >= 1:
			benign_subscore = 'Benign'
		elif ben_intensities['Strong'] >= 2:
			benign_subscore = 'Benign'
		elif ben_intensities['Strong'] == 1:
			benign_subscore = 'Likely Benign'
		elif ben_intensities['Supporting'] >= 2:
			benign_subscore = 'Likely Benign'
	#############################################################################
	if (benign_subscore == 'Uncertain Significance') & (pathogenic_subscore != 'Uncertain Significance'):
		verdict = pathogenic_subscore
	elif (benign_subscore != 'Uncertain Significance'):	# & (pathogenic_subscore == 'Uncertain Significance'): #questo serve per gestire quando ho 1 strong benigno e dei punti di patogenicita\' attivi
		if (ben_intensities['Strong'] == 1) & (ben_intensities['Stand Alone'] == 0) & (ben_intensities['Supporting'] == 0):
			if ((pat_intensities['Very Strong'] == 0) & (pat_intensities['Strong'] == 0) & (pat_intensities['Moderate'] ==0) & (pat_intensities['Supporting'] == 0)):
				verdict = benign_subscore
			else: verdict = pathogenic_subscore
		else:
			if (pathogenic_subscore == 'Uncertain Significance'): verdict = benign_subscore

    # Inseire Algoritmo per Intensita\' VUS
	if verdict == 'Uncertain Significance':
		vusstatus = 'Unknown'
		#if (pat_intensities['Very Strong'] == 1) & (pat_intensities['Strong'] == 0) & (pat_intensities['Supporting'] == 0) & (pat_intensities['Moderate'] == 0):
		if (pat_intensities['Very Strong'] == 1) & (pat_intensities['Strong'] == 0):
			if (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif ((ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2)):
				vusstatus = 'Cold'
			elif ((ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1)):
				vusstatus = 'Cold'
			elif  ((ben_intensities['Strong'] == 1)&(ben_intensities['Supporting'] == 0)):
				if (pat_intensities['Supporting'] == 1): vusstatus='Hot'
				else: vusstatus = 'Middle'
			elif (ben_intensities['Supporting'] == 1) & (ben_intensities['Strong'] == 0):#@
				vusstatus = 'Hot'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Hot'
			elif (pat_intensities['Moderate'] == 1) & (ben_intensities['Supporting'] == 1):#@
				vusstatus = 'Hot'
			elif ((pat_intensities['Supporting']== 1) & (ben_intensities['Supporting']==1)):#@
				vusstatus = 'Middle'
		elif (pat_intensities['Strong'] >= 1) & (pat_intensities['Very Strong'] == 0) & (ben_intensities['Stand Alone'] == 1):
				vusstatus = 'Middle'
		elif (pat_intensities['Strong'] == 1) & (pat_intensities['Very Strong'] == 0):
			if (ben_intensities['Strong'] == 2):#@@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1): #@@@@
				if (pat_intensities['Supporting'] == 1): vusstatus = 'Hot'
				else: vusstatus = 'Middle'
			elif (ben_intensities['Supporting'] == 2): #@@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Supporting'] == 0):#@
				vusstatus = 'Hot'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Supporting'] == 0):#@
				vusstatus = 'Hot'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Supporting'] == 1):#@
				vusstatus = 'Hot'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Supporting'] == 1):#@
				vusstatus = 'Hot'
		elif (pat_intensities['Moderate'] == 1) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
			if (ben_intensities['Strong'] == 2):#@@@@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2) & (pat_intensities['Supporting'] == 1):#@
				vusstatus = 'Cold'#
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 1):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 2):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2) & (pat_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 1):#@
				vusstatus = 'Middle'#c
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2) & (pat_intensities['Supporting'] == 2):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 1):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 2):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 3):#@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2) & (pat_intensities['Supporting'] == 3):#@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 3):#@
				vusstatus = 'Hot' #m
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 3):#@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 2):#@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 2):#@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 1):#@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 3):#@
				vusstatus = 'Hot'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 3):#@
				vusstatus = 'Hot'
		elif (pat_intensities['Moderate'] == 2) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
			if (ben_intensities['Strong'] == 2):#@@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 0):#@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2) & (pat_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 0):#@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 0):#@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 1):#@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2) & (pat_intensities['Supporting'] == 1):#@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 1):#@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 1):#@
				vusstatus = 'Hot'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 1):#@
				vusstatus = 'Hot'
		elif (pat_intensities['Moderate'] == 3) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
			if (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2):
				vusstatus = 'Hot'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1):
				vusstatus = 'Hot'
			elif (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 0):
				vusstatus = 'Hot'
		elif (pat_intensities['Supporting'] >= 4) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
			if (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Hot'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1):#@
				vusstatus = 'Hot'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2):#@
				vusstatus = 'Cold'
		elif (pat_intensities['Supporting'] == 3) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
			if (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1):#@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2):#@
				vusstatus = 'Cold'
		elif (pat_intensities['Supporting'] == 2) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
			if (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2):#@
				vusstatus = 'Cold'
		elif (pat_intensities['Supporting'] == 1) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
			if (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2):#@
				vusstatus = 'Cold'
		elif (pat_intensities['Supporting'] == 0) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
			if (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0):
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1):
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0):
				vusstatus = 'Cold'
		elif (pat_intensities['Supporting']>0) | (pat_intensities['Moderate']>0) | (pat_intensities['Strong']>0) | (pat_intensities['Very Strong']>0):
			if (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 3):
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 2):
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 1):
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 3) & (ben_intensities['Supporting'] == 0):
				vusstatus = 'Cold'
		elif ((ben_intensities['Supporting'] >= 1) | (ben_intensities['Strong'] >= 1)):
			if ((pat_intensities['Supporting']==0) & pat_intensities['Moderate']==0 & (pat_intensities['Strong']==0) & (pat_intensities['Very Strong']==0)):
				vusstatus = 'Cold'
		elif ((ben_intensities['Supporting'] >= 0) & (ben_intensities['Strong'] >= 0)):
			if ((pat_intensities['Supporting']==0) & pat_intensities['Moderate']==0 & (pat_intensities['Strong']==0) & (pat_intensities['Very Strong']==0)):
				vusstatus = 'Cold'
		#######################################################################################
		#print ('VUSSTATUSSSSSSS111111111111111:',vusstatus)
		#######################################################################################
		if vusstatus=='Middle':
			# if (variant.loc[index,'PM2_final'] == 1) & (n_PM != 0) &(n_BP <=1):
			# 	# print('in pppot 1')
			# 	variant.loc[index,'PPPOT_final'] = 1
			# 	pat_model.loc['PPPOT','Presenza'] = 1
			# 	variant.loc[index,'PPPOT_intensita_final'] = 'Supporting'
			# 	if str(variant.loc[index,'PPPOT_cause_final']) != 'nan':
			# 		variant.loc[index,'PPPOT_cause_final'] = variant.loc[index,'PPPOT_cause_final'] + 'Verifica segregazione in ulteriori affetti (PP1) o compatibilita\' di fenotipo (PP4). '
			# 		pat_model.loc['PPPOT','Causa'] = pat_model.loc['PPPOT','Causa'] + 'Verifica segregazione in ulteriori affetti (PP1) o compatibilita\' di fenotipo (PP4). '
			# 	else:
			# 		variant.loc[index,'PPPOT_cause_final'] = 'Verifica segregazione in ulteriori affetti (PP1) o compatibilita\' di fenotipo (PP4). '
			# 		pat_model.loc['PPPOT','Causa'] = 'Verifica segregazione in ulteriori affetti (PP1) o compatibilita\' di fenotipo (PP4). '
			#
			# if (variant.loc[index,'PM2_final'] == 1) & (n_PP == 2) &(n_BP <=1):
			# 	variant.loc[index,'PPPOT_final'] = 1
			# 	pat_model.loc['PPPOT','Presenza'] = 1
			# 	variant.loc[index,'PPPOT_intensita_final'] = 'Supporting'
			# 	if str(variant.loc[index,'PPPOT_cause_final']) != 'nan':
			# 		variant.loc[index,'PPPOT_cause_final'] = variant.loc[index,'PPPOT_cause_final'] + 'Verifica segregazione in ulteriori affetti (PP1) o compatibilita\' di fenotipo (PP4). '
			# 	else:
			# 		variant.loc[index,'PPPOT_cause_final'] = 'Verifica segregazione in ulteriori affetti (PP1) o compatibilita\' di fenotipo (PP4). '

			if variant.loc[index,'var_on_gene']>=2:
				#print('1')
				variant.loc[index,'PPPOT_final'] = 1
				pat_model.loc['PPPOT','Presenza'] = 1
				variant.loc[index,'PPPOT_cause_final'] = ' Potenziale PM3-PM6-PP1 per eterozigosita\' composta'
			if variant.loc[index,'zigosita']=='homo':
				# print('2')
				variant.loc[index,'PPPOT_final'] = 1
				pat_model.loc['PPPOT','Presenza'] = 1
				variant.loc[index,'PPPOT_cause_final'] = 'Potenziale PM3-PM6-PP1 per variante in omozigosi'
			if 'AD' in variant.loc[index,'allinheritance']:
				# print('3')
				variant.loc[index,'PPPOT_final'] = 1
				pat_model.loc['PPPOT','Presenza'] = 1
				variant.loc[index,'PPPOT_cause_final'] = 'Potenziale PM3-PM6-PP1 per variante de novo'

			# if (variant.loc[index,'PM2_final'] == 1) & (variant.loc[index,'PP3_final'] == 1) & (('intron_variant' in variant.loc[index,'consequence']) | ('missense' in variant.loc[index,'consequence']) | ('synonimous' in variant.loc[index,'consequence'])) & (n_BP ==0):
			# 	variant.loc[index,'PPPOT_final'] = 1
			# 	pat_model.loc['PPPOT','Presenza'] = 1
			# 	variant.loc[index,'PPPOT_intensita_final'] = 'Moderate'
			# 	if str(variant.loc[index,'PPPOT_cause_final']) != 'nan':
			# 		variant.loc[index,'PPPOT_cause_final'] = variant.loc[index,'PPPOT_cause_final'] + 'Verifica stato allelico (PM3) o insorgenza de novo (PM6). '
			# 	else:
			# 		variant.loc[index,'PPPOT_cause_final'] = 'Verifica stato allelico (PM3) o insorgenza de novo (PM6). '
			###################################################################################################
			###################################################################################################
			present_pat = pat_model[pat_model['Presenza']==1]
			pat_intensities = present_pat['Intensita'].value_counts()
			if 'Very Strong' not in pat_intensities:
				pat_intensities['Very Strong'] = 0
			if 'Strong' not in pat_intensities:
				pat_intensities['Strong'] = 0
			if 'Moderate' not in pat_intensities:
				pat_intensities['Moderate'] = 0
			if 'Supporting' not in pat_intensities:
				pat_intensities['Supporting'] = 0
			###################################################################################################
			#if (pat_intensities['Very Strong'] == 1) & (pat_intensities['Strong'] == 0) & (pat_intensities['Supporting'] == 0) & (pat_intensities['Moderate'] == 0):
			if (pat_intensities['Very Strong'] == 1) & (pat_intensities['Strong'] == 0):
				if (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 0):#@
					vusstatus = 'Cold'
				elif ((ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2)):
					vusstatus = 'Cold'
				elif ((ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1)):
					vusstatus = 'Cold'
				elif  ((ben_intensities['Strong'] == 1)&(ben_intensities['Supporting'] == 0)):
					if (pat_intensities['Supporting'] == 1): vusstatus = 'Hot'
					else: vusstatus = 'Middle'
				elif (ben_intensities['Supporting'] == 1) & (ben_intensities['Strong'] == 0):#@
					vusstatus = 'Hot'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0):#@
					vusstatus = 'Hot'
				elif (pat_intensities['Moderate']==1) & (ben_intensities['Supporting'] == 1):#@
					vusstatus = 'Hot'
				elif (pat_intensities['Supporting']==1) & (ben_intensities['Supporting']==1):#@
					vusstatus = 'Middle'
			elif (pat_intensities['Strong']>=1) & (pat_intensities['Very Strong']==0) & (ben_intensities['Stand Alone'] == 1):
					vusstatus = 'Middle'
			elif (pat_intensities['Strong']==1) & (pat_intensities['Very Strong'] == 0):
				if (ben_intensities['Strong'] == 2):#@@
					vusstatus='Cold'
				elif (ben_intensities['Strong'] == 1): #@@@@
					if (pat_intensities['Supporting'] == 1): vusstatus='Hot'
					else: vusstatus='Middle'
				elif (ben_intensities['Supporting'] == 2): #@@
					vusstatus = 'Middle'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Supporting'] == 0):#@
					vusstatus = 'Hot'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Supporting'] == 0):#@
					vusstatus = 'Hot'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Supporting'] == 1):#@
					vusstatus = 'Hot'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Supporting'] == 1):#@
					vusstatus = 'Hot'
			elif (pat_intensities['Moderate'] == 1) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
				if (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 1):#@
					vusstatus = 'Middle'
				elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 3):#@
					vusstatus = 'Middle'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2) & (pat_intensities['Supporting'] == 3):#@
					vusstatus = 'Middle'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 3):#@
					vusstatus = 'Hot' #m
				elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 3):#@
					vusstatus = 'Middle'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 1):#@
					vusstatus = 'Middle'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 2):#@
					vusstatus = 'Hot'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 2):#@
					vusstatus = 'Hot'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 3):#@
					vusstatus = 'Hot'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 3):#@
					vusstatus = 'Hot'
			elif (pat_intensities['Moderate'] == 2) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
				if (ben_intensities['Strong'] == 2):#@@
					vusstatus = 'Cold'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 0):#@
					vusstatus = 'Middle'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 0):#@
					vusstatus = 'Middle'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 0):#@
					vusstatus = 'Middle'
				elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 1):#@
					vusstatus = 'Middle'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2) & (pat_intensities['Supporting'] == 1):#@
					vusstatus = 'Middle'
				elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 1):#@
					vusstatus = 'Middle'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 1):#@
					vusstatus = 'Hot'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 1):#@
					vusstatus = 'Hot'
			elif (pat_intensities['Moderate'] == 3) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
				if (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2):
					vusstatus = 'Hot'
				elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1):
					vusstatus = 'Hot'
				elif (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 0):
					vusstatus = 'Hot'
			elif (pat_intensities['Supporting'] >= 4) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
				if (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0):#@
					vusstatus = 'Hot'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1):#@
					vusstatus = 'Hot'
				elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0):#@
					vusstatus = 'Cold'
				elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1):#@
					vusstatus = 'Cold'
				elif (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 0):#@
					vusstatus = 'Cold'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2):#@
					vusstatus = 'Cold'
			elif (pat_intensities['Supporting'] == 3) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
				if (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0):#@
					vusstatus = 'Middle'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1):#@
					vusstatus = 'Middle'
				elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0):#@
					vusstatus = 'Cold'
				elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1):#@
					vusstatus = 'Cold'
				elif (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 0):#@
					vusstatus = 'Cold'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2):#@
					vusstatus = 'Cold'
			#######################################################################################
			#print ('VUSSTATUSSSSSSS222222:',vusstatus)
			#vusstatus = 'Hot' #cold, middle
	else:  vusstatus = 'Unknown'
	return verdict, vusstatus,variant.loc[index,'PMPOT_final'],variant.loc[index,'PMPOT_intensita_final'],variant.loc[index,'PMPOT_cause_final'], variant.loc[index,'PPPOT_final'], variant.loc[index,'PPPOT_intensita_final'], variant.loc[index,'PPPOT_cause_final']


def variant_interpretation(variant):
	# print('variant_interpretation')
	variant['BA1_intensita_final'].fillna(strength_dict['BA1'],inplace=True)
	variant['BS1_intensita_final'].fillna(strength_dict['BS1'],inplace=True)
	variant['BS2_intensita_final'].fillna(strength_dict['BS2'],inplace=True)
	variant['BS3_intensita_final'].fillna(strength_dict['BS3'],inplace=True)
	variant['BS4_intensita_final'].fillna(strength_dict['BS4'],inplace=True)
	variant['BP1_intensita_final'].fillna(strength_dict['BP1'],inplace=True)
	variant['BP2_intensita_final'].fillna(strength_dict['BP2'],inplace=True)
	variant['BP3_intensita_final'].fillna(strength_dict['BP3'],inplace=True)
	variant['BP4_intensita_final'].fillna(strength_dict['BP4'],inplace=True)
	variant['BP5_intensita_final'].fillna(strength_dict['BP5'],inplace=True)
	variant['BP6_intensita_final'].fillna(strength_dict['BP6'],inplace=True)
	variant['BP7_intensita_final'].fillna(strength_dict['BP7'],inplace=True)
	variant['PVS1_intensita_final'].fillna(strength_dict['PVS1'],inplace=True)
	variant['PS1_intensita_final'].fillna(strength_dict['PS1'],inplace=True)
	variant['PS2_intensita_final'].fillna(strength_dict['PS2'],inplace=True)
	variant['PS3_intensita_final'].fillna(strength_dict['PS3'],inplace=True)
	variant['PS4_intensita_final'].fillna(strength_dict['PS4'],inplace=True)
	variant['PM1_intensita_final'].fillna(strength_dict['PM1'],inplace=True)
	variant['PM2_intensita_final'].fillna(strength_dict['PM2'],inplace=True)
	variant['PM3_intensita_final'].fillna(strength_dict['PM3'],inplace=True)
	variant['PM4_intensita_final'].fillna(strength_dict['PM4'],inplace=True)
	variant['PM5_intensita_final'].fillna(strength_dict['PM5'],inplace=True)
	variant['PM6_intensita_final'].fillna(strength_dict['PM6'],inplace=True)
	variant['PP1_intensita_final'].fillna(strength_dict['PP1'],inplace=True)
	variant['PP2_intensita_final'].fillna(strength_dict['PP2'],inplace=True)
	variant['PP3_intensita_final'].fillna(strength_dict['PP3'],inplace=True)
	variant['PP4_intensita_final'].fillna(strength_dict['PP4'],inplace=True)
	variant['PP5_intensita_final'].fillna(strength_dict['PP5'],inplace=True)
	variant['PMPOT_intensita_final'].fillna(strength_dict['PMPOT'],inplace=True)
	variant['PPPOT_intensita_final'].fillna(strength_dict['PPPOT'],inplace=True)

	variant['BA1_final'].fillna(0,inplace=True)
	variant['BS1_final'].fillna(0,inplace=True)
	variant['BS2_final'].fillna(0,inplace=True)
	variant['BS3_final'].fillna(0,inplace=True)
	variant['BS4_final'].fillna(0,inplace=True)
	variant['BP1_final'].fillna(0,inplace=True)
	variant['BP2_final'].fillna(0,inplace=True)
	variant['BP3_final'].fillna(0,inplace=True)
	variant['BP4_final'].fillna(0,inplace=True)
	variant['BP5_final'].fillna(0,inplace=True)
	variant['BP6_final'].fillna(0,inplace=True)
	variant['BP7_final'].fillna(0,inplace=True)
	variant['PVS1_final'].fillna(0,inplace=True)
	variant['PS2_final'].fillna(0,inplace=True)
	variant['PS3_final'].fillna(0,inplace=True)
	variant['PS1_final'].fillna(0,inplace=True)
	variant['PS4_final'].fillna(0,inplace=True)
	variant['PM1_final'].fillna(0,inplace=True)
	variant['PM2_final'].fillna(0,inplace=True)
	variant['PM3_final'].fillna(0,inplace=True)
	variant['PM4_final'].fillna(0,inplace=True)
	variant['PM5_final'].fillna(0,inplace=True)
	variant['PM6_final'].fillna(0,inplace=True)
	variant['PP1_final'].fillna(0,inplace=True)
	variant['PP2_final'].fillna(0,inplace=True)
	variant['PP3_final'].fillna(0,inplace=True)
	variant['PP4_final'].fillna(0,inplace=True)
	variant['PP5_final'].fillna(0,inplace=True)
	variant['PMPOT_final'].fillna(0,inplace=True)
	variant['PPPOT_final'].fillna(0,inplace=True)

	benign_subscore = 'Uncertain Significance'
	pathogenic_subscore = 'Uncertain Significance'
	verdict = 'Uncertain Significance'
	punti_pat = ['PVS1', 'PS1', 'PS2', 'PS3', 'PS4', 'PM1', 'PM2', 'PM3', 'PM4', 'PM5', 'PM6', 'PP1', 'PP2', 'PP3', 'PP4', 'PP5', 'PMPOT', 'PPPOT']
	punti_ben = ['BA1', 'BS1', 'BS2', 'BS3', 'BS4', 'BP1', 'BP2', 'BP3', 'BP4', 'BP5', 'BP6', 'BP7']

	n_PM = 0
	n_PP = 0
	n_BP = 0
	pat_model = {}
	pat_mode1l = {}
	index = variant.index[0]

	for p in punti_pat:

		if (variant.loc[index, p+'_intensita_final'] == 'Moderate') & (variant.loc[index, p+'_final'] == 1) & ((p == 'PM1') | (p == 'PM3') | (p == 'PM4') | (p == 'PM5') | (p == 'PM6')):
			n_PM = n_PM + 1
		if (variant.loc[index, p+'_intensita_final'] == 'Supporting') & (variant.loc[index, p+'_final'] == 1) & ((p == 'PP1') | (p == 'PP2') | (p == 'PP4') | (p == 'PP5') | (p == 'PP6')):
			n_PP = n_PP + 1
		pat_model[p] = {'Presenza': variant.loc[index, p+'_final'], 'Intensita': variant.loc[index, p+'_intensita_final']}
	pat_model = pd.DataFrame.from_dict(pat_model, orient='index')

	if len(pat_model.loc[(pat_model['Intensita'].astype(str) != 'Very Strong') & (pat_model['Intensita'].astype(str) != 'Strong') & (pat_model['Intensita'].astype(str) != 'Moderate') & (pat_model['Intensita'].astype(str) != 'Supporting')]) != 0:
		benign_subscore = 'UNKNOWN'
		pathogenic_subscore = 'UNKNOWN'
		verdict = 'UNKNOWN'
	ben_model = {}
	for p in punti_ben:
		if (variant.loc[index, p+'_intensita_final'] == 'Supporting') &  (variant.loc[index, p+'_final'] == 1) & ((p == 'BP1') | (p == 'BP2') | (p == 'BP3') | (p == 'BP5') | (p == 'BP6') | (p == 'BP6')):
			n_BP = n_BP + 1
		#gestisco i diversi modi di scrivere Stand Alone
		if (p == 'BA1') & ((variant.loc[index, p+'_intensita_final'] == 'Stand-Alone') | (variant.loc[index, p+'_intensita_final'] == 'Stand Alone') | (variant.loc[index, p+'_intensita_final'] == 'Stand-alone') | (variant.loc[index, p+'_intensita_final'] == 'Stand alone')):
			ben_model[p] = {'Presenza': variant.loc[index, p+'_final'], 'Intensita': 'Stand Alone'}
		else:
			ben_model[p] = {'Presenza': variant.loc[index, p+'_final'], 'Intensita': variant.loc[index, p+'_intensita_final']}
	ben_model = pd.DataFrame.from_dict(ben_model, orient='index')

	if len(ben_model.loc[((ben_model['Intensita'].astype(str) != 'Stand Alone')) & (ben_model['Intensita'].astype(str) != 'Strong') & (ben_model['Intensita'].astype(str) != 'Moderate') & (ben_model['Intensita'].astype(str) != 'Supporting')]) != 0:
		benign_subscore = 'UNKNOWN'
		pathogenic_subscore = 'UNKNOWN'
		verdict = 'UNKNOWN'

	if (variant.loc[index,'PM2_final'] == 1) & (n_PM != 0) &(n_BP <=1):
		variant.loc[index,'PPPOT_final'] = 1
		pat_model.loc['PPPOT','Presenza'] = 1
		variant.loc[index,'PPPOT_intensita_final'] = 'Supporting'
		if str(variant.loc[index,'PPPOT_cause_final']) != 'nan':
			variant.loc[index,'PPPOT_cause_final'] = str(variant.loc[index,'PPPOT_cause_final']) + 'Verifica segregazione in ulteriori affetti (PP1) o compatibilità di fenotipo (PP4). '
			# pat_model.loc['PPPOT','Causa'] = pat_model.loc['PPPOT','Causa'] + 'Verifica segregazione in ulteriori affetti (PP1) o compatibilità di fenotipo (PP4). '
		else:
			variant.loc[index,'PPPOT_cause_final'] = 'Verifica segregazione in ulteriori affetti (PP1) o compatibilità di fenotipo (PP4). '
			# pat_model.loc['PPPOT','Causa'] = 'Verifica segregazione in ulteriori affetti (PP1) o compatibilità di fenotipo (PP4). '

	if (variant.loc[index,'PM2_final'] == 1) & (n_PP == 2) &(n_BP <=1):
		variant.loc[index,'PPPOT_final'] = 1
		variant.loc[index,'PPPOT_intensita_final'] = 'Supporting'
		if str(variant.loc[index,'PPPOT_cause_final']) != 'nan':
			variant.loc[index,'PPPOT_cause_final'] = variant.loc[index,'PPPOT_cause_final'] + 'Verifica segregazione in ulteriori affetti (PP1) o compatibilità di fenotipo (PP4). '
		else:
			variant.loc[index,'PPPOT_cause_final'] = 'Verifica segregazione in ulteriori affetti (PP1) o compatibilità di fenotipo (PP4). '

	if variant.loc[index,'var_on_gene']>=2:
		variant.loc[index,'PMPOT_final'] = 1
		variant.loc[index,'PMPOT_cause_final'] = ' Potenziale PM3 per eterozigosita\' composta'
	elif variant.loc[index,'zigosita']=='homo':
		variant.loc[index,'PMPOT_final'] = 1
		variant.loc[index,'PMPOT_intensita_final'] = 'Supporting'
		variant.loc[index,'PMPOT_cause_final'] = 'Potenziale PM3 supporting per variante in omozigosi'
	elif variant.loc[index,'allinheritance'] == 'AD':
		variant.loc[index,'PMPOT_final'] = 1
		variant.loc[index,'PMPOT_cause_final'] = 'Potenziale PM6 per variante de novo'
	if (variant.loc[index,'consequence'] == None):
		variant.loc[index,'consequence'] = ''
	if (variant.loc[index,'PM2_final'] == 1) & (variant.loc[index,'PP3_final'] == 1) & (('intron_variant' in variant.loc[index,'consequence']) | ('missense' in variant.loc[index,'consequence']) | ('synonimous' in variant.loc[index,'consequence'])) & (n_BP ==0):
		variant.loc[index,'PMPOT_final'] = 1
		variant.loc[index,'PMPOT_intensita_final'] = 'Moderate'
		if str(variant.loc[index,'PMPOT_cause_final']) != 'nan':
			variant.loc[index,'PMPOT_cause_final'] = variant.loc[index,'PMPOT_cause_final'] + 'Verifica stato allelico (PM3) o insorgenza de novo (PM6). '
		else:
			variant.loc[index,'PMPOT_cause_final'] = 'Verifica stato allelico (PM3) o insorgenza de novo (PM6). '

	present_pat = pat_model[pat_model['Presenza']==1]
	pat_intensities = present_pat['Intensita'].value_counts()
	if 'Very Strong' not in pat_intensities:
		pat_intensities['Very Strong'] = 0
	if 'Strong' not in pat_intensities:
		pat_intensities['Strong'] = 0
	if 'Moderate' not in pat_intensities:
		pat_intensities['Moderate'] = 0
	if 'Supporting' not in pat_intensities:
		pat_intensities['Supporting'] = 0

	present_ben = ben_model[ben_model['Presenza']==1]
	ben_intensities = present_ben['Intensita'].value_counts()
	if 'Stand Alone' not in ben_intensities:
		ben_intensities['Stand Alone'] = 0
	if 'Strong' not in ben_intensities:
		ben_intensities['Strong'] = 0
	if 'Supporting' not in ben_intensities:
		ben_intensities['Supporting'] = 0

	###############Algorithm Varsome Interpratation#############################
	if pathogenic_subscore != 'UNKNOWN':
		if (pat_intensities['Very Strong'] >= 2):
			pathogenic_subscore = 'Pathogenic'
		elif (pat_intensities['Very Strong'] >= 1):
			if pat_intensities['Strong'] >= 1:
				pathogenic_subscore = 'Pathogenic'
			elif pat_intensities['Moderate'] >= 2:
				pathogenic_subscore = 'Pathogenic'
			elif (pat_intensities['Moderate'] ==1) & (pat_intensities['Supporting'] >=1):
				pathogenic_subscore = 'Pathogenic'
			elif pat_intensities['Supporting'] >=2:
				pathogenic_subscore = 'Pathogenic'
			elif pat_intensities['Moderate'] ==1:
				pathogenic_subscore = 'Likely Pathogenic'

		elif ((pat_intensities['Strong'] >= 2) & (pat_intensities['Very Strong'] == 0)):
			pathogenic_subscore = 'Pathogenic'
			# print('HEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE')
		elif ((pat_intensities['Strong'] == 1) & (pat_intensities['Very Strong'] == 0)):
			if (pat_intensities['Moderate'] == 0) & (pat_intensities['Supporting'] >= 2): #modificato da >=1 supporting a >=2
				pathogenic_subscore = 'Likely Pathogenic'
			elif (((pat_intensities['Moderate'] == 1) | (pat_intensities['Moderate'] == 2)) & (pat_intensities['Supporting'] == 0)):
				pathogenic_subscore = 'Likely Pathogenic'
			elif (pat_intensities['Moderate'] == 1) & (pat_intensities['Supporting'] >= 4):#modificato da >=3 supporting a >=4
				pathogenic_subscore = 'Pathogenic'
			elif (pat_intensities['Moderate'] == 1) & (pat_intensities['Supporting'] < 4):#mod da <3 a <4
				pathogenic_subscore = 'Likely Pathogenic'
			elif (pat_intensities['Moderate'] == 2) & (pat_intensities['Supporting'] >= 2):
				pathogenic_subscore = 'Pathogenic'
			elif (pat_intensities['Moderate'] == 2) & (pat_intensities['Supporting'] < 2):
				pathogenic_subscore = 'Likely Pathogenic'
			elif pat_intensities['Moderate'] >= 3:
				pathogenic_subscore = 'Pathogenic'

		elif ((pat_intensities['Moderate'] == 1) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0)):
			if pat_intensities['Supporting'] >= 4: # 25022022 modificato per nuovo algoritmo #sul web 4
				pathogenic_subscore = 'Likely Pathogenic'
			elif pat_intensities['Supporting'] < 4: # 25022022 modificato per nuovo algoritmo #sul web 4
				pathogenic_subscore = 'Uncertain Significance'
		elif ((pat_intensities['Moderate'] >= 3) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0)):
			pathogenic_subscore =  'Likely Pathogenic'
			# print('OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO')
		elif ((pat_intensities['Moderate'] >= 2) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0)):
			if pat_intensities['Supporting'] == 1:
				pathogenic_subscore = 'Uncertain Significance'
			elif (pat_intensities['Supporting'] == 2):
				pathogenic_subscore =  'Likely Pathogenic'
			elif pat_intensities['Supporting'] >= 3:
				pathogenic_subscore =  'Likely Pathogenic'
	#############################################################################
	if (benign_subscore == 'Uncertain Significance') & (pathogenic_subscore != 'Uncertain Significance'):
		verdict = pathogenic_subscore
	elif (benign_subscore != 'Uncertain Significance'):# & (pathogenic_subscore == 'Uncertain Significance'): #questo serve per gestire quando ho 1 strong benigno e dei punti di patogenicità attivi
		if (ben_intensities['Strong'] == 1) & (ben_intensities['Stand Alone'] == 0) & (ben_intensities['Supporting'] == 0):
			if ((pat_intensities['Very Strong'] == 0) & (pat_intensities['Strong'] == 0) & (pat_intensities['Moderate'] ==0) & (pat_intensities['Supporting'] == 0)):
				verdict = benign_subscore
			else:
				verdict = pathogenic_subscore
		else:
			if (pathogenic_subscore == 'Uncertain Significance'):
				verdict = benign_subscore

    # Inseire Algoritmo per Intensità VUS
	if verdict == 'Uncertain Significance':
		vusstatus = 'Unkown'
		#if (pat_intensities['Very Strong'] == 1) & (pat_intensities['Strong'] == 0) & (pat_intensities['Supporting'] == 0) & (pat_intensities['Moderate'] == 0):
		if (pat_intensities['Very Strong'] == 1) & (pat_intensities['Strong'] == 0):
			if (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif ((ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1)) | ((ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0)) | ((ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2)):#  | @ |  @
				vusstatus = 'Middle'
			elif (ben_intensities['Supporting'] == 1) & (ben_intensities['Strong'] == 0):#@
				vusstatus = 'Hot'#@
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Hot'#@
			elif (pat_intensities['Moderate'] == 1) & (ben_intensities['Supporting'] == 1):#@
				vusstatus = 'Hot'#@
			elif (pat_intensities['Supporting'] == 1) & (ben_intensities['Supporting'] == 1):#@
				vusstatus = 'Middle'#@
			# elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2) & (pat_intensities['Strong'] == 0) & (pat_intensities['Supporting'] == 0) & (pat_intensities['Moderate'] == 0):
			# 	vusstatus = 'Middle'
		elif (pat_intensities['Strong'] == 1) & (pat_intensities['Very Strong'] == 0):
			if (ben_intensities['Strong'] == 2):#@@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1): #@@@@
				vusstatus = 'Middle'
			elif (ben_intensities['Supporting'] == 2): #@@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Supporting'] == 0):#@
				vusstatus = 'Hot'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Supporting'] == 0):#@
				vusstatus = 'Hot'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Supporting'] == 1):#@
				vusstatus = 'Hot'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Supporting'] == 1):#@
				vusstatus = 'Hot'
		elif (pat_intensities['Moderate'] == 1) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
			if (ben_intensities['Strong'] == 2):#@@@@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2) & (pat_intensities['Supporting'] == 1):#@
				vusstatus = 'Cold'#
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 1):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 2):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2) & (pat_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 1):#@
				vusstatus = 'Middle'#c
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2) & (pat_intensities['Supporting'] == 2):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 1):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 2):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 3):#@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2) & (pat_intensities['Supporting'] == 3):#@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 3):#@
				vusstatus = 'Hot' #m
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 3):#@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 2):#@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 2):#@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 1):#@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 3):#@
				vusstatus = 'Hot'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 3):#@
				vusstatus = 'Hot'
		elif (pat_intensities['Moderate'] == 2) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
			if (ben_intensities['Strong'] == 2):#@@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2) & (pat_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 0):#@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 0):#@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 1):#@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2) & (pat_intensities['Supporting'] == 1):#@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 1):#@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 1):#@
				vusstatus = 'Hot'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 1):#@
				vusstatus = 'Hot'
		elif (pat_intensities['Moderate'] == 3) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
			if (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2):
				vusstatus = 'Hot'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1):
				vusstatus = 'Hot'
			elif (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 0):
				vusstatus = 'Hot'
		elif (pat_intensities['Supporting'] == 4) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
			if (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1):#@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2):#@
				vusstatus = 'Cold'
		elif (pat_intensities['Supporting'] == 3) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
			if (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2):#@
				vusstatus = 'Cold'
		elif (pat_intensities['Supporting'] == 2) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
			if (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2):#@
				vusstatus = 'Cold'
		elif (pat_intensities['Supporting'] == 1) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
			if (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2):#@
				vusstatus = 'Cold'
		elif (pat_intensities['Supporting'] == 0) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
			if (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0):
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1):
				vusstatus = 'Cold'
		elif (pat_intensities['Supporting'] > 0) | (pat_intensities['Moderate'] > 0) | (pat_intensities['Strong'] > 0) | (pat_intensities['Very Strong'] > 0):
			if (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 3):
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 2):
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 1):
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 3) & (ben_intensities['Supporting'] == 0):
				vusstatus = 'Cold'
		#vusstatus = 'Hot' #cold, middle
	else:  vusstatus = 'Unkown'

	return verdict, vusstatus, variant.loc[index,'PMPOT_final'],variant.loc[index, 'PMPOT_intensita_final'],variant.loc[index,'PMPOT_cause_final'], variant.loc[index,'PPPOT_final'], variant.loc[index,'PPPOT_intensita_final'], variant.loc[index,'PPPOT_cause_final']
