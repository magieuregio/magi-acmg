# Generated by Django 4.0.2 on 2022-07-19 07:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('checklist', '0015_resultinterpretationrestore_and_more'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='historicalresultinterpretationrestore',
            options={'get_latest_by': 'history_date', 'ordering': ('-history_date', '-history_id'), 'verbose_name': 'historical Result Interpretation Restore'},
        ),
        migrations.AlterModelOptions(
            name='resultinterpretationrestore',
            options={'verbose_name': 'Result Interpretation Restore', 'verbose_name_plural': 'Result Interpretation Restore'},
        ),
        migrations.AddField(
            model_name='historicalresultinterpretation',
            name='cadd_score',
            field=models.FloatField(default=-999),
        ),
        migrations.AddField(
            model_name='resultinterpretation',
            name='cadd_score',
            field=models.FloatField(default=-999),
        ),
    ]
