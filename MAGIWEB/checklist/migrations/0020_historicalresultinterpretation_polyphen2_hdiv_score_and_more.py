# Generated by Django 4.0.2 on 2022-10-24 09:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('checklist', '0019_alter_historicalresultinterpretation_pppot_cause_final_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='historicalresultinterpretation',
            name='polyphen2_hdiv_score',
            field=models.FloatField(default=-999),
        ),
        migrations.AddField(
            model_name='historicalresultinterpretation',
            name='polyphen2_hvar_score',
            field=models.FloatField(default=-999),
        ),
        migrations.AddField(
            model_name='resultinterpretation',
            name='polyphen2_hdiv_score',
            field=models.FloatField(default=-999),
        ),
        migrations.AddField(
            model_name='resultinterpretation',
            name='polyphen2_hvar_score',
            field=models.FloatField(default=-999),
        ),
    ]
