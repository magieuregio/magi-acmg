# Generated by Django 4.0.2 on 2022-04-08 08:46

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import simple_history.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Sample',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sample', models.CharField(blank=True, max_length=50, null=True, unique=True)),
                ('timestamp_sample', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_sample', models.DateTimeField(auto_now=True, null=True)),
                ('riferimento', models.CharField(blank=True, max_length=100, null=True)),
                ('familiarita', models.CharField(blank=True, choices=[(None, '---'), ('PROBANDO', 'PROBANDO'), ('NONNOTO', 'NON NOTO'), ('MARITO', 'MARITO'), ('MOGLIE', 'MOGLIE'), ('PADRE', 'PADRE'), ('MADRE', 'MADRE'), ('FIGLIO', 'FIGLIO'), ('FIGLIO2', 'FIGLIO2'), ('FIGLIA', 'FIGLIa'), ('FIGLIA2', 'FIGLIA2'), ('FRATELLO', 'FRATELLO'), ('SORELLLA', 'SORELLA'), ('NONNO', 'NONNO'), ('NONNA', 'NONNA'), ('BISNONNO', 'BISNONNO'), ('BISNONNA', 'BISNONNA'), ('ZIO', 'ZIO'), ('ZIA', 'ZIA'), ('CUGINO', 'CUGINO'), ('CUGINA', 'CUGINA'), ('NIPOTE', 'NIPOTE'), ('MARITO', 'MARITO'), ('MOGLIE', 'MOGLIE'), ('COGNATO', 'COGNATO'), ('COGNATA', 'COGNATA'), ('PARTNER', 'PARTNER')], default='PROBANDO', max_length=50, null=True)),
                ('familiarita_relativa', models.CharField(blank=True, choices=[(None, '---'), ('PROBANDO', 'PROBANDO'), ('NONNOTO', 'NON NOTO'), ('MARITO', 'MARITO'), ('MOGLIE', 'MOGLIE'), ('PADRE', 'PADRE'), ('MADRE', 'MADRE'), ('FIGLIO', 'FIGLIO'), ('FIGLIO2', 'FIGLIO2'), ('FIGLIA', 'FIGLIa'), ('FIGLIA2', 'FIGLIA2'), ('FRATELLO', 'FRATELLO'), ('SORELLLA', 'SORELLA'), ('NONNO', 'NONNO'), ('NONNA', 'NONNA'), ('BISNONNO', 'BISNONNO'), ('BISNONNA', 'BISNONNA'), ('ZIO', 'ZIO'), ('ZIA', 'ZIA'), ('CUGINO', 'CUGINO'), ('CUGINA', 'CUGINA'), ('NIPOTE', 'NIPOTE'), ('MARITO', 'MARITO'), ('MOGLIE', 'MOGLIE'), ('COGNATO', 'COGNATO'), ('COGNATA', 'COGNATA'), ('PARTNER', 'PARTNER')], max_length=50, null=True, verbose_name="Familiarita' Relativa")),
                ('fenotipo', models.CharField(blank=True, max_length=50, null=True, verbose_name='Sospetto Diagnostico')),
                ('user_sample', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='user', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Campione',
                'verbose_name_plural': 'Campioni',
                'permissions': (('read_samplengs', 'Can read samplengs'),),
            },
        ),
        migrations.CreateModel(
            name='HistoricalSample',
            fields=[
                ('id', models.BigIntegerField(auto_created=True, blank=True, db_index=True, verbose_name='ID')),
                ('sample', models.CharField(blank=True, db_index=True, max_length=50, null=True)),
                ('timestamp_sample', models.DateTimeField(blank=True, editable=False, null=True)),
                ('updated_sample', models.DateTimeField(blank=True, editable=False, null=True)),
                ('riferimento', models.CharField(blank=True, max_length=100, null=True)),
                ('familiarita', models.CharField(blank=True, choices=[(None, '---'), ('PROBANDO', 'PROBANDO'), ('NONNOTO', 'NON NOTO'), ('MARITO', 'MARITO'), ('MOGLIE', 'MOGLIE'), ('PADRE', 'PADRE'), ('MADRE', 'MADRE'), ('FIGLIO', 'FIGLIO'), ('FIGLIO2', 'FIGLIO2'), ('FIGLIA', 'FIGLIa'), ('FIGLIA2', 'FIGLIA2'), ('FRATELLO', 'FRATELLO'), ('SORELLLA', 'SORELLA'), ('NONNO', 'NONNO'), ('NONNA', 'NONNA'), ('BISNONNO', 'BISNONNO'), ('BISNONNA', 'BISNONNA'), ('ZIO', 'ZIO'), ('ZIA', 'ZIA'), ('CUGINO', 'CUGINO'), ('CUGINA', 'CUGINA'), ('NIPOTE', 'NIPOTE'), ('MARITO', 'MARITO'), ('MOGLIE', 'MOGLIE'), ('COGNATO', 'COGNATO'), ('COGNATA', 'COGNATA'), ('PARTNER', 'PARTNER')], default='PROBANDO', max_length=50, null=True)),
                ('familiarita_relativa', models.CharField(blank=True, choices=[(None, '---'), ('PROBANDO', 'PROBANDO'), ('NONNOTO', 'NON NOTO'), ('MARITO', 'MARITO'), ('MOGLIE', 'MOGLIE'), ('PADRE', 'PADRE'), ('MADRE', 'MADRE'), ('FIGLIO', 'FIGLIO'), ('FIGLIO2', 'FIGLIO2'), ('FIGLIA', 'FIGLIa'), ('FIGLIA2', 'FIGLIA2'), ('FRATELLO', 'FRATELLO'), ('SORELLLA', 'SORELLA'), ('NONNO', 'NONNO'), ('NONNA', 'NONNA'), ('BISNONNO', 'BISNONNO'), ('BISNONNA', 'BISNONNA'), ('ZIO', 'ZIO'), ('ZIA', 'ZIA'), ('CUGINO', 'CUGINO'), ('CUGINA', 'CUGINA'), ('NIPOTE', 'NIPOTE'), ('MARITO', 'MARITO'), ('MOGLIE', 'MOGLIE'), ('COGNATO', 'COGNATO'), ('COGNATA', 'COGNATA'), ('PARTNER', 'PARTNER')], max_length=50, null=True, verbose_name="Familiarita' Relativa")),
                ('fenotipo', models.CharField(blank=True, max_length=50, null=True, verbose_name='Sospetto Diagnostico')),
                ('history_id', models.AutoField(primary_key=True, serialize=False)),
                ('history_date', models.DateTimeField()),
                ('history_change_reason', models.CharField(max_length=100, null=True)),
                ('history_type', models.CharField(choices=[('+', 'Created'), ('~', 'Changed'), ('-', 'Deleted')], max_length=1)),
                ('history_user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('user_sample', models.ForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='+', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'historical Campione',
                'ordering': ('-history_date', '-history_id'),
                'get_latest_by': 'history_date',
            },
            bases=(simple_history.models.HistoricalChanges, models.Model),
        ),
    ]
