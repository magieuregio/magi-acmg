"""    *** ****  ****  **** ****     18 FEBRUARY 2022    """ """ """  """ """ """

AUTHOR: ING. MUHARREM DAJA

CONTACT: REMIDAJA@OUTLOOK.COM || MUHARREM.DAJA@FTI.EDU.AL

""" """ """  """ """ """  """ """ """  """ """ """  """ """ """  """ """ """  """


from rest_framework import serializers
from acept.models import Sample
from django.contrib.auth.models import User

class SampleSerializer(serializers.ModelSerializer):

	user_sample = serializers.CharField(read_only=True)
	# group_user = serializers.CharField(read_only=True)
	genetista = serializers.CharField(read_only=True)
	timestamp_sample = serializers.DateTimeField(format="%d-%m-%Y")
	updated_sample = serializers.DateTimeField(format="%d-%m-%Y")

	class Meta:
		model = Sample
		fields = ('pk','sample_id', 'timestamp_sample', 'updated_sample', 'riferimento',
				'familiarita', 'familiarita_relativa', 'fenotipo', 'user_sample',
				'codice_pannello', 'group_user', 'genetista', 'id_interno')


class UserSerializer(serializers.ModelSerializer):

    password = serializers.CharField(write_only=True,style={'input_type': 'password'})

    class Meta:
        model = User
        fields = ('pk', 'username', 'password', 'last_login', 'is_superuser',
                    'first_name', 'last_name', 'email', 'is_staff', 'is_active',
                    'date_joined', 'groups')
