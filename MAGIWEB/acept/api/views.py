"""    *** ****  ****  **** ****     18 FEBRUARY 2022    """ """ """  """ """ """

AUTHOR: ING. MUHARREM DAJA

CONTACT: REMIDAJA@OUTLOOK.COM || MUHARREM.DAJA@FTI.EDU.AL

""" """ """  """ """ """  """ """ """  """ """ """  """ """ """  """ """ """  """




from rest_framework import viewsets,mixins,generics,permissions,renderers,authentication,status
from rest_framework.permissions import IsAuthenticated,AllowAny
from rest_framework.decorators import api_view
from rest_framework.decorators import authentication_classes, permission_classes
from rest_framework.viewsets import ModelViewSet,ReadOnlyModelViewSet
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.authentication import BaseAuthentication,SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework import filters
from rest_framework.decorators import action
from rest_framework.response import Response
from django.http import JsonResponse
from django.http import HttpResponseRedirect

from rest_framework.filters import SearchFilter
from rest_framework import filters
from django_filters.rest_framework import DjangoFilterBackend

import io
from rest_framework.parsers import JSONParser
from rest_framework.renderers import JSONRenderer
from rest_framework import serializers
from io import BytesIO

from rest_framework.viewsets import ModelViewSet,ReadOnlyModelViewSet

from django.shortcuts import get_object_or_404
from django.shortcuts import redirect

from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache

from acept.models import Sample
from acept.api.serializers import SampleSerializer
from django.contrib.auth.models import User
from acept.api.serializers import UserSerializer

from rest_framework.pagination import PageNumberPagination

import pandas as pd



# @api_view(['GET'])
# def samples(request):
# 	samples = Sample.objects.all()
# 	samples = samples.order_by('-timestamp_sample')
# 	paginator = PageNumberPagination()
# 	paginator.page_size = 5
# 	samples = paginator.paginate_queryset(samples, request)
# 	serializer = SampleSerializer(samples, many=True)
# 	return paginator.get_paginated_response(serializer.data)

@method_decorator(never_cache,name='dispatch')
class SampleViewSet(ModelViewSet):
	serializer_class = SampleSerializer
	# authentication_classes = (BasicAuthentication, SessionAuthentication,)
	# permission_classes = (IsAuthenticated,)
	filter_backends = (filters.SearchFilter, DjangoFilterBackend, filters.OrderingFilter,)
	search_fields = ('$sample_id', str('$group_user'), str('$genetista'),)
	filter_fields = ('sample_id', str('group_user'), str('genetista'),)

	@api_view(['GET'])
	def samples(request):
		samples = Sample.objects.all()
		samples = samples.order_by('-timestamp_sample')
		paginator = PageNumberPagination()
		paginator.page_size = 18
		samples = paginator.paginate_queryset(samples, request)
		serializer = SampleSerializer(samples, many=True)
		return paginator.get_paginated_response(serializer.data)

	def get_queryset(self):
		samples = Sample.objects.all()
		user = self.request.user
		samples = samples.order_by('-timestamp_sample')
		# paginator = PageNumberPagination()
		# paginator.page_size = 5
		# samples = paginator.paginate_queryset(samples, request)
		# serializer = SampleSerializer(samples, many=True)
		# return paginator.get_paginated_response(serializer.data)


		return samples

	def create(self, request, *args, **kwargs):
		serializer = self.get_serializer(data=request.data)
		serializer.is_valid(raise_exception=True)
		self.perform_create(serializer)
		headers = self.get_success_headers(serializer.data)
		return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

	def retrieve(self, request, *args, **kwargs):
		sample = self.get_object()
		serializer = self.get_serializer(sample)
		return Response(serializer.data)


	def destroy(self, request, *args, **kwargs):
		sample = self.get_object()
		self.perform_destroy(sample)
		return Response({"message": "It was deleted"}, status=status.HTTP_204_NO_CONTENT)

	def update(self, request, *args, **kwargs):
		partial = kwargs.pop('partial', False)
		samples = self.get_object()
		serializer = SampleSerializer(samples ,data=request.data, partial=partial)
		serializer.is_valid(raise_exception=True)
		self.perform_update(serializer)

		if getattr(samples, '_prefetched_objects_cache', None):
			# If 'prefetch_related' has been applied to a queryset, we need to
			# forcibly invalidate the prefetch cache on the instance.
			samples._prefetched_objects_cache = {}

		return Response(serializer.data)

class UserViewSet(ModelViewSet):

    serializer_class = UserSerializer
    # authentication_classes = (BasicAuthentication, SessionAuthentication,)
    # permission_classes = (IsAuthenticated,)
    filter_backends = (filters.SearchFilter, DjangoFilterBackend, filters.OrderingFilter,)
    search_fields = ('$first_name', '$last_name', '$groups', '$username',)
    filter_fields = ('first_name', 'last_name', 'groups', 'username',)

    def get_queryset(self):
        users = User.objects.all()
        return users

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def retrieve(self, request, *args, **kwargs):
        user = self.get_object()
        serializer = self.get_serializer(user)
        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        user = self.get_object()
        self.perform_destroy(user)
        return Response({"message": "It was deleted"}, status=status.HTTP_204_NO_CONTENT)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial',False)
        user = self.get_object()

        if 'runinterpretation' in request.headers['name']:

            serializer = self.get_serializer(user, data=request.data, partial=partial)
            serializer.is_valid(raise_exception=True)
            self.perform_update(serializer)

            if getattr(user, '_prefetched_objects_cache', None):
                # If 'prefetch_related' has been applied to a queryset, we need to
                # forcibly invalidate the prefetch cache on the instance.
                user._prefetched_objects_cache = {}

            return Response(serializer.data)
