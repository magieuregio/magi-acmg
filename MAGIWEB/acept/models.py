"""    *** ****  ****  **** ****     18 FEBRUARY 2022    """ """ """  """ """ """

AUTHOR: ING. MUHARREM DAJA

CONTACT: REMIDAJA@OUTLOOK.COM || MUHARREM.DAJA@FTI.EDU.AL

""" """ """  """ """ """  """ """ """  """ """ """  """ """ """  """ """ """  """
from django.db import models

# Create your models here.
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from simple_history.models import HistoricalRecords

CHOICES_FAMILIARI = (
    (None, '---'),
    ('PROBANDO', 'PROBANDO'),
    ('NONNOTO', 'NON NOTO'),
    ('MARITO', 'MARITO'),
    ('MOGLIE', 'MOGLIE'),
    ('PADRE', 'PADRE'),
    ('MADRE', 'MADRE'),
    ('FIGLIO', 'FIGLIO'),
    ('FIGLIO2', 'FIGLIO2'),
    ('FIGLIA', 'FIGLIa'),
    ('FIGLIA2', 'FIGLIA2'),
    ('FRATELLO', 'FRATELLO'),
    ('SORELLLA', 'SORELLA'),
    ('NONNO', 'NONNO'),
    ('NONNA', 'NONNA'),
    ('BISNONNO', 'BISNONNO'),
    ('BISNONNA', 'BISNONNA'),
    ('ZIO', 'ZIO'),
    ('ZIA', 'ZIA'),
    ('CUGINO', 'CUGINO'),
    ('CUGINA', 'CUGINA'),
    ('NIPOTE', 'NIPOTE'),
    ('MARITO', 'MARITO'),
    ('MOGLIE', 'MOGLIE'),
    ('COGNATO', 'COGNATO'),
    ('COGNATA', 'COGNATA'),
    ('PARTNER', 'PARTNER'),
)

class Sample(models.Model):
    id_interno = models.CharField(max_length=25, null=True, blank=True, unique=True)
    sample_id = models.CharField(max_length=50, null=True, blank=True)
    timestamp_sample = models.DateTimeField(auto_now_add=True, auto_now=False, null=True, blank=True)
    updated_sample = models.DateTimeField(auto_now_add=False, auto_now=True, null=True, blank=True)
    codice_pannello = models.CharField(max_length=50, null=True, blank=True)
    riferimento = models.CharField(max_length=100, null=True, blank=True)
    familiarita = models.CharField(max_length=50, choices=CHOICES_FAMILIARI,
                                    default='PROBANDO', null=True, blank=True)
    familiarita_relativa = models.CharField(max_length=50, null=True, blank=True,
                                                choices=CHOICES_FAMILIARI, verbose_name="Familiarita\' Relativa")
    fenotipo = models.CharField(max_length=50, null=True, blank=True, verbose_name='Sospetto Diagnostico')
    group_user = models.ForeignKey(Group, on_delete=models.CASCADE, related_name='group_user', null=True, blank=True)
    user_sample = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user', null=True, blank=True)
    genetista = models.ForeignKey(User, on_delete=models.CASCADE,related_name='genetista',
                                    # limit_choices_to={'groups__name': "GENETISTA", 'groups__name': "CASASOLLIEVO", 'groups__name': "MAGIEUREGIO"},
                                    # limit_choices_to={'groups__name': "CASASOLLIEVO"},
                                    null=True, blank=True)

    history = HistoricalRecords()

    class Meta:
        permissions = (
            ( "read_samplengs", "Can read samplengs"),
        )
        verbose_name = 'Campione'
        verbose_name_plural = 'Campioni'

    def __unicode__(self):
        return ('%s' - '%s' - '%s' - '%s') % (self.sample_id, self.codice_pannello, self.group_user)

    def __str__(self):
        return self.id_interno + ' - ' +  self.sample_id + ' - ' + self.codice_pannello + ' - ' + str(self.group_user)
