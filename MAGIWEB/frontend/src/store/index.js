// """    *** ****  ****  **** ****     18 FEBRUARY 2022    """ """ """  """ """ """
//
// AUTHOR: ING. MUHARREM DAJA
//
// CONTACT: REMIDAJA@OUTLOOK.COM || MUHARREM.DAJA@FTI.EDU.AL
//
// """ """ """  """ """ """  """ """ """  """ """ """  """ """ """  """ """ """  """
import { createStore } from 'vuex'

export default createStore({
  state: {
    user: {
      token: '',
      isAuthenticated: false
    },
    nameuser: '',
    namegroup: ''
  },
  getters: {
    getName(state) {
      return state.nameuser;
    },
    getNameGroup(state) {
      return state.namegroup
    },
    getToken(state) {
      return state.user.token;
    }
  },
  mutations: {
    initializeStore(state) {
      if (localStorage.getItem('token')) {
        state.user.token = localStorage.getItem('token')
        state.user.isAuthenticated = true
      }
      else {
        state.user.token = ''
        state.user.isAuthenticated = false
      }
    },
    setToken(state, token) {
      state.user.token = token
      state.user.isAuthenticated = true
    },
    removeToken(state) {
      state.user.token = ''
      state.user.isAuthenticated = false
    },
    setName(state, payload) {
      state.nameuser = payload;
    },
    setNameGroup(state, payload) {
      state.namegroup = payload
    }
  },
  actions: {
    setName({commit}, payload) {
      commit('setName', payload);
    },
    setNameGroup({commit}, payload) {
      commit('setNameGroup', payload)
    }
  },
  modules: {
  }
})
