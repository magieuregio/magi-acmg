// """    *** ****  ****  **** ****     18 FEBRUARY 2022    """ """ """  """ """ """
//
// AUTHOR: ING. MUHARREM DAJA
//
// CONTACT: REMIDAJA@OUTLOOK.COM || MUHARREM.DAJA@FTI.EDU.AL
//
// """ """ """  """ """ """  """ """ """  """ """ """  """ """ """  """ """ """  """
const BundleTracker = require("webpack-bundle-tracker");

module.exports = {
  publicPath: "http://62.173.183.235:8805/",
  outputDir: './dist/',

  chainWebpack: config => {

    config.optimization
      .splitChunks(false)

    config
      .plugin('BundleTracker')
      .use(BundleTracker, [{filename: '../frontend/webpack-stats.json'}])

    config.resolve.alias
      .set('__STATIC__', 'static')

    config.devServer
      .public('http://62.173.183.235:8805')
      .host('62.173.183.235')
      .port(8805)
      .hotOnly(true)
      .watchOptions({poll: 1000})
      .https(false)
      .headers({"Access-Control-Allow-Origin": ["\*"]})
  }
};
