"""MAGIWEB URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from django.shortcuts import render

from django.views.generic import TemplateView

from django.conf.urls.static import static
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from MAGIWEB.settings import MEDIA_URL, IMG_URL#, OTHERS_URL

from rest_framework.routers import DefaultRouter

from acept.api.views import SampleViewSet
from acept.api.views import UserViewSet
from checklist.api.views import ResultInterpretationViewSet
from sanger.api.views import AllVariationViewSet
from ngs.api.views import PreSelectionViewSet
from acept.api import views

router = DefaultRouter()

router.register(r"sample", SampleViewSet, basename='Sample')
router.register(r"users", UserViewSet, basename='Users')
router.register(r"resultinterpretation", ResultInterpretationViewSet, basename='ResultInterpretation')
router.register(r"allvariation", AllVariationViewSet, basename='AllVariation')
router.register(r"preselection", PreSelectionViewSet, basename='PreSelection')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', TemplateView.as_view(template_name='base1.html'), name='app'),
    path('http://62.173.183.235:8805/', TemplateView.as_view(template_name='base1.html'), name='app'),
    path(r'api/', include(router.urls)),
    path(r'api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('apis/', include('api.urls')),
    path('apilog/v1/', include('djoser.urls')),
    path('apilog/v1/', include('djoser.urls.authtoken')),
    path('get_samples/', views.SampleViewSet.samples, name='samples'),
    # path('http://192.168.1.48:8800/http://192.168.1.48:8083/dashboard/yes', admin.site.urls)
]



urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.STATIC_URL, document_root = settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)
urlpatterns += static(settings.ASSETS_URL, document_root =settings.ASSETS_ROOT)
urlpatterns += static(settings.IMG_URL, document_root =settings.IMG_ROOT)
urlpatterns += static(settings.MYDATA_URL, document_root =settings.MYDATA_ROOT)
# urlpatterns += static(settings.OTHERS_URL, document_root =settings.OTHERS_ROOT)
