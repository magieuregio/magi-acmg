"""
WSGI config for MAGIWEB project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/4.0/howto/deployment/wsgi/
"""

import os, sys, site

from django.core.wsgi import get_wsgi_application

site.addsitedir('/home/bioinfo/VIRTUAL38/lib/python3.8/site-packages')
sys.path.append('/home/bioinfo/VIRTUAL38/')
sys.path.append('/home/bioinfo/VIRTUAL38/MAGIWEB/MAGIWEB')

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'MAGIWEB.settings')

application = get_wsgi_application()
