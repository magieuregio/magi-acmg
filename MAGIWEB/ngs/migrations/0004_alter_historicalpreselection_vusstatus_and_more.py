# Generated by Django 4.0.2 on 2022-04-27 10:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ngs', '0003_historicalpreselection_annotazione_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='historicalpreselection',
            name='vusstatus',
            field=models.CharField(blank=True, choices=[('NON NOTO', 'NON NOTO'), ('CALDA', 'CALDA'), ('TIEPIDA', 'TIEPIDA'), ('FREDDA', 'FREDDA')], default='SCONOSCIUTO', max_length=20, null=True),
        ),
        migrations.AlterField(
            model_name='preselection',
            name='vusstatus',
            field=models.CharField(blank=True, choices=[('NON NOTO', 'NON NOTO'), ('CALDA', 'CALDA'), ('TIEPIDA', 'TIEPIDA'), ('FREDDA', 'FREDDA')], default='SCONOSCIUTO', max_length=20, null=True),
        ),
    ]
