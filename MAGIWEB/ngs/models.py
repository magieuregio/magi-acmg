"""    *** ****  ****  **** ****     18 FEBRUARY 2022    """ """ """  """ """ """

AUTHOR: ING. MUHARREM DAJA

CONTACT: REMIDAJA@OUTLOOK.COM || MUHARREM.DAJA@FTI.EDU.AL

""" """ """  """ """ """  """ """ """  """ """ """  """ """ """  """ """ """  """
from django.db import models

# Create your models here.


from simple_history.models import HistoricalRecords
from django.contrib.auth.models import User

# Create your models here.

CHOICES_SEX = (
	('M','M'),
	('F','F'),
)


CHOICES_RILEVANZASOMATICA = (
	('Tier I','Tier I'),
	('Tier II', 'Tier II'),
	('Tier III','Tier III'),
	('Tier IV','Tier IV'),
)

Lavorazione = 'In Lavorazione'
PCR_eluizione_sequenze = 'PCR_eluizione_sequenze'
concluso = 'Concluso'
NOSANGER = 'NOSANGER'

CHOICES_LAVORAZIONE = (
                (Lavorazione, 'In Lavorazione'),
                (PCR_eluizione_sequenze, 'PCR_eluizione_sequenze'),
                (concluso, 'Concluso'),
                (NOSANGER, 'NOSANGER'),
                ('ESCLUSO', 'ESCLUSO'),
)

CHOICES_RISULTATO = (
                ('Primary', 'Primary'),
                ('Secondary', 'Secondary'),
)

CHOICES_VUSSTATUS = (
        ('NON NOTO', 'NON NOTO'),
        ('CALDA', 'CALDA'),
        ('TIEPIDA', 'TIEPIDA'),
        ('FREDDA', 'FREDDA'),
)

VS = 'Very Strong'
S = 'Strong'
SP = 'Supporting'
M = 'Moderate'
UNK = 'UNKNOWN'

CHOICES_INTENSITA_PATHOGENETIC = (
    (VS, 'Very Strong'),
    (S, 'Strong'),
    (M, 'Moderate'),
    (SP, 'Supporting')
)

CHOICES_INTENSITA_AUTOPVS1 = (
    (VS, 'Very Strong'),
    (S, 'Strong'),
    (M, 'Moderate'),
    (SP, 'Supporting'),
    (UNK, 'UNKNOWN')
)

class PreSelection(models.Model):
    select = models.BooleanField(default=False)
    timestamp_selection = models.DateTimeField(auto_now_add=True, auto_now=False,
                                                null=True, blank=True)
    updated_selection = models.DateTimeField(auto_now_add=False, auto_now=True,
                                                null=True, blank=True)
    sample_id = models.CharField(max_length=50, null=True, blank=True)
    id_interno = models.CharField(max_length=25, null=True, blank=True)
    hgvs = models.CharField(max_length=115, null=True, blank=True)
    annotazione = models.CharField(max_length=50, null=True, blank=True)
    chrom = models.CharField(max_length=100, null=True, blank=True)
    POS = models.IntegerField(default=0)
    rsID = models.CharField(max_length=100, null=True, blank=True)
    GENE = models.CharField(max_length=100, null=True, blank=True)
    consequence = models.CharField(max_length=100, null=True, blank=True)
    hgvs_c = models.CharField(max_length=100, null=True, blank=True)
    hgvs_p = models.CharField(max_length=100,null=True,blank=True)
    unbalance = models.CharField(max_length=25, null=True, blank=True)
    inheritance = models.CharField(max_length=100, null=True, blank=True)
    allphenotype = models.CharField(max_length=770, null=True, blank=True)
    allinheritance = models.CharField(max_length=400, null=True, blank=True)
    definitivemaf = models.FloatField(default=-999)
    definitivemaf2 = models.FloatField(default=-999)
    decisionmaf = models.CharField(max_length=110, null=True, blank=True)
    decisionINFO = models.CharField(max_length=20, null=True, blank=True)
    pop_allele_count = models.IntegerField(default=0, blank=True, null=True)
    mafmin20 = models.IntegerField(default=1)
    comments = models.CharField(max_length=250, null=True, blank=True)
    descriptions = models.CharField(max_length=250, null=True, blank=True)
    MAF_selection_AD = models.IntegerField(default=0)
    MAF_selection_AR = models.IntegerField(default=0)
    MAF_selection = models.IntegerField(default=0)
    predictors_decision = models.CharField(max_length=30, null=True, blank=True)
    predictors_B = models.CharField(max_length=30, null=True, blank=True)
    predictors_D = models.CharField(max_length=30, null=True, blank=True)
    predictors_introns = models.CharField(max_length=30, null=True, blank=True)
    controls_nhomalt = models.IntegerField(default=0)
    prevalence = models.FloatField(default=-999)
    ada_score = models.FloatField(default=-999)
    rf_score = models.FloatField(default=-999)
    acmg_verdict = models.CharField(max_length=30, null=True, blank=True)
    acmg_classifications = models.TextField(null=True, blank=True)
    acmg_classifications_varsome = models.TextField(null=True, blank=True)
    acmg_classifications_final = models.TextField(null=True, blank=True)
    publications = models.TextField(null=True, blank=True)
    clinvar2 = models.TextField(null=True, blank=True)
    saphetor = models.TextField(null=True, blank=True)
    mis_z = models.CharField(max_length=30, null=True, blank=True)
    lof_z = models.FloatField(default=-999)
    num_mis = models.IntegerField(default=0)
    perc_misB = models.FloatField(default=-999)
    perc_totMIS = models.FloatField(default=-999)
    gene_maf = models.FloatField(default=-999)
    fromFILE = models.IntegerField(default=0)
    BA1_points = models.IntegerField(default=0)
    BS1_prev_points = models.IntegerField(default=0)
    BS1_maf_points = models.IntegerField(default=0)
    benign2_points = models.IntegerField(default=0)
    benign2AD_points = models.IntegerField(default=0)
    BP1_points = models.IntegerField(default=0)
    benign1_points = models.IntegerField(default=0)
    notbenign_points = models.IntegerField(default=0)
    benign2int_points = models.IntegerField(default=0)
    notbenignint_points = models.IntegerField(default=0)
    fromACMG_points = models.IntegerField(default=0)
    checklist_points_sum = models.IntegerField(default=0)
    apiversion = models.CharField(max_length=30, null=True, blank=True)
    cadd_score = models.FloatField(default=-999)
    sift_score = models.FloatField(default=-999)
    polyphen2_hdiv_score = models.FloatField(default=-999)
    polyphen2_hvar_score = models.FloatField(default=-999)
    amp_verdict = models.CharField(max_length=50,choices=CHOICES_RILEVANZASOMATICA,null=True,blank=True)
    amp_classifications = models.TextField(null=True,blank=True)
    drug_verdict = models.CharField(max_length=50,choices=CHOICES_RILEVANZASOMATICA,null=True,blank=True)
    drug_classifications = models.TextField(null=True,blank=True)
    revel_score = models.FloatField(default=-999)
    autopvs1 = autopvs1 = models.CharField(max_length=25, choices=CHOICES_INTENSITA_AUTOPVS1, default = VS,
													null=True, blank=True)
    PVS1_old = models.CharField(max_length=50, choices=CHOICES_INTENSITA_PATHOGENETIC,
													default=M, null=True, blank=True)
    spliceAI = models.FloatField(default=-999)
    vusstatus = models.CharField(max_length=20, choices=CHOICES_VUSSTATUS,
                                    default='SCONOSCIUTO', null=True, blank=True)
    PMPOT_final = models.BooleanField(default=False, null=True)
    PMPOT_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_PATHOGENETIC,
                                                            default=M, null=True, blank=True)
    PMPOT_cause_final = models.CharField(max_length=500, null=True, blank=True, default='')
    PPPOT_final = models.BooleanField(default=False, null=True)
    PPPOT_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_PATHOGENETIC,
                                                default=SP, null=True, blank=True)
    PPPOT_cause_final = models.CharField(max_length=500, null=True, blank=True, default='')
    PP3_final = models.BooleanField(default=False, null=True)
    PP3_cause_final = models.CharField(max_length=500, null=True, blank=True, default='')
    BP4_final = models.BooleanField(default=False, null=True)
    BP4_cause_final = models.CharField(max_length=500, null=True, blank=True, default='')
    PVS1_final = models.BooleanField(default=False, null=True)
    PVS1_cause_final = models.CharField(max_length=500, null=True, blank=True, default='')
    STATO = models.CharField(max_length=30, choices=CHOICES_LAVORAZIONE, default='In Lavorazione', null=True, blank=True)
    importanza = models.CharField(max_length=50, choices=CHOICES_RISULTATO, null=True, blank=True)
    PVS1_intensita_final = models.CharField(max_length=50, choices=CHOICES_INTENSITA_PATHOGENETIC,
												null=True, blank=True)
    zigosita = models.CharField(max_length=10, null=True, blank=True)
    var_on_gene = models.IntegerField(default=0)
    acmg_verdict_varsome = models.CharField(max_length=30,null=True,blank=True)


    history = HistoricalRecords()

    class Meta:
        unique_together = ('id_interno', 'hgvs')

        verbose_name = 'Pre Selection NGS'
        verbose_name_plural = 'Pre Selection NGS'

    def __unicode__(self):
        return '%s (%s - %s -%s)' % (self.sample_id, self.hgvs, self.GENE, self.acmg_verdict)

    def __str__(self):
        return self.id_interno+ '- ('+self.hgvs+')'
