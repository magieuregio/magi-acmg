from django.apps import AppConfig


class NgsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ngs'
