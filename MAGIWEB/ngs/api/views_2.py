"""    *** ****  ****  **** ****     18 FEBRUARY 2022    """ """ """  """ """ """

AUTHOR: ING. MUHARREM DAJA

CONTACT: REMIDAJA@OUTLOOK.COM || MUHARREM.DAJA@FTI.EDU.AL

""" """ """  """ """ """  """ """ """  """ """ """  """ """ """  """ """ """  """
from rest_framework import mixins
from rest_framework import generics
# from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework import viewsets

from ngs.models import PreSelection
from ngs.api.serializers import PreSelectionSerializer

import pandas as pd
import numpy as np
import re

from django.shortcuts import render
from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework import permissions, renderers, authentication, status
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.decorators import api_view
from rest_framework.decorators import authentication_classes, permission_classes
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet, ReadOnlyModelViewSet
from rest_framework.authentication import BaseAuthentication, SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework import filters
from django_filters.rest_framework import DjangoFilterBackend

from ngs.api.permissions import IsOwnerOrReadOnly
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from rest_framework.authtoken.models import Token

from acept.models import Sample

from ngs.api.variant_varsome_info import reinterpretation_verdict, query_varsome_saphetor
from ngs.api.variant_varsome_info import query_varsome_gene, varsome_consequences
from ngs.api.variant_varsome_info import set_var_PMPOT
from ngs.api.load import load_preselection, load_checklist, load_acept_sanger#, load_sanger

import datetime

cols1 = ['sample','GENE','annotation','ada_score','rf_score','HGVS_c', 'allinheritance']
# cols1 = ['sample','GENE','HGVS','annotation','ada_score','rf_score','HGVS_c', 'allinheritance']
# cols1 = ['sample','GENE','annotation','ada_score','rf_score','HGVS_c']

class PreSelectionViewSet(ModelViewSet):

    serializer_class = PreSelectionSerializer
    authentication_classes = (TokenAuthentication, SessionAuthentication,)
    permission_classes = (IsAuthenticated,)
    filter_backends = (filters.SearchFilter, DjangoFilterBackend, filters.OrderingFilter,)
    search_fields = ('$sample_id', '$hgvs',)
    filter_fields = ('sample_id', 'hgvs',)

    def calculate_id_interno(self):
        last_id = Sample.objects.all().order_by("-pk")[0]
        _num_ = last_id.id_interno
        num = re.split('[A-Z]+', _num_, flags=re.IGNORECASE)[-1]
        YEAR = num.split('.')[-1]
        num = num.split('.')[0]

        if int(datetime.datetime.now().year) != int(YEAR):
            num_ = 1
        else:
            try:
                num = int(num)
                num_ = num + 1
            except ValueError:
                num = int(num)
                num_ = num + 1

        ID_ = 'M'+str(num_)+'.'+str(datetime.datetime.now().year)
        return ID_

    def get_queryset(self):
        preselections = PreSelection.objects.all()
        return preselections

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers = headers)

    def retrieve(self, request, *args, **kwargs):
        preselection = self.get_object()
        serializer = self.get_serializer(preselection)
        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        preselection = self.get_object()
        self.perform_destroy(preselection)
        return Response({"message": "It was deleted"}, status=status.HTTP_204_NO_CONTENT)

    def update(self, request, *args, **kwargs):
        lista_input = []
        flag = False
        lista_samples = []
        lista_pannelli = []
        lista_annotazioni = []
        lista_zigosita = []
        partial = kwargs.pop('partial', False)
        preselection = self.get_object()

        if request.data['acmg_classifications'] != '':
            utente = request.data['acmg_verdict'].split('###')[0]
            group_utente = request.data['acmg_verdict'].split('###')[1]
            id_utente = User.objects.get(username=utente).pk

            input = request.data['acmg_classifications']
            for el in input.split('\n'):
                lista_input.append(el)

            for el in lista_input:
                lista_samples.append(el.split('\t')[0])
                lista_pannelli.append(el.split('\t')[1])
                lista_annotazioni.append(el.split('\t')[2])
            for el in lista_annotazioni:
                if((len(el.split(':')[-1]) != 0) and 'c.' not in el.split(':')[-1]):
                    lista_zigosita.append(el.split(':')[-1])
                else:
                    a = 'het'
                    lista_zigosita.append(a)
            variants = pd.DataFrame(lista_samples, columns=['sample'])
            variants['codice_pannello'] = pd.DataFrame(lista_pannelli)
            variants['annotation'] = pd.DataFrame(lista_annotazioni)
            variants['zigosita'] = pd.DataFrame(lista_zigosita)

            SAMPLE = pd.DataFrame(columns=cols1)

            for index, row in variants.iterrows():
            	sample_x = variants.loc[index, 'sample']
            	gene = variants.loc[index,'annotation'].split(':')[0]
            	SAMPLE.loc[index, 'sample'] =  variants.loc[index,'sample']
            	# SAMPLE.loc[index, 'HGVS'] =  variants.loc[index,'HGVS']
            	# SAMPLE.loc[index, 'CHROM'] =  variants.loc[index,'HGVS'].split(':')[0]
            	# SAMPLE.loc[index, 'POS'] =  variants.loc[index,'HGVS'].split(':')[1].split('-')[0]
            	SAMPLE.loc[index, 'GENE'] =  variants.loc[index,'annotation'].split(':')[0]
            	SAMPLE.loc[index, 'annotation'] =  variants.loc[index,'annotation']
            	SAMPLE.loc[index, 'amp_verdict'] = None
            	SAMPLE.loc[index, 'amp_classifications'] = None
            	SAMPLE.loc[index, 'drug_verdict'] = None
            	SAMPLE.loc[index, 'drug_classifications'] = None
            	SAMPLE.loc[index, 'zigosita'] = variants.loc[index, 'zigosita']
            	SAMPLE.loc[index, 'codice_pannello'] = variants.loc[index, 'codice_pannello']

            SAMPLE['sample'] = SAMPLE['sample'].astype('str')
            SAMPLE['sample'].replace(r'\.202$','.2020',inplace=True,regex=True)
            SAMPLE['sample'] = SAMPLE['sample'].astype('str')

            for index,row in SAMPLE.iterrows():
            	agene = SAMPLE.loc[index,'annotation'].split(':')[0]
            	if ('c.' not in SAMPLE.loc[index,'annotation'].split(':')[1]):
                    rawinput = ':'.join([SAMPLE.loc[index,'annotation'].split(':')[1], SAMPLE.loc[index,'annotation'].split(':')[2]])
            	else:
                    rawinput = ':'.join([SAMPLE.loc[index,'annotation'].split(':')[0], SAMPLE.loc[index,'annotation'].split(':')[1]])
            	try:
                    rawinput2 = ':'.join([SAMPLE.loc[index,'annotation'].split(':')[0], SAMPLE.loc[index,'annotation'].split(':')[2]])
            	except:
                    rawinput2 = ':'.join([SAMPLE.loc[index,'annotation'].split(':')[0], SAMPLE.loc[index,'annotation'].split(':')[1]])

            	saphetor = query_varsome_saphetor(rawinput, rawinput2 ,SAMPLE,index)
            	saphetor = query_varsome_gene(str(row.GENE),saphetor,index)
            	saphetor = varsome_consequences(str(row.GENE), saphetor, index)

            	saphetor.loc[:, 'genetista_name'] = utente
            	saphetor.loc[:, 'genetista_id'] = id_utente
            	saphetor.loc[:, 'group_id'] = group_utente

            	# try:
                #     if (str(group_utente) == str(Sample.objects.get(sample_id = row['sample'], codice_pannello = row['codice_pannello']).group_user_id)):
                #         print('yessssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss')
                #         id_interno = Sample.objects.get(sample_id = row['sample'], codice_pannello = row['codice_pannello'], genetista_id = id_utente).id_interno
                #         saphetor.at[index, 'id_interno'] = id_interno
            	# except:
                #     print('nooooooooooooooooooooooooooooooooooooooooooooooooooooo')
                #     if (flag == False):
                #         id_interno = self.calculate_id_interno()
                #         print('id interno isssssssssssss', id_interno)
                #         saphetor.at[index, 'id_interno'] = id_interno
                #         flag = True
                #     else:
                #         i = 1
                #         a = id_interno.split('.')[0]
                #         c = id_interno.split('.')[1]
                #         b = int(a.split('M')[1]) + i
                #         i = i + 1
                #         id_interno = 'M' + str(b) + '.' + c
                #         # print('yes', id_interno)
                #         saphetor.at[index, 'id_interno'] = id_interno

            SAMPLE_go = saphetor
            SAMPLE_go = set_var_PMPOT(SAMPLE_go)
            one = SAMPLE_go[SAMPLE_go['var_on_gene'] != 1]
            second = one.drop_duplicates(subset=["sample", "var_on_gene"], keep=False)
            list_of_single_column = second['sample'].tolist()
            for el in list_of_single_column:
                for index, row in SAMPLE_go.iterrows():
                    if (el == row['sample']):
                        SAMPLE_go.loc[index, 'var_on_gene'] = 1

            result = reinterpretation_verdict(SAMPLE_go)


            result_var = result[0]
            # result_var.at[0, 'id_interno'] = 'M3.2022'
            # result_var.at[1, 'id_interno'] = 'M3.2022'
            # result_var.at[2, 'id_interno'] = 'M4.2022'

            # result_var.loc[:, 'done'] = 'NO'
            # print('STARTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTt')
            # for index, row in result_var.iterrows():
            #     print('rowwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww', row['done'])
            #     if (row['done'] == 'NO'):
            #         try:
            #             if (str(group_utente) == str(Sample.objects.get(sample_id = row['sample'], codice_pannello = row['codice_pannello']).group_user_id)):
            #                 print('yessssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss')
            #                 id_interno = Sample.objects.get(sample_id = row['sample'], codice_pannello = row['codice_pannello'], genetista_id = id_utente).id_interno
            #                 result_var.at[index, 'id_interno'] = id_interno
            #                 conditions = [result_var['sample'].eq(row['sample']) & result_var['codice_pannello'].eq(row['codice_pannello'])]
            #                 choices = [id_interno]
            #                 choices_ = ['YES']
            #                 result_var['id_interno'] = np.select(conditions, choices, default=0)
            #                 result_var['done'] = np.select(conditions, choices_, default=0)
            #         except:
            #                 print('nooooooooooooooooooooooooooooooooooooooooooooooooooooo')
            #                 if (flag == False):
            #                     id_interno = self.calculate_id_interno()
            #                     print('id interno isssssssssssss', id_interno)
            #                     result_var.at[index, 'id_interno'] = id_interno
            #                     result_var.at[index, 'done'] = 'YES'
            #                     conditions = [result_var['sample'].eq(row['sample']) & result_var['codice_pannello'].eq(row['codice_pannello'])]
            #                     choices = [id_interno]
            #                     choices_ = ['YES']
            #                     result_var['id_interno'] = np.select(conditions, choices, default=0)
            #                     result_var['done'] = np.select(conditions, choices_, default=0)
            #                     # print(result_var[(result_var["sample"]==row['sample']) & (result_var["codice_pannello"]==row['codice_pannello'])])
            #                     # print(result_var[(result_var["sample"]==row['sample']) & (result_var["codice_pannello"]==row['codice_pannello'])]['id_interno'])
            #                     # print(result_var[(result_var["sample"]==row['sample']) & (result_var["codice_pannello"]==row['codice_pannello'])]['done'])
            #                     # result_var[(result_var["sample"]==row['sample']) & (result_var["codice_pannello"]==row['codice_pannello'])].loc[:, 'id_interno'] = id_interno
            #                     # result_var[(result_var["sample"]==row['sample']) & (result_var["codice_pannello"]==row['codice_pannello'])].loc[:, 'done'] = 'YES'
            #                     print('ADTEREEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE')
            #                     print(result_var[(result_var["sample"]==row['sample']) & (result_var["codice_pannello"]==row['codice_pannello'])])
            #                     print(result_var[(result_var["sample"]==row['sample']) & (result_var["codice_pannello"]==row['codice_pannello'])]['id_interno'])
            #                     print(result_var[(result_var["sample"]==row['sample']) & (result_var["codice_pannello"]==row['codice_pannello'])]['done'])
            #                     flag = True
            #                 else:
            #                     i = 1
            #                     a = id_interno.split('.')[0]
            #                     c = id_interno.split('.')[1]
            #                     b = int(a.split('M')[1]) + i
            #                     i = i + 1
            #                     id_interno = 'M' + str(b) + '.' + c
            #                     print('seconddddddddddddddddddddddddddddddddddddddddddddddddd', id_interno)
            #                     result_var.at[index, 'id_interno'] = id_interno
            #                     result_var.at[index, 'done'] = 'YES'
            #                     conditions = [result_var['sample'].eq(row['sample']) & result_var['codice_pannello'].eq(row['codice_pannello'])]
            #                     choices = [id_interno]
            #                     choices_ = ['YES']
            #                     result_var['id_interno'] = np.select(conditions, choices, default=0)
            #                     result_var['done'] = np.select(conditions, choices_, default=0)
            result_inter = result[1]
            # print(result_var)
            result_inter.loc[:, 'genetista_name'] = result_var['genetista_name']
            result_inter.loc[:, 'genetista_id'] = result_var['genetista_id']
            result_inter.loc[:, 'group_id'] = result_var['group_id']
            result_inter.loc[:, 'codice_pannello'] = result_var['codice_pannello']
            # result_inter.at[0, 'id_interno'] = 'M3.2022'
            # result_inter.at[1, 'id_interno'] = 'M3.2022'
            # result_inter.at[2, 'id_interno'] = 'M4.2022'
        	# print('startssssssssssssssssssssssssss')
        	# print(str(result_var['group_id']), 'sdsdsd', str(Sample.objects.get(sample_id = row['sample'], codice_pannello = row['codice_pannello'], genetista_id = result_var['genetista_id'])))

            # try:
            #     if (str(group_utente) == str(Sample.objects.get(sample_id = row['sample'], codice_pannello = row['codice_pannello']).group_user_id)):
            #         print('yes')
            #         id_interno = Sample.objects.get(sample_id = row['sample'], codice_pannello = row['codice_pannello'], genetista_id = id_utente).id_interno
            # except:
            #     id_interno = self.calculate_id_interno()

            # result_var.loc[:, 'id_interno'] = id_interno
            # result_inter.loc[:, 'id_interno'] = id_interno

            # print('id interno is')
            # print(id_interno)


            # print(result_inter[['sample', 'id_interno', 'choice_interpretation', 'choice_interpretation_final']])
            print('yessssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss')
            # print(result_var[['sample', 'id_interno']])
            #
            # load_acept_sanger(result_var)
            # # load_sanger(result_var)
            # load_preselection(result_var)
            # load_checklist(result_inter)
            #
            #
            # result_var.to_csv('/home/bioinfo/VIRTUAL38/MAGIWEB/DATA/result_var_1.csv',sep='\t',index=False)
            # result_inter.to_csv('/home/bioinfo/VIRTUAL38/MAGIWEB/DATA/result_interpret_1.csv',sep='\t',index=False)

        # serializer verything went OK"})= self.get_serializer(preselection, data=request.data, partial=partial)
        # serializer.is_valid(raise_exception=True)
        # self.perform_update(serializer)
        #
        # if getattr(preselection, '_prefetched_objects_cache', None):
        #     # If 'prefetch_related' has been applied to a queryset, we need to
        #     # forcibly invalidate the prefetch cache on the instance.
        #     preselection._prefetched_objects_cache = {}
        #
        # return Response(serializer.data)
        # return Response({"message": "Everything went OK"})


    # def update(self, request, *args, **kwargs):
    #     partial = kwargs.pop('partial', False)
    #     preselection = self.get_object()
    #
    #     # if 'runvarsome' in request.headers['name']:
    #
    #     data1 = request.data
    #     # print('data1')
    #     # print(data1)
    #     data2 = pd.Series(data1).to_frame()
    #     # print('data2')
    #     # print(data2)
    #     sample = pd.DataFrame(data2.T)
    #     # print('sample')
    #     # print(sample)
    #     sample1 = sample
    #     sample1 = sample1.rename({'sample_id': 'sample', 'hgvs': 'HGVS', 'acmg_classifications': 'acmg_classificatins' }, axis=1)
    #
    #
    #
    #     result = reinterpretation_verdict(sample1)
    #     # print('result is')
    #     # print(result)
    #     # result.to_csv('/home/bioinfo/VIRTUAL38/MAGIWEB/DATA/result.csv',sep='\t',index=False)
    #     i = 0
    #     for x in result:
    #         x.to_csv('/home/bioinfo/VIRTUAL38/MAGIWEB/DATA/result_'+str(i)+'.csv',sep='\t',index=False)
    #         i += 1
    #
    #
    #     serializer = self.get_serializer(preselection, data=request.data, partial=partial)
    #     serializer.is_valid(raise_exception=True)
    #     self.perform_update(serializer)
    #
    #     if getattr(preselection, '_prefetched_objects_cache', None):
    #         # If 'prefetch_related' has been applied to a queryset, we need to
    #         # forcibly invalidate the prefetch cache on the instance.
    #         preselection._prefetched_objects_cache = {}
    #
    #     return Response(serializer.data)
