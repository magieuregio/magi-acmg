#!/home/bioinfo/VIRTUAL38/bin/python3.8
# coding: utf-8
##################################
"""    *** ****  ****  **** ****     18 FEBRUARY 2022    """ """ """  """ """ """

AUTHOR: ING. MUHARREM DAJA

CONTACT: REMIDAJA@OUTLOOK.COM || MUHARREM.DAJA@FTI.EDU.AL

""" """ """  """ """ """  """ """ """  """ """ """  """ """ """  """ """ """  """


import csv
import os, sys
from os import system
import psycopg2
django_proj = '/home/bioinfo/VIRTUAL38/MAGIWEB'
sys.path.append(django_proj)
os.environ['DJANGO_SETTINGS_MODULE'] ='MAGIWEB.settings'
import pandas as pd
import numpy as np
import re
import glob
import django
django.setup()
from django.db import IntegrityError
from django.contrib.auth.models import User
from django.forms import model_to_dict
from acept.models import Sample
from sanger.models import AllVariation
from checklist.models import ResultInterpretation, ResultInterpretationRestore
from ngs.models import PreSelection
import datetime


#################### -------- SANGER  ALL VARIATION -------- #############################
##########################################################################################

dict_vusstatus = {
    'Cold':'FREDDA',
    'Middle': 'TIEPIDA',
    'Hot':'CALDA',
    'Unknown':'NON NOTO'
}

dict_choice_interpretation = {
	'VUS': 'Uncertain Significance',
	'Patogenetica': 'Pathogenic',
	'Probabilmente Patogenetica': 'Likely Pathogenic',
	'Probabilmente Benigna' : 'Likely Benign',
	'Benigna': 'Benign'
}

diction_cause = {
	'BA1_cause_final' : 'Allele frequency is >5% in Exome Sequencing Project, 1000 Genomes Project, or Exome Aggregation Consortium.',
	'BS1_cause_final': 'Allele frequency is greater than expected for disorder.',
	'BS2_cause_final': 'Observed in a healthy adult individual for a recessive (homozygous), dominant (heterozygous), or X-linked (hemizygous)\
 disorder, with full penetrance expected at an early age.',
	'BS3_cause_final': 'Well-established in vitro or in vivo functional studies show no damaging effect on protein function or splicing.',
	'BS4_cause_final': 'Lack of segregation in affected members of a family.',
	'BP1_cause_final': 'Missense variant in a gene for which primarily truncating variants are known to cause disease.',
	'BP2_cause_final': 'Observed in trans with a pathogenic variant for a fully penetrant dominant gene/disorder or observed in cis with a\
 pathogenic variant in any inheritance pattern.',
 	'BP3_cause_final': 'In-frame deletions/insertions in a repetitive region without a known function.',
	'BP4_cause_final' :'Multiple lines of computational evidence suggest no impact on gene or gene product (conservation, evolutionary,\
 splicing impact, etc).',
	'BP5_cause_final': 'Variant found in a case with an alternate molecular basis for disease.',
	'BP6_cause_final': 'Reputable source recently reports variant as benign, but the evidence is not available to the laboratory to\
 perform an independent evaluation.',
	'BP7_cause_final': 'A synonymous (silent) variant for which splicing prediction algorithms predict no impact to the splice consensus\
 sequence nor the creation of a new splice site AND the nucleotide is not highly conserved.',
	'PVS1_cause_final':'Null variant (nonsense, frameshift, canonical ±1 or 2 splice sites, initiation codon, single or multiexon deletion)\
 in a gene where LOF is a known mechanism of disease.',
 	'PS1_cause_final':'Same amino acid change as a previously established pathogenic variant regardless of nucleotide change..',
	'PS2_cause_final':'De novo (both maternity and paternity confirmed) in a patient with the disease and no family history.',
 	'PS3_cause_final':'Well-established in vitro or in vivo functional studies supportive of a damaging effect on the gene or gene product.',
	'PS4_cause_final':'The prevalence of the variant in affected individuals is significantly increased compared with the prevalence in controls.',
	'PM1_cause_final':'Located in a mutational hot spot and/or critical and well-established functional domain (e.g., active site of an enzyme)\
 without benign variation.',
	'PM2_cause_final':'Absent from controls (or at extremely low frequency if recessive) in Exome Sequencing Project, 1000 Genomes Project, or\
 Exome Aggregation Consortium.',
	'PM3_cause_final': 'For recessive disorders, detected in trans with a pathogenic variant.',
	'PM4_cause_final':'Protein length changes as a result of in-frame deletions/insertions in a non-repeat region or stop-loss variants.',
	'PM5_cause_final':'Novel missense change at an amino acid residue where a different missense change determined to be pathogenic has been seen\
 before.',
	'PM6_cause_final': 'Assumed de novo, but without confirmation of paternity and maternity.',
	'PP1_cause_final':'Cosegregation with disease in multiple affected family members in a gene definitively known to cause the disease.',
	'PP2_cause_final':'Missense variant in a gene that has a low rate of benign missense variation and in which missense variants are a common\
 mechanism of disease.',
	'PP3_cause_final':'Multiple lines of computational evidence support a deleterious effect on the gene or gene product (conservation, evolutionary,\
 splicing impact, etc.).',
	'PP4_cause_final':'Patient’s phenotype or family history is highly specific for a disease with a single genetic etiology.',
	'PP5_cause_final':'Reputable source recently reports variant as pathogenic, but the evidence is not available to the laboratory to perform an\
 independent evaluation.'
}

dict_BA1 = {
    'Stand-Alone': 'Stand Alone',
    'Stand-alone': 'Stand Alone',
    'Stand alone':'Stand Alone'
}

columns_criteri_cause_final = ['BA1_cause_final', 'BS1_cause_final', 'BS2_cause_final', 'BS3_cause_final', 'BS4_cause_final', 'BP1_cause_final',
									'BP2_cause_final', 'BP3_cause_final', 'BP4_cause_final', 'BP5_cause_final', 'BP6_cause_final', 'BP7_cause_final', 'PVS1_cause_final',
									'PS1_cause_final', 'PS2_cause_final', 'PS3_cause_final', 'PS4_cause_final', 'PM1_cause_final', 'PM2_cause_final', 'PM3_cause_final',
									'PM4_cause_final', 'PM5_cause_final', 'PM6_cause_final', 'PP1_cause_final', 'PP2_cause_final', 'PP3_cause_final', 'PP4_cause_final',
									'PP5_cause_final']

lista_id_interno = []
lista_sanger = []

def calculate_id_interno():
    try:
        last_id = Sample.objects.all().order_by("-pk")[0]
        _num_ = last_id.id_interno
    except:
        _num_ = 'M0.2022'
    num = re.split('[A-Z]+', _num_, flags=re.IGNORECASE)[-1]
    YEAR = num.split('.')[-1]
    num = num.split('.')[0]

    if int(datetime.datetime.now().year) != int(YEAR):
        num_ = 1
    else:
        try:
            num = int(num)
            num_ = num + 1
        except ValueError:
            num = int(num)
            num_ = num + 1

    ID_ = 'M'+str(num_)+'.'+str(datetime.datetime.now().year)
    return ID_

def load_acept(datareader):
	lista_id_interno = []
	datareader['sample'] = datareader['sample'].astype('str')
	datareader['sample'].replace(r'\.202$', '.2020', inplace=True, regex=True)
	datareader['sample'] = datareader['sample'].astype('str')
	for index, row in datareader.iterrows():
		if (Sample.objects.filter(sample_id = row['sample'], codice_pannello = row['codice_pannello'], genetista_id = row['genetista_id']).count() == 1):
		    # print('nowwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww')
		    id_interno_v = Sample.objects.get(sample_id = row['sample'], codice_pannello = row['codice_pannello'], genetista_id = row['genetista_id']).id_interno
		elif (Sample.objects.filter(sample_id = row['sample'], codice_pannello = row['codice_pannello'], genetista_id = row['genetista_id']).count() > 1):
		    print(Sample.objects.filter(sample_id = row['sample'], codice_pannello = row['codice_pannello'], genetista_id = row['genetista_id'])[0])
		    # print('second')
		    a = str(Sample.objects.filter(sample_id = row['sample'], codice_pannello = row['codice_pannello'], genetista_id = row['genetista_id'])[0]).split(' - ')[0]
		    # print(a)
		    id_interno_v = a
		else:
		    id_interno_v = calculate_id_interno()


		print('id internoooooooooooooooooooooooooooooooo', row['sample'], id_interno_v)

		lista_id_interno.append(id_interno_v)
		try:
		    Sample.objects.create(id_interno = id_interno_v, sample_id = row['sample'], codice_pannello = row['codice_pannello'],
                group_user_id = row['group_id'], genetista_id = row['genetista_id'], user_sample_id = row['genetista_id'])
		    print('yessssssssssssssssssssssssssssssssssssssss   createddddddddddddddddddddddddddddddddddd')
		except IntegrityError:
		    print('probelmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm')
		    # Sample.objects.create(id_interno = row['id_interno'], sample_id = row['sample'], codice_pannello = row['codice_pannello'],
            #     group_user_id = row['group_id'], genetista_id = row['genetista_id'], user_sample_id = row['genetista_id'])
		    Sample.objects.filter(sample_id = row['sample']).update(
                sample_id = row['sample'], codice_pannello = row['codice_pannello'], group_user_id = row['group_id'],
                genetista_id = row['genetista_id'], user_sample_id = row['genetista_id'])
	print('listaaaaaaaaaaaaaaaaaaaaaaaaa', lista_id_interno)

		# try:
		# 	AllVariation.objects.create(id_interno = row['id_interno'], sample_id = row['sample'],hgvs = row['HGVS'], gene = row['GENE'],
		# 		annotazione = row['annotation'], genetista_id = row['genetista_id'], genetista_name = row['genetista_name'],
		# 		vusstatus = row['vusstatus'], rilevanza = row['choice_interpretation_final'], pannello_text = row['codice_pannello']
		# 		)
		# except IntegrityError:
		# 	AllVariation.objects.filter(id_interno = row['id_interno'], hgvs = row['HGVS']).update(
		# 		sample_id = row['sample'], hgvs = row['HGVS'], gene = row['GENE'], annotazione = row['annotation'],
		# 		genetista_id = row['genetista_id'], genetista_name = row['genetista_name'], vusstatus = row['vusstatus'],
		# 		rilevanza = row['choice_interpretation_final'], pannello_text = row['codice_pannello']
		# 		)


def load_sanger(datareader):
	lista_sanger = []
	datareader['sample'] = datareader['sample'].astype('str')
	datareader['sample'].replace(r'\.202$', '.2020', inplace=True, regex=True)
	datareader['sample'] = datareader['sample'].astype('str')
	for index, row in datareader.iterrows():
		id_interno_vs = str(Sample.objects.filter(sample_id = row['sample'], codice_pannello = row['codice_pannello'], genetista_id = row['genetista_id'])[0]).split(' - ')[0]
		lista_sanger.append(id_interno_vs)
	datareader['id_interno'] = pd.DataFrame(lista_sanger)
	for index, row in datareader.iterrows():
		# print('rowwwwwwwwwwwwwwww', row)
		try:
			AllVariation.objects.create(sample_id = row['sample'],hgvs = row['HGVS'], gene = row['GENE'],
				annotazione = row['annotation'], genetista_id = row['genetista_id'], genetista_name = row['genetista_name'],
				vusstatus = row['vusstatus'], rilevanza = row['choice_interpretation_final'], pannello_text = row['codice_pannello'],
				id_interno = row['id_interno']
				)
		except IntegrityError:
			AllVariation.objects.filter(id_interno = row['id_interno'], hgvs = row['HGVS']).update(
				sample_id = row['sample'], hgvs = row['HGVS'], gene = row['GENE'], annotazione = row['annotation'],
				genetista_id = row['genetista_id'], genetista_name = row['genetista_name'], vusstatus = row['vusstatus'],
				rilevanza = row['choice_interpretation_final'], pannello_text = row['codice_pannello'],
				id_interno = row['id_interno']
				)

def load_preselection(dataloader):
	lista_pres = []
	dataloader['sample'] = dataloader['sample'].astype('str')
	dataloader['sample'].replace(r'\.202$','.2020',inplace=True,regex=True)
	dataloader['sample'] = dataloader['sample'].astype('str')
	dataloader['vusstatus'] = dataloader['vusstatus'].replace(dict_vusstatus)
	dataloader['pop_allele_count'] = dataloader['pop_allele_count'].fillna(0)
	dataloader['BA1'] = dataloader['BA1'].fillna(0)
	for index, row in dataloader.iterrows():
		id_interno_ps = str(Sample.objects.filter(sample_id = row['sample'], codice_pannello = row['codice_pannello'], genetista_id = row['genetista_id'])[0]).split(' - ')[0]
		lista_pres.append(id_interno_ps)
	dataloader['id_interno'] = pd.DataFrame(lista_pres)
	for index, row in dataloader.iterrows():
		try:
			PreSelection.objects.create(sample_id = row['sample'],
				hgvs = row['HGVS'],chrom = row['CHROM'], annotazione = row['annotation'],
				POS = row['POS'], id_interno = row['id_interno'],
				# rsID = row['ID'],
				GENE = row['GENE'],
				consequence = row['consequence'],hgvs_c = row['HGVS_c'],
				# unbalance = row['unbalance'],
				# inheritance = row['inheritance'],
				# allphenotype = row['allphenotype'],
				allinheritance = row['allinheritance'],
				# definitivemaf = row['definitivemaf'],
				definitivemaf2 = row['definitivemaf2'],pop_allele_count = row['pop_allele_count'],
				# mafmin20 = row['MAF<20'],
				# comments = row['comments'],
				# descriptions = row['Description'],
				# MAF_selection_AD = row['MAF_selection_AD'],MAF_selection_AR = row['MAF_selection_AR'],
				# MAF_selection = row['MAF_selection'],predictors_decision = row['predictors_decision'],
				# predictors_B = row['predictors_B'],predictors_D = row['predictors_D'],
				# predictors_introns = row['predictors_introns'],controls_nhomalt = row['controls_nhomalt'],
				# prevalence = row['prevalence'],
				ada_score = row['ada_score'],rf_score = row['rf_score'],
				acmg_verdict = row['acmg_verdict'],acmg_classifications = row['acmg_classifications'],
				publications = row['publications'],clinvar2 = row['clinvar2'],saphetor = row['saphetor'],
				mis_z = row['mis_z'],lof_z = row['lof_z'], zigosita = row['zigosita'],
				# num_mis = row['num_mis'],perc_misB = row['perc_misB'],
				decisionmaf = row['decisionmaf'],
				# perc_totMIS = row['perc_totMIS'],gene_maf = row['gene_maf'],
				revel_score = row['revel_score'], cadd_score = row['cadd_score'],
				sift_score = row['sift_score'], polyphen2_hdiv_score = row['polyphen2_hdiv_score'],
				polyphen2_hvar_score = row['polyphen2_hvar_score'],
				# fromFILE = row['fromFILE'],
				BA1_points = row['BA1'], var_on_gene = row['var_on_gene'],
				# BS1_prev_points = row['BS1_prev_points'],BS1_maf_points = row['BS1_maf_points'],
				# benign2_points = row['benign2_points'],benign2AD_points = row['benign2AD_points'],
				BP1_points = row['BP1_1'],
				# benign1_points = row['benign1_points'],
				# notbenign_points = row['notbenign_points'],benign2int_points = row['benign2int_points'],
				# notbenignint_points = row['notbenignint_points'],fromACMG_points = row['fromACMG_points'],
				# checklist_points_sum = row['checklist_points_sum'],
				apiversion = row['APIversion'],
				# hgvs_p = row['HGVS_p'],
				amp_verdict = row['AMP_verdict'], amp_classifications = row['AMP_classification'],
				drug_verdict = row['drug_verdict'], drug_classifications = row['drug_classifications'],
				# importanza = row['importanza'],
				vusstatus = row['vusstatus'],
				decisionINFO = row['decisionINFO'], PP3_final = row['PP3_final'],
				PP3_cause_final = row['PP3_cause_final'], BP4_final = row['BP4_final'],
				BP4_cause_final = row['BP4_cause_final'], PMPOT_final = row['PMPOT_final'],
				PMPOT_intensita_final = row['PMPOT_intensita_final'],
				PMPOT_cause_final = row['PMPOT_cause_final'], PPPOT_final = row['PPPOT_final'],
				PPPOT_cause_final = row['PPPOT_cause_final'], PPPOT_intensita_final = row['PPPOT_intensita_final'],
				autopvs1 = row['autopvs1'], spliceAI = row['spliceAI'], acmg_verdict_varsome = row['acmg_verdict_varsome'],
				acmg_classifications_varsome = row['acmg_classification_varsome'],
				acmg_classifications_final = row['acmg_classification_final']
				)
		except IntegrityError:
			PreSelection.objects.filter(id_interno=row['id_interno'],hgvs=row['HGVS']).update(sample_id = row['sample'],
					hgvs = row['HGVS'],chrom = row['CHROM'], annotazione = row['annotation'],
					POS = row['POS'], id_interno = row['id_interno'],
					# rsID = row['ID'],
					GENE = row['GENE'],
					consequence = row['consequence'],hgvs_c = row['HGVS_c'],
					# unbalance = row['unbalance'],
					# inheritance = row['inheritance'],
					# allphenotype = row['allphenotype'],
					allinheritance = row['allinheritance'],
					# definitivemaf = row['definitivemaf'],
					definitivemaf2 = row['definitivemaf2'],pop_allele_count = row['pop_allele_count'],
					# mafmin20 = row['MAF<20'],
					# comments = row['comments'],
					# descriptions = row['Description'],
					# MAF_selection_AD = row['MAF_selection_AD'],MAF_selection_AR = row['MAF_selection_AR'],
					# MAF_selection = row['MAF_selection'],predictors_decision = row['predictors_decision'],
					# predictors_B = row['predictors_B'],predictors_D = row['predictors_D'],
					# predictors_introns = row['predictors_introns'],controls_nhomalt = row['controls_nhomalt'],
					# prevalence = row['prevalence'],
					ada_score = row['ada_score'],rf_score = row['rf_score'],
					acmg_verdict = row['acmg_verdict'],acmg_classifications = row['acmg_classifications'],
					publications = row['publications'],clinvar2 = row['clinvar2'],saphetor = row['saphetor'],
					mis_z = row['mis_z'],lof_z = row['lof_z'], zigosita = row['zigosita'],
					# num_mis = row['num_mis'],perc_misB = row['perc_misB'],
					decisionmaf = row['decisionmaf'],
					# perc_totMIS = row['perc_totMIS'],gene_maf = row['gene_maf'],
					revel_score = row['revel_score'], cadd_score = row['cadd_score'],
					sift_score = row['sift_score'], polyphen2_hdiv_score = row['polyphen2_hdiv_score'],
					polyphen2_hvar_score = row['polyphen2_hvar_score'],
					# fromFILE = row['fromFILE'],
					BA1_points = row['BA1'], var_on_gene = row['var_on_gene'],
					# BS1_prev_points = row['BS1_prev_points'],BS1_maf_points = row['BS1_maf_points'],
					# benign2_points = row['benign2_points'],benign2AD_points = row['benign2AD_points'],
					BP1_points = row['BP1_1'],
					# benign1_points = row['benign1_points'],
					# notbenign_points = row['notbenign_points'],benign2int_points = row['benign2int_points'],
					# notbenignint_points = row['notbenignint_points'],fromACMG_points = row['fromACMG_points'],
					# checklist_points_sum = row['checklist_points_sum'],
					apiversion = row['APIversion'],
					# hgvs_p = row['HGVS_p'],
					amp_verdict = row['AMP_verdict'], amp_classifications = row['AMP_classification'],
					drug_verdict = row['drug_verdict'], drug_classifications = row['drug_classifications'],
					# importanza = row['importanza'],
					vusstatus = row['vusstatus'],
					decisionINFO = row['decisionINFO'], PP3_final = row['PP3_final'],
					PP3_cause_final = row['PP3_cause_final'], BP4_final = row['BP4_final'],
					BP4_cause_final = row['BP4_cause_final'], PMPOT_final = row['PMPOT_final'],
					PMPOT_intensita_final = row['PMPOT_intensita_final'],
					PMPOT_cause_final = row['PMPOT_cause_final'], PPPOT_final = row['PPPOT_final'],
					PPPOT_cause_final = row['PPPOT_cause_final'], PPPOT_intensita_final = row['PPPOT_intensita_final'],
					autopvs1 = row['autopvs1'], spliceAI = row['spliceAI'], acmg_verdict_varsome = row['acmg_verdict_varsome'],
					acmg_classifications_varsome = row['acmg_classification_varsome'],
					acmg_classifications_final = row['acmg_classification_final']
					)

def load_checklist(dataloader):
	lista_check = []
	dataloader['sample'] = dataloader['sample'].astype('str')
	dataloader['sample'].replace(r'\.202$','.2020',inplace=True,regex=True)
	dataloader['sample'] = dataloader['sample'].astype('str')
	dataloader['BA1_intensita_final'] = dataloader['BA1_intensita_final'].replace(dict_BA1)
	dataloader['choice_interpretation'] = dataloader['choice_interpretation'].replace(dict_choice_interpretation)
	dataloader['autopvs1'] = dataloader['autopvs1'].replace("None", "UNKNOWN")
	dataloader['PVS1_intensita_final'] = dataloader['PVS1_intensita_final'].replace("Unknown", "Strong")
	dataloader['allinheritance'] = dataloader['allinheritance'].fillna('-999')
	dataloader['spliceAI'].fillna(-999, inplace=True)
	dataloader['rf_score'].fillna(-999, inplace=True)
	dataloader['ada_score'].fillna(-999, inplace=True)
	dataloader['revel_score'].fillna(-999, inplace=True)
	dataloader['cadd_score'].fillna(-999, inplace=True)
	dataloader['sift_score'].fillna(-999, inplace=True)
	dataloader['polyphen2_hdiv_score'].fillna(-999, inplace=True)
	dataloader['polyphen2_hvar_score'].fillna(-999, inplace=True)
	for index, row in dataloader.iterrows():
		id_interno_ch = str(Sample.objects.filter(sample_id = row['sample'], codice_pannello = row['codice_pannello'], genetista_id = row['genetista_id'])[0]).split(' - ')[0]
		lista_check.append(id_interno_ch)
	dataloader['id_interno'] = pd.DataFrame(lista_check)
	for index, row in dataloader.iterrows():
		for i in columns_criteri_cause_final:
			if pd.isnull(row[i]):
				dataloader[i].fillna(diction_cause[i],inplace=True)
	for index, row in dataloader.iterrows():
		try:
			ResultInterpretation.objects.create(sample_id = row['sample'], id_interno = row['id_interno'],
    			hgvs = row['HGVS'],BA1_final = row['BA1_final'],
    			BA1_intensita_final = row['BA1_intensita_final'],BA1_cause_final = row['BA1_cause_final'],BS1_final = row['BS1_final'],
    			BS1_intensita_final = row['BS1_intensita_final'],BS1_cause_final = row['BS1_cause_final'],
    			BS2_final = row['BS2_final'],BS2_intensita_final = row['BS2_intensita_final'],BS2_cause_final = row['BS2_cause_final'],
    			BS3_final = row['BS3_final'],BS3_intensita_final = row['BS3_intensita_final'],
    			BS3_cause_final = row['BS3_cause_final'],BS4_final = row['BS4_final'],
    			BS4_intensita_final = row['BS4_intensita_final'],BS4_cause_final = row['BS4_cause_final'],BP1_final = row['BP1_final'],
    			BP1_intensita_final = row['BP1_intensita_final'],BP1_cause_final = row['BP1_cause_final'],
    			BP2_final = row['BP2_final'],BP2_cause_final = row['BP2_cause_final'],
    			BP2_intensita_final = row['BP2_intensita_final'],BP3_final = row['BP3_final'],
    			BP3_intensita_final = row['BP3_intensita_final'],BP3_cause_final = row['BP3_cause_final'],
    			BP4_final = row['BP4_final'],BP4_intensita_final = row['BP4_intensita_final'],BP4_cause_final = row['BP4_cause_final'],
    			BP5_final = row['BP5_final'],BP5_intensita_final = row['BP5_intensita_final'],
    			BP5_cause_final = row['BP5_cause_final'],BP6_final = row['BP6_final'],BP6_intensita_final = row['BP6_intensita_final'],
    			BP6_cause_final = row['BP6_cause_final'],BP7_final = row['BP7_final'],
    			BP7_intensita_final = row['BP7_intensita_final'],BP7_cause_final = row['BP7_cause_final'],
    			PVS1_final = row['PVS1_final'],PVS1_intensita_final = row['PVS1_intensita_final'],
    			PVS1_cause_final = row['PVS1_cause_final'],PS1_final = row['PS1_final'],
    			PS1_intensita_final = row['PS1_intensita_final'],PS1_cause_final = row['PS1_cause_final'],
    			PS2_final = row['PS2_final'],PS2_intensita_final = row['PS2_intensita_final'],
    			PS2_cause_final = row['PS2_cause_final'],PS3_final = row['PS3_final'], cadd_score = row['cadd_score'],
    			PS3_intensita_final = row['PS3_intensita_final'],PS3_cause_final = row['PS3_cause_final'],
    			PS4_final = row['PS4_final'],PS4_intensita_final = row['PS4_intensita_final'],
    			PS4_cause_final = row['PS4_cause_final'],PM1_final = row['PM1_final'], PM1_intensita_final = row['PM1_intensita_final'],
    			PM1_cause_final = row['PM1_cause_final'], PM2_final = row['PM2_final'], PM2_intensita_final = row['PM2_intensita_final'],
    			PM2_cause_final = row['PM2_cause_final'], PM3_final = row['PM3_final'], PM3_intensita_final = row['PM3_intensita_final'],
    			PM3_cause_final = row['PM3_cause_final'], PM4_final = row['PM4_final'], PM4_intensita_final = row['PM4_intensita_final'],
    			PM4_cause_final = row['PM4_cause_final'], PM5_final = row['PM5_final'], PM5_intensita_final = row['PM5_intensita_final'],
    			PM5_cause_final = row['PM5_cause_final'], PM6_final = row['PM6_final'], PM6_intensita_final = row['PM6_intensita_final'],
    			PM6_cause_final = row['PM6_cause_final'], PP1_final = row['PP1_final'], PP1_intensita_final = row['PP1_intensita_final'],
    			PP1_cause_final = row['PP1_cause_final'], PP2_final = row['PP2_final'], PP2_intensita_final = row['PP2_intensita_final'],
    			PP2_cause_final = row['PP2_cause_final'], PP3_final = row['PP3_final'], PP3_intensita_final = row['PP3_intensita_final'],
    			PP3_cause_final = row['PP3_cause_final'], PP4_final = row['PP4_final'], PP4_intensita_final = row['PP4_intensita_final'],
    			PP4_cause_final = row['PP4_cause_final'], PP5_final = row['PP5_final'], PP5_intensita_final = row['PP5_intensita_final'],
    			PP5_cause_final = row['PP5_cause_final'], choice_interpretation = row['choice_interpretation'],
    			choice_interpretation_final = row['choice_interpretation_final'], vusstatus = row['vusstatus'],
    			PMPOT_final = row['PMPOT_final'], PMPOT_intensita_final = row['PMPOT_intensita_final'],
    			PMPOT_cause_final = row['PMPOT_cause_final'], PPPOT_final = row['PPPOT_final'],
    			PPPOT_intensita_final = row['PPPOT_intensita_final'], PPPOT_cause_final = row['PPPOT_cause_final'],
    			var_on_gene = row['var_on_gene'], allinheritance = row['allinheritance'], zigosita = row['zigosita'],
    			spliceAI = row['spliceAI'], autopvs1 = row['autopvs1'], consequence = row['consequence'],
    			annotazione = row['annotation'], operatoreGENETISTA = row['genetista_name'], codice_pannello = row['codice_pannello'],
    			ada_score = row['ada_score'], rf_score = row['rf_score'], revel_score = row['revel_score'], sift_score = row['sift_score'],
    			polyphen2_hdiv_score = row['polyphen2_hdiv_score'], polyphen2_hvar_score = row['polyphen2_hvar_score']
    			)
			ResultInterpretationRestore.objects.create(sample_id = row['sample'], id_interno = row['id_interno'],
    			hgvs = row['HGVS'],BA1_final = row['BA1_final'],
    			BA1_intensita_final = row['BA1_intensita_final'],BA1_cause_final = row['BA1_cause_final'],BS1_final = row['BS1_final'],
    			BS1_intensita_final = row['BS1_intensita_final'],BS1_cause_final = row['BS1_cause_final'],
    			BS2_final = row['BS2_final'],BS2_intensita_final = row['BS2_intensita_final'],BS2_cause_final = row['BS2_cause_final'],
    			BS3_final = row['BS3_final'],BS3_intensita_final = row['BS3_intensita_final'],
    			BS3_cause_final = row['BS3_cause_final'],BS4_final = row['BS4_final'],
    			BS4_intensita_final = row['BS4_intensita_final'],BS4_cause_final = row['BS4_cause_final'],BP1_final = row['BP1_final'],
    			BP1_intensita_final = row['BP1_intensita_final'],BP1_cause_final = row['BP1_cause_final'],
    			BP2_final = row['BP2_final'],BP2_cause_final = row['BP2_cause_final'],
    			BP2_intensita_final = row['BP2_intensita_final'],BP3_final = row['BP3_final'], cadd_score = row['cadd_score'],
    			BP3_intensita_final = row['BP3_intensita_final'],BP3_cause_final = row['BP3_cause_final'],
    			BP4_final = row['BP4_final'],BP4_intensita_final = row['BP4_intensita_final'],BP4_cause_final = row['BP4_cause_final'],
    			BP5_final = row['BP5_final'],BP5_intensita_final = row['BP5_intensita_final'],
    			BP5_cause_final = row['BP5_cause_final'],BP6_final = row['BP6_final'],BP6_intensita_final = row['BP6_intensita_final'],
    			BP6_cause_final = row['BP6_cause_final'],BP7_final = row['BP7_final'],
    			BP7_intensita_final = row['BP7_intensita_final'],BP7_cause_final = row['BP7_cause_final'],
    			PVS1_final = row['PVS1_final'],PVS1_intensita_final = row['PVS1_intensita_final'],
    			PVS1_cause_final = row['PVS1_cause_final'],PS1_final = row['PS1_final'],
    			PS1_intensita_final = row['PS1_intensita_final'],PS1_cause_final = row['PS1_cause_final'],
    			PS2_final = row['PS2_final'],PS2_intensita_final = row['PS2_intensita_final'],
    			PS2_cause_final = row['PS2_cause_final'],PS3_final = row['PS3_final'],
    			PS3_intensita_final = row['PS3_intensita_final'],PS3_cause_final = row['PS3_cause_final'],
    			PS4_final = row['PS4_final'],PS4_intensita_final = row['PS4_intensita_final'],
    			PS4_cause_final = row['PS4_cause_final'],PM1_final = row['PM1_final'], PM1_intensita_final = row['PM1_intensita_final'],
    			PM1_cause_final = row['PM1_cause_final'], PM2_final = row['PM2_final'], PM2_intensita_final = row['PM2_intensita_final'],
    			PM2_cause_final = row['PM2_cause_final'], PM3_final = row['PM3_final'], PM3_intensita_final = row['PM3_intensita_final'],
    			PM3_cause_final = row['PM3_cause_final'], PM4_final = row['PM4_final'], PM4_intensita_final = row['PM4_intensita_final'],
    			PM4_cause_final = row['PM4_cause_final'], PM5_final = row['PM5_final'], PM5_intensita_final = row['PM5_intensita_final'],
    			PM5_cause_final = row['PM5_cause_final'], PM6_final = row['PM6_final'], PM6_intensita_final = row['PM6_intensita_final'],
    			PM6_cause_final = row['PM6_cause_final'], PP1_final = row['PP1_final'], PP1_intensita_final = row['PP1_intensita_final'],
    			PP1_cause_final = row['PP1_cause_final'], PP2_final = row['PP2_final'], PP2_intensita_final = row['PP2_intensita_final'],
    			PP2_cause_final = row['PP2_cause_final'], PP3_final = row['PP3_final'], PP3_intensita_final = row['PP3_intensita_final'],
    			PP3_cause_final = row['PP3_cause_final'], PP4_final = row['PP4_final'], PP4_intensita_final = row['PP4_intensita_final'],
    			PP4_cause_final = row['PP4_cause_final'], PP5_final = row['PP5_final'], PP5_intensita_final = row['PP5_intensita_final'],
    			PP5_cause_final = row['PP5_cause_final'], choice_interpretation = row['choice_interpretation'],
    			choice_interpretation_final = row['choice_interpretation_final'], vusstatus = row['vusstatus'],
    			PMPOT_final = row['PMPOT_final'], PMPOT_intensita_final = row['PMPOT_intensita_final'],
    			PMPOT_cause_final = row['PMPOT_cause_final'], PPPOT_final = row['PPPOT_final'],
    			PPPOT_intensita_final = row['PPPOT_intensita_final'], PPPOT_cause_final = row['PPPOT_cause_final'],
    			var_on_gene = row['var_on_gene'], allinheritance = row['allinheritance'], zigosita = row['zigosita'],
    			spliceAI = row['spliceAI'], autopvs1 = row['autopvs1'], consequence = row['consequence'],
    			annotazione = row['annotation'], operatoreGENETISTA = row['genetista_name'], codice_pannello = row['codice_pannello'],
    			ada_score = row['ada_score'], rf_score = row['rf_score'], revel_score = row['revel_score'], sift_score = row['sift_score'],
    			polyphen2_hdiv_score = row['polyphen2_hdiv_score'], polyphen2_hvar_score = row['polyphen2_hvar_score']
    			)
		except IntegrityError:
			print ('start update!!!')
			ResultInterpretation.objects.filter(id_interno=row['id_interno'],hgvs=row['HGVS']).update(sample_id = row['sample'],
					hgvs = row['HGVS'],BA1_final = row['BA1_final'], id_interno = row['id_interno'],
					BA1_intensita_final = row['BA1_intensita_final'],BA1_cause_final = row['BA1_cause_final'],BS1_final = row['BS1_final'],
					BS1_intensita_final = row['BS1_intensita_final'],BS1_cause_final = row['BS1_cause_final'],
					BS2_final = row['BS2_final'],BS2_intensita_final = row['BS2_intensita_final'],BS2_cause_final = row['BS2_cause_final'],
					BS3_final = row['BS3_final'],BS3_intensita_final = row['BS3_intensita_final'],
					BS3_cause_final = row['BS3_cause_final'],BS4_final = row['BS4_final'],
					BS4_intensita_final = row['BS4_intensita_final'],BS4_cause_final = row['BS4_cause_final'],BP1_final = row['BP1_final'],
					BP1_intensita_final = row['BP1_intensita_final'],BP1_cause_final = row['BP1_cause_final'],
					BP2_final = row['BP2_final'],BP2_cause_final = row['BP2_cause_final'],
					BP2_intensita_final = row['BP2_intensita_final'],BP3_final = row['BP3_final'],
					BP3_intensita_final = row['BP3_intensita_final'],BP3_cause_final = row['BP3_cause_final'],
					BP4_final = row['BP4_final'],BP4_intensita_final = row['BP4_intensita_final'],BP4_cause_final = row['BP4_cause_final'],
					BP5_final = row['BP5_final'],BP5_intensita_final = row['BP5_intensita_final'],
					BP5_cause_final = row['BP5_cause_final'],BP6_final = row['BP6_final'],BP6_intensita_final = row['BP6_intensita_final'],
					BP6_cause_final = row['BP6_cause_final'],BP7_final = row['BP7_final'], cadd_score = row['cadd_score'],
					BP7_intensita_final = row['BP7_intensita_final'],BP7_cause_final = row['BP7_cause_final'],
					PVS1_final = row['PVS1_final'],PVS1_intensita_final = row['PVS1_intensita_final'],
					PVS1_cause_final = row['PVS1_cause_final'],PS1_final = row['PS1_final'],
					PS1_intensita_final = row['PS1_intensita_final'],PS1_cause_final = row['PS1_cause_final'],
					PS2_final = row['PS2_final'],PS2_intensita_final = row['PS2_intensita_final'],
					PS2_cause_final = row['PS2_cause_final'],PS3_final = row['PS3_final'],
					PS3_intensita_final = row['PS3_intensita_final'],PS3_cause_final = row['PS3_cause_final'],
					PS4_final = row['PS4_final'],PS4_intensita_final = row['PS4_intensita_final'], codice_pannello = row['codice_pannello'],
					PS4_cause_final = row['PS4_cause_final'],PM1_final = row['PM1_final'], PM1_intensita_final = row['PM1_intensita_final'],
					PM1_cause_final = row['PM1_cause_final'], PM2_final = row['PM2_final'], PM2_intensita_final = row['PM2_intensita_final'],
					PM2_cause_final = row['PM2_cause_final'], PM3_final = row['PM3_final'], PM3_intensita_final = row['PM3_intensita_final'],
					PM3_cause_final = row['PM3_cause_final'], PM4_final = row['PM4_final'], PM4_intensita_final = row['PM4_intensita_final'],
					PM4_cause_final = row['PM4_cause_final'], PM5_final = row['PM5_final'], PM5_intensita_final = row['PM5_intensita_final'],
					PM5_cause_final = row['PM5_cause_final'], PM6_final = row['PM6_final'], PM6_intensita_final = row['PM6_intensita_final'],
					PM6_cause_final = row['PM6_cause_final'], PP1_final = row['PP1_final'], PP1_intensita_final = row['PP1_intensita_final'],
					PP1_cause_final = row['PP1_cause_final'], PP2_final = row['PP2_final'], PP2_intensita_final = row['PP2_intensita_final'],
					PP2_cause_final = row['PP2_cause_final'], PP3_final = row['PP3_final'], PP3_intensita_final = row['PP3_intensita_final'],
					PP3_cause_final = row['PP3_cause_final'], PP4_final = row['PP4_final'], PP4_intensita_final = row['PP4_intensita_final'],
					PP4_cause_final = row['PP4_cause_final'], PP5_final = row['PP5_final'], PP5_intensita_final = row['PP5_intensita_final'],
					PP5_cause_final = row['PP5_cause_final'], choice_interpretation = row['choice_interpretation'],
					choice_interpretation_final = row['choice_interpretation_final'], vusstatus = row['vusstatus'],
					PMPOT_final = row['PMPOT_final'], PMPOT_intensita_final = row['PMPOT_intensita_final'],
					PMPOT_cause_final = row['PMPOT_cause_final'], PPPOT_final = row['PPPOT_final'],
					PPPOT_intensita_final = row['PPPOT_intensita_final'], PPPOT_cause_final = row['PPPOT_cause_final'],
					var_on_gene = row['var_on_gene'], allinheritance = row['allinheritance'], zigosita = row['zigosita'],
					spliceAI = row['spliceAI'], autopvs1 = row['autopvs1'], consequence = row['consequence'],
					annotazione = row['annotation'], operatoreGENETISTA = row['genetista_name'],
					ada_score = row['ada_score'], rf_score = row['rf_score'], revel_score = row['revel_score'], sift_score = row['sift_score'],
					polyphen2_hdiv_score = row['polyphen2_hdiv_score'], polyphen2_hvar_score = row['polyphen2_hvar_score']
					)
			ResultInterpretationRestore.objects.filter(id_interno=row['id_interno'],hgvs=row['HGVS']).update(sample_id = row['sample'],
					hgvs = row['HGVS'],BA1_final = row['BA1_final'], id_interno = row['id_interno'],
					BA1_intensita_final = row['BA1_intensita_final'],BA1_cause_final = row['BA1_cause_final'],BS1_final = row['BS1_final'],
					BS1_intensita_final = row['BS1_intensita_final'],BS1_cause_final = row['BS1_cause_final'],
					BS2_final = row['BS2_final'],BS2_intensita_final = row['BS2_intensita_final'],BS2_cause_final = row['BS2_cause_final'],
					BS3_final = row['BS3_final'],BS3_intensita_final = row['BS3_intensita_final'],
					BS3_cause_final = row['BS3_cause_final'],BS4_final = row['BS4_final'],
					BS4_intensita_final = row['BS4_intensita_final'],BS4_cause_final = row['BS4_cause_final'],BP1_final = row['BP1_final'],
					BP1_intensita_final = row['BP1_intensita_final'],BP1_cause_final = row['BP1_cause_final'],
					BP2_final = row['BP2_final'],BP2_cause_final = row['BP2_cause_final'],
					BP2_intensita_final = row['BP2_intensita_final'],BP3_final = row['BP3_final'],
					BP3_intensita_final = row['BP3_intensita_final'],BP3_cause_final = row['BP3_cause_final'],
					BP4_final = row['BP4_final'],BP4_intensita_final = row['BP4_intensita_final'],BP4_cause_final = row['BP4_cause_final'],
					BP5_final = row['BP5_final'],BP5_intensita_final = row['BP5_intensita_final'],
					BP5_cause_final = row['BP5_cause_final'],BP6_final = row['BP6_final'],BP6_intensita_final = row['BP6_intensita_final'],
					BP6_cause_final = row['BP6_cause_final'],BP7_final = row['BP7_final'], cadd_score = row['cadd_score'],
					BP7_intensita_final = row['BP7_intensita_final'],BP7_cause_final = row['BP7_cause_final'],
					PVS1_final = row['PVS1_final'],PVS1_intensita_final = row['PVS1_intensita_final'],
					PVS1_cause_final = row['PVS1_cause_final'],PS1_final = row['PS1_final'],
					PS1_intensita_final = row['PS1_intensita_final'],PS1_cause_final = row['PS1_cause_final'],
					PS2_final = row['PS2_final'],PS2_intensita_final = row['PS2_intensita_final'],
					PS2_cause_final = row['PS2_cause_final'],PS3_final = row['PS3_final'],
					PS3_intensita_final = row['PS3_intensita_final'],PS3_cause_final = row['PS3_cause_final'],
					PS4_final = row['PS4_final'],PS4_intensita_final = row['PS4_intensita_final'], codice_pannello = row['codice_pannello'],
					PS4_cause_final = row['PS4_cause_final'],PM1_final = row['PM1_final'], PM1_intensita_final = row['PM1_intensita_final'],
					PM1_cause_final = row['PM1_cause_final'], PM2_final = row['PM2_final'], PM2_intensita_final = row['PM2_intensita_final'],
					PM2_cause_final = row['PM2_cause_final'], PM3_final = row['PM3_final'], PM3_intensita_final = row['PM3_intensita_final'],
					PM3_cause_final = row['PM3_cause_final'], PM4_final = row['PM4_final'], PM4_intensita_final = row['PM4_intensita_final'],
					PM4_cause_final = row['PM4_cause_final'], PM5_final = row['PM5_final'], PM5_intensita_final = row['PM5_intensita_final'],
					PM5_cause_final = row['PM5_cause_final'], PM6_final = row['PM6_final'], PM6_intensita_final = row['PM6_intensita_final'],
					PM6_cause_final = row['PM6_cause_final'], PP1_final = row['PP1_final'], PP1_intensita_final = row['PP1_intensita_final'],
					PP1_cause_final = row['PP1_cause_final'], PP2_final = row['PP2_final'], PP2_intensita_final = row['PP2_intensita_final'],
					PP2_cause_final = row['PP2_cause_final'], PP3_final = row['PP3_final'], PP3_intensita_final = row['PP3_intensita_final'],
					PP3_cause_final = row['PP3_cause_final'], PP4_final = row['PP4_final'], PP4_intensita_final = row['PP4_intensita_final'],
					PP4_cause_final = row['PP4_cause_final'], PP5_final = row['PP5_final'], PP5_intensita_final = row['PP5_intensita_final'],
					PP5_cause_final = row['PP5_cause_final'], choice_interpretation = row['choice_interpretation'],
					choice_interpretation_final = row['choice_interpretation_final'], vusstatus = row['vusstatus'],
					PMPOT_final = row['PMPOT_final'], PMPOT_intensita_final = row['PMPOT_intensita_final'],
					PMPOT_cause_final = row['PMPOT_cause_final'], PPPOT_final = row['PPPOT_final'],
					PPPOT_intensita_final = row['PPPOT_intensita_final'], PPPOT_cause_final = row['PPPOT_cause_final'],
					var_on_gene = row['var_on_gene'], allinheritance = row['allinheritance'], zigosita = row['zigosita'],
					spliceAI = row['spliceAI'], autopvs1 = row['autopvs1'], consequence = row['consequence'],
					annotazione = row['annotation'], operatoreGENETISTA = row['genetista_name'],
					ada_score = row['ada_score'], rf_score = row['rf_score'], revel_score = row['revel_score'], sift_score = row['sift_score'],
					polyphen2_hdiv_score = row['polyphen2_hdiv_score'], polyphen2_hvar_score = row['polyphen2_hvar_score']
					)

	path_data = '/home/bioinfo/VIRTUAL38/MAGIWEB/DATA/'
	file_list = glob.glob(path_data+'/*')
	for file_ in file_list:
		system(' '.join(['rm',file_]))
