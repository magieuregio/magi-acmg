"""    *** ****  ****  **** ****     18 FEBRUARY 2022    """ """ """  """ """ """

AUTHOR: ING. MUHARREM DAJA

CONTACT: REMIDAJA@OUTLOOK.COM || MUHARREM.DAJA@FTI.EDU.AL

""" """ """  """ """ """  """ """ """  """ """ """  """ """ """  """ """ """  """
from rest_framework import mixins
from rest_framework import generics
# from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework import viewsets

from ngs.models import PreSelection
from ngs.api.serializers import PreSelectionSerializer

import pandas as pd

from django.shortcuts import render
from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework import permissions, renderers, authentication, status
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.decorators import api_view
from rest_framework.decorators import authentication_classes, permission_classes
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet, ReadOnlyModelViewSet
from rest_framework.authentication import BaseAuthentication, SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework import filters
from django_filters.rest_framework import DjangoFilterBackend

from ngs.api.permissions import IsOwnerOrReadOnly
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token

from ngs.api.variant_varsome_info import reinterpretation_verdict, query_varsome_saphetor
from ngs.api.variant_varsome_info import query_varsome_gene, varsome_consequences
from ngs.api.load import load_sanger

cols1 = ['sample','GENE','HGVS','annotation','ada_score','rf_score','HGVS_c']
# cols1 = ['sample','GENE','annotation','ada_score','rf_score','HGVS_c']

class PreSelectionViewSet(ModelViewSet):

    serializer_class = PreSelectionSerializer
    authentication_classes = (TokenAuthentication, SessionAuthentication,)
    permission_classes = (IsAuthenticated,)
    filter_backends = (filters.SearchFilter, DjangoFilterBackend, filters.OrderingFilter,)
    search_fields = ('$sample_id', '$hgvs',)
    filter_fields = ('sample_id', 'hgvs',)

    def get_queryset(self):
        preselections = PreSelection.objects.all()
        return preselections

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers = headers)

    def retrieve(self, request, *args, **kwargs):
        preselection = self.get_object()
        serializer = self.get_serializer(preselection)
        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        preselection = self.get_object()
        self.perform_destroy(preselection)
        return Response({"message": "It was deleted"}, status=status.HTTP_204_NO_CONTENT)

    def update(self, request, *args, **kwargs):
        lista_input = []
        lista_samples = []
        lista_annotazioni = []
        lista_hgvs = []
        partial = kwargs.pop('partial', False)
        preselection = self.get_object()

        if request.data['acmg_classifications'] != '':
            utente = request.data['acmg_verdict']
            print('utente is', utente)
            input = request.data['acmg_classifications']
            for el in input.split('\n'):
                lista_input.append(el)

            for el in lista_input:
                lista_samples.append(el.split('\t')[0])
                lista_annotazioni.append(el.split('\t')[1])
                lista_hgvs.append(el.split('\t')[2])

            variants = pd.DataFrame(lista_samples, columns=['sample'])
            variants['annotation'] = pd.DataFrame(lista_annotazioni)
            variants['HGVS'] = pd.DataFrame(lista_hgvs)

            SAMPLE = pd.DataFrame(columns=cols1)

            for index, row in variants.iterrows():
            	sample_x = variants.loc[index, 'sample']
            	gene = variants.loc[index,'annotation'].split(':')[0]
            	SAMPLE.loc[index, 'sample'] =  variants.loc[index,'sample']
            	SAMPLE.loc[index, 'HGVS'] =  variants.loc[index,'HGVS']
            	SAMPLE.loc[index, 'CHROM'] =  variants.loc[index,'HGVS'].split(':')[0]
            	SAMPLE.loc[index, 'POS'] =  variants.loc[index,'HGVS'].split(':')[1].split('-')[0]
            	SAMPLE.loc[index, 'GENE'] =  variants.loc[index,'annotation'].split(':')[0]
            	SAMPLE.loc[index, 'annotation'] =  variants.loc[index,'annotation']
            	SAMPLE.loc[index, 'amp_verdict'] = None
            	SAMPLE.loc[index, 'amp_classifications'] = None
            	SAMPLE.loc[index, 'drug_verdict'] = None
            	SAMPLE.loc[index, 'drug_classifications'] = None

            SAMPLE['sample'] = SAMPLE['sample'].astype('str')
            SAMPLE['sample'].replace(r'\.202$','.2020',inplace=True,regex=True)
            SAMPLE['sample'] = SAMPLE['sample'].astype('str')

            for index,row in SAMPLE.iterrows():
            	rawinput = ':'.join([SAMPLE.loc[index,'annotation'].split(':')[1], SAMPLE.loc[index,'annotation'].split(':')[2]])
            	rawinput2 = ':'.join([SAMPLE.loc[index,'annotation'].split(':')[0], SAMPLE.loc[index,'annotation'].split(':')[2]])
            	saphetor = query_varsome_saphetor(rawinput, rawinput2 ,SAMPLE,index)
            	saphetor = query_varsome_gene(str(row.GENE),saphetor,index)
            	saphetor = varsome_consequences(str(row.GENE), saphetor, index)

            saphetor.loc[:, 'genetista_name'] = utente
            print(saphetor.T)
            print('function call')
            b = reinterpretation_verdict(saphetor)
            print(b.T)
            load_sanger(saphetor)

        serializer = self.get_serializer(preselection, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(preselection, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            preselection._prefetched_objects_cache = {}

        return Response(serializer.data)


    # def update(self, request, *args, **kwargs):
    #     partial = kwargs.pop('partial', False)
    #     preselection = self.get_object()
    #
    #     # if 'runvarsome' in request.headers['name']:
    #
    #     data1 = request.data
    #     # print('data1')
    #     # print(data1)
    #     data2 = pd.Series(data1).to_frame()
    #     # print('data2')
    #     # print(data2)
    #     sample = pd.DataFrame(data2.T)
    #     # print('sample')
    #     # print(sample)
    #     sample1 = sample
    #     sample1 = sample1.rename({'sample_id': 'sample', 'hgvs': 'HGVS', 'acmg_classifications': 'acmg_classificatins' }, axis=1)
    #
    #
    #
    #     result = reinterpretation_verdict(sample1)
    #     # print('result is')
    #     # print(result)
    #     # result.to_csv('/home/bioinfo/VIRTUAL38/MAGIWEB/DATA/result.csv',sep='\t',index=False)
    #     i = 0
    #     for x in result:
    #         x.to_csv('/home/bioinfo/VIRTUAL38/MAGIWEB/DATA/result_'+str(i)+'.csv',sep='\t',index=False)
    #         i += 1
    #
    #
    #     serializer = self.get_serializer(preselection, data=request.data, partial=partial)
    #     serializer.is_valid(raise_exception=True)
    #     self.perform_update(serializer)
    #
    #     if getattr(preselection, '_prefetched_objects_cache', None):
    #         # If 'prefetch_related' has been applied to a queryset, we need to
    #         # forcibly invalidate the prefetch cache on the instance.
    #         preselection._prefetched_objects_cache = {}
    #
    #     return Response(serializer.data)
