from rest_framework import serializers
from ngs.models import PreSelection
import re



class PreSelectionSerializer(serializers.ModelSerializer):

	class Meta:

		model = PreSelection

		fields = ('pk', 'select', 'timestamp_selection', 'updated_selection', 'sample_id',
					'hgvs', 'chrom', 'POS', 'rsID', 'GENE', 'consequence', 'hgvs_c',
					'hgvs_p', 'unbalance', 'inheritance', 'allphenotype', 'allinheritance',
					'definitivemaf', 'definitivemaf2', 'pop_allele_count', 'mafmin20',
					'comments', 'descriptions', 'MAF_selection_AD', 'MAF_selection_AR',
					'MAF_selection', 'predictors_decision', 'predictors_B',
					'predictors_D', 'predictors_introns', 'controls_nhomalt',
					'prevalence', 'ada_score', 'rf_score', 'acmg_verdict',
					'acmg_classifications', 'acmg_classifications_varsome',
					'acmg_classifications_final', 'publications', 'clinvar2', 'saphetor',
					'mis_z', 'lof_z', 'num_mis', 'perc_misB', 'perc_totMIS', 'gene_maf',
					'fromFILE', 'BA1_points', 'BS1_prev_points', 'BS1_maf_points',
					'benign2_points', 'benign2AD_points', 'BP1_points', 'benign1_points',
					'notbenign_points', 'benign2int_points', 'notbenignint_points',
					'fromACMG_points', 'checklist_points_sum', 'apiversion',
					'amp_verdict', 'amp_classifications', 'drug_verdict',
					'drug_classifications', 'revel_score', 'autopvs1', 'PVS1_old',
					'spliceAI', 'vusstatus', 'PMPOT_final', 'PMPOT_intensita_final',
					'PMPOT_cause_final', 'PPPOT_final', 'PPPOT_intensita_final',
					'PPPOT_cause_final', 'PP3_final', 'PP3_cause_final', 'BP4_final',
					'BP4_cause_final', 'PVS1_final', 'PVS1_cause_final', 'STATO',
					'importanza', 'PVS1_intensita_final', 'zigosita', 'var_on_gene',
					'acmg_verdict', 'annotazione', 'id_interno', 'cadd_score', 'polyphen2_hdiv_score',
					'polyphen2_hvar_score', 'sift_score')

	def validate_sample_id(self, value):
		regex = re.compile(r'^\w{0,10}\d+[.]\d{4,4}$')
		if value == '':
			raise serializers.ValidationError("Sample ID non puo essere vuoto")
		elif not (re.match(regex, value)):
			raise serializers.ValidationError("Attenzione, Sample ID deve essere in un altro formato!")
		return value

	def validate_hgvs(self, value):
		if self.instance and self.instance.hgvs != value:
			raise serializers.ValidationError("You are not ALLOWED to edit HGVS field")
		return value
