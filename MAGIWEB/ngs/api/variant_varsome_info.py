#!/home/bioinfo/VIRTUAL38/bin/python3.8

# -*- coding: utf-8 -*-
import os,re,glob,csv,argparse,datetime,sys,subprocess
from os.path import isfile, join
import numpy as np
import pandas as pd
import ngs.api.fourth_b_diagnosys_variantinterpretation as fourth_b
import os.path
from varsome_api.client import VarSomeAPIClient,VarSomeAPIException
from varsome_api.models.variant import AnnotatedVariant
import sqlite3
import requests
from requests.structures import CaseInsensitiveDict
from requests.auth import HTTPBasicAuth
from pandas.io.json import json_normalize
from ngs.api.getSequence import getsequence
import json
import datetime
import time
import io
import math
path = os.getcwd()
pd.options.display.float_format = '{:,.5f}'.format

def query_varsome_saphetor(input,input2,SAMPLE,index):
	strength_dict={'BA1': 'Stand-alone', 'BS1': 'Strong', 'BS2': 'Strong', 'BS3': 'Strong', 'BS4': 'Strong', 'BP1': 'Supporting', 'BP2': 'Supporting', 'BP3': 'Supporting', 'BP4': 'Supporting', 'BP5': 'Supporting', 'BP6': 'Supporting', 'BP7': 'Supporting',
	'PVS1': 'Very Strong', 'PS1': 'Strong', 'PS2': 'Strong', 'PS3': 'Strong', 'PS4': 'Strong', 'PM1': 'Moderate', 'PM2': 'Moderate', 'PM3': 'Moderate', 'PM4': 'Moderate', 'PM5': 'Moderate', 'PM6': 'Moderate',
	'PP1': 'Supporting','PP2': 'Supporting', 'PP3': 'Supporting','PP4': 'Supporting','PP5': 'Supporting', 'PMPOT': 'Moderate', 'PPPOT': 'Supporting'}
	acmg_verdict = ''
	acmg_classifications = ''
	publications = ''
	clinvar2 = ''
	saphetor = ''
	lista_problems = []
	APIversion = ''
	# #rf_score = -999
	# #ada_score = -999
	rf_score = np.nan
	ada_score = np.nan
	revel_score = np.nan
	cadd_score = np.nan
	sift_score = np.nan
	polyphen2_hdiv_score = np.nan
	polyphen2_hvar_score = np.nan
	definitivemaf2 = None
	definitivemaf2_info = None
	definitivemaf2_description = None
	#######################ADDEDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
	SAMPLE['comments'] = np.nan
	SAMPLE['decisionmaf'] =np.nan
	#url2 = "/".join(('https://api.varsome.com/lookup',''.join((input,'?add-all-data=0&add-source-databases=ncbi-dbsnp,refseq-transcripts,dbnsfp-dbscsnv&add-ACMG-annotation=1'))))
	url2 = "/".join(('https://stable-api.varsome.com/lookup',''.join((input,'/hg38?add-all-data=0&add-source-databases=ncbi-dbsnp,refseq-transcripts,dbnsfp,dbnsfp-dbscsnv&add-ACMG-annotation=1'))))
	headers = CaseInsensitiveDict()
	headers["Accept"] = "application/json"
	headers["user-agent"] = "VarSomeApiClientPython/2.0"
	headers["Authorization"] = "Token " + "so9N8mY0?liIn3k@c470r#6WU!VRllW3MT4CFcjV"
	resp = requests.get(url2, headers=headers)
	print(url2)
	print('resp status code is', resp.status_code)
	if resp.status_code!=200:
		# print('i am ahbere', index)
		url3 = "/".join(('https://stable-api.varsome.com/lookup',''.join((input2,'/hg38?add-all-data=0&add-source-databases=ncbi-dbsnp,refseq-transcripts,dbnsfp,dbnsfp-dbscsnv&add-ACMG-annotation=1'))))
		headers = CaseInsensitiveDict()
		headers["Accept"] = "application/json"
		headers["user-agent"] = "VarSomeApiClientPython/2.0"
		headers["Authorization"] = "Token " + "so9N8mY0?liIn3k@c470r#6WU!VRllW3MT4CFcjV"
		resp = requests.get(url3, headers=headers)
		# try:
		# 	resp = requests.get(url3, headers=headers)
		# except:
		# 	print('resp is ', resp, resp.text, index)
		# 	lista_problems.append(index)
		# 	pass
	if resp.status_code==200:
		data = json.loads(resp.text)
		al_freq = {}
		_type_ = str((type(data)))
		try:
			if 'refseq_transcripts' in data[0].keys():
				strand = data[0]['refseq_transcripts'][0]['items'][0]['strand']
				if (str(strand) == '-'):
					strand = -1
				else:
					strand = 1
				print('strand isssss', strand)
				consequence = data[0]['refseq_transcripts'][0]['items'][0]['coding_impact']
				chr = data[0]['chromosome']
				position = data[0]['pos']
				ref = data[0]['ref']
				alt = data[0]['alt']
				if (len(ref) == 0):
					start = position - 1
					end = position + len(alt)
					ref = '-'
					distance = len(alt)
					seqresult = getsequence(chr, start + 1, end, strand, distance)
					ref_pvs1 = seqresult[0]
					alt_pvs1 = seqresult
					hgvs_pvs1 = str(chr[3:]) + '-' + str(position) + '-' + str(ref_pvs1) + '-' + str(alt_pvs1)
					print('get sequence function', seqresult, ref_pvs1, alt_pvs1)
					hgvs = str(chr) + ':' + str(start) + '-' + str(end) + ':' + str(ref) + '/' + str(alt)
				elif (len(alt) == 0):
					start = position
					end = position + len(ref)
					alt = '-'
					distance = len(ref)
					seqresult = getsequence(chr, start - 1, end, strand, distance)
					ref_pvs1 = seqresult
					alt_pvs1 = seqresult[0]
					hgvs_pvs1 = str(chr[3:]) + '-' + str(position) + '-' + str(ref_pvs1) + '-' + str(alt_pvs1)
					print('get sequence function', seqresult, ref_pvs1, alt_pvs1)
					hgvs = str(chr) + ':' + str(start) + '-' + str(end) + ':' + str(ref) + '/' + str(alt)
				else:
					ref_pvs1 = ref
					alt_pvs1 = alt
					hgvs_pvs1 = str(chr[3:]) + '-' + str(position) + '-' + str(ref_pvs1) + '-' + str(alt_pvs1)
					hgvs = str(chr) + ':' + str(position) + '-' + str(position) + ':' + str(ref) + '/' + str(alt)
				SAMPLE.loc[index, 'HGVS'] = str(hgvs)
				SAMPLE.loc[index, 'strand'] = strand
				SAMPLE.loc[index, 'HGVS_PVS1'] = hgvs_pvs1
				SAMPLE.loc[index, 'CHROM'] = str(chr)
				SAMPLE.loc[index, 'POS'] = str(position)
		except KeyError:
		# except:
			if 'refseq_transcripts' in data.keys():
				strand = data['refseq_transcripts'][0]['items'][0]['strand']
				if (str(strand) == '-'):
					strand = -1
				else:
					strand = 1
				print('strand isssss 22222', strand)
				consequence = data['refseq_transcripts'][0]['items'][0]['coding_impact']
				chr = data['chromosome']
				position = data['pos']
				ref = data['ref']
				alt = data['alt']
				if (len(ref) == 0):
					start = position - 1
					end = position + len(alt)
					ref = '-'
					distance = len(alt)
					seqresult = getsequence(chr, start + 1, end, strand, distance)
					ref_pvs1 = seqresult[0]
					alt_pvs1 = seqresult
					hgvs_pvs1 = str(chr[3:]) + '-' + str(position) + '-' + str(ref_pvs1) + '-' + str(alt_pvs1)
					print('get sequence function', seqresult, ref_pvs1, alt_pvs1)
					hgvs = str(chr) + ':' + str(start) + '-' + str(end) + ':' + str(ref) + '/' + str(alt)
				elif (len(alt) == 0):
					start = position
					end = position + len(ref)
					alt = '-'
					distance = len(ref)
					seqresult = getsequence(chr, start - 1, end, strand, distance)
					ref_pvs1 = seqresult
					alt_pvs1 = seqresult[0]
					hgvs_pvs1 = str(chr[3:]) + '-' + str(position) + '-' + str(ref_pvs1) + '-' + str(alt_pvs1)
					print('get sequence function', seqresult, ref_pvs1, alt_pvs1)
					hgvs = str(chr) + ':' + str(start) + '-' + str(end) + ':' + str(ref) + '/' + str(alt)
				else:
					ref_pvs1 = ref
					alt_pvs1 = alt
					hgvs_pvs1 = str(chr[3:]) + '-' + str(position) + '-' + str(ref_pvs1) + '-' + str(alt_pvs1)
					hgvs = str(chr) + ':' + str(position) + '-' + str(position) + ':' + str(ref) + '/' + str(alt)
				SAMPLE.loc[index, 'HGVS'] = str(hgvs)
				SAMPLE.loc[index, 'strand'] = strand
				SAMPLE.loc[index, 'HGVS_PVS1'] = hgvs_pvs1
				SAMPLE.loc[index, 'CHROM'] = str(chr)
				SAMPLE.loc[index, 'POS'] = str(position)
		if _type_ == '<class \'dict\'>':
			data = data
		else:
			if len([x for x in data if x['chromosome'] == SAMPLE.loc[index, 'HGVS'].split(':')[0]])!= 0:
				data = [x for x in data if x['chromosome'] == SAMPLE.loc[index, 'HGVS'].split(':')[0]][0]
			else:
				data = {}
				if str(SAMPLE.loc[index, 'comments'])=='nan':
					SAMPLE.loc[index, 'comments'] = 'Exception: cromosoma non trovato'
				else:
					SAMPLE.loc[index, 'comments'] = SAMPLE.loc[index, 'comments'] + ', Exception: cromosoma non trovato'
		# print('dataaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')
		# print (data.keys())
		if 'acmg_annotation' in data.keys():
			classification = []
			acmg_verdict = data['acmg_annotation']['verdict']['ACMG_rules']['verdict']
			APIversion = data['acmg_annotation']['version_name']
			acmg_classifications = data['acmg_annotation']['classifications']
			for rule in acmg_classifications:
				p = rule['name']
				if 'strength' in rule.keys():
					s = rule['strength']
					if str(SAMPLE.loc[index, 'comments'])=='nan':
						SAMPLE.loc[index, 'comments'] = 'Point ' + p + ': strenght different from default'
					else:
						SAMPLE.loc[index, 'comments'] = SAMPLE.loc[index, 'comments'] + ', Point ' + p + ': strenght different from default'
				else:
					s = strength_dict[p]
				classification.append((p, s, rule['user_explain'][0]))
		if 'publications' in data.keys():
			publications = data['publications']
		if 'ncbi_clinvar2' in data.keys():
			clinvar_dict = []
			for k in data['ncbi_clinvar2'][0].keys():
				val = data['ncbi_clinvar2'][0][k]
				clinvar_dict.append((k, val))
			clinvar2 = clinvar_dict
		if 'saphetor_known_pathogenicity' in data.keys():
			saphetor = data['saphetor_known_pathogenicity'][0]['items'][0]['annotations']
		if 'dbnsfp' in data.keys():
			# print('yesssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss dbnsp in data keys')
			#print(data['dbnsfp'][0]['revel_score'])
			if 'revel_score' in data['dbnsfp'][0].keys():
				if (data['dbnsfp'][0]['revel_score'] is None) == False:
					revel_score = data['dbnsfp'][0]['revel_score']
					revel_score =  float(revel_score)
			if 'sift_score' in data['dbnsfp'][0].keys():
				if (data['dbnsfp'][0]['sift_score'][0] is None) == False:
					sift_score = data['dbnsfp'][0]['sift_score'][0]
					sift_score =  float(sift_score)
		if 'dbnsfp_premium' in data.keys():
			if 'polyphen2_hdiv_score' in data['dbnsfp_premium'][0].keys():
				if (data['dbnsfp_premium'][0]['polyphen2_hdiv_score'][0] is None) == False:
					polyphen2_hdiv_score = data['dbnsfp_premium'][0]['polyphen2_hdiv_score'][0]
					polyphen2_hdiv_score =  float(polyphen2_hdiv_score)
			if 'polyphen2_hvar_score' in data['dbnsfp_premium'][0].keys():
				if (data['dbnsfp_premium'][0]['polyphen2_hvar_score'][0] is None) == False:
					polyphen2_hvar_score = data['dbnsfp_premium'][0]['polyphen2_hvar_score'][0]
					polyphen2_hvar_score =  float(polyphen2_hvar_score)
		if 'cadd' in data.keys():
			if (data['cadd'][0]['cadd_score'] is None) == False:
				cadd_score = (data['cadd'][0]['cadd_score'])
				cadd_score =  float(cadd_score)
		if 'dbnsfp_dbscsnv' in data.keys():
			#print(data['dbnsfp_dbscsnv'])
			if 'rf_score' in data['dbnsfp_dbscsnv'][0].keys():

				#print(data['dbnsfp_dbscsnv'][0])
				if (data['dbnsfp_dbscsnv'][0]['rf_score'] is None) == False:
					rf_score = data['dbnsfp_dbscsnv'][0]['rf_score']
					rf_score = float(rf_score[0])
			if 'ada_score' in data['dbnsfp_dbscsnv'][0].keys():
				# print(data['dbnsfp_dbscsnv'])
				if (data['dbnsfp_dbscsnv'][0]['ada_score'] is None) == False:
					ada_score = data['dbnsfp_dbscsnv'][0]['ada_score']
					ada_score = float(ada_score[0])
		if 'gnomad_exomes' in data.keys():
			if 'af' in data['gnomad_exomes'][0].keys():
				tot_af = data['gnomad_exomes'][0]['af']
			else:
				tot_af = None
			if 'an' in data['gnomad_exomes'][0].keys():
				tot_an = data['gnomad_exomes'][0]['an']
			else:
				tot_an = None
			if 'ac_afr' in data['gnomad_exomes'][0].keys():
				al_freq['afr'] = {'ac': data['gnomad_exomes'][0]['ac_afr'], 'an': data['gnomad_exomes'][0]['an_afr']}
			if 'ac_amr' in data['gnomad_exomes'][0].keys():
				al_freq['amr'] = {'ac': data['gnomad_exomes'][0]['ac_amr'], 'an': data['gnomad_exomes'][0]['an_amr']}
			if 'ac_eas' in data['gnomad_exomes'][0].keys():
				al_freq['eas'] = {'ac': data['gnomad_exomes'][0]['ac_eas'], 'an': data['gnomad_exomes'][0]['an_eas']}
			if 'ac_nfe' in data['gnomad_exomes'][0].keys():
				al_freq['nfe'] = {'ac': data['gnomad_exomes'][0]['ac_nfe'], 'an': data['gnomad_exomes'][0]['an_nfe']}
			if 'ac_sas' in data['gnomad_exomes'][0].keys():
				al_freq['sas'] = {'ac': data['gnomad_exomes'][0]['ac_sas'], 'an': data['gnomad_exomes'][0]['an_sas']}
			al_freq = pd.DataFrame.from_dict(al_freq, orient='index')
			if 'ac' in al_freq.keys():
				al_freq['af'] = al_freq['ac']/al_freq['an']
			if 'af' in al_freq.keys():
				an = al_freq[al_freq['af']==al_freq['af'].max()].iloc[:,0][0]
				ac = al_freq[al_freq['af']==al_freq['af'].max()].iloc[:,1][0]
				al_freq = al_freq[al_freq.an>=2000]
				maf = al_freq['af'].max() #nm['max_maf_ba1']
				# if max_maf!=maf:
				# 	nm['comments'] = 'Not taken the maximum pop MAF because it did not have at least 2000 alleles'
				definitivemaf2 = maf
				definitivemaf2_description = 'VARSOME gnomAD Exome pop MAF'
				definitivemaf2_info = 'VARSOME_popmax'
			else:
				ac = None
				an = None
				definitivemaf2 = tot_af
				definitivemaf2_description = 'VARSOME gnomAD Exome Allele Frequency'
				definitivemaf2_info = 'VARSOME_gnomAD'
		else:
			tot_af = None
			ac = None
			an = None
			tot_an = None
			maf = None
			definitivemaf2 = None
			definitivemaf2_description = None
			definitivemaf2_info = None

	SAMPLE.loc[index, 'acmg_verdict'] = str(acmg_verdict)
	try: SAMPLE.loc[index, 'acmg_classifications'] = str(classification)
	except: SAMPLE.loc[index, 'acmg_classifications'] = 'Exception da Verificate!!!'
	SAMPLE.loc[index, 'definitivemaf2'] = definitivemaf2
	SAMPLE.loc[index, 'decisionINFO'] = definitivemaf2_info
	if definitivemaf2_description != None:
		if str(SAMPLE.loc[index, 'comments']) != 'nan':
			SAMPLE.loc[index, 'comments'] = str(SAMPLE.loc[index, 'comments']) + ', ' + definitivemaf2_description
		else:
			SAMPLE.loc[index, 'comments'] = definitivemaf2_description
	SAMPLE.loc[index, 'definitivemaf2'] = np.where(definitivemaf2 == None, SAMPLE.loc[index,'decisionmaf'], definitivemaf2 )
	SAMPLE.loc[index, 'decisionINFO'] = np.where(definitivemaf2 == None, SAMPLE.loc[index, 'decisionINFO'] , definitivemaf2_info )
	SAMPLE.loc[index, 'decisionmaf'] = SAMPLE.loc[index, 'definitivemaf2']
	SAMPLE.loc[index, 'publications'] = str(publications)
	# try:
	SAMPLE.loc[index, 'consequence'] = str(consequence)
	SAMPLE.loc[index, 'REF'] = str(ref)
	SAMPLE.loc[index, 'ALT'] = str(alt)
	# except:
	# 	pass
	SAMPLE.loc[index, 'clinvar2'] = str(clinvar2)
	SAMPLE.loc[index, 'saphetor'] = str(saphetor)
	SAMPLE.loc[index,'revel_score'] = float(revel_score)
	SAMPLE.loc[index,'cadd_score'] = float(cadd_score)
	SAMPLE.loc[index,'ada_score'] = float(ada_score)
	SAMPLE.loc[index,'rf_score'] = float(rf_score)
	SAMPLE.loc[index,'sift_score'] = float(sift_score)
	SAMPLE.loc[index,'polyphen2_hdiv_score'] = float(polyphen2_hdiv_score)
	SAMPLE.loc[index,'polyphen2_hvar_score'] = float(polyphen2_hvar_score)
	SAMPLE.loc[index,'strand'] = int(strand)
	SAMPLE.loc[index,'HGVS_PVS1'] = str(hgvs_pvs1)
	SAMPLE.loc[index,'AMP_verdict'] = '-----'
	SAMPLE.loc[index,'AMP_classification'] = '-----'
	SAMPLE.loc[index,'drug_verdict'] = '-----'
	SAMPLE.loc[index,'drug_classification'] = '-----'
	try: SAMPLE.loc[index, 'APIversion'] = str(APIversion)
	except: SAMPLE.loc[index, 'APIversion'] = ''
	# print ('heheehhhhhhhhhhhhhhhhhhhhhhhhhhh')
	# print (SAMPLE['REF'])
	return SAMPLE

def query_varsome_gene(GENE,SAMPLE,index):
	#url = "/".join(('https://api.varsome.com/lookup/gene', GENE, 'hg38?add-source-databases=gnomad_genes'))
	# url = "/".join(('https://stable-api.varsome.com/lookup/gene', GENE, 'hg38?add-source-databases=gnomad_genes'))
	url = "/".join(('https://stable-api.varsome.com/lookup/gene', GENE, 'hg38?add-all-data=0&add-source-databases=gnomad_genes,cgd'))
	headers = CaseInsensitiveDict()
	headers["Accept"] = "application/json"
	headers["user-agent"] = "VarSomeApiClientPython/2.0"
	headers["Authorization"] = "Token " + "so9N8mY0?liIn3k@c470r#6WU!VRllW3MT4CFcjV"
	resp = requests.get(url, headers=headers)
	try:
		data = json.loads(resp.text)
		data_df = json_normalize(data['gnomad_genes']['items'])
		mis_z = data_df['mis_z'].astype(float)
		lof_z = data_df['lof_z'].astype(float)
		cgd = ''
		variant_orig = []
		if 'cgd' in data.keys():
			if 'inheritance' in data['cgd'].keys():
				cgd = data['cgd']['inheritance']
				inheritance_dict = {'Digenic':'DIG', 'Digenic recessive':'DIG','Digenic dominant':'DIG','digenic':'DIG'}
				cgd = '/'.join(pd.Series(cgd.split('/')).replace(inheritance_dict, regex=True).unique().tolist())
	except:
		mis_z = -999
		lof_z = -999
		cgd = ''
	SAMPLE.loc[index,'mis_z'] = float(mis_z)
	SAMPLE.loc[index,'lof_z'] = float(lof_z)
	SAMPLE.loc[index,'allinheritance'] = cgd
	return SAMPLE

def varsome_consequences(GENE, SAMPLE, index):
	#print('Function varsome_consequences')
	try:
		gene_res = pd.read_csv(''.join([gene_varsome,GENE, '.csv']), header=0, sep='\t')
		gene_stats = pd.read_csv(''.join([gene_varsome,GENE, '_clinical_statistics.csv']), header=None, sep='\t')
		SAMPLE = BP1(gene_stats, SAMPLE, index)
		SAMPLE = BS1(gene_res, SAMPLE, index)
		SAMPLE = BA1(gene_res, SAMPLE, index)
	except:
		SAMPLE.loc[index, 'comments'] = ('Files about gene '+ GENE + ' not found')
		SAMPLE.loc[index,'BA1'] = None
		SAMPLE.loc[index,'pop_allele_count'] = None
		SAMPLE.loc[index,'BP1_1'] = -999
		SAMPLE.loc[index,'BP1_2'] = -999
		SAMPLE.loc[index,'BP1_3'] = -999
		SAMPLE.loc[index,'BS1'] = 0.0005
	return SAMPLE


def reinterpretation_verdict(SAMPLE):
	print('reinterpretation_verdict')
	SAMPLE_verdict = SAMPLE[['sample','HGVS', 'CHROM', 'POS', 'REF', 'ALT', 'GENE', 'strand', 'HGVS_PVS1', 'acmg_verdict','acmg_classifications', 'consequence','ada_score','rf_score','revel_score',
	'cadd_score', 'sift_score', 'polyphen2_hdiv_score', 'polyphen2_hvar_score', 'var_on_gene','allinheritance','zigosita', 'annotation', 'predictors_decision', 'pred_finalresult']]
	# SAMPLE_verdict = SAMPLE[['sample','HGVS', 'GENE','acmg_verdict','acmg_classifications', 'ada_score','rf_score','revel_score','var_on_gene','allinheritance','zigosita']]
	SAMPLE_verdict.rename(columns={'sample':'sample_id'})
	SAMPLE_model = pd.DataFrame(columns=['sample_id','HGVS', 'GENE', 'BA1_final', 'BA1_intensita_final', 'BA1_cause_final', 'BS1_final', 'BS1_intensita_final', 'BS1_cause_final',
	'BS2_final', 'BS2_intensita_final', 'BS2_cause_final', 'BS3_final', 'BS3_intensita_final', 'BS3_cause_final', 'BS4_final', 'BS4_intensita_final', 'BS4_cause_final', 'BP1_final', 'BP1_intensita_final', 'BP1_cause_final',
	 'BP2_final', 'BP2_intensita_final', 'BP2_cause_final',  'BP3_final', 'BP3_intensita_final', 'BP3_cause_final', 'BP4_final', 'BP4_intensita_final', 'BP4_cause_final', 'BP5_final', 'BP5_intensita_final', 'BP5_cause_final',
	 'BP6_final', 'BP6_intensita_final', 'BP6_cause_final', 'BP7_final', 'BP7_intensita_final', 'BP7_cause_final', 'PVS1_final', 'PVS1_intensita_final', 'PVS1_cause_final', 'PS1_final', 'PS1_intensita_final', 'PS1_cause_final',
	 'PS2_final', 'PS2_intensita_final', 'PS2_cause_final', 'PS3_final', 'PS3_intensita_final', 'PS3_cause_final', 'PS4_final', 'PS4_intensita_final', 'PS4_cause_final', 'PM1_final', 'PM1_intensita_final', 'PM1_cause_final',
	 'PM2_final', 'PM2_intensita_final', 'PM2_cause_final', 'PM3_final', 'PM3_intensita_final', 'PM3_cause_final', 'PM4_final', 'PM4_intensita_final', 'PM4_cause_final', 'PM5_final', 'PM5_intensita_final', 'PM5_cause_final',
	 'PM6_final', 'PM6_intensita_final', 'PM6_cause_final', 'PP1_final', 'PP1_intensita_final', 'PP1_cause_final','PP2_final', 'PP2_intensita_final', 'PP2_cause_final', 'PP3_final', 'PP3_intensita_final', 'PP3_cause_final',
	 'PP3_final_old', 'PP3_intensita_final_old', 'PP3_cause_final_old','PVS1_final_old', 'PVS1_intensita_final_old', 'PVS1_cause_final_old', 'BP4_final_old', 'BP4_intensita_final_old', 'BP4_cause_final_old', 'cadd_score',
	 'revel_score', 'ada_score', 'rf_score', 'sift_score', 'polyphen2_hdiv_score', 'polyphen2_hvar_score',
	 'PP4_final', 'PP4_intensita_final', 'PP4_cause_final', 'PP5_final', 'PP5_intensita_final', 'PP5_cause_final', 'choice_interpretation_final','vusstatus','PMPOT_final','PMPOT_intensita_final','PMPOT_cause_final','PPPOT_final','PPPOT_intensita_final','PPPOT_cause_final', 'choice_interpretation'])
	SAMPLE_model[['sample_id','HGVS', 'GENE', 'choice_interpretation','var_on_gene','allinheritance','zigosita', 'annotation', 'ada_score', 'rf_score', 'revel_score', 'cadd_score', 'sift_score', 'polyphen2_hdiv_score', 'polyphen2_hvar_score']] = SAMPLE[['sample','HGVS', 'GENE','acmg_verdict',
	'var_on_gene','allinheritance','zigosita', 'annotation', 'ada_score', 'rf_score', 'revel_score', 'cadd_score', 'sift_score', 'polyphen2_hdiv_score', 'polyphen2_hvar_score']]
	# print(SAMPLE_model)
	sospetti_omim='/home/bioinfo/VIRTUAL38/MAGIWEB/DATA1/sospetti-omim.csv'
	SAMPLE_model = fourth_b.fill_checklist_model(SAMPLE_model, SAMPLE_verdict,sospetti_omim)
	# print(SAMPLE_model.columns)
	#print(SAMPLE_model[SAMPLE_model['choice_interpretation']!=SAMPLE_model['choice_interpretation_final']])
	SAMPLE['vusstatus'] = SAMPLE_model['vusstatus']
	SAMPLE[['PP3_final', 'PP3_cause_final']] = SAMPLE_model[['PP3_final', 'PP3_cause_final']]
	SAMPLE[['BP4_final', 'BP4_cause_final']] = SAMPLE_model[['BP4_final', 'BP4_cause_final']]
	SAMPLE[['PM2_final', 'PM2_cause_final']] = SAMPLE_model[['PM2_final', 'PM2_cause_final']]
	SAMPLE[['PMPOT_final','PMPOT_intensita_final','PMPOT_cause_final','PPPOT_final','PPPOT_intensita_final','PPPOT_cause_final']] = SAMPLE_model[['PMPOT_final','PMPOT_intensita_final','PMPOT_cause_final','PPPOT_final','PPPOT_intensita_final','PPPOT_cause_final']]
	SAMPLE[['autopvs1','PVS1_final', 'PVS1_cause_final', 'spliceAI','PVS1_intensita_final']] = SAMPLE_model[['autopvs1','PVS1_final', 'PVS1_cause_final', 'spliceAI','PVS1_intensita_final']]
	SAMPLE['acmg_verdict_varsome'] = SAMPLE['acmg_verdict']
	SAMPLE['choice_interpretation_final'] = SAMPLE_model['choice_interpretation_final']
	# print(SAMPLE.loc[SAMPLE['acmg_verdict'] != SAMPLE['choice_interpretation_final'],'choice_interpretation_final'])
	SAMPLE.loc[SAMPLE['acmg_verdict']!=SAMPLE['choice_interpretation_final'],'acmg_verdict'] = SAMPLE.loc[SAMPLE['acmg_verdict'] != SAMPLE['choice_interpretation_final'],'choice_interpretation_final']
	#print(SAMPLE[['GENE', 'acmg_verdict_varsome', 'acmg_verdict', 'choice_interpretation_final']])
	punti = ['BA1', 'BS1', 'BS2', 'BS3', 'BS4', 'BP1', 'BP2', 'BP3', 'BP4', 'BP5', 'BP6', 'BP7','PVS1', 'PS1', 'PS2', 'PS3', 'PS4', 'PM1', 'PM2', 'PM3', 'PM4', 'PM5', 'PM6', 'PP1', 'PP2', 'PP3', 'PP4', 'PP5','PMPOT','PPPOT']
	# pat_model[p] = {'Presenza': variant.loc[index, p+'_final'], 'Intensita': variant.loc[index, p+'_intensita_final'], 'Causa': variant.loc[index, p+'_cause_final']}
	SAMPLE_model['acmg_classification_final'] = ''
	# SAMPLE_model['comments_verdict'] = ''
	for index, row in SAMPLE_model.iterrows():
		classification = []
		#print(SAMPLE_model.loc[index,'HGVS'])
		# print(SAMPLE_model.loc[index,'PVS1_cause_final'])
		for p in punti:
			# print(p)

			if str(SAMPLE_model.loc[index,p + '_final']) == '1':
				# print(SAMPLE_model.loc[index,p + '_cause_final'])
				classification.append((p, SAMPLE_model.loc[index,p + '_intensita_final'], SAMPLE_model.loc[index,p + '_cause_final']))
			elif (p=='PVS1') & (str(SAMPLE_model.loc[index,p + '_cause_final']) != 'nan'):
				# print(SAMPLE_model.loc[index,'comments_verdict'])
				SAMPLE_model.loc[index,'comments_verdict'] = ' ' +SAMPLE_model.loc[index,'comments_verdict'] + ' ' + SAMPLE_model.loc[index,p + '_cause_final']
				# print(SAMPLE_model.loc[index,'comments_verdict'])
		# print(classification)
		SAMPLE_model.loc[index, 'acmg_classification_final'] = str(classification)
	# print(SAMPLE_model[['sample_id','HGVS', 'GENE','acmg_classifications_final']])
	# print(SAMPLE_verdict[['sample_id','HGVS', 'GENE']])
	SAMPLE_model = SAMPLE_model.rename(columns={'sample_id':'sample'})
	SAMPLE = pd.merge(SAMPLE, SAMPLE_model[['sample','HGVS', 'GENE','acmg_classification_final','comments_verdict']], how='left', on=['sample','HGVS', 'GENE'])
	# print(SAMPLE['comments'])
	SAMPLE['comments'].fillna('',inplace=True)
	SAMPLE['comments'] = SAMPLE['comments'] + SAMPLE['comments_verdict']
	# print(SAMPLE['comments'])
	SAMPLE['acmg_classification_varsome'] = SAMPLE['acmg_classifications']
	SAMPLE['acmg_classifications'] = SAMPLE['acmg_classification_final']

	return SAMPLE, SAMPLE_model


def set_var_PMPOT(SAMPLE):
	print('set_var_PMPOT', SAMPLE)
	SAMPLE['var_on_gene'] = 0
	SAMPLE['acmg_verdict'] = SAMPLE['acmg_verdict'].fillna('Empty')
	for index, row in SAMPLE.iterrows():
		SAMPLE_P = SAMPLE[(~(SAMPLE['acmg_verdict'].str.contains('Benign')))]
		SAMPLE.loc[index,'var_on_gene'] = len(SAMPLE_P[SAMPLE_P['GENE']==row.GENE])
	return SAMPLE
