#!/home/bioinfo/VIRTUAL38/bin/python3.8

# -*- coding: utf-8 -*-
import os,re,glob,csv,argparse,datetime,sys,subprocess
from os.path import isfile, join
import numpy as np
import pandas as pd
import ngs.api.fourth_b_diagnosys_variantinterpretation as fourth_b
import os.path
from varsome_api.client import VarSomeAPIClient,VarSomeAPIException
from varsome_api.models.variant import AnnotatedVariant
import sqlite3
import requests
from requests.structures import CaseInsensitiveDict
from requests.auth import HTTPBasicAuth
from pandas.io.json import json_normalize
import json
import datetime
import time
import io
import math
path = os.getcwd()
pd.options.display.float_format = '{:,.5f}'.format

geno37 = join('/home/bioinfo/','dataset/GENOME/37/all_chr37.fa')
geno38 = join('/home/bioinfo/','dataset/GENOME/38/all_chr38.fa')

HGMD_37 = join('/home/bioinfo/','dataset/HGMD/37/HGMD_pro_2014_2.bed')
HGMD_38 = join('/home/bioinfo/','dataset/HGMD/38/LiftOverHGMD_pro_2014_2.bed')


def load_omim(sospetti_omim):
	SOSPETTI=pd.read_csv(sospetti_omim,sep='\t',header=0)
	SOSPETTI.fillna('Unkwnow',inplace=True)
	new=SOSPETTI["GENE (geneMimNumber)"].str.split(" ", n = 1, expand = True)
	# making separate first name column from new data frame
	SOSPETTI["GENE"]= new[0]
	# making separate last name column from new data frame
	SOSPETTI["geneMimNumber"]= new[1]
	SOSPETTI.drop(columns =["GENE (geneMimNumber)"], inplace = True)
	return SOSPETTI

def invert_MAF(SAMPLE,cols1, t=0.8):
	if len(SAMPLE) >=1:
		SAMPLE['MAX_MAF'] = SAMPLE['decisionmaf']
		SAMPLE.loc[(SAMPLE['MAX_MAF']>=t) & (SAMPLE['MAX_MAF']!=1),'comments']='MAF>80%: inversion applied'
		SAMPLE.loc[(SAMPLE['MAX_MAF']>=t) & (SAMPLE['MAX_MAF']!=1),'MAX_MAF']=1-SAMPLE.loc[SAMPLE['MAX_MAF']>=t,'MAX_MAF']
		SAMPLE = SAMPLE[(SAMPLE['comments']!='MAF>80%: inversion applied')] #& (SAMPLE['unbalance'].str.split('=').str.get(1).astype(float)<=0.95)]
	else: SAMPLE = pd.DataFrame(columns=cols1)
	return SAMPLE

def cut_MAF(SAMPLE,cols1,t=0.03):
	if len(SAMPLE) >=1:
		SAMPLE.loc[SAMPLE['MAX_MAF']<=t,'MAF<20']=1
		SAMPLE.loc[SAMPLE['MAX_MAF']>t,'MAF<20']=0
		SAMPLE.loc[SAMPLE['MAX_MAF']>t,'Description']='DISCARDED: MAF > 10%'
		# Include in the analysis variants with  unknown MAF
		SAMPLE['MAF<20'].fillna(value=1, inplace=True)
	else: SAMPLE = pd.DataFrame(columns=cols1)
	return SAMPLE

def include_exceptions(SAMPLE, exception):
	SAMPLE['fromFILE'] = 0
	exceptions=pd.read_csv(exception, sep='\t')
	# print(SAMPLE)

	for index, row in exceptions.iterrows():
		if row.HGVS in list(SAMPLE['HGVS']):
			SAMPLE.loc[SAMPLE['HGVS']==row.HGVS,'MAF<20']=1
			SAMPLE.loc[SAMPLE['HGVS']==row.HGVS,'Description']='SELECTED'
			SAMPLE.loc[SAMPLE['HGVS']==row.HGVS,'MAF_selection']=1
			SAMPLE.loc[SAMPLE['HGVS']==row.HGVS,'comments']='Selected from exception'
		# if row.rsID in list(SAMPLE['ID']):
		# 	SAMPLE.loc[SAMPLE['ID']==row.rsID,'MAF<20']=1
		# 	SAMPLE.loc[SAMPLE['ID']==row.rsID,'Description']='SELECTED'
		# 	SAMPLE.loc[SAMPLE['ID']==row.rsID,'MAF_selection']=1
		# 	SAMPLE.loc[SAMPLE['ID']==row.rsID,'comments']='Selected from exception'
	return SAMPLE

def MAF_t(SAMPLE,SOSPETTI,genes,ds,PREV_MAF,PREV,cols1):
	SAMPLE['MAF_selection']=0
	SAMPLE['MAF_selection_AD']=0
	SAMPLE['MAF_selection_AR']=0
	if len(SAMPLE) >=1:
		SAMPLE.loc[:,'controls_nhomalt'] = SAMPLE['gnomAD_exomes_controls_nhomalt'].astype(float)+SAMPLE['gnomAD_genomes_controls_nhomalt'].astype(float)
		mask1 = SAMPLE['controls_nhomalt'] == -1998.0
		mask2 = SAMPLE['controls_nhomalt'] == -999.0
		SAMPLE.loc[mask1,'controls_nhomalt'] = 0
		SAMPLE.loc[mask2,'controls_nhomalt'] = 0
		SAMPLE.reset_index(drop=True, inplace=True)
		for index, row in SAMPLE.iterrows():
				if row['GENE'] in genes:
					inheritance=SOSPETTI[SOSPETTI['GENE']==row['GENE']]['phenotypeInheritance'].unique()
					_allphenotype_ = SOSPETTI[SOSPETTI['GENE']==row['GENE']]['phenotype (phenotypeMimNumber)'].unique()
					try: prevalence=PREV[PREV['Sospetto diagnostico']==ds.upper()]['Prevalenza'].item()
					except: prevalence = '1:2000'
					temp=PREV_MAF[PREV_MAF['Frequenza']==prevalence]
					try:
						if (isinstance(inheritance[0], str)) and (inheritance[0].find('X-linked')==-1):
							if row['MAX_MAF']<=0.00025:
								SAMPLE.loc[index,'MAF_selection_AD']=1
							if row['MAX_MAF']<=0.022:
								SAMPLE.loc[index,'MAF_selection_AR']=1
						elif (isinstance(inheritance[0], str)) and (inheritance[0].find('X-linked')!=-1):
							n1=float(prevalence.split(':')[0])
							n2=float(prevalence.split(':')[1])
							if row['MAX_MAF']<=(n1/n2):
								SAMPLE.loc[index,'MAF_selection_AD']=1
								SAMPLE.loc[index,'MAF_selection_AR']=1
								SAMPLE.loc[index,'comments']='X-linked'
						else:
							SAMPLE.loc[index,'comments']='hereditary model is missing!'
							if row['MAX_MAF']<=0.022:
								SAMPLE.loc[index,'MAF_selection_AR']=1
					except:
						SAMPLE.loc[index,'comments']='hereditary model is missing! Not Gene in Data!'
						if row['MAX_MAF']<=0.022:
							SAMPLE.loc[index,'MAF_selection_AR']=1
					SAMPLE.loc[index,'allinheritance'] = str(sorted(inheritance))
					SAMPLE.loc[index,'allphenotype'] = str(sorted(_allphenotype_))

					for item in sorted(inheritance):
						if not item == 'None':
							if 'Autosomal dominant' in item:
								SAMPLE['MAF_selection']= np.where(SAMPLE['MAF_selection']==1,1,SAMPLE['MAF_selection_AD'])
								SAMPLE.loc[index,'inheritance']= 'AD'
								SAMPLE.loc[index,'prevalence'] = 0.00025
							elif 'X-linked' in item:
								SAMPLE['MAF_selection']= np.where(SAMPLE['MAF_selection']==1,1,SAMPLE['MAF_selection_AR'])
								SAMPLE.loc[index,'inheritance']= 'XL'
								SAMPLE.loc[index,'prevalence'] = 0.022
							elif 'Autosomal recessive' in item:
								SAMPLE['MAF_selection']= np.where(SAMPLE['MAF_selection']==1,1,SAMPLE['MAF_selection_AR'])
								SAMPLE.loc[index,'inheritance']= 'AR'
								SAMPLE.loc[index,'prevalence'] = 0.022
								break
							else:
								SAMPLE.loc[index,'inheritance']= np.nan
					SAMPLE.loc[(SAMPLE['MAF_selection']==0) & (pd.isnull(SAMPLE['Description'])),'Description']='MAF above threshold'
				else:
					print('errore gene')
	else: SAMPLE = pd.DataFrame(columns=cols1)

	SAMPLE['inheritance'].fillna('AR',inplace=True)
	SAMPLE['inheritance'].fillna(0.02200,inplace=True)
	return SAMPLE

def exclude_problemregion(SAMPLE,regioniproblematiche):
	SAMPLE['fromFILE'] = 1
	regioniproblematiche = pd.read_csv(regioniproblematiche,sep='\t',header=0)[['tipo','hgvs','categoria']]
	regioniproblematiche['HGVS'] = regioniproblematiche['hgvs']

	PROBLEMATICA = regioniproblematiche[regioniproblematiche['tipo']== 'PROBLEMATICA']
	NONREFERTABILE = regioniproblematiche[regioniproblematiche['tipo']== 'NON REFERTABILE']
	OMOLOGIA = regioniproblematiche[regioniproblematiche['tipo']=='OMOLOGIA']
	FUNCTIONALBENIGN = regioniproblematiche[(regioniproblematiche['tipo']=='FUNCTIONAL IMPACT') & (regioniproblematiche['categoria'].str.contains('benigna'))]
	FUNCTIONALPATHOG = regioniproblematiche[(regioniproblematiche['tipo']=='FUNCTIONAL IMPACT') & (regioniproblematiche['categoria'].str.contains('patogenetica'))]

	for index, row in PROBLEMATICA.iterrows():
		for hgvs in list(SAMPLE['HGVS']):
			START = int(str(row.HGVS).split(':')[1].split('-')[0])
			END = int(str(row.HGVS).split(':')[1].split('-')[1])
			if ((int(str(hgvs).split(':')[1].split('-')[0]) >= START) & (int(str(hgvs).split(':')[1].split('-')[0]) <= END)):
				SAMPLE.loc[SAMPLE['HGVS']==hgvs,'Description']='NOT SELECTED'
				SAMPLE.loc[SAMPLE['HGVS']==hgvs,'fromFILE']=0
				SAMPLE.loc[SAMPLE['HGVS']==hgvs,'comments']='Reg. Problematica from FILE'

	for index, row in NONREFERTABILE.iterrows():
		for hgvs in list(SAMPLE['HGVS']):
			START = int(str(row.HGVS).split(':')[1].split('-')[0])
			END = int(str(row.HGVS).split(':')[1].split('-')[1])
			if ((int(str(hgvs).split(':')[1].split('-')[0]) >= START) & (int(str(hgvs).split(':')[1].split('-')[0]) <= END)):
				SAMPLE.loc[SAMPLE['HGVS']==hgvs,'Description']='NOT SELECTED'
				SAMPLE.loc[SAMPLE['HGVS']==hgvs,'fromFILE']=0
				SAMPLE.loc[SAMPLE['HGVS']==hgvs,'comments']='Non Refertabile from FILE'
	#### ANCORA DA TESTARE!!!###
	for index, row in OMOLOGIA.iterrows():
		for hgvs in list(SAMPLE['HGVS']):
			START = int(str(row.HGVS).split(':')[1].split('-')[0])
			END = int(str(row.HGVS).split(':')[1].split('-')[1])
			if ((int(str(hgvs).split(':')[1].split('-')[0]) >= START) & (int(str(hgvs).split(':')[1].split('-')[0]) <= END)):
				SAMPLE.loc[SAMPLE['HGVS']==hgvs,'Description']='OMOLOGIA'
				SAMPLE.loc[SAMPLE['HGVS']==hgvs,'fromFILE']=0
				SAMPLE.loc[SAMPLE['HGVS']==hgvs,'comments']='Omologia from FILE'

	for index, row in FUNCTIONALBENIGN.iterrows():
		if str(row.HGVS).split(':')[1] in list(SAMPLE['HGVS'].str.split(':').str.get(1)):
			SAMPLE.loc[SAMPLE['HGVS'].str.contains(str(row.HGVS).split(':')[1]),'Description']='BENIGNA'
			SAMPLE.loc[SAMPLE['HGVS'].str.contains(str(row.HGVS).split(':')[1]),'fromFILE']=0
			SAMPLE.loc[SAMPLE['HGVS'].str.contains(str(row.HGVS).split(':')[1]),'comments']='Benigna from FILE'

	for index, row in FUNCTIONALPATHOG.iterrows():
		if str(row.HGVS).split(':')[1] in list(SAMPLE['HGVS'].str.split(':').str.get(1)):
			SAMPLE.loc[SAMPLE['HGVS'].str.contains(str(row.HGVS).split(':')[1]),'Description']='SELECTED'
			SAMPLE.loc[SAMPLE['HGVS'].str.contains(str(row.HGVS).split(':')[1]),'fromFILE']=1
			SAMPLE.loc[SAMPLE['HGVS'].str.contains(str(row.HGVS).split(':')[1]),'comments']='Patogenetica from FILE'
	return SAMPLE

def make_predictions(SAMPLE,traduttore,traduttore2,PRED,cols1):
	SAMPLE['predictors_decision'] = 'NULL'
	SAMPLE['predictors_B'] = 'NULL'
	SAMPLE['predictors_D'] = 'NULL'

	if len(SAMPLE) >=1:
		P_TABLE=pd.DataFrame()
		for predictor in PRED:
			if predictor+'_pred' in SAMPLE.columns:
				P_TABLE[predictor+'_result']=0
				for index, row in SAMPLE.iterrows():
					item=str(row[predictor+'_pred']).split('&')[0]
					r1 = re.findall(r"^\w",str(row[predictor+'_pred']))
					r2 = re.findall(r"^.&\w",str(row[predictor+'_pred']))

					if r1: item = r1[0]
					elif r2: item = r2[0].split('&')[1]
					else: item = 'n'

					if not str(item): P_TABLE.loc[index,predictor+'_result']='NULL'
					elif str(item)=='nan': P_TABLE.loc[index,predictor+'_result']='NULL'
					elif str(item)=='n': P_TABLE.loc[index,predictor+'_result']='NULL'
					else:
						if predictor == 'MutationTaster':
							if traduttore2[item]=='D': P_TABLE.loc[index,predictor+'_result']='D'
							elif traduttore2[item]=='B': P_TABLE.loc[index,predictor+'_result']='B'
							else: P_TABLE.loc[index,predictor+'_result']='NULL'
						else:
							if traduttore[item]=='D': P_TABLE.loc[index,predictor+'_result']='D'
							elif traduttore[item]=='B': P_TABLE.loc[index,predictor+'_result']='B'
							else: P_TABLE.loc[index,predictor+'_result']='NULL'
			else:
				P_TABLE[predictor+'_result'] = SAMPLE[predictor+'_rankscore'].astype(float)
				SAMPLE[predictor+'_rankscore']=SAMPLE[predictor+'_rankscore'].astype(float)
				SAMPLE.loc[SAMPLE[predictor+'_rankscore']==float(-999),predictor+'_rankscore']=-999

				if predictor == 'REVEL':
					P_TABLE.loc[(SAMPLE[predictor+'_rankscore']>=0.7),predictor+'_result']='D'
					P_TABLE.loc[(SAMPLE[predictor+'_rankscore']<0.7),predictor+'_result']='B'
					P_TABLE.loc[(SAMPLE[predictor+'_rankscore']==-999),predictor+'_result']='NULL'
					P_TABLE.loc[(SAMPLE[predictor+'_rankscore']==np.NaN),predictor+'_result']='NULL'
					P_TABLE.loc[(SAMPLE[predictor+'_rankscore'].isnull()),predictor+'_result']='NULL'
				elif predictor == 'CADD':
					P_TABLE.loc[(SAMPLE[predictor+'_rankscore']>=0.6),predictor+'_result']='D'
					P_TABLE.loc[(SAMPLE[predictor+'_rankscore']<0.6),predictor+'_result']='B'
					P_TABLE.loc[(SAMPLE[predictor+'_rankscore']==-999),predictor+'_result']='NULL'
					P_TABLE.loc[(SAMPLE[predictor+'_rankscore']==np.NaN),predictor+'_result']='NULL'
					P_TABLE.loc[(SAMPLE[predictor+'_rankscore'].isnull()),predictor+'_result']='NULL'
				else:
					P_TABLE.loc[(SAMPLE[predictor+'_rankscore']>=0.6),predictor+'_result']='D'
					P_TABLE.loc[(SAMPLE[predictor+'_rankscore']<0.6),predictor+'_result']='B'
					P_TABLE.loc[(SAMPLE[predictor+'_rankscore']==-999),predictor+'_result']='NULL'
					P_TABLE.loc[(SAMPLE[predictor+'_rankscore']==np.NaN),predictor+'_result']='NULL'
					P_TABLE.loc[(SAMPLE[predictor+'_rankscore'].isnull()),predictor+'_result']='NULL'

		for index,row in P_TABLE.iterrows():
			benign = 0
			deleterius = 0
			null = 0

			counts = pd.DataFrame(row.value_counts()).T
			try: deleterius=counts['D'].values[0]
			except: deleterius=0
			try: benign=counts['B'].values[0]
			except: benign=0
			try: null=counts['NULL'].values[0]
			except: null=0

			total = deleterius+benign+null
			if int(total)==int(len(PRED)):
				SAMPLE.loc[index,'predictors_B']=(str(benign)+'/'+str(total))
				SAMPLE.loc[index,'predictors_D']=(str(deleterius)+'/'+str(total))
			else: print ('NON TORNANO I CONTI DEI PREDITTORI!!! Verificare!!!')
		SAMPLE.loc[(SAMPLE['predictors_D'].str.split('/').str.get(0).astype(int)>=10),'predictors_decision']='D'
		SAMPLE.loc[(SAMPLE['predictors_B'].str.split('/').str.get(0).astype(int)>=10),'predictors_decision']='B'

	else: SAMPLE = pd.DataFrame(columns=cols1)
	return SAMPLE

def query_varsome_saphetor(input,input2,SAMPLE,index):
	strength_dict={'BA1': 'Stand-alone', 'BS1': 'Strong', 'BS2': 'Strong', 'BS3': 'Strong', 'BS4': 'Strong', 'BP1': 'Supporting', 'BP2': 'Supporting', 'BP3': 'Supporting', 'BP4': 'Supporting', 'BP5': 'Supporting', 'BP6': 'Supporting', 'BP7': 'Supporting',
	'PVS1': 'Very Strong', 'PS1': 'Strong', 'PS2': 'Strong', 'PS3': 'Strong', 'PS4': 'Strong', 'PM1': 'Moderate', 'PM2': 'Moderate', 'PM3': 'Moderate', 'PM4': 'Moderate', 'PM5': 'Moderate', 'PM6': 'Moderate',
	'PP1': 'Supporting','PP2': 'Supporting', 'PP3': 'Supporting','PP4': 'Supporting','PP5': 'Supporting'}
	acmg_verdict = ''
	acmg_classificatins = ''
	publications = ''
	clinvar2 = ''
	saphetor = ''
	APIversion = ''
	rf_score = np.nan
	ada_score = np.nan
	definitivemaf2 = None
	definitivemaf2_info = None
	definitivemaf2_description = None
	SAMPLE['comments'] = np.nan
	SAMPLE['decisionmaf'] =np.nan
	url2 = "/".join(('https://stable-api.varsome.com/lookup',''.join((input,'/hg38?add-all-data=0&add-source-databases=ncbi-dbsnp,refseq-transcripts,dbnsfp-dbscsnv&add-ACMG-annotation=1'))))
	headers = CaseInsensitiveDict()
	headers["Accept"] = "application/json"
	headers["user-agent"] = "VarSomeApiClientPython/2.0"
	headers["Authorization"] = "Token " + "so9N8mY0?liIn3k@c470r#6WU!VRllW3MT4CFcjV"
	resp = requests.get(url2, headers=headers)
	if resp.status_code!=200:
		url3 = "/".join(('https://stable-api.varsome.com/lookup',''.join((input2,'/hg38?add-all-data=0&add-source-databases=ncbi-dbsnp,refseq-transcripts,dbnsfp-dbscsnv&add-ACMG-annotation=1'))))
		headers = CaseInsensitiveDict()
		headers["Accept"] = "application/json"
		headers["user-agent"] = "VarSomeApiClientPython/2.0"
		headers["Authorization"] = "Token " + "so9N8mY0?liIn3k@c470r#6WU!VRllW3MT4CFcjV"
		resp = requests.get(url3, headers=headers)
	if resp.status_code==200:
		data = json.loads(resp.text)
		al_freq = {}
		_type_ = str((type(data)))
		if _type_ == '<class \'dict\'>':
			data = data
		else:
			if len([x for x in data if x['chromosome'] == SAMPLE.loc[index, 'HGVS'].split(':')[0]])!= 0:
				data = [x for x in data if x['chromosome'] == SAMPLE.loc[index, 'HGVS'].split(':')[0]][0]
			else:
				data = {}
				if str(SAMPLE.loc[index, 'comments'])=='nan':
					SAMPLE.loc[index, 'comments'] = 'Exception: cromosoma non trovato'
				else:
					SAMPLE.loc[index, 'comments'] = SAMPLE.loc[index, 'comments'] + ', Exception: cromosoma non trovato'
		if 'acmg_annotation' in data.keys():
			classification = []
			acmg_verdict = data['acmg_annotation']['verdict']['ACMG_rules']['verdict']
			APIversion = data['acmg_annotation']['version_name']
			acmg_classificatins = data['acmg_annotation']['classifications']
			for rule in acmg_classificatins:
				p = rule['name']
				if 'strength' in rule.keys():
					s = rule['strength']
					if str(SAMPLE.loc[index, 'comments'])=='nan':
						SAMPLE.loc[index, 'comments'] = 'Point ' + p + ': strenght different from default'
					else:
						SAMPLE.loc[index, 'comments'] = SAMPLE.loc[index, 'comments'] + ', Point ' + p + ': strenght different from default'
				else:
					s = strength_dict[p]
				classification.append((p, s, rule['user_explain'][0]))
		if 'publications' in data.keys():
			publications = data['publications']
		if 'ncbi_clinvar2' in data.keys():
			clinvar_dict = []
			for k in data['ncbi_clinvar2'][0].keys():
				val = data['ncbi_clinvar2'][0][k]
				clinvar_dict.append((k, val))
			clinvar2 = clinvar_dict
		if 'saphetor_known_pathogenicity' in data.keys():
			saphetor = data['saphetor_known_pathogenicity'][0]['items'][0]['annotations']
		if 'dbnsfp_dbscsnv' in data.keys():
			if 'rf_score' in data['dbnsfp_dbscsnv'][0].keys():
				if (data['dbnsfp_dbscsnv'][0]['rf_score'] is None) == False:
					rf_score = data['dbnsfp_dbscsnv'][0]['rf_score'][0]
					rf_score = float(rf_score)
			if 'ada_score' in data['dbnsfp_dbscsnv'][0].keys():
				if (data['dbnsfp_dbscsnv'][0]['ada_score'] is None) == False:
					ada_score = data['dbnsfp_dbscsnv'][0]['ada_score'][0]
					ada_score = float(ada_score)
		if 'gnomad_exomes' in data.keys():
			if 'af' in data['gnomad_exomes'][0].keys():
				tot_af = data['gnomad_exomes'][0]['af']
			else:
				tot_af = None
			if 'an' in data['gnomad_exomes'][0].keys():
				tot_an = data['gnomad_exomes'][0]['an']
			else:
				tot_an = None
			if 'ac_afr' in data['gnomad_exomes'][0].keys():
				al_freq['afr'] = {'ac': data['gnomad_exomes'][0]['ac_afr'], 'an': data['gnomad_exomes'][0]['an_afr']}
			if 'ac_amr' in data['gnomad_exomes'][0].keys():
				al_freq['amr'] = {'ac': data['gnomad_exomes'][0]['ac_amr'], 'an': data['gnomad_exomes'][0]['an_amr']}
			if 'ac_eas' in data['gnomad_exomes'][0].keys():
				al_freq['eas'] = {'ac': data['gnomad_exomes'][0]['ac_eas'], 'an': data['gnomad_exomes'][0]['an_eas']}
			if 'ac_nfe' in data['gnomad_exomes'][0].keys():
				al_freq['nfe'] = {'ac': data['gnomad_exomes'][0]['ac_nfe'], 'an': data['gnomad_exomes'][0]['an_nfe']}
			if 'ac_sas' in data['gnomad_exomes'][0].keys():
				al_freq['sas'] = {'ac': data['gnomad_exomes'][0]['ac_sas'], 'an': data['gnomad_exomes'][0]['an_sas']}
			al_freq = pd.DataFrame.from_dict(al_freq, orient='index')
			if 'ac' in al_freq.keys():
				al_freq['af'] = al_freq['ac']/al_freq['an']
			if 'af' in al_freq.keys():
				an = al_freq[al_freq['af']==al_freq['af'].max()].iloc[:,0][0]
				ac = al_freq[al_freq['af']==al_freq['af'].max()].iloc[:,1][0]
				al_freq = al_freq[al_freq.an>=2000]
				maf = al_freq['af'].max() #nm['max_maf_ba1']
				definitivemaf2 = maf
				definitivemaf2_description = 'VARSOME gnomAD Exome pop MAF'
				definitivemaf2_info = 'VARSOME_popmax'
			else:
				ac = None
				an = None
				definitivemaf2 = tot_af
				definitivemaf2_description = 'VARSOME gnomAD Exome Allele Frequency'
				definitivemaf2_info = 'VARSOME_gnomAD'
		else:
			tot_af = None
			ac = None
			an = None
			tot_an = None
			maf = None
			definitivemaf2 = None
			definitivemaf2_description = None
			definitivemaf2_info = None
	SAMPLE.loc[index, 'acmg_verdict'] = str(acmg_verdict)
	try: SAMPLE.loc[index, 'acmg_classificatins'] = str(classification)
	except: SAMPLE.loc[index, 'acmg_classificatins'] = 'Exception da Verificate!!!'
	SAMPLE.loc[index, 'definitivemaf2'] = definitivemaf2
	SAMPLE.loc[index, 'decisionINFO'] = definitivemaf2_info
	if definitivemaf2_description != None:
		if str(SAMPLE.loc[index, 'comments']) != 'nan':
			SAMPLE.loc[index, 'comments'] = str(SAMPLE.loc[index, 'comments']) + ', ' + definitivemaf2_description
		else:
			SAMPLE.loc[index, 'comments'] = definitivemaf2_description
	SAMPLE.loc[index, 'definitivemaf2'] = np.where(definitivemaf2 == None, SAMPLE.loc[index,'decisionmaf'], definitivemaf2 )
	SAMPLE.loc[index, 'decisionINFO'] = np.where(definitivemaf2 == None, SAMPLE.loc[index, 'decisionINFO'] , definitivemaf2_info )
	SAMPLE.loc[index, 'decisionmaf'] = SAMPLE.loc[index, 'definitivemaf2']
	SAMPLE.loc[index, 'publications'] = str(publications)
	SAMPLE.loc[index, 'clinvar2'] = str(clinvar2)
	SAMPLE.loc[index, 'saphetor'] = str(saphetor)
	SAMPLE.loc[index,'ada_score'] = float(ada_score)
	SAMPLE.loc[index,'rf_score'] = float(rf_score)
	SAMPLE.loc[index, 'APIversion'] = str(APIversion)
	return SAMPLE

def query_varsome_gene(GENE,SAMPLE,index):
	url = "/".join(('https://stable-api.varsome.com/lookup/gene', GENE, 'hg38?add-source-databases=gnomad_genes'))
	headers = CaseInsensitiveDict()
	headers["Accept"] = "application/json"
	headers["user-agent"] = "VarSomeApiClientPython/2.0"
	headers["Authorization"] = "Token " + "so9N8mY0?liIn3k@c470r#6WU!VRllW3MT4CFcjV"
	resp = requests.get(url, headers=headers)
	try:
		data = json.loads(resp.text)
		data_df = json_normalize(data['gnomad_genes']['items'])
		mis_z = data_df['mis_z'].astype(float)
		lof_z = data_df['lof_z'].astype(float)
	except:
		mis_z = -999
		lof_z = -999
	SAMPLE.loc[index,'mis_z'] = float(mis_z)
	SAMPLE.loc[index,'lof_z'] = float(lof_z)
	return SAMPLE

def varsome_consequences(GENE, SAMPLE, index):
	try:
		gene_res = pd.read_csv(''.join([gene_varsome,GENE, '.csv']), header=0, sep='\t')
		gene_stats = pd.read_csv(''.join([gene_varsome,GENE, '_clinical_statistics.csv']), header=None, sep='\t')
		SAMPLE = BP1(gene_stats, SAMPLE, index)
		SAMPLE = BS1(gene_res, SAMPLE, index)
		SAMPLE = BA1(gene_res, SAMPLE, index)
		tmp = gene_res[gene_res['hgvs'] == SAMPLE.loc[index, 'HGVS']]
		if tmp.empty:
			SAMPLE.loc[index, 'ID'] = -999
		else:
			SAMPLE.loc[index, 'ID'] = tmp.iloc[0, 5]

	except:
		#print('Files about gene '+ GENE + ' not found')
		SAMPLE.loc[index, 'comments'] = ('Files about gene '+ GENE + ' not found')
		SAMPLE.loc[index,'BA1'] = None
		SAMPLE.loc[index,'pop_allele_count'] = -999
		SAMPLE.loc[index,'BP1_1'] = -999
		SAMPLE.loc[index,'BP1_2'] = -999
		SAMPLE.loc[index,'BP1_3'] = -999
		SAMPLE.loc[index,'BS1'] = 0.0005
	return SAMPLE

def BP1(gene_stats, SAMPLE, index):
	mis = gene_stats[gene_stats.iloc[:,0] == 'missense']
	bp1_1 = mis[(mis.iloc[:,1] != 'Uncertain Significance')].iloc[:,2].sum()
	mis_b = mis[(mis.iloc[:,1] == 'Benign') | (mis.iloc[:,1] == 'Likely Benign')].iloc[:,2].sum()
	mis_n_v = mis_b + mis[(mis.iloc[:,1] == 'Pathogenic') | (mis.iloc[:,1] == 'Likely Pathogenic')].iloc[:,2].sum()
	bp1_2 = mis_b/mis_n_v
	tot_b = gene_stats[(gene_stats.iloc[:,1] == 'Benign') | (gene_stats.iloc[:,1] == 'Likely Benign')].iloc[:,2].sum()
	bp1_3 = tot_b/(gene_stats.iloc[:,2].sum())
	SAMPLE.loc[index,'BP1_1'] = bp1_1
	SAMPLE.loc[index,'BP1_2'] = bp1_2
	SAMPLE.loc[index,'BP1_3'] = bp1_3
	return SAMPLE

def BS1(gene_res, SAMPLE, index):
	mis = gene_res[((gene_res.coding_impact == 'missense') | (gene_res.coding_impact == 'frameshift') | (gene_res.coding_impact == 'nonsense') | (gene_res.coding_impact == 'start loss') | (gene_res.coding_impact == 'stopLoss')) & (gene_res.verdict == 'Pathogenic')] #((gene_res.verdict == 'Likely Pathogenic') | (gene_res.verdict == 'Pathogenic'))]
	maf = pd.to_numeric(mis.exome_max_maf_BA1)
	bs1 = maf.max()
	SAMPLE.loc[index,'BS1'] = bs1
	return SAMPLE

def BA1(gene_res, SAMPLE, index):
	var = gene_res[gene_res.hgvs == SAMPLE.loc[index, 'HGVS']]
	maf =  var.exome_max_maf_BA1 #gnomad_exomes_pop_allele_count
	if len(maf)>0:
		SAMPLE.loc[index,'BA1'] = float(maf.iloc[0])
		SAMPLE.loc[index,'pop_allele_count'] = var.gnomad_exomes_pop_allele_count.iloc[0]
	else:
		SAMPLE.loc[index,'BA1'] = None
		SAMPLE.loc[index,'pop_allele_count'] = -999
	return SAMPLE

def make_intron(SAMPLE):
	xxx = SAMPLE[SAMPLE['consequence'].str.contains('intron')]
	xxx.loc[:,'ada_score'] = xxx['ada_score'].astype(float)
	xxx.loc[:,'rf_score'] = xxx['rf_score'].astype(float)
	xxx.loc[:,'predictors_introns'] = np.where((xxx['rf_score'].isnull()) | (xxx['ada_score'].isnull()), 'NULL', np.where((xxx['rf_score'] >= 0.6) & (xxx['ada_score'] >= 0.6),'D','B'))
	SAMPLE2 = pd.merge(SAMPLE,xxx[['CHROM','POS','predictors_introns']],on=['CHROM','POS'],how='left')
	return SAMPLE2

def find_benignity2(SAMPLE):
	ba1 = (SAMPLE['BA1']>=0.05)
	ba1 = ba1.rename("BA1")
	# # BS1:  2 valori, uno per quello basato sulla prevalenza (solo quelli selezionati), l'altro per quello basato sulla max MAF di gnomAD ----- #+2
	bs1_maf = (SAMPLE['definitivemaf2'] >= SAMPLE['BS1'])
	bs1_maf = bs1_maf.rename("BS1_maf")
	bs1_prev = (SAMPLE['definitivemaf2'] >= SAMPLE['prevalence'])
	bs1_prev = bs1_prev.rename("BS1_prev")
	#benign2=(SAMPLE['controls_nhomalt'] > int(3)) #+1 BS2
	benign2 = pd.Series([False]*len(SAMPLE)) #+1 BS2
	benign2 = benign2.rename("benign2")
	#benign2AD=((SAMPLE['inheritance']=='AD') & (SAMPLE['controls_nhomalt'] > int(5))) #+1 BS2
	benign2AD = pd.Series([False]*len(SAMPLE))
	benign2AD = benign2AD.rename("benign2AD")
	bp1 = (SAMPLE['BP1_1']>=30) & (SAMPLE['BP1_2']>=0.90) & (SAMPLE['BP1_3']>=0.15)
	bp1 = bp1.rename("BP1")
	# BP4 -------- #+1
	benign1= ((SAMPLE['predictors_decision'].astype(str)=='B') & (SAMPLE['predictors_decision'].astype(str)!='NULL')) #+1 BP4
	benign1 = benign1.rename("benign1")
	notbenign = ((SAMPLE['predictors_decision'].astype(str)=='D') & (SAMPLE['predictors_decision'].astype(str)!='NULL')) # punteggio -1 PP3,
	notbenign = notbenign.rename("notbenign")
	benign2int = ((SAMPLE['predictors_introns'].astype(str)=='B') & (SAMPLE['predictors_introns'].astype(str)!='NULL')) #+1 BP4
	benign2int = benign2int.rename("benign2int")
	notbenignint = ((SAMPLE['predictors_introns'].astype(str)=='D') & (SAMPLE['predictors_introns'].astype(str)!='NULL')) #-1 BP4
	notbenignint = notbenignint.rename("notbenignint")
	# BP6 -------- #+1
	fromACMG=(SAMPLE['acmg_verdict'] =='Benign') #+ 1 BP6
	fromACMG = fromACMG.rename("fromACMG")
	fromFILE=((SAMPLE['fromFILE']==1))
	fromFILE = fromFILE.rename("fromFILE")
	checklist_bool = pd.concat([bs1_prev, ba1, bs1_maf, benign2, benign2AD, bp1, benign1, notbenign, benign2int, notbenignint, fromACMG], axis=1)

	checklist_val = checklist_bool.copy()
	checklist_val2 = checklist_bool.copy()

	checklist_val['BA1_points'] = np.where(checklist_val['BA1'] == True, 3, 0)
	checklist_val['BS1_maf_points'] = np.where(checklist_val['BS1_maf'] == True, 2, 0)
	checklist_val['benign2_points'] = np.where(checklist_val['benign2'] == True, 1, 0)
	checklist_val['benign2AD_points'] = np.where(checklist_val['benign2AD'] == True, 1, 0)
	checklist_val['BP1_points'] = np.where(checklist_val['BP1'] == True, 1, 0)
	checklist_val['benign1_points'] = np.where(checklist_val['benign1'] == True, 1, 0)
	checklist_val['notbenign_points'] = np.where(checklist_val['notbenign'] == True, -1, 0)
	checklist_val['benign2int_points'] = np.where(checklist_val['benign2int'] == True, 1, 0)
	checklist_val['notbenignint_points'] = np.where(checklist_val['notbenignint'] == True, -1, 0)
	checklist_val['fromACMG_points'] = np.where(checklist_val['fromACMG'] == True, 1, 0)
	checklist_val2['BS1_prev_points'] = np.where(checklist_val['BS1_prev'] == True, 2, 0)
	checklist_val = checklist_val.loc[:,'BA1_points':]
	checklist_val2 = checklist_val2.loc[:,'BS1_prev_points':]
	checklist_val["checklist_points_sum"] = checklist_val.sum(axis=1)
	_checklist_val_final_ = pd.concat([SAMPLE, checklist_val2.loc[:, 'BS1_prev_points':]], axis=1)
	checklist_val_final = pd.concat([_checklist_val_final_, checklist_val.loc[:, 'BA1_points':]], axis=1)
	checklist_val_final = checklist_val_final.drop(['BA1'], axis=1)#, 'gene_maf'
	checklist_val_final = checklist_val_final.rename(index=str, columns={'BP1_1': 'num_mis', 'BP1_2': 'perc_misB', 'BP1_3':'perc_totMIS', 'BS1':'gene_maf'})
	return checklist_val_final

def reinterpretation_verdict(SAMPLE):
	# print('reinterpretation_verdict')
	SAMPLE_verdict = SAMPLE[['sample','HGVS', 'GENE','acmg_verdict','acmg_classificatins']]
	SAMPLE_verdict.rename(columns={'sample':'sample_id'})
	SAMPLE_model = pd.DataFrame(columns=['sample_id','HGVS', 'GENE', 'BA1_final', 'BA1_intensita_final', 'BA1_cause_final', 'BS1_final', 'BS1_intensita_final', 'BS1_cause_final',
	'BS2_final', 'BS2_intensita_final', 'BS2_cause_final', 'BS3_final', 'BS3_intensita_final', 'BS3_cause_final', 'BS4_final', 'BS4_intensita_final', 'BS4_cause_final', 'BP1_final', 'BP1_intensita_final', 'BP1_cause_final',
	 'BP2_final', 'BP2_intensita_final', 'BP2_cause_final',  'BP3_final', 'BP3_intensita_final', 'BP3_cause_final', 'BP4_final', 'BP4_intensita_final', 'BP4_cause_final', 'BP5_final', 'BP5_intensita_final', 'BP5_cause_final',
	 'BP6_final', 'BP6_intensita_final', 'BP6_cause_final', 'BP7_final', 'BP7_intensita_final', 'BP7_cause_final', 'PVS1_final', 'PVS1_intensita_final', 'PVS1_cause_final', 'PS1_final', 'PS1_intensita_final', 'PS1_cause_final',
	 'PS2_final', 'PS2_intensita_final', 'PS2_cause_final', 'PS3_final', 'PS3_intensita_final', 'PS3_cause_final', 'PS4_final', 'PS4_intensita_final', 'PS4_cause_final', 'PM1_final', 'PM1_intensita_final', 'PM1_cause_final',
	 'PM2_final', 'PM2_intensita_final', 'PM2_cause_final', 'PM3_final', 'PM3_intensita_final', 'PM3_cause_final', 'PM4_final', 'PM4_intensita_final', 'PM4_cause_final', 'PM5_final', 'PM5_intensita_final', 'PM5_cause_final',
	 'PM6_final', 'PM6_intensita_final', 'PM6_cause_final', 'PP1_final', 'PP1_intensita_final', 'PP1_cause_final','PP2_final', 'PP2_intensita_final', 'PP2_cause_final', 'PP3_final', 'PP3_intensita_final', 'PP3_cause_final',
	 'PP4_final', 'PP4_intensita_final', 'PP4_cause_final', 'PP5_final', 'PP5_intensita_final', 'PP5_cause_final', 'choice_interpretation', 'choice_interpretation_final'])
	SAMPLE_model[['sample_id','HGVS', 'GENE', 'choice_interpretation']] = SAMPLE[['sample','HGVS', 'GENE','acmg_verdict']]
	sospetti_omim='/home/bioinfo/VIRTUAL38/MAGIWEB/DATA/sospetti-omim.csv'
	SAMPLE_model = fourth_b.fill_checklist_model(SAMPLE_model, SAMPLE_verdict,sospetti_omim)
	# print(SAMPLE_model.columns)
	# print(SAMPLE_model[SAMPLE_model['choice_interpretation']!=SAMPLE_model['choice_interpretation_final']])
	SAMPLE['acmg_verdict_varsome'] = SAMPLE['acmg_verdict']
	SAMPLE['choice_interpretation_final'] = SAMPLE_model['choice_interpretation_final']
	SAMPLE.loc[SAMPLE['acmg_verdict']!=SAMPLE['choice_interpretation_final'],'acmg_verdict'] = SAMPLE.loc[SAMPLE['acmg_verdict'] != SAMPLE['choice_interpretation_final'],'choice_interpretation_final']
	# print(SAMPLE[['GENE', 'acmg_verdict_varsome', 'acmg_verdict', 'choice_interpretation_final']])
	punti = ['BA1', 'BS1', 'BS2', 'BS3', 'BS4', 'BP1', 'BP2', 'BP3', 'BP4', 'BP5', 'BP6', 'BP7','PVS1', 'PS1', 'PS2', 'PS3', 'PS4', 'PM1', 'PM2', 'PM3', 'PM4', 'PM5', 'PM6', 'PP1', 'PP2', 'PP3', 'PP4', 'PP5']
	# pat_model[p] = {'Presenza': variant.loc[index, p+'_final'], 'Intensita': variant.loc[index, p+'_intensita_final'], 'Causa': variant.loc[index, p+'_cause_final']}
	SAMPLE_model['acmg_classificatins_final'] = ''
	for index, row in SAMPLE_model.iterrows():
		classification = []
		# print(SAMPLE_model.loc[index,'HGVS'])
		for p in punti:
			if str(SAMPLE_model.loc[index,p + '_cause_final']) != 'nan':
				classification.append((p, SAMPLE_model.loc[index,p + '_intensita_final'], SAMPLE_model.loc[index,p + '_cause_final']))
		# print(classification)
		SAMPLE_model.loc[index, 'acmg_classificatins_final'] = str(classification)
	# print(SAMPLE_model[['sample_id','HGVS', 'GENE','acmg_classificatins_final']])
	# print(SAMPLE_verdict[['sample_id','HGVS', 'GENE']])
	SAMPLE_model = SAMPLE_model.rename(columns={'sample_id':'sample'})
	SAMPLE = pd.merge(SAMPLE, SAMPLE_model[['sample','HGVS', 'GENE','acmg_classificatins_final']], how='left', on=['sample','HGVS', 'GENE'])
	SAMPLE['acmg_classificatins_varsome'] = SAMPLE['acmg_classificatins']
	SAMPLE['acmg_classificatins'] = SAMPLE['acmg_classificatins_final']
	return SAMPLE#, SAMPLE_model

if __name__=="__main__":
	gene_varsome = '/home/bioinfo/PROJECT/diagnosys/bin/SELEZIONEAUTOMATICA/GENEVAR/'
	variants = pd.read_csv('/home/bioinfo/VIRTUAL/EUREGIO/utils/variants_to_analyse.csv', sep='\t')
	cols1 = ['sample','GENE','HGVS','annotation','ada_score','rf_score','HGVS_c']

	prev='/home/bioinfo/PROJECT/diagnosys/bin/SELEZIONEAUTOMATICA/Prevalenza.csv'
	prev_maf='/home/bioinfo/PROJECT/diagnosys/bin/SELEZIONEAUTOMATICA/Freq_maf.csv'
	sospetti_omim='/home/bioinfo/PROJECT/diagnosys/bin/SELEZIONEAUTOMATICA/OMIM/sospetti-omim.csv'
	exception='/home/bioinfo/PROJECT/diagnosys/bin/SELEZIONEAUTOMATICA/eccezioni.csv'
	regioniproblematiche= '/home/bioinfo/PROJECT/diagnosys/bin/SELEZIONEAUTOMATICA/regioniproblematiche.txt'
	gene_varsome = '/home/bioinfo/PROJECT/diagnosys/bin/SELEZIONEAUTOMATICA/GENEVAR/'

	PRED=['CADD','DANN','EigenPC','FATHMM',
		'LRT','MCAP','MetaLR',
		'MetaSVM','MutPred','MutationAssessor',
		'MutationTaster','PROVEAN','Polyphen2HDIV',
		'Polyphen2HVAR','REVEL','SIFT',
		'VEST3']

	traduttore={'D':'D','P':'D','T':'B','N':'B','H':'D','B':'B','A':'D','L':'B','M':'D','-999.0':'NULL','-999':'NULL','.':'NULL','U':'NULL'}
	traduttore2={'D':'D','P':'B','T':'B','N':'B','H':'D','B':'B','A':'D','L':'B','M':'D','-999.0':'NULL','-999':'NULL','.':'NULL','U':'NULL'}

	# LOAD REQUIRED FILES
	# Load OMIM file
	SOSPETTI=load_omim(sospetti_omim)

	# Load Prevalence file
	PREV=pd.read_csv(prev, sep=',')
	PREV['Sospetto diagnostico'] = PREV['Sospetto diagnostico'].str.upper()
	PREV.fillna('NA', inplace=True)

	# Load Prevalence-MAF conversion table
	PREV_MAF=pd.read_csv(prev_maf, sep='\t')
	PREV_MAF.fillna('NA', inplace=True)
	SAMPLE = pd.DataFrame(columns=cols1)
	for index, row in variants.iterrows():
		sample_x = variants.loc[index, 'sample']
		gene = variants.loc[index,'annotation'].split(':')[0]
		SAMPLE.loc[index, 'sample'] =  variants.loc[index,'sample']
		SAMPLE.loc[index, 'HGVS'] =  variants.loc[index,'HGVS']
		SAMPLE.loc[index, 'CHROM'] =  variants.loc[index,'HGVS'].split(':')[0]
		SAMPLE.loc[index, 'POS'] =  variants.loc[index,'HGVS'].split(':')[1].split('-')[0]
		SAMPLE.loc[index, 'GENE'] =  variants.loc[index,'annotation'].split(':')[0]
		SAMPLE.loc[index, 'annotation'] =  variants.loc[index,'annotation']
		SAMPLE.loc[index, 'amp_verdict'] = None
		SAMPLE.loc[index, 'amp_classifications'] = None
		SAMPLE.loc[index, 'drug_verdict'] = None
		SAMPLE.loc[index, 'drug_classifications'] = None

	SAMPLE=include_exceptions(SAMPLE, exception)
	SAMPLE['sample'] = SAMPLE['sample'].astype('str')
	SAMPLE['sample'].replace(r'\.202$','.2020',inplace=True,regex=True)
	SAMPLE['sample'] = SAMPLE['sample'].astype('str')

	for index,row in SAMPLE.iterrows():
		#rawinput = ':'.join([SAMPLE.loc[index,'annotation'].split(':')[1].split('.')[0], SAMPLE.loc[index,'annotation'].split(':')[3]])
		#rawinput2 = ':'.join([SAMPLE.loc[index,'annotation'].split(':')[0], SAMPLE.loc[index,'annotation'].split(':')[3]])
		rawinput = ':'.join([SAMPLE.loc[index,'annotation'].split(':')[1], SAMPLE.loc[index,'annotation'].split(':')[2]])
		rawinput2 = ':'.join([SAMPLE.loc[index,'annotation'].split(':')[0], SAMPLE.loc[index,'annotation'].split(':')[2]])
		saphetor = query_varsome_saphetor(rawinput, rawinput2 ,SAMPLE,index)
		saphetor = query_varsome_gene(str(row.GENE),saphetor,index)
		saphetor = varsome_consequences(str(row.GENE), saphetor, index)
##############################################################################
	if len(SAMPLE) == 0:
		SAMPLE = pd.DataFrame(columns=['sample_id','HGVS','CHROM','POS','ID','GENE','consequence','HGVS_c','unbalance',
					'inheritance','allphenotype','allinheritance','definitivemaf','definitivemaf2','pop_allele_count',
					'MAF<20','comments','Description','MAF_selection_AD','MAF_selection_AR',
					'MAF_selection', 'predictors_decision','predictors_B','predictors_D','predictors_introns',
					'controls_nhomalt','prevalence','ada_score','rf_score',
					'acmg_verdict','acmg_classificatins',
					'publications','clinvar2','saphetor','mis_z','lof_z','num_mis',
					'perc_misB','perc_totMIS','gene_maf',
					'fromFILE','BA1_points','BS1_prev_points','BS1_maf_points','benign2_points',
					'benign2AD_points','BP1_points', 'benign1_points','notbenign_points',
					'benign2int_points','notbenignint_points','fromACMG_points',
					'checklist_points_sum','APIversion',
					'amp_verdict','amp_classifications','drug_verdict','drug_classifications'])
	else:
		print
		SAMPLE = saphetor
		SAMPLE['ID']= 'Unknown'
		SAMPLE=exclude_problemregion(SAMPLE,regioniproblematiche)
		SAMPLE = reinterpretation_verdict(SAMPLE)
		SAMPLE = SAMPLE[['sample','HGVS','CHROM','POS','ID','GENE','HGVS_c','ada_score','rf_score',
			'acmg_verdict','acmg_classificatins','pop_allele_count',
			'publications','clinvar2','saphetor','mis_z','lof_z','BP1_1','BA1',
			'BP1_2','BP1_3','BS1','fromFILE','APIversion',
			'amp_verdict','amp_classifications','drug_verdict','drug_classifications']]
		SAMPLE = SAMPLE.reindex(SAMPLE.columns.tolist() + ['consequence','unbalance',
				'inheritance','allphenotype','allinheritance','definitivemaf','definitivemaf2',
				'MAF<20','comments','Description','MAF_selection_AD','MAF_selection_AR',
				'MAF_selection', 'predictors_decision','predictors_B','predictors_D','predictors_introns',
				'controls_nhomalt','prevalence'], axis=1)
		SAMPLE = find_benignity2(SAMPLE)
		SAMPLE['sample_id'] = SAMPLE['sample']

	SAMPLE['definitivemaf'].fillna(-999,inplace=True)
	SAMPLE['definitivemaf2'].fillna(-999,inplace=True)
	SAMPLE['ada_score'].fillna(-999,inplace=True)
	SAMPLE['rf_score'].fillna(-999,inplace=True)
	SAMPLE['pop_allele_count'].fillna(-999,inplace=True)
	SAMPLE['lof_z'].fillna(-999,inplace=True)
	SAMPLE['num_mis'].fillna(-999,inplace=True)
	SAMPLE['perc_misB'].fillna(-999,inplace=True)
	SAMPLE['perc_totMIS'].fillna(-999,inplace=True)
	SAMPLE['gene_maf'].fillna(-999,inplace=True)
	SAMPLE['pop_allele_count'].fillna(-999,inplace=True)
	SAMPLE['pop_allele_count'] = SAMPLE['pop_allele_count'].replace(np.nan, -999)
	SAMPLE['pop_allele_count'] = SAMPLE['pop_allele_count'].astype(int)
	SAMPLE['lof_z'] = SAMPLE['lof_z'].astype(int)
	SAMPLE['num_mis'] = SAMPLE['num_mis'].astype(int)

	SAMPLE['ada_score'].fillna(-999,inplace=True)
	SAMPLE['rf_score'].fillna(-999,inplace=True)
	SAMPLE['MAF_selection_AD'].fillna(-999,inplace=True)
	SAMPLE['MAF_selection_AR'].fillna(-999,inplace=True)
	SAMPLE['MAF_selection'].fillna(-999,inplace=True)
	SAMPLE['prevalence'].fillna(-999,inplace=True)
	SAMPLE.fillna(SAMPLE[['MAF<20','BA1_points','BS1_prev_points','BS1_maf_points','benign2_points',
	'benign2AD_points','BP1_points', 'benign1_points','notbenign_points',
	'benign2int_points','notbenignint_points','fromACMG_points','checklist_points_sum']].fillna(-999), inplace=True)
	SAMPLE['MAF_selection_AD'] = SAMPLE['MAF_selection_AD'].astype(int)
	SAMPLE['MAF_selection_AR'] = SAMPLE['MAF_selection_AR'].astype(int)
	SAMPLE['MAF_selection'] = SAMPLE['MAF_selection'].astype(int)
	SAMPLE['controls_nhomalt'].fillna(-999,inplace=True)
	SAMPLE[['MAF<20','BA1_points','BS1_prev_points','BS1_maf_points','benign2_points',
					'benign2AD_points','BP1_points', 'benign1_points','notbenign_points',
					'benign2int_points','notbenignint_points','fromACMG_points',
					'checklist_points_sum', 'controls_nhomalt']] = SAMPLE[['MAF<20','BA1_points','BS1_prev_points',
					'BS1_maf_points','benign2_points','benign2AD_points','BP1_points','benign1_points','notbenign_points','benign2int_points','notbenignint_points',
					'fromACMG_points','checklist_points_sum', 'controls_nhomalt']].astype(int)
	SAMPLE['mis_z'].fillna(-999,inplace=True)
	SAMPLE['APIversion'].fillna('Unknown',inplace=True)

	SAMPLE['amp_verdict'].fillna('Unknown',inplace=True)
	SAMPLE['amp_classifications'].fillna('Unknown',inplace=True)
	SAMPLE['drug_verdict'].fillna('Unknown',inplace=True)
	SAMPLE['drug_classifications'].fillna('Unknown',inplace=True)

	SAMPLE.fillna('Unknown', inplace=True)
	SAMPLE['mis_z'] =  SAMPLE['mis_z'].map('{:,.5f}'.format)
	SAMPLE = SAMPLE[['sample_id','HGVS','CHROM','POS','ID','GENE','consequence','HGVS_c','unbalance',
				'inheritance','allphenotype','allinheritance','definitivemaf','definitivemaf2','pop_allele_count',
				'MAF<20','comments','Description','MAF_selection_AD','MAF_selection_AR',
				'MAF_selection', 'predictors_decision','predictors_B','predictors_D','predictors_introns',
				'controls_nhomalt','prevalence','ada_score','rf_score',
				'acmg_verdict','acmg_classificatins',
				'publications','clinvar2','saphetor','mis_z','lof_z','num_mis',
				'perc_misB','perc_totMIS','gene_maf',
				'fromFILE','BA1_points','BS1_prev_points','BS1_maf_points','benign2_points',
				'benign2AD_points','BP1_points', 'benign1_points','notbenign_points',
				'benign2int_points','notbenignint_points','fromACMG_points',
				'checklist_points_sum','APIversion',
				'amp_verdict','amp_classifications','drug_verdict','drug_classifications']]
	samples = SAMPLE['sample_id'].unique()
	for s in samples:
		test = re.compile(r'^\w{0,10}\d+\.\d{4,4}$')
		sample_id = str(s)
		path_django = '/home/bioinfo/VIRTUAL/EUREGIO/NGS_RESULT/annot'
		result1 = join('/home/bioinfo/VIRTUAL/EUREGIO/DOWNLOADS/VARSOME/',str(sample_id)+'_variant2_selection.csv')
		df = SAMPLE.loc[SAMPLE['sample_id'] == str(sample_id)]
		result_django1 = join(path_django,str(sample_id)+'_variant2_selection.csv')
		#print (df)
		try:
			df[['sample_id','HGVS','CHROM','POS','ID','GENE','acmg_verdict',
			'acmg_classificatins','publications','clinvar2','saphetor',
			'mis_z','lof_z','num_mis','perc_misB','perc_totMIS','gene_maf']].to_csv(result1,sep='\t',index=False)
			if re.match(test, str(sample_id)):
				df.to_csv(result_django1,sep='\t',index=False)
		except:
			df = SAMPLE.loc[SAMPLE['sample_id'] == 'Unknown']
			df[['sample_id','HGVS','CHROM','POS','ID','GENE','acmg_verdict',
			'acmg_classificatins','publications','clinvar2','saphetor',
			'mis_z','lof_z','num_mis','perc_misB','perc_totMIS','gene_maf']].to_csv(result1,sep='\t',index=False)
