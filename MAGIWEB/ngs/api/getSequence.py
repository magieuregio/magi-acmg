#!/home/bioinfo/VIRTUAL38/bin/python3.8

# -*- coding: utf-8 -*-

# Load libraries
from os import getenv
from os import listdir, system
from Bio import Entrez, SeqIO,Seq
from Bio.Seq import Seq
# from Bio.Alphabet import generic_dna
import subprocess



Entrez.email = "giuse179@gmail.com"     # Always tell NCBI who you are

chromosome38 = {
		'chr1':'NC_000001','chr2':'NC_000002','chr3':'NC_000003','chr4':'NC_000004',
		'chr5':'NC_000005','chr6':'NC_000006','chr7':'NC_000007','chr8':'NC_000008',
		'chr9':'NC_000009','chr10':'NC_000010','chr11':'NC_000011','chr12':'NC_000012',
		'chr13':'NC_000013','chr14':'NC_000014','chr15':'NC_000015','chr16':'NC_000016',
		'chr17':'NC_000017','chr18':'NC_000018','chr19':'NC_000019','chr20':'NC_000020',
		'chr21':'NC_000021','chr22':'NC_000022','chrX':'NC_000023','chrY':'NC_000024','chrMT':'NC_012920'
}

chromosome37 = {
		'chr1': 'NC_000001.10','chr2': 'NC_000002.11','chr3': 'NC_000003.11','chr4': 'NC_000004.11', 'chr5': 'NC_000005.9',
		'chr6': 'NC_000006.11','chr7': 'NC_000007.13','chr8': 'NC_000008.10','chr9': 'NC_000009.11','chr10': 'NC_000010.10',
		'chr11': 'NC_000011.9','chr12': 'NC_000012.11','chr13': 'NC_000013.10','chr14': 'NC_000014.8','chr15': 'NC_000015.9',
		'chr16': 'NC_000016.9','chr17': 'NC_000017.10','chr18': 'NC_000018.9','chr19': 'NC_000019.9','chr20': 'NC_000020.10',
		'chr21': 'NC_000021.8','chr22': 'NC_000022.10','chrX': 'NC_000023.10','chrY': 'NC_000024.9','chrMT': 'NC_001807.4'
		}
###########FROM POSITION TO SEQUENCE#################
def getsequence(CHR,START,END,STRAND,DISTANCE):
       my_seq = ''
       sequence = ''
       genome = '/home/bioinfo/dataset/GENOME/38/all_chr38.fa'

       if START <= END:
           START_ = START
           END_ = END
       else:
           START_ = END
           END_ = START

       #print 'ciao############',START,END,STRAND,DISTANCE,GENOME,genome
       try:
       # if True:
                # if STRAND == 1:
			#print 'ciao2'
                    seq_start= int(START_)# - 1#-int(DISTANCE)
                    seq_stop= (int(START_)) +int(DISTANCE)
                    SEQ = str(CHR)+':'+str(seq_start)+'-'+str(seq_stop)
                    print('SEQQQQQ', SEQ)
		#print SEQ,'#######################'
                    proc = subprocess.Popen(['samtools','faidx',genome,SEQ],stdout=subprocess.PIPE)
                    (out, err) = proc.communicate()
                    print('OUTTTTTTTTTTTTTTt', out)
                    myseq = out.split(b'\n')[1:][0]
                    print('MSEQ 1', myseq)
                    myseq = str(myseq)
                    myseq = myseq.replace("b", "")
                    print('MYSEQ 2', myseq)
                    myseq = myseq.replace("'", "")
                    print('MYSEQ 3', myseq)

                    # out = str(out)
                    # print('OUTTTTTTT', out, err)
		#print 'ciao'
                    # myseq = out.split('\n')[1:]
                    # print('SEQQQQQ', myseq)
                    # for m in myseq: my_seq += m
                    # sequence = my_seq
                    return myseq #sequence[:DISTANCE]+' '+sequence[DISTANCE:-DISTANCE]+' '+sequence[-DISTANCE:]

                # elif STRAND == -1:
                #         seq_start= int(START_) - 1#-int(DISTANCE)
                #         seq_stop= (int(START_) - 1) +int(DISTANCE)
                #         SEQ = str(CHR)+':'+str(seq_start)+'-'+str(seq_stop)
                #         # print('SEQQQQQ', SEQ)
                #         proc = subprocess.Popen(['samtools','faidx',genome,SEQ],stdout=subprocess.PIPE)
                #         (out, err) = proc.communicate()
                #         # print('OUTTTTTTT', out, err)
                #         # out = str(out)
                #         # print('OUTTTTTTT', out, err)
                #         myseq = out.split(b'\n')[1:][0]
                #         myseq = str(myseq)
                #         myseq = myseq.replace("b", "")
                #         myseq = myseq.replace("'", "")
                #         # print('SEQQQQQ', myseq)
                #         # for m in myseq: my_seq += m
                #         # record = Seq(my_seq)
                #         # sequence = record.split('\n')#[1:]#.reverse_complement()
                #         # print('SEQQQQQ FINAL', sequence)
                #         # # print('SEQQQQQ FINAL', str(sequence[0]).split('\n')[0])
                #         return myseq#sequence#[:DISTANCE]+' '+sequence[DISTANCE:-DISTANCE]+' '+sequence[-DISTANCE:]
       except:
               return 'SEQ NON TROVATA!!!'
