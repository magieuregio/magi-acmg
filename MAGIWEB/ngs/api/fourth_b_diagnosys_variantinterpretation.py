#!/home/bioinfo/VIRTUAL38/bin/python3.8

# -*- coding: utf-8 -*-

# Load libraries
import os,re,glob,csv,argparse,datetime,sys,subprocess
from os.path import isfile, join

import numpy as np
import pandas as pd
from varsome_api.client import VarSomeAPIClient,VarSomeAPIException
from varsome_api.models.variant import AnnotatedVariant
import sqlite3
from os import listdir, system
import requests
from requests.structures import CaseInsensitiveDict
from requests.auth import HTTPBasicAuth
from pandas.io.json import json_normalize
import json
import ast

from ngs.api.autopvs1_inst.autoPVS1 import AutoPVS1
from ngs.api.getSequence import getsequence

path = os.getcwd()

pd.options.display.float_format = '{:,.5f}'.format

strength_dict={'BA1':'Stand Alone','BS1':'Strong','BS2':'Strong','BS3':'Strong',
		'BS4':'Strong','BP1':'Supporting','BP2':'Supporting',
		'BP3':'Supporting','BP4':'Supporting','BP5':'Supporting',
		'BP6':'Supporting','BP7':'Supporting','PVS1':'Very Strong','PS1':'Strong',
		'PS2':'Strong','PS3':'Strong','PS4':'Strong','PM1':'Moderate','PM2':'Moderate','PM3':'Moderate',
		'PM4':'Moderate','PM5':'Moderate','PM6':'Moderate','PP1':'Supporting','PP2':'Supporting',
		'PP3':'Supporting','PP4':'Supporting','PP5':'Supporting','PMPOT': 'Moderate','PPPOT':'Supporting'}


#############################################################################
def PVS1(hgvs, gene):
	print('PVS1')
	PVS1_final = 0
	int_dict = {'VeryStrong':'Very Strong','Strong':'Strong','Moderate':'Moderate','Supporting':'Supporting','Unset':'Very Strong','Unmet':'Very Strong'}
	PVS1_intensita_final = 'Very Strong'
	Autopvs1='None'

	#True <SELEZIONEAUTOMATICA.autopvs1_inst.pvs1.PVS1 object at 0x7f2577ce16d0> SS9 Strength.Unset AAAAAAAAAAAAAAAAAAAAAAA
	#True <SELEZIONEAUTOMATICA.autopvs1_inst.pvs1.PVS1 object at 0x7f8022555160> NF1 Strength.Unset AAAAAAAAAAAAAAAAAAAAAAA
	#2-27485431-G-A
	# NF1	True	Very Strong
	# NF2	False	Unset
	# NF3	True	Strong
	# NF4 False	Unset
	# NF5 True	Strong
	# NF6	True	Moderate
	# SS1	True	Very Strong
	# SS2	False	Unset
	# SS3	True	Strong
	# SS4	False	Unset
	# SS5	True	Strong
	# SS6	True	Moderate
	# SS7	False	Unset
	# SS8	True	Strong
	# SS9	True	Moderate
	# SS10	True	Strong
	# IC1	True	Very Strong
	# IC2	True	Strong
	# IC3	True	Moderate
	# IC4	True	Supporting
	# IC5	False	Unset
	#if True:
	try:
		autopvs1=AutoPVS1(hgvs,'hg38', gene)	#'5-90791233-C-T' 6-42698508-42698508-C-G , hgvs
		if autopvs1.islof:
			Autopvs1 = 'SI'
			if (str(autopvs1.pvs1.criterion)=='NF1'):
				PVS1_final = 1
				PVS1_intensita_final = 'Very Strong'
				PVS1_cause_final = str(autopvs1.pvs1.criterion)+' - Exon in present in biologically-relevant transcripts(s).'
			elif (str(autopvs1.pvs1.criterion)=='NF3'):
				PVS1_final = 1
				PVS1_intensita_final = 'Strong'
				PVS1_cause_final = str(autopvs1.pvs1.criterion)+' - Strong Truncated/altered region is critical to protein function.'
			elif (str(autopvs1.pvs1.criterion)=='NF5'):
				PVS1_final = 1
				PVS1_intensita_final = 'Strong'
				PVS1_cause_final = str(autopvs1.pvs1.criterion)+' - Lof variants is this exon are not frequent in the general population and exon is present in biologically-relevant transcript(s) - Variant removes >10% of protein.'
			elif (str(autopvs1.pvs1.criterion)=='NF6'):
				PVS1_final = 1
				PVS1_intensita_final = 'Strong'
				PVS1_cause_final = str(autopvs1.pvs1.criterion)+' Moderate - Lof variants is this exon are not frequent in the general population and exon is present in biologically-relevant transcript(s) - Variant removes <10% of protein.'
			elif (str(autopvs1.pvs1.criterion)=='SS1'):
				PVS1_final = 1
				PVS1_intensita_final = 'Very Strong'
				PVS1_cause_final = str(autopvs1.pvs1.criterion)+'  - Exon in present in biologically-relevant transcripts(s).'
			elif (str(autopvs1.pvs1.criterion)=='SS3'):
				PVS1_final = 1
				PVS1_intensita_final = 'Strong'
				PVS1_cause_final = str(autopvs1.pvs1.criterion)+'  - Truncated/altered region is critical to protein function.'
			elif (str(autopvs1.pvs1.criterion)=='SS5'):
				PVS1_final = 1
				PVS1_intensita_final = 'Strong'
				PVS1_cause_final = str(autopvs1.pvs1.criterion)+'  - Lof variants is this exon are not frequent in the general population and exon is present in biologically-relevant transcript(s) - Variant removes >10% of protein.'
			elif (str(autopvs1.pvs1.criterion)=='SS6'):
				PVS1_final = 1
				PVS1_intensita_final = 'Strong'
				PVS1_cause_final = str(autopvs1.pvs1.criterion)+'  Moderate - Lof variants is this exon are not frequent in the general population and exon is present in biologically-relevant transcript(s) - Variant removes <10% of protein.'
			elif (str(autopvs1.pvs1.criterion)=='SS8'):
				PVS1_final = 1
				PVS1_intensita_final = 'Strong'
				PVS1_cause_final = str(autopvs1.pvs1.criterion)+' - Lof variants is this exon are not frequent in the general population and exon is present in biologically-relevant transcript(s) - Variant removes >10% of protein.'
			elif (str(autopvs1.pvs1.criterion)=='SS9'):
				PVS1_final = 1
				PVS1_intensita_final = 'Strong'
				PVS1_cause_final = str(autopvs1.pvs1.criterion)+' Moderate - Lof variants is this exon are not frequent in the general population and exon is present in biologically-relevant transcript(s) - Variant removes <10% of protein.'
			elif (str(autopvs1.pvs1.criterion)=='SS10'):
				PVS1_final = 1
				PVS1_intensita_final = 'Strong'
				PVS1_cause_final = str(autopvs1.pvs1.criterion)+'  - Truncated/altered region is critical to protein function.'
################################################################################
			elif (str(autopvs1.pvs1.criterion)=='IC1'):
				PVS1_final = 1
				PVS1_intensita_final = 'Very Strong'
				PVS1_cause_final = str(autopvs1.pvs1.criterion)+'  - Pathogenic variant(s) upstream of closest potential in-frame start codon (>6).'
			elif (str(autopvs1.pvs1.criterion)=='IC2'):
				PVS1_final = 1
				PVS1_intensita_final = 'Strong'
				PVS1_cause_final = str(autopvs1.pvs1.criterion)+'  - Pathogenic variant(s) upstream of closest potential in-frame start codon (4-6).'
			elif (str(autopvs1.pvs1.criterion)=='IC3'):
				PVS1_final = 1
				PVS1_intensita_final = 'Strong'
				PVS1_cause_final = str(autopvs1.pvs1.criterion)+' - Moderate - Pathogenic variant(s) upstream of closest potential in-frame start codon (1-3).'
			elif (str(autopvs1.pvs1.criterion)=='IC4'):
				PVS1_final = 1
				PVS1_intensita_final = 'Strong'
				PVS1_cause_final = str(autopvs1.pvs1.criterion)+'  - Supporting - No pathogenic variant(s) upstream of closest potential in-frame start codon.'
################################################################################
			else:
				PVS1_final = 0
				PVS1_cause_final = 'Unset'
				PVS1_intensita_final = int_dict[str(autopvs1.pvs1.strength).split('.')[1]]
		else:
			if (str(autopvs1.pvs1.criterion)=='NF2'):
				PVS1_final = 0
				PVS1_intensita_final = 'Unset'
				PVS1_cause_final = str(autopvs1.pvs1.criterion)+' - Exon is present in biologically-relevant transcript(s).'
				Autopvs1 = 'NO'
			elif (str(autopvs1.pvs1.criterion)=='NF4'):
				PVS1_final = 0
				PVS1_intensita_final = 'Unset'
				PVS1_cause_final = str(autopvs1.pvs1.criterion)+' - Lof variants in this exon are frequent in the general population and/or exon is absent from biologically-relevant transcript(s).'
				Autopvs1 = 'NO'
			elif (str(autopvs1.pvs1.criterion)=='SS2'):
				PVS1_final = 0
				PVS1_intensita_final = 'Unset'
				PVS1_cause_final = str(autopvs1.pvs1.criterion)+' - Exon is absent from biologically-relevant transcript(s).'
				Autopvs1 = 'NO'
			elif (str(autopvs1.pvs1.criterion)=='SS4'):
				PVS1_final = 0
				PVS1_intensita_final = 'Unset'
				PVS1_cause_final = str(autopvs1.pvs1.criterion)+' - Lof variants in this exon are frequent in the general population and/or exon is absent from biologically-relevant transcript(s).'
				Autopvs1 = 'NO'
			elif (str(autopvs1.pvs1.criterion)=='SS7'):
				PVS1_final = 0
				PVS1_intensita_final = 'Unset'
				PVS1_cause_final = str(autopvs1.pvs1.criterion)+' - Lof variants in this exon are frequent in the general population and/or exon is absent from biologically-relevant transcript(s).'
				Autopvs1 = 'NO'
			elif (str(autopvs1.pvs1.criterion)=='IC5'):
				PVS1_final = 0
				PVS1_intensita_final = 'Unset'
				PVS1_cause_final = str(autopvs1.pvs1.criterion)
				Autopvs1 = 'NO'
			else:
				PVS1_final = 0
				PVS1_intensita_final = 'Very Strong'
				PVS1_cause_final = np.nan #autopvs1.pvs1.criterion
				Autopvs1='NO'
	except:
		PVS1_final = 0
		PVS1_intensita_final = 'Very Strong'
		PVS1_cause_final = np.nan
		Autopvs1='Verifica Eccezione!!!'

	return PVS1_final,PVS1_intensita_final,Autopvs1,PVS1_cause_final


# def PP3_BP4(SAMPLE_verdict):
# 	if SAMPLE_verdict.shape[0]!=0:
# 		sample = SAMPLE_verdict.iloc[0,0]
# 		PP3 = 0
# 		BP4 = 0
# 		##spliceAI
# 		vcf = pd.DataFrame(columns=['#CHROM','POS','ID','REF','ALT','QUAL','FILTER','INFO'])
# 		print('BP3 STARTS HERE')
# 		print(SAMPLE_verdict.columns)
# 		SAMPLE_verdict = SAMPLE_verdict.sort_values(by=['HGVS'])
# 		vcf[['#CHROM','POS','REF','ALT']] = SAMPLE_verdict[['CHROM','POS','REF','ALT']]
# 		vcf.loc[:,'ID'] = '.'
# 		vcf.loc[:,'QUAL'] = '.'
# 		vcf.loc[:,'FILTER'] = '.'
# 		vcf.loc[:,'INFO'] = '.'
# 		_input_vcf = join('/home/bioinfo/VIRTUAL38/MAGIWEB/DATA/',str(SAMPLE_verdict.iloc[0,0])+'_'+str(SAMPLE_verdict.iloc[0,12])+'_input.vcf')
# 		_output_vcf = join('/home/bioinfo/VIRTUAL38/MAGIWEB/DATA/',str(SAMPLE_verdict.iloc[0,0])+'_'+str(SAMPLE_verdict.iloc[0,12])+'_output.vcf')
# 		vcf.to_csv(_input_vcf, sep='\t', index=False)
# 		with open(_input_vcf, 'r') as original: data = original.read()
# 		with open(_input_vcf, 'w') as modified: modified.write("##fileformat=VCFv4.2\n##contig=<ID=chr1,length=248956422>\n##contig=<ID=chr2,length=242193529>\n##contig=<ID=chr3,length=198295559>\n##contig=<ID=chr4,length=190214555>\n##contig=<ID=chr5,length=181538259>\n##contig=<ID=chr6,length=170805979>\n##contig=<ID=chr7,length=159345973>\n##contig=<ID=chr8,length=145138636>\n##contig=<ID=chr9,length=138394717>\n##contig=<ID=chr10,length=133797422>\n##contig=<ID=chr11,length=135086622>\n##contig=<ID=chr12,length=133275309>\n##contig=<ID=chr13,length=114364328>\n##contig=<ID=chr14,length=107043718>\n##contig=<ID=chr15,length=101991189>\n##contig=<ID=chr16,length=90338345>\n##contig=<ID=chr17,length=83257441>\n##contig=<ID=chr18,length=80373285>\n##contig=<ID=chr19,length=58617616>\n##contig=<ID=chr20,length=64444167>\n##contig=<ID=chr21,length=46709983>\n##contig=<ID=chr22,length=50818468>\n##contig=<ID=chrX,length=156040895>\n##contig=<ID=chrY,length=57227415>\n##contig=<ID=chrMT,length=16569>\n" + data)
# 		os.system(' '.join(['/home/bioinfo/VIRTUAL38/bin/spliceai','-I',_input_vcf,'-O', _output_vcf,'-R',geno38,'-A','grch38']))
# 		sAI_result = pd.read_csv(_output_vcf,sep='\t',skiprows=28,header=0)
# 		# print('sai result')
# 		# print(sAI_result)
# 		# print(sAI_result['INFO'].str.split('SpliceAI=').str.get(1).str.split('|')[2])
# 		# print(sAI_result[sAI_result['#CHROM']=='chr5'])
# 		try:
# 		 	sAI_result['DS_AG'] = sAI_result['INFO'].str.split('SpliceAI=').str.get(1).str.split('|').str.get(2)
# 		 	sAI_result['DS_AG'] = sAI_result['DS_AG'].replace('.',np.nan)
# 		 	sAI_result['DS_AL'] = sAI_result['INFO'].str.split('SpliceAI=').str.get(1).str.split('|').str.get(3)
# 		 	sAI_result['DS_AL'] = sAI_result['DS_AL'].replace('.',np.nan)
# 		 	sAI_result['DS_DG'] = sAI_result['INFO'].str.split('SpliceAI=').str.get(1).str.split('|').str.get(4)
# 		 	sAI_result['DS_DG'] = sAI_result['DS_DG'].replace('.',np.nan)
# 		 	sAI_result['DS_DL'] = sAI_result['INFO'].str.split('SpliceAI=').str.get(1).str.split('|').str.get(5)
# 		 	sAI_result['DS_DL'] = sAI_result['DS_DL'].replace('.',np.nan)
# 		except:
# 		 	sAI_result['DS_AG'] = np.nan
# 		 	sAI_result['DS_AL'] = np.nan
# 		 	sAI_result['DS_DG'] = np.nan
# 		 	sAI_result['DS_DL'] = np.nan
#
# 		SAMPLE_verdict.loc[(SAMPLE_verdict['revel_score']>=0.644),'revel_result']='D'
# 		SAMPLE_verdict.loc[(SAMPLE_verdict['revel_score']<0.290),'revel'+'_result']='B'
# 		SAMPLE_verdict.loc[(SAMPLE_verdict['revel_score']<0.644)&(SAMPLE_verdict['revel_score']>0.290),'revel_result']='NULL'
# 		SAMPLE_verdict.loc[(SAMPLE_verdict['revel_score']==-999),'revel_result']='NULL'
# 		SAMPLE_verdict.loc[(SAMPLE_verdict['revel_score']==np.NaN),'revel_result']='NULL'
# 		SAMPLE_verdict.loc[(SAMPLE_verdict['revel_score'].isnull()),'revel_result']='NULL'
#
# 		SAMPLE_verdict.loc[(SAMPLE_verdict['cadd_score']>=25.3),'cadd_result']='D'
# 		SAMPLE_verdict.loc[(SAMPLE_verdict['cadd_score']<22.7),'cadd_result']='B'
# 		SAMPLE_verdict.loc[(SAMPLE_verdict['cadd_score']<25.3)&(SAMPLE_verdict['cadd_score']>22.7),'cadd_result']='NULL'
# 		SAMPLE_verdict.loc[(SAMPLE_verdict['cadd_score']==-999),'cadd_result']='NULL'
# 		SAMPLE_verdict.loc[(SAMPLE_verdict['cadd_score']==np.NaN),'cadd_result']='NULL'
# 		SAMPLE_verdict.loc[(SAMPLE_verdict['cadd_score'].isnull()),'cadd_result']='NULL'
#
# 		for index,row in SAMPLE_verdict[['revel_result','cadd_result','predictors_decision']].iterrows():
# 			benign = 0
# 			deleterius = 0
# 			null = 0
#
# 			counts = pd.DataFrame(row.value_counts()).T
# 			try: deleterius=counts['D'].values[0]
# 			except: deleterius=0
# 			try: benign=counts['B'].values[0]
# 			except: benign=0
# 			try: null=counts['NULL'].values[0]
# 			except: null=0
# 			total = deleterius+benign+null
#
# 			#print (deleterius,benign,null)
#
# 			if deleterius>=2: SAMPLE_verdict.loc[(index,'pred_finalresult')] = 'D'
# 			if benign>=2: SAMPLE_verdict.loc[(index,'pred_finalresult')] = 'B'
#
# 		SAMPLE_verdict['pred_finalresult'].fillna('NULL',inplace=True)
#
# 		print (SAMPLE_verdict[['revel_result','cadd_result','predictors_decision','pred_finalresult']])
#
# 		sAI_result['spliceAI_score'] = sAI_result[['DS_AG','DS_AL','DS_DG','DS_DL']].max(axis=1)
# 		sAI_result = sAI_result.rename(columns={'#CHROM':'CHROM'})
# 		sAI_result1 = sAI_result[['CHROM','POS','REF','ALT','spliceAI_score']]
# 		sAI_result['POS'] = sAI_result['POS'].astype('str')
# 		SAMPLE_verdict = pd.merge(SAMPLE_verdict,sAI_result[['CHROM','POS','REF','ALT','spliceAI_score']], how='left', on =['CHROM','POS','REF','ALT'])
#
# 		SAMPLE_verdict['PP3'] = 0
# 		SAMPLE_verdict['BP4'] = 0
#
# 		if (('missense' in SAMPLE_verdict['consequence'].values) and ('ada_score' in SAMPLE_verdict.columns)):
# 			SAMPLE_verdict['PP3'] = np.where((SAMPLE_verdict['consequence']=='missense') | (SAMPLE_verdict['consequence']=='missense'),np.where(((SAMPLE_verdict['ada_score']).astype(float)>=0.708)&((SAMPLE_verdict['rf_score']).astype(float)>=0.598),1,SAMPLE_verdict['PP3']),SAMPLE_verdict['PP3'])
# 		elif (('missense' in SAMPLE_verdict['consequence'].values) and ('ada_score' not in SAMPLE_verdict.columns)):
# 			SAMPLE_verdict['PP3'] = np.where(SAMPLE_verdict['consequence']=='missense',np.where((SAMPLE_verdict['revel_score']).astype(float)>=0.7,1,SAMPLE_verdict['PP3']),SAMPLE_verdict['PP3'])
# 		if (('synonimous' in SAMPLE_verdict['consequence'].values) and ('ada_score' in SAMPLE_verdict.columns)):
# 			SAMPLE_verdict['PP3'] = np.where((SAMPLE_verdict['consequence']=='synonimous_variant&splice_region_variant') | (SAMPLE_verdict['consequence']=='splice_region_variant&synonymous_variant'),np.where(((SAMPLE_verdict['ada_score']).astype(float)>=0.708)&((SAMPLE_verdict['rf_score']).astype(float)>=0.598),1,SAMPLE_verdict['PP3']),SAMPLE_verdict['PP3'])
# 		if (('intronic' in SAMPLE_verdict['consequence'].values) and ('ada_score' in SAMPLE_verdict.columns)):
# 			SAMPLE_verdict['PP3'] = np.where('intron_variant' in SAMPLE_verdict['consequence'],np.where(((SAMPLE_verdict['ada_score']).astype(float)>=0.708)&((SAMPLE_verdict['rf_score']).astype(float)>=0.598)&((SAMPLE_verdict['spliceAI_score']).astype(float)>=0.8),1,SAMPLE_verdict['PP3']),SAMPLE_verdict['PP3'])
#
# 		SAMPLE_verdict['BP4'] = np.where(SAMPLE_verdict['consequence']=='missense',np.where((SAMPLE_verdict['revel_score']).astype(float)<=0.15,1,SAMPLE_verdict['BP4']),SAMPLE_verdict['BP4'])
#
# 	return SAMPLE_verdict

def PP3_BP4(SAMPLE_verdict):
	if SAMPLE_verdict.shape[0]!=0:
		sample = SAMPLE_verdict.iloc[0,0]
		SAMPLE_verdict['pred_finalresult']='NULL'
		PP3 = 0
		BP4 = 0

		SAMPLE_verdict.loc[(SAMPLE_verdict['revel_score']>=0.644),'revel_result']='D'
		SAMPLE_verdict.loc[(SAMPLE_verdict['revel_score']<0.290),'revel'+'_result']='B'
		SAMPLE_verdict.loc[(SAMPLE_verdict['revel_score']<0.644)&(SAMPLE_verdict['revel_score']>0.290),'revel_result']='NULL'
		SAMPLE_verdict.loc[(SAMPLE_verdict['revel_score']==-999),'revel_result']='NULL'
		SAMPLE_verdict.loc[(SAMPLE_verdict['revel_score']==np.NaN),'revel_result']='NULL'
		SAMPLE_verdict.loc[(SAMPLE_verdict['revel_score'].isnull()),'revel_result']='NULL'

		SAMPLE_verdict.loc[(SAMPLE_verdict['cadd_score']>=25.3),'cadd_result']='D'
		SAMPLE_verdict.loc[(SAMPLE_verdict['cadd_score']<22.7),'cadd_result']='B'
		SAMPLE_verdict.loc[(SAMPLE_verdict['cadd_score']<25.3)&(SAMPLE_verdict['cadd_score']>22.7),'cadd_result']='NULL'
		SAMPLE_verdict.loc[(SAMPLE_verdict['cadd_score']==-999),'cadd_result']='NULL'
		SAMPLE_verdict.loc[(SAMPLE_verdict['cadd_score']==np.NaN),'cadd_result']='NULL'
		SAMPLE_verdict.loc[(SAMPLE_verdict['cadd_score'].isnull()),'cadd_result']='NULL'

		#for index,row in SAMPLE_verdict[['revel_result','cadd_result','predictors_decision']].iterrows():
		for index,row in SAMPLE_verdict[['revel_result','cadd_result','predictors_decision']].iterrows():
			benign = 0
			deleterius = 0
			null = 0
			counts = pd.DataFrame(row.value_counts()).T
			try: deleterius=counts['D'].values[0]
			except: deleterius=0
			try: benign=counts['B'].values[0]
			except: benign=0
			try: null=counts['NULL'].values[0]
			except: null=0
			total = deleterius+benign+null
			if deleterius>=1: SAMPLE_verdict.loc[(index,'pred_finalresult')] = 'D'
			if benign>=1: SAMPLE_verdict.loc[(index,'pred_finalresult')] = 'B'

		SAMPLE_verdict['spliceAI_score'] = -999
		SAMPLE_verdict['pred_finalresult'].fillna('NULL',inplace=True)
		SAMPLE_verdict['PP3'] = 0
		SAMPLE_verdict['BP4'] = 0

		SAMPLE_verdict['PP3'] = np.where(len(SAMPLE_verdict['consequence'])>=1,np.where((SAMPLE_verdict['pred_finalresult'])=='D',1,SAMPLE_verdict['PP3']),SAMPLE_verdict['PP3'])
		SAMPLE_verdict['PP3'] = np.where(((SAMPLE_verdict['ada_score'])!=-999)|((SAMPLE_verdict['rf_score'])!=-999),np.where(((SAMPLE_verdict['ada_score']).astype(float)>=0.708)&((SAMPLE_verdict['rf_score']).astype(float)>=0.598),1,SAMPLE_verdict['PP3']),SAMPLE_verdict['PP3'])
		SAMPLE_verdict['BP4'] = np.where(len(SAMPLE_verdict['consequence'])>=1,np.where((SAMPLE_verdict['pred_finalresult'])=='B',1,SAMPLE_verdict['BP4']),SAMPLE_verdict['BP4'])
		SAMPLE_verdict['BP4'] = np.where(len(SAMPLE_verdict['consequence'])>=1,np.where(((SAMPLE_verdict['ada_score']).astype(float)<=0.308)&((SAMPLE_verdict['ada_score']).astype(float)!=-999)&((SAMPLE_verdict['rf_score']).astype(float)<=0.298)&((SAMPLE_verdict['rf_score']).astype(float)<=-999),1,SAMPLE_verdict['BP4']),SAMPLE_verdict['BP4'])


		# SAMPLE_verdict['PP3'] = np.where(SAMPLE_verdict['consequence']=='missense',np.where((SAMPLE_verdict['pred_finalresult'])=='D',1,SAMPLE_verdict['PP3']),SAMPLE_verdict['PP3'])
		# SAMPLE_verdict['PP3'] = np.where(SAMPLE_verdict['consequence']=='missense',np.where((SAMPLE_verdict['pred_finalresult'])=='D',1,SAMPLE_verdict['PP3']),SAMPLE_verdict['PP3'])
		# SAMPLE_verdict['PP3'] = np.where((SAMPLE_verdict['consequence']=='missense') | (SAMPLE_verdict['consequence']=='missense'),np.where(((SAMPLE_verdict['ada_score']).astype(float)>=0.708)&((SAMPLE_verdict['rf_score']).astype(float)>=0.598),1,SAMPLE_verdict['PP3']),SAMPLE_verdict['PP3'])
		# SAMPLE_verdict['PP3'] = np.where((SAMPLE_verdict['consequence']=='synonimous') | (SAMPLE_verdict['consequence']=='synonimous'),np.where(((SAMPLE_verdict['ada_score']).astype(float)>=0.708)&((SAMPLE_verdict['rf_score']).astype(float)>=0.598),1,SAMPLE_verdict['PP3']),SAMPLE_verdict['PP3'])
		# SAMPLE_verdict['PP3'] = np.where('intronic' in SAMPLE_verdict['consequence'],np.where(((SAMPLE_verdict['ada_score']).astype(float)>=0.708)&((SAMPLE_verdict['rf_score']).astype(float)>=0.598),1,SAMPLE_verdict['PP3']),SAMPLE_verdict['PP3'])
		# SAMPLE_verdict['BP4'] = np.where(SAMPLE_verdict['consequence']=='missense',np.where((SAMPLE_verdict['pred_finalresult'])=='B',1,SAMPLE_verdict['BP4']),SAMPLE_verdict['BP4'])
		# SAMPLE_verdict['BP4'] = np.where(SAMPLE_verdict['consequence']=='missense',np.where((SAMPLE_verdict['pred_finalresult'])=='B',1,SAMPLE_verdict['BP4']),SAMPLE_verdict['BP4'])
		# SAMPLE_verdict['BP4'] = np.where((SAMPLE_verdict['consequence']=='missense') | (SAMPLE_verdict['consequence']=='missense'),np.where(((SAMPLE_verdict['ada_score']).astype(float)<=0.308)&((SAMPLE_verdict['rf_score']).astype(float)<=0.298),1,SAMPLE_verdict['BP4']),SAMPLE_verdict['BP4'])
		# SAMPLE_verdict['BP4'] = np.where((SAMPLE_verdict['consequence']=='synonimous') | (SAMPLE_verdict['consequence']=='synonimous'),np.where(((SAMPLE_verdict['ada_score']).astype(float)<=0.308)&((SAMPLE_verdict['rf_score']).astype(float)<=0.298),1,SAMPLE_verdict['BP4']),SAMPLE_verdict['BP4'])
		# SAMPLE_verdict['BP4'] = np.where('intronic' in SAMPLE_verdict['consequence'],np.where(((SAMPLE_verdict['ada_score']).astype(float)<=0.308)&((SAMPLE_verdict['rf_score']).astype(float)<=0.298),1,SAMPLE_verdict['BP4']),SAMPLE_verdict['BP4'])
	return SAMPLE_verdict


def fill_checklist_model(SAMPLE_model, SAMPLE_verdict,omim):
	print('fill_checklist_model')
	SAMPLE_verdict_P_B = PP3_BP4(SAMPLE_verdict)
	# print(SAMPLE_verdict.T)
	# print(spliceAI, PP3, BP4)

	SAMPLE_model = pd.merge(SAMPLE_model, SAMPLE_verdict_P_B[['HGVS','spliceAI_score','PP3','BP4']], how='left', on = 'HGVS')
	SAMPLE_model['comments_verdict'] = ''
	SAMPLE_verdict.loc[:,'consequence'] = 'missense_variant'
	# SAMPLE_verdict['acmg_classifications'] = SAMPLE_verdict['acmg_classifications'].astype('str')
	for i,row in SAMPLE_verdict.iterrows():
		if (row.acmg_classifications) == 'Exception da Verificate!!!':
			print('exception')
			explanation = ()
		else:
			row.acmg_classifications = row.acmg_classifications.replace('â\\x87\\x92', '>')
			row.acmg_classifications = row.acmg_classifications.replace('%%', '')
			# print('rowwwwwwwwwwwwwwwwwwww', row.acmg_classifications)
			explanation = ast.literal_eval(row.acmg_classifications)

		for tuple in explanation:
			if len(tuple)==3:
				punto = tuple[0]
				strength = tuple[1]
				cause = tuple[2]
			elif len(tuple)==2:
				punto = tuple[0]
				strength = strength_dict[punto]
				cause = tuple[1]
			SAMPLE_model.loc[i, '_'.join([punto, 'final'])] = 1

			if (punto[0]=='B') & (strength == 'Moderate'): #all benign point if moderate, set to default
				SAMPLE_model.loc[i, '_'.join([punto, 'intensita_final'])] = strength_dict[punto]
			elif (punto[0]=='B') & (strength == 'Very Strong'): #all benign point if very strong set to default
				SAMPLE_model.loc[i, '_'.join([punto, 'intensita_final'])] = strength_dict[punto]
			else:
				SAMPLE_model.loc[i, '_'.join([punto, 'intensita_final'])] = strength
			# print(punto)
			if (punto=='PM2'):
				SAMPLE_model.loc[i, '_'.join([punto, 'intensita_final'])] = strength_dict[punto]
				cause = 'Absent from controls (or at extremely low frequency if recessive) in Exome Sequencing Project, 1000 Genomes Project, or Exome Aggregation Consortium.'

			if (punto=='BS2') & ((omim.split('/')[-1].split('.')[0] == 'omim_OCULARE') | (omim.split('/')[-1].split('.')[0] == 'omim_MIXED1') | (omim.split('/')[-1].split('.')[0] == 'omim_LYMPHOBESITY') | (omim.split('/')[-1].split('.')[0] == 'omim_VASCULAR') | (omim.split('/')[-1].split('.')[0] == 'omim_NEUROLOGY')):
				file = '/home/bioinfo/VIRTUAL38/MAGIWEB/DATA1/OMIM/' + omim.split('/')[-1].split('.')[0] + '_IT.csv'
				omim_cgd = pd.read_csv(file, sep='\t')
				omim_cgd_gene = omim_cgd[omim_cgd['GENE']==row.GENE]
				if not omim_cgd_gene.empty:
					if (omim_cgd_gene.iloc[0,-1] == 'AD') & (omim_cgd_gene.iloc[0,-3] == 'Autosomal recessive'):
						SAMPLE_model.loc[i, '_'.join(['BS2', 'final'])] = 0
						cause =' DISABLED for inheritance pattern.' +  cause
				else:
					try: genenimancanti+=str(row.GENE+',')
					except: genenimancanti=str(row.GENE+',')
			SAMPLE_model.loc[i, '_'.join([punto, 'cause_final'])] = cause

		# chr = SAMPLE_verdict.loc[i,'HGVS'].split(':')[0]
		# pos = SAMPLE_verdict.loc[i,'HGVS'].split(':')[1].split('-')[0]
		# ref = SAMPLE_verdict.loc[i,'HGVS'].split(':')[2].split('/')[0]
		# alt = SAMPLE_verdict.loc[i,'HGVS'].split(':')[2].split('/')[1]
		# if ref =='-':
		# 	ref = 'A'
		# 	alt = 'A' + alt
		# if alt =='-':
		# 	alt = 'A'
		# 	ref = 'A' + ref
		# hgvs_pvs1 = chr + '-' + pos + '-' + ref + '-' + alt
		#
		# gene=SAMPLE_verdict.loc[i,'GENE']
		chr = SAMPLE_verdict.loc[i,'CHROM'][3:] #.split(':')[0]
		pos = SAMPLE_verdict.loc[i,'POS'] #.split(':')[1].split('-')[0]
		ref = SAMPLE_verdict.loc[i,'REF'] #.split(':')[2].split('/')[0]
		alt = SAMPLE_verdict.loc[i,'ALT'] #.split(':')[2].split('/')[1]
		strand = int(SAMPLE_verdict.loc[i,'strand'])
		hgvs_pvs1 = SAMPLE_verdict.loc[i,'HGVS_PVS1']


		# if ref =='-':
		# 	ref = 'A'
		# 	alt = 'A' + alt
		# if alt =='-':
		# 	alt = 'A'
		# 	ref = 'A' + ref
		# hgvs_pvs1 = str(chr) + '-' + str(pos) + '-' + str(ref) + '-' + str(alt)
		# hgvs_pvs1 = 'X-38288029-TTGT-T'
		gene=SAMPLE_verdict.loc[i,'GENE']
		print('TEST VARSOMEEEEEEEEEEEEEEEEEEEEEEEEEEEEE', chr, pos, ref, alt, gene, hgvs_pvs1, strand)
		PVS1_final, PVS1_intensita_final, Autopvs1, PVS1_cause_final = PVS1(hgvs_pvs1,gene)

		SAMPLE_model.loc[i,'PVS1_final_old'] = SAMPLE_model.loc[i,'PVS1_final']
		SAMPLE_model.loc[i,'PVS1_intensita_final_old'] = SAMPLE_model.loc[i,'PVS1_intensita_final']
		SAMPLE_model.loc[i,'PVS1_cause_final_old'] = SAMPLE_model.loc[i,'PVS1_cause_final']

		SAMPLE_model.loc[i,'autopvs1'] = Autopvs1
		SAMPLE_model.loc[i,'PVS1_final'] = PVS1_final
		SAMPLE_model.loc[i,'PVS1_intensita_final'] = PVS1_intensita_final

		SAMPLE_model.loc[i,'PVS1_cause_final'] = PVS1_cause_final

		SAMPLE_model.loc[i,'spliceAI'] = SAMPLE_model.loc[i,'spliceAI_score']
		SAMPLE_model.loc[i,'PP3_final_old'] = SAMPLE_model.loc[i,'PP3_final']
		SAMPLE_model.loc[i,'PP3_intensita_final_old'] = SAMPLE_model.loc[i,'PP3_intensita_final']
		SAMPLE_model.loc[i,'PP3_cause_final_old'] = SAMPLE_model.loc[i,'PP3_cause_final']

		SAMPLE_model.loc[i,'BP4_final_old'] = SAMPLE_model.loc[i,'BP4_final']
		SAMPLE_model.loc[i,'BP4_intensita_final_old'] = SAMPLE_model.loc[i,'BP4_intensita_final']
		SAMPLE_model.loc[i,'BP4_cause_final_old'] = SAMPLE_model.loc[i,'BP4_cause_final']

		SAMPLE_model.loc[i,'PP3_final'] = SAMPLE_model.loc[i,'PP3']

		if SAMPLE_model.loc[i,'PP3'] == 1:
			SAMPLE_model.loc[i,'PP3_intensita_final'] = 'Supporting'
			SAMPLE_model.loc[i,'PP3_cause_final'] = 'Predictors above Treshold'
		else:
			SAMPLE_model.loc[i,'PP3_cause_final'] = np.nan
		SAMPLE_model.loc[i,'BP4_final'] = SAMPLE_model.loc[i,'BP4']
		if SAMPLE_model.loc[i,'BP4'] == 1:
			SAMPLE_model.loc[i,'BP4_intensita_final'] = 'Supporting'
			SAMPLE_model.loc[i,'BP4_cause_final'] = 'Predictors under Treshold'
		else:
			SAMPLE_model.loc[i,'BP4_cause_final'] = np.nan
		SAMPLE_model.loc[i, 'consequence'] = SAMPLE_verdict.loc[i, 'consequence']
		if SAMPLE_model.loc[i,'PVS1_final'] == 1:
			SAMPLE_model.loc[i,'PS3_final'] = 0
			SAMPLE_model.loc[i,'BS3_final'] = 0
			SAMPLE_model.loc[i,'PP3_final'] = 0
			SAMPLE_model.loc[i,'PM4_final'] = 0
			SAMPLE_model.loc[i,'BP4_final'] = 0
			SAMPLE_model.loc[i,'BP7_final'] = 0

		if SAMPLE_model.loc[i,'PS1_final'] == 1:
			SAMPLE_model.loc[i,'PP5_final'] = 0

		if explanation!=():
			SAMPLE_model.loc[i,['choice_interpretation_final','vusstatus','PMPOT_final','PMPOT_intensita_final','PMPOT_cause_final','PPPOT_final','PPPOT_intensita_final','PPPOT_cause_final']] = variant_interpretation(SAMPLE_model.drop(SAMPLE_model.index.difference([i])))
			if str(SAMPLE_model.loc[i,'vusstatus']) == '-999':
				SAMPLE_model.loc[i,'vusstatus'] = 'Middle'
				SAMPLE_model.loc[i,'comments_verdict'] = 'Vusstatus da ricontrollare!'
				print(SAMPLE_model.loc[i,'comments_verdict'],'fb')
		else:
			SAMPLE_model.loc[i,'choice_interpretation_final'] = 'UNKNOWN'
			SAMPLE_model.loc[i,'choice_interpretation'] = 'UNKNOWN'

	try: pd.DataFrame({'GENE':[genenimancanti]}).to_csv(''.join(['GENIMANCANTI_VERIFICA']), sep='\t')
	except: pass

	SAMPLE_model['BA1_intensita_final'].fillna(strength_dict['BA1'],inplace=True)
	SAMPLE_model['BS1_intensita_final'].fillna(strength_dict['BS1'],inplace=True)
	SAMPLE_model['BS2_intensita_final'].fillna(strength_dict['BS2'],inplace=True)
	SAMPLE_model['BS3_intensita_final'].fillna(strength_dict['BS3'],inplace=True)
	SAMPLE_model['BS4_intensita_final'].fillna(strength_dict['BS4'],inplace=True)
	SAMPLE_model['BP1_intensita_final'].fillna(strength_dict['BP1'],inplace=True)
	SAMPLE_model['BP2_intensita_final'].fillna(strength_dict['BP2'],inplace=True)
	SAMPLE_model['BP3_intensita_final'].fillna(strength_dict['BP3'],inplace=True)
	SAMPLE_model['BP4_intensita_final'].fillna(strength_dict['BP4'],inplace=True)
	SAMPLE_model['BP5_intensita_final'].fillna(strength_dict['BP5'],inplace=True)
	SAMPLE_model['BP6_intensita_final'].fillna(strength_dict['BP6'],inplace=True)
	SAMPLE_model['BP7_intensita_final'].fillna(strength_dict['BP7'],inplace=True)
	SAMPLE_model['PVS1_intensita_final'].fillna(strength_dict['PVS1'],inplace=True)
	SAMPLE_model['PS1_intensita_final'].fillna(strength_dict['PS1'],inplace=True)
	SAMPLE_model['PS2_intensita_final'].fillna(strength_dict['PS2'],inplace=True)
	SAMPLE_model['PS3_intensita_final'].fillna(strength_dict['PS3'],inplace=True)
	SAMPLE_model['PS4_intensita_final'].fillna(strength_dict['PS4'],inplace=True)
	SAMPLE_model['PM1_intensita_final'].fillna(strength_dict['PM1'],inplace=True)
	SAMPLE_model['PM2_intensita_final'].fillna(strength_dict['PM2'],inplace=True)
	SAMPLE_model['PM3_intensita_final'].fillna(strength_dict['PM3'],inplace=True)
	SAMPLE_model['PM4_intensita_final'].fillna(strength_dict['PM4'],inplace=True)
	SAMPLE_model['PM5_intensita_final'].fillna(strength_dict['PM5'],inplace=True)
	SAMPLE_model['PM6_intensita_final'].fillna(strength_dict['PM6'],inplace=True)
	SAMPLE_model['PP1_intensita_final'].fillna(strength_dict['PP1'],inplace=True)
	SAMPLE_model['PP2_intensita_final'].fillna(strength_dict['PP2'],inplace=True)
	SAMPLE_model['PP3_intensita_final'].fillna(strength_dict['PP3'],inplace=True)
	SAMPLE_model['PP4_intensita_final'].fillna(strength_dict['PP4'],inplace=True)
	SAMPLE_model['PP5_intensita_final'].fillna(strength_dict['PP5'],inplace=True)
	SAMPLE_model['PMPOT_intensita_final'].fillna(strength_dict['PMPOT'],inplace=True)
	SAMPLE_model['PPPOT_intensita_final'].fillna(strength_dict['PPPOT'],inplace=True)

	SAMPLE_model['BA1_final'].fillna(0,inplace=True)
	SAMPLE_model['BS1_final'].fillna(0,inplace=True)
	SAMPLE_model['BS2_final'].fillna(0,inplace=True)
	SAMPLE_model['BS3_final'].fillna(0,inplace=True)
	SAMPLE_model['BS4_final'].fillna(0,inplace=True)
	SAMPLE_model['BP1_final'].fillna(0,inplace=True)
	SAMPLE_model['BP2_final'].fillna(0,inplace=True)
	SAMPLE_model['BP3_final'].fillna(0,inplace=True)
	SAMPLE_model['BP4_final'].fillna(0,inplace=True)
	SAMPLE_model['BP5_final'].fillna(0,inplace=True)
	SAMPLE_model['BP6_final'].fillna(0,inplace=True)
	SAMPLE_model['BP7_final'].fillna(0,inplace=True)
	SAMPLE_model['PVS1_final'].fillna(0,inplace=True)
	SAMPLE_model['PS2_final'].fillna(0,inplace=True)
	SAMPLE_model['PS3_final'].fillna(0,inplace=True)
	SAMPLE_model['PS1_final'].fillna(0,inplace=True)
	SAMPLE_model['PS4_final'].fillna(0,inplace=True)
	SAMPLE_model['PM1_final'].fillna(0,inplace=True)
	SAMPLE_model['PM2_final'].fillna(0,inplace=True)
	SAMPLE_model['PM3_final'].fillna(0,inplace=True)
	SAMPLE_model['PM4_final'].fillna(0,inplace=True)
	SAMPLE_model['PM5_final'].fillna(0,inplace=True)
	SAMPLE_model['PM6_final'].fillna(0,inplace=True)
	SAMPLE_model['PP1_final'].fillna(0,inplace=True)
	SAMPLE_model['PP2_final'].fillna(0,inplace=True)
	SAMPLE_model['PP3_final'].fillna(0,inplace=True)
	SAMPLE_model['PP4_final'].fillna(0,inplace=True)
	SAMPLE_model['PP5_final'].fillna(0,inplace=True)
	SAMPLE_model['PMPOT_final'].fillna(0,inplace=True)
	SAMPLE_model['PPPOT_final'].fillna(0,inplace=True)
	return SAMPLE_model


def variant_interpretation(variant):
	# print('variant_interpretation')
	variant['BA1_intensita_final'].fillna(strength_dict['BA1'],inplace=True)
	variant['BS1_intensita_final'].fillna(strength_dict['BS1'],inplace=True)
	variant['BS2_intensita_final'].fillna(strength_dict['BS2'],inplace=True)
	variant['BS3_intensita_final'].fillna(strength_dict['BS3'],inplace=True)
	variant['BS4_intensita_final'].fillna(strength_dict['BS4'],inplace=True)
	variant['BP1_intensita_final'].fillna(strength_dict['BP1'],inplace=True)
	variant['BP2_intensita_final'].fillna(strength_dict['BP2'],inplace=True)
	variant['BP3_intensita_final'].fillna(strength_dict['BP3'],inplace=True)
	variant['BP4_intensita_final'].fillna(strength_dict['BP4'],inplace=True)
	variant['BP5_intensita_final'].fillna(strength_dict['BP5'],inplace=True)
	variant['BP6_intensita_final'].fillna(strength_dict['BP6'],inplace=True)
	variant['BP7_intensita_final'].fillna(strength_dict['BP7'],inplace=True)
	variant['PVS1_intensita_final'].fillna(strength_dict['PVS1'],inplace=True)
	variant['PS1_intensita_final'].fillna(strength_dict['PS1'],inplace=True)
	variant['PS2_intensita_final'].fillna(strength_dict['PS2'],inplace=True)
	variant['PS3_intensita_final'].fillna(strength_dict['PS3'],inplace=True)
	variant['PS4_intensita_final'].fillna(strength_dict['PS4'],inplace=True)
	variant['PM1_intensita_final'].fillna(strength_dict['PM1'],inplace=True)
	variant['PM2_intensita_final'].fillna(strength_dict['PM2'],inplace=True)
	variant['PM3_intensita_final'].fillna(strength_dict['PM3'],inplace=True)
	variant['PM4_intensita_final'].fillna(strength_dict['PM4'],inplace=True)
	variant['PM5_intensita_final'].fillna(strength_dict['PM5'],inplace=True)
	variant['PM6_intensita_final'].fillna(strength_dict['PM6'],inplace=True)
	variant['PP1_intensita_final'].fillna(strength_dict['PP1'],inplace=True)
	variant['PP2_intensita_final'].fillna(strength_dict['PP2'],inplace=True)
	variant['PP3_intensita_final'].fillna(strength_dict['PP3'],inplace=True)
	variant['PP4_intensita_final'].fillna(strength_dict['PP4'],inplace=True)
	variant['PP5_intensita_final'].fillna(strength_dict['PP5'],inplace=True)
	variant['PMPOT_intensita_final'].fillna(strength_dict['PMPOT'],inplace=True)
	variant['PPPOT_intensita_final'].fillna(strength_dict['PPPOT'],inplace=True)

	variant['BA1_final'].fillna(0,inplace=True)
	variant['BS1_final'].fillna(0,inplace=True)
	variant['BS2_final'].fillna(0,inplace=True)
	variant['BS3_final'].fillna(0,inplace=True)
	variant['BS4_final'].fillna(0,inplace=True)
	variant['BP1_final'].fillna(0,inplace=True)
	variant['BP2_final'].fillna(0,inplace=True)
	variant['BP3_final'].fillna(0,inplace=True)
	variant['BP4_final'].fillna(0,inplace=True)
	variant['BP5_final'].fillna(0,inplace=True)
	variant['BP6_final'].fillna(0,inplace=True)
	variant['BP7_final'].fillna(0,inplace=True)
	variant['PVS1_final'].fillna(0,inplace=True)
	variant['PS2_final'].fillna(0,inplace=True)
	variant['PS3_final'].fillna(0,inplace=True)
	variant['PS1_final'].fillna(0,inplace=True)
	variant['PS4_final'].fillna(0,inplace=True)
	variant['PM1_final'].fillna(0,inplace=True)
	variant['PM2_final'].fillna(0,inplace=True)
	variant['PM3_final'].fillna(0,inplace=True)
	variant['PM4_final'].fillna(0,inplace=True)
	variant['PM5_final'].fillna(0,inplace=True)
	variant['PM6_final'].fillna(0,inplace=True)
	variant['PP1_final'].fillna(0,inplace=True)
	variant['PP2_final'].fillna(0,inplace=True)
	variant['PP3_final'].fillna(0,inplace=True)
	variant['PP4_final'].fillna(0,inplace=True)
	variant['PP5_final'].fillna(0,inplace=True)
	variant['PMPOT_final'].fillna(0,inplace=True)
	variant['PPPOT_final'].fillna(0,inplace=True)

	benign_subscore = 'Uncertain Significance'
	pathogenic_subscore = 'Uncertain Significance'
	verdict = 'Uncertain Significance'
	punti_pat = ['PVS1', 'PS1', 'PS2', 'PS3', 'PS4', 'PM1', 'PM2', 'PM3', 'PM4', 'PM5', 'PM6', 'PP1', 'PP2', 'PP3', 'PP4', 'PP5','PMPOT','PPPOT']
	punti_ben = ['BA1', 'BS1', 'BS2', 'BS3', 'BS4', 'BP1', 'BP2', 'BP3', 'BP4', 'BP5', 'BP6', 'BP7']

	n_PM = 0
	n_PP = 0
	n_BP = 0
	pat_model = {}
	index = variant.index[0]
	#dataframe of pathogenicity, rows the criteria and cols the 3 characteristics
	for p in punti_pat:
		if (variant.loc[index, p+'_intensita_final'] == 'Moderate') & (variant.loc[index, p+'_final'] == 1) & ((p == 'PM1') | (p == 'PM3') | (p == 'PM4') | (p == 'PM5') | (p == 'PM6')):
			n_PM = n_PM + 1
		if (variant.loc[index, p+'_intensita_final'] == 'Supporting') & (variant.loc[index, p+'_final'] == 1) & ((p == 'PP1') | (p == 'PP2') | (p == 'PP4') | (p == 'PP5') | (p == 'PP6')):
			n_PP = n_PP + 1
		pat_model[p] = {'Presenza': variant.loc[index, p+'_final'],'Intensita': variant.loc[index, p+'_intensita_final']}
	pat_model = pd.DataFrame.from_dict(pat_model, orient='index')

	if len(pat_model.loc[(pat_model['Intensita'].astype(str) != 'Very Strong') & (pat_model['Intensita'].astype(str) != 'Strong') & (pat_model['Intensita'].astype(str) != 'Moderate') & (pat_model['Intensita'].astype(str) != 'Supporting')]) != 0:
		benign_subscore = 'UNKNOWN'
		pathogenic_subscore = 'UNKNOWN'
		verdict = 'UNKNOWN'
	#dataframe of benignity, rows the criteria and cols the 3 characteristics
	ben_model = {}
	for p in punti_ben:
		if (variant.loc[index, p+'_intensita_final'] == 'Supporting') &  (variant.loc[index, p+'_final'] == 1) & ((p == 'BP1') | (p == 'BP2') | (p == 'BP3') | (p == 'BP4') | (p == 'BP5') | (p == 'BP6')):
			n_BP = n_BP + 1
		#gestisco i diversi modi di scrivere Stand Alone
		if (p == 'BA1') & ((variant.loc[index, p+'_intensita_final'] == 'Stand-Alone') | (variant.loc[index, p+'_intensita_final'] == 'Stand Alone') | (variant.loc[index, p+'_intensita_final'] == 'Stand-alone') | (variant.loc[index, p+'_intensita_final'] == 'Stand alone')):
			ben_model[p] = {'Presenza': variant.loc[index, p+'_final'], 'Intensita': 'Stand Alone'}
		else:
			ben_model[p] = {'Presenza': variant.loc[index, p+'_final'], 'Intensita': variant.loc[index, p+'_intensita_final']}
	ben_model = pd.DataFrame.from_dict(ben_model, orient='index')
	if len(ben_model.loc[((ben_model['Intensita'].astype(str) != 'Stand Alone')) & (ben_model['Intensita'].astype(str) != 'Strong')  & (ben_model['Intensita'].astype(str) != 'Moderate')  & (ben_model['Intensita'].astype(str) != 'Supporting')]) != 0:
		benign_subscore = 'UNKNOWN'
		pathogenic_subscore = 'UNKNOWN'
		verdict = 'UNKNOWN'

	# if (variant.loc[index,'PM2_final'] == 1) & (n_PM != 0) &(n_BP <=1):
	# 	variant.loc[index,'PPPOT_final'] = 1
	# 	pat_model.loc['PPPOT','Presenza'] = 1
	# 	variant.loc[index,'PPPOT_intensita_final'] = 'Supporting'
	# 	if str(variant.loc[index,'PPPOT_cause_final']) != 'nan':
	# 		variant.loc[index,'PPPOT_cause_final'] = str(variant.loc[index,'PPPOT_cause_final']) + 'Verifica segregazione in ulteriori affetti (PP1) o compatibilità di fenotipo (PP4). '
	# 		# pat_model.loc['PPPOT','Causa'] = pat_model.loc['PPPOT','Causa'] + 'Verifica segregazione in ulteriori affetti (PP1) o compatibilità di fenotipo (PP4). '
	# 	else:
	# 		variant.loc[index,'PPPOT_cause_final'] = 'Verifica segregazione in ulteriori affetti (PP1) o compatibilità di fenotipo (PP4). '
	# 		# pat_model.loc['PPPOT','Causa'] = 'Verifica segregazione in ulteriori affetti (PP1) o compatibilità di fenotipo (PP4). '
	#
	# if (variant.loc[index,'PM2_final'] == 1) & (n_PP == 2) &(n_BP <=1):
	# 	variant.loc[index,'PPPOT_final'] = 1
	# 	variant.loc[index,'PPPOT_intensita_final'] = 'Supporting'
	# 	if str(variant.loc[index,'PPPOT_cause_final']) != 'nan':
	# 		variant.loc[index,'PPPOT_cause_final'] = variant.loc[index,'PPPOT_cause_final'] + 'Verifica segregazione in ulteriori affetti (PP1) o compatibilità di fenotipo (PP4). '
	# 	else:
	# 		variant.loc[index,'PPPOT_cause_final'] = 'Verifica segregazione in ulteriori affetti (PP1) o compatibilità di fenotipo (PP4). '

	# if variant.loc[index,'var_on_gene']>=2:
	# 	variant.loc[index,'PMPOT_final'] = 1
	# 	variant.loc[index,'PMPOT_cause_final'] = ' Potenziale PM3 per eterozigosita\' composta'
	# elif variant.loc[index,'zigosita']=='homo':
	# 	variant.loc[index,'PMPOT_final'] = 1
	# 	variant.loc[index,'PMPOT_intensita_final'] = 'Supporting'
	# 	variant.loc[index,'PMPOT_cause_final'] = 'Potenziale PM3 supporting per variante in omozigosi'
	# elif variant.loc[index,'allinheritance'] == 'AD':
	# 	variant.loc[index,'PMPOT_final'] = 1
	# 	variant.loc[index,'PMPOT_cause_final'] = 'Potenziale PM6 per variante de novo'
	# if (variant.loc[index,'consequence'] == None):
	# 	variant.loc[index,'consequence'] = ''
	# if (variant.loc[index,'PM2_final'] == 1) & (variant.loc[index,'PP3_final'] == 1) & (('intron_variant' in variant.loc[index,'consequence']) | ('missense' in variant.loc[index,'consequence']) | ('synonimous' in variant.loc[index,'consequence'])) & (n_BP ==0):
	# 	variant.loc[index,'PMPOT_final'] = 1
	# 	variant.loc[index,'PMPOT_intensita_final'] = 'Moderate'
	# 	if str(variant.loc[index,'PMPOT_cause_final']) != 'nan':
	# 		variant.loc[index,'PMPOT_cause_final'] = variant.loc[index,'PMPOT_cause_final'] + 'Verifica stato allelico (PM3) o insorgenza de novo (PM6). '
	# 	else:
	# 		variant.loc[index,'PMPOT_cause_final'] = 'Verifica stato allelico (PM3) o insorgenza de novo (PM6). '
	present_pat = pat_model[pat_model['Presenza']==1]
	pat_intensities = present_pat['Intensita'].value_counts()
	if 'Very Strong' not in pat_intensities:
		pat_intensities['Very Strong'] = 0
	if 'Strong' not in pat_intensities:
		pat_intensities['Strong'] = 0
	if 'Moderate' not in pat_intensities:
		pat_intensities['Moderate'] = 0
	if 'Supporting' not in pat_intensities:
		pat_intensities['Supporting'] = 0

	present_ben = ben_model[ben_model['Presenza']==1]
	ben_intensities = present_ben['Intensita'].value_counts()
	if 'Stand Alone' not in ben_intensities:
		ben_intensities['Stand Alone'] = 0
	if 'Strong' not in ben_intensities:
		ben_intensities['Strong'] = 0
	if 'Supporting' not in ben_intensities:
		ben_intensities['Supporting'] = 0

	#print ('AAAAAAAAAAA:',pat_intensities,'pat intesity')
	###############Algorithm Varsome Interpratation#############################
	if pathogenic_subscore != 'UNKNOWN':
		if (pat_intensities['Very Strong'] >= 2):
			pathogenic_subscore = 'Pathogenic'
		elif (pat_intensities['Very Strong'] >= 1):
			if pat_intensities['Strong'] >= 1:
				pathogenic_subscore = 'Pathogenic'
			elif pat_intensities['Moderate'] >= 2:
				pathogenic_subscore = 'Pathogenic'
			elif (pat_intensities['Moderate'] ==1) & (pat_intensities['Supporting'] >=1):
				pathogenic_subscore = 'Pathogenic'
			elif pat_intensities['Supporting'] >=2:
				pathogenic_subscore = 'Pathogenic'
			elif pat_intensities['Moderate'] ==1:
				pathogenic_subscore = 'Likely Pathogenic'
		elif ((pat_intensities['Strong'] >= 2) & (pat_intensities['Very Strong'] == 0)):
			#print ('PATHOGENICCCCCCCCCCCCC')
			pathogenic_subscore = 'Pathogenic'
		elif ((pat_intensities['Strong'] == 1) & (pat_intensities['Very Strong'] == 0)):
			if (pat_intensities['Moderate'] == 0) & (pat_intensities['Supporting'] >= 2): #modificato da >=1 supporting a >=2
				pathogenic_subscore = 'Likely Pathogenic'
			elif (((pat_intensities['Moderate'] == 1) | (pat_intensities['Moderate'] == 2)) & (pat_intensities['Supporting'] == 0)):
				pathogenic_subscore = 'Likely Pathogenic'
			elif (pat_intensities['Moderate'] == 1) & (pat_intensities['Supporting'] >= 4):#modificato da >=3 supporting a >=4
				pathogenic_subscore = 'Pathogenic'
			elif (pat_intensities['Moderate'] == 1) & (pat_intensities['Supporting'] < 4):#mod da <3 a <4
				pathogenic_subscore = 'Likely Pathogenic'
			elif (pat_intensities['Moderate'] == 2) & (pat_intensities['Supporting'] >= 2):
				pathogenic_subscore = 'Pathogenic'
			elif (pat_intensities['Moderate'] == 2) & (pat_intensities['Supporting'] < 2):
				pathogenic_subscore = 'Likely Pathogenic'
			elif pat_intensities['Moderate'] >= 3:
				pathogenic_subscore = 'Pathogenic'
		elif ((pat_intensities['Moderate'] == 1) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0)):
			if pat_intensities['Supporting'] >= 4: # 25022022 modificato per nuovo algoritmo #sul web 4
				pathogenic_subscore = 'Likely Pathogenic'
			elif pat_intensities['Supporting'] < 4: # 25022022 modificato per nuovo algoritmo #sul web 4
				pathogenic_subscore = 'Uncertain Significance'
		elif ((pat_intensities['Moderate'] >= 3) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0)):
			pathogenic_subscore =  'Likely Pathogenic'
		elif ((pat_intensities['Moderate'] >= 2) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0)):
			if pat_intensities['Supporting'] == 1:
				pathogenic_subscore = 'Uncertain Significance'
			elif (pat_intensities['Supporting'] == 2):
				pathogenic_subscore =  'Likely Pathogenic'
			elif pat_intensities['Supporting'] >= 3:
				pathogenic_subscore =  'Likely Pathogenic'

	if benign_subscore != 'UNKNOWN':
		if ben_intensities['Stand Alone'] >= 1:
			benign_subscore = 'Benign'
		elif ben_intensities['Strong'] >= 2:
			benign_subscore = 'Benign'
		elif ben_intensities['Strong'] == 1:
			benign_subscore = 'Likely Benign'
		elif ben_intensities['Supporting'] >= 2:
			benign_subscore = 'Likely Benign'
	#############################################################################
	if (benign_subscore == 'Uncertain Significance') & (pathogenic_subscore != 'Uncertain Significance'):
		verdict = pathogenic_subscore
	elif (benign_subscore != 'Uncertain Significance'):	# & (pathogenic_subscore == 'Uncertain Significance'): #questo serve per gestire quando ho 1 strong benigno e dei punti di patogenicita\' attivi
		if (ben_intensities['Strong'] == 1) & (ben_intensities['Stand Alone'] == 0) & (ben_intensities['Supporting'] == 0):
			if ((pat_intensities['Very Strong'] == 0) & (pat_intensities['Strong'] == 0) & (pat_intensities['Moderate'] ==0) & (pat_intensities['Supporting'] == 0)):
				verdict = benign_subscore
			else: verdict = pathogenic_subscore
		else:
			if (pathogenic_subscore == 'Uncertain Significance'): verdict = benign_subscore

    # Inseire Algoritmo per Intensita\' VUS
	if verdict == 'Uncertain Significance':
		vusstatus = 'Unknown'
		if (pat_intensities['Very Strong'] == 1) & (pat_intensities['Strong'] == 0):
			if (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif ((ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2)):
				vusstatus = 'Cold'
			elif ((ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1)):
				vusstatus = 'Cold'
			elif  ((ben_intensities['Strong'] == 1)&(ben_intensities['Supporting'] == 0)):
				if (pat_intensities['Supporting'] == 1): vusstatus='Hot'
				else: vusstatus = 'Middle'
			elif (ben_intensities['Supporting'] == 1) & (ben_intensities['Strong'] == 0):#@
				vusstatus = 'Hot'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Hot'
			elif (pat_intensities['Moderate'] == 1) & (ben_intensities['Supporting'] == 1):#@
				vusstatus = 'Hot'
			elif ((pat_intensities['Supporting']== 1) & (ben_intensities['Supporting']==1)):#@
				vusstatus = 'Middle'
		elif (pat_intensities['Strong'] >= 1) & (pat_intensities['Very Strong'] == 0) & (ben_intensities['Stand Alone'] == 1):
				vusstatus = 'Middle'
		elif (pat_intensities['Strong'] == 1) & (pat_intensities['Very Strong'] == 0):
			if ((ben_intensities['Strong'])+(ben_intensities['Supporting']))>2:#@@
				if ((pat_intensities['Supporting']+pat_intensities['Moderate'])>2): vusstatus = 'Hot'
				else: vusstatus = 'Middle'
			elif ((ben_intensities['Strong'])+(ben_intensities['Supporting']))== 2:#@@
				if ((pat_intensities['Supporting']+pat_intensities['Moderate'])>=2): vusstatus = 'Hot'
				else: vusstatus = 'Middle'
			elif  ((ben_intensities['Strong'])+(ben_intensities['Supporting']))==1: #@@@@
				if (pat_intensities['Supporting'] == 1): vusstatus = 'Hot'
				else: vusstatus = 'Middle'
			elif (ben_intensities['Supporting'] == 2): #@@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Supporting'] == 0):#@
				vusstatus = 'Hot'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Supporting'] == 0):#@
				vusstatus = 'Hot'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Supporting'] == 1):#@
				vusstatus = 'Hot'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Supporting'] == 1):#@
				vusstatus = 'Hot'
		elif (pat_intensities['Moderate'] == 1) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
			if (ben_intensities['Strong'] == 2):#@@@@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2) & (pat_intensities['Supporting'] == 1):#@
				vusstatus = 'Cold'#
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 1):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 2):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2) & (pat_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 1):#@
				vusstatus = 'Middle'#c
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2) & (pat_intensities['Supporting'] == 2):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 1):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 2):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 3):#@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2) & (pat_intensities['Supporting'] == 3):#@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 3):#@
				vusstatus = 'Hot' #m
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 3):#@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 2):#@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 2):#@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 1):#@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 3):#@
				vusstatus = 'Hot'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 3):#@
				vusstatus = 'Hot'
		elif (pat_intensities['Moderate'] == 2) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
			if (ben_intensities['Strong'] == 2):#@@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 0):#@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2) & (pat_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 0):#@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 0):#@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 1):#@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2) & (pat_intensities['Supporting'] == 1):#@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 1):#@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 1):#@
				vusstatus = 'Hot'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 1):#@
				vusstatus = 'Hot'
		elif (pat_intensities['Moderate'] == 3) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
			if (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2):
				vusstatus = 'Hot'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1):
				vusstatus = 'Hot'
			elif (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 0):
				vusstatus = 'Hot'
		elif (pat_intensities['Supporting'] >= 4) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
			if (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Hot'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1):#@
				vusstatus = 'Hot'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2):#@
				vusstatus = 'Cold'
		elif (pat_intensities['Supporting'] == 3) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
			if (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1):#@
				vusstatus = 'Middle'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2):#@
				vusstatus = 'Cold'
		elif (pat_intensities['Supporting'] == 2) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
			if (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2):#@
				vusstatus = 'Cold'
		elif (pat_intensities['Supporting'] == 1) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
			if (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 0):#@
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2):#@
				vusstatus = 'Cold'
		elif (pat_intensities['Supporting'] == 0) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
			if (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0):
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1):
				vusstatus = 'Cold'
			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0):
				vusstatus = 'Cold'
		elif ((ben_intensities['Strong'])+(ben_intensities['Supporting']))==2:
			if ((pat_intensities['Supporting'])+(pat_intensities['Moderate'])+(pat_intensities['Strong'])+(pat_intensities['Very Strong']))>2:
				vusstatus = 'Middle'
			elif ((pat_intensities['Supporting'])+(pat_intensities['Moderate'])+(pat_intensities['Strong'])+(pat_intensities['Very Strong']))==2:
				vusstatus = 'Cold'
			elif ((pat_intensities['Supporting'])+(pat_intensities['Moderate'])+(pat_intensities['Strong'])+(pat_intensities['Very Strong']))<2:
				vusstatus = 'Cold'
		elif ((ben_intensities['Strong'])+(ben_intensities['Supporting']))==3:
			if ((pat_intensities['Supporting'])+(pat_intensities['Moderate'])+(pat_intensities['Strong'])+(pat_intensities['Very Strong']))>3:
				vusstatus = 'Middle'
			elif ((pat_intensities['Supporting'])+(pat_intensities['Moderate'])+(pat_intensities['Strong'])+(pat_intensities['Very Strong']))==3:
				vusstatus = 'Cold'
			elif ((pat_intensities['Supporting'])+(pat_intensities['Moderate'])+(pat_intensities['Strong'])+(pat_intensities['Very Strong']))<3:
				vusstatus = 'Cold'
		elif ((ben_intensities['Strong'])+(ben_intensities['Supporting']))==4:
			if ((pat_intensities['Supporting'])+(pat_intensities['Moderate'])+(pat_intensities['Strong'])+(pat_intensities['Very Strong']))>4:
				vusstatus = 'Middle'
			elif ((pat_intensities['Supporting'])+(pat_intensities['Moderate'])+(pat_intensities['Strong'])+(pat_intensities['Very Strong']))==4:
				vusstatus = 'Cold'
			elif ((pat_intensities['Supporting'])+(pat_intensities['Moderate'])+(pat_intensities['Strong'])+(pat_intensities['Very Strong']))<4:
				vusstatus = 'Cold'
		elif ((ben_intensities['Strong'])+(ben_intensities['Supporting']))>=5:
			if ((pat_intensities['Supporting'])+(pat_intensities['Moderate'])+(pat_intensities['Strong'])+(pat_intensities['Very Strong']))>=5:
				vusstatus = 'Cold'
		#######################################################################################
		if vusstatus=='Middle':
			#print (str(variant.loc[index,'allinheritance']))
			if variant.loc[index,'var_on_gene']>=2:
				variant.loc[index,'PPPOT_final'] = 1
				pat_model.loc['PPPOT','Presenza'] = 1
				variant.loc[index,'PPPOT_cause_final'] = ' Potenziale PM3-PM6-PP1 per eterozigosita\' composta'
				if ((n_PM==1)&(n_PP==2)): variant.loc[index,'PPPOT_intensita_final'] = 'Supporting'
				elif ((n_PM==2)&(n_PP==0)): variant.loc[index,'PPPOT_intensita_final'] = 'Supporting'
				else: variant.loc[index,'PPPOT_intensita_final'] = 'Moderate'
			if variant.loc[index,'zigosita']=='homo':
				variant.loc[index,'PPPOT_final'] = 1
				pat_model.loc['PPPOT','Presenza'] = 1
				variant.loc[index,'PPPOT_cause_final'] = 'Potenziale PM3-PM6-PP1 per variante in omozigosi'
				if ((n_PM==1)&(n_PP==2)): variant.loc[index,'PPPOT_intensita_final'] = 'Supporting'
				elif ((n_PM==2)&(n_PP==0)): variant.loc[index,'PPPOT_intensita_final'] = 'Supporting'
				else: variant.loc[index,'PPPOT_intensita_final'] = 'Moderate'
			if 'AD' in str(variant.loc[index,'allinheritance']):
				variant.loc[index,'PPPOT_final'] = 1
				pat_model.loc['PPPOT','Presenza'] = 1
				variant.loc[index,'PPPOT_cause_final'] = 'Potenziale PM3-PM6-PP1 per variante de novo'
				if ((n_PM==1)&(n_PP==2)): variant.loc[index,'PPPOT_intensita_final'] = 'Supporting'
				elif ((n_PM==2)&(n_PP==0)): variant.loc[index,'PPPOT_intensita_final'] = 'Supporting'
				else: variant.loc[index,'PPPOT_intensita_final'] = 'Moderate'
			###################################################################################################
			###################################################################################################
			present_pat = pat_model[pat_model['Presenza']==1]
			pat_intensities = present_pat['Intensita'].value_counts()
			if 'Very Strong' not in pat_intensities:
				pat_intensities['Very Strong'] = 0
			if 'Strong' not in pat_intensities:
				pat_intensities['Strong'] = 0
			if 'Moderate' not in pat_intensities:
				pat_intensities['Moderate'] = 0
			if 'Supporting' not in pat_intensities:
				pat_intensities['Supporting'] = 0
			###################################################################################################
			#if (pat_intensities['Very Strong'] == 1) & (pat_intensities['Strong'] == 0) & (pat_intensities['Supporting'] == 0) & (pat_intensities['Moderate'] == 0):
			if (pat_intensities['Very Strong'] == 1) & (pat_intensities['Strong'] == 0):
				if (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 0):#@
					vusstatus = 'Cold'
				elif ((ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2)):
					vusstatus = 'Cold'
				elif ((ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1)):
					vusstatus = 'Cold'
				elif  ((ben_intensities['Strong'] == 1)&(ben_intensities['Supporting'] == 0)):
					if (pat_intensities['Supporting'] == 1): vusstatus = 'Hot'
					else: vusstatus = 'Middle'
				elif (ben_intensities['Supporting'] == 1) & (ben_intensities['Strong'] == 0):#@
					vusstatus = 'Hot'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0):#@
					vusstatus = 'Hot'
				elif (pat_intensities['Moderate']==1) & (ben_intensities['Supporting'] == 1):#@
					vusstatus = 'Hot'
				elif (pat_intensities['Supporting']==1) & (ben_intensities['Supporting']==1):#@
					vusstatus = 'Middle'
			elif (pat_intensities['Strong']>=1) & (pat_intensities['Very Strong']==0) & (ben_intensities['Stand Alone'] == 1):
					vusstatus = 'Middle'
			elif (pat_intensities['Strong'] == 1) & (pat_intensities['Very Strong'] == 0):
				if ((ben_intensities['Strong'])+(ben_intensities['Supporting']))>2:#@@
					if ((pat_intensities['Supporting']+pat_intensities['Moderate'])>2): vusstatus = 'Hot'
					else: vusstatus = 'Middle'
				elif ((ben_intensities['Strong'])+(ben_intensities['Supporting']))== 2:#@@
					if ((pat_intensities['Supporting']+pat_intensities['Moderate'])>=2): vusstatus = 'Hot'
					else: vusstatus = 'Middle'
				elif  ((ben_intensities['Strong'])+(ben_intensities['Supporting']))==1: #@@@@
					if (pat_intensities['Supporting'] == 1): vusstatus = 'Hot'
					else: vusstatus = 'Middle'
				elif (ben_intensities['Supporting'] == 2): #@@
					vusstatus = 'Middle'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Supporting'] == 0):#@
					vusstatus = 'Hot'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Supporting'] == 0):#@
					vusstatus = 'Hot'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Supporting'] == 1):#@
					vusstatus = 'Hot'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Supporting'] == 1):#@
					vusstatus = 'Hot'
			elif (pat_intensities['Moderate'] == 1) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
				if (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 1):#@
					vusstatus = 'Middle'
				elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 3):#@
					vusstatus = 'Middle'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2) & (pat_intensities['Supporting'] == 3):#@
					vusstatus = 'Middle'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 3):#@
					vusstatus = 'Hot' #m
				elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 3):#@
					vusstatus = 'Middle'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 1):#@
					vusstatus = 'Middle'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 2):#@
					vusstatus = 'Hot'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 2):#@
					vusstatus = 'Hot'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 3):#@
					vusstatus = 'Hot'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 3):#@
					vusstatus = 'Hot'
			elif (pat_intensities['Moderate'] == 2) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
				if (ben_intensities['Strong'] == 2):#@@
					vusstatus = 'Cold'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 0):#@
					vusstatus = 'Middle'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 0):#@
					vusstatus = 'Middle'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 0):#@
					vusstatus = 'Middle'
				elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 1):#@
					vusstatus = 'Middle'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2) & (pat_intensities['Supporting'] == 1):#@
					vusstatus = 'Middle'
				elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 1):#@
					vusstatus = 'Middle'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 1):#@
					vusstatus = 'Hot'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 1):#@
					vusstatus = 'Hot'
			elif (pat_intensities['Moderate'] == 3) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
				if (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2):
					vusstatus = 'Hot'
				elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1):
					vusstatus = 'Hot'
				elif (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 0):
					vusstatus = 'Hot'
			elif (pat_intensities['Supporting'] >= 4) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
				if (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0):#@
					vusstatus = 'Hot'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1):#@
					vusstatus = 'Hot'
				elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0):#@
					vusstatus = 'Cold'
				elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1):#@
					vusstatus = 'Cold'
				elif (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 0):#@
					vusstatus = 'Cold'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2):#@
					vusstatus = 'Cold'
			elif (pat_intensities['Supporting'] == 3) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
				if (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0):#@
					vusstatus = 'Middle'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1):#@
					vusstatus = 'Middle'
				elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0):#@
					vusstatus = 'Cold'
				elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1):#@
					vusstatus = 'Cold'
				elif (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 0):#@
					vusstatus = 'Cold'
				elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2):#@
					vusstatus = 'Cold'
			elif ((ben_intensities['Strong'])+(ben_intensities['Supporting']))==2:
				if ((pat_intensities['Supporting'])+(pat_intensities['Moderate'])+(pat_intensities['Strong'])+(pat_intensities['Very Strong']))>2:
					vusstatus = 'Middle'
				elif ((pat_intensities['Supporting'])+(pat_intensities['Moderate'])+(pat_intensities['Strong'])+(pat_intensities['Very Strong']))==2:
					vusstatus = 'Middle'
				elif ((pat_intensities['Supporting'])+(pat_intensities['Moderate'])+(pat_intensities['Strong'])+(pat_intensities['Very Strong']))<2:
					vusstatus = 'Middle'
			elif ((ben_intensities['Strong'])+(ben_intensities['Supporting']))==3:
				if ((pat_intensities['Supporting'])+(pat_intensities['Moderate'])+(pat_intensities['Strong'])+(pat_intensities['Very Strong']))>3:
					vusstatus = 'Middle'
				elif ((pat_intensities['Supporting'])+(pat_intensities['Moderate'])+(pat_intensities['Strong'])+(pat_intensities['Very Strong']))==3:
					vusstatus = 'Middle'
				elif ((pat_intensities['Supporting'])+(pat_intensities['Moderate'])+(pat_intensities['Strong'])+(pat_intensities['Very Strong']))<3:
					vusstatus = 'Middle'
			elif ((ben_intensities['Strong'])+(ben_intensities['Supporting']))==4:
				if ((pat_intensities['Supporting'])+(pat_intensities['Moderate'])+(pat_intensities['Strong'])+(pat_intensities['Very Strong']))>4:
					vusstatus = 'Middle'
				elif ((pat_intensities['Supporting'])+(pat_intensities['Moderate'])+(pat_intensities['Strong'])+(pat_intensities['Very Strong']))==4:
					vusstatus = 'Middle'
				elif ((pat_intensities['Supporting'])+(pat_intensities['Moderate'])+(pat_intensities['Strong'])+(pat_intensities['Very Strong']))<4:
					vusstatus = 'Middle'
			elif ((ben_intensities['Strong'])+(ben_intensities['Supporting']))>=5:
				if ((pat_intensities['Supporting'])+(pat_intensities['Moderate'])+(pat_intensities['Strong'])+(pat_intensities['Very Strong']))>=5:
					vusstatus = 'Middle'
			#######################################################################################
	elif ((verdict == 'Pathogenic') | (verdict == 'Likely Pathogenic') | (verdict == 'Benign') | (verdict == 'Likely Benign') ): vusstatus = 'Unknown'
	else:  vusstatus = 'Unknown'
	return verdict, vusstatus,variant.loc[index,'PMPOT_final'],variant.loc[index,'PMPOT_intensita_final'],variant.loc[index,'PMPOT_cause_final'], variant.loc[index,'PPPOT_final'], variant.loc[index,'PPPOT_intensita_final'], variant.loc[index,'PPPOT_cause_final']


	# present_pat = pat_model[pat_model['Presenza']==1]
	# pat_intensities = present_pat['Intensita'].value_counts()
	# if 'Very Strong' not in pat_intensities:
	# 	pat_intensities['Very Strong'] = 0
	# if 'Strong' not in pat_intensities:
	# 	pat_intensities['Strong'] = 0
	# if 'Moderate' not in pat_intensities:
	# 	pat_intensities['Moderate'] = 0
	# if 'Supporting' not in pat_intensities:
	# 	pat_intensities['Supporting'] = 0
	#
	# present_ben = ben_model[ben_model['Presenza']==1]
	# ben_intensities = present_ben['Intensita'].value_counts()
	# if 'Stand Alone' not in ben_intensities:
	# 	ben_intensities['Stand Alone'] = 0
	# if 'Strong' not in ben_intensities:
	# 	ben_intensities['Strong'] = 0
	# if 'Supporting' not in ben_intensities:
	# 	ben_intensities['Supporting'] = 0
	#
	# ###############Algorithm Varsome Interpratation#############################
	# if pathogenic_subscore != 'UNKNOWN':
	# 	if (pat_intensities['Very Strong'] >= 2):
	# 		pathogenic_subscore = 'Pathogenic'
	# 	elif (pat_intensities['Very Strong'] >= 1):
	# 		if pat_intensities['Strong'] >= 1:
	# 			pathogenic_subscore = 'Pathogenic'
	# 		elif pat_intensities['Moderate'] >= 2:
	# 			pathogenic_subscore = 'Pathogenic'
	# 		elif (pat_intensities['Moderate'] ==1) & (pat_intensities['Supporting'] >=1):
	# 			pathogenic_subscore = 'Pathogenic'
	# 		elif pat_intensities['Supporting'] >=2:
	# 			pathogenic_subscore = 'Pathogenic'
	# 		elif pat_intensities['Moderate'] ==1:
	# 			pathogenic_subscore = 'Likely Pathogenic'
	#
	# 	if ((pat_intensities['Strong'] >= 2) & (pat_intensities['Very Strong'] == 0)):
	# 		pathogenic_subscore = 'Pathogenic'
	# 	elif ((pat_intensities['Strong'] == 1) & (pat_intensities['Very Strong'] == 0)):
	# 		if (pat_intensities['Moderate'] == 0) & (pat_intensities['Supporting'] >= 2): #modificato da >=1 supporting a >=2
	# 			pathogenic_subscore = 'Likely Pathogenic'
	# 		elif (((pat_intensities['Moderate'] == 1) | (pat_intensities['Moderate'] == 2)) & (pat_intensities['Supporting'] == 0)):
	# 			pathogenic_subscore = 'Likely Pathogenic'
	# 		elif (pat_intensities['Moderate'] == 1) & (pat_intensities['Supporting'] >= 4):#modificato da >=3 supporting a >=4
	# 			pathogenic_subscore = 'Pathogenic'
	# 		elif (pat_intensities['Moderate'] == 1) & (pat_intensities['Supporting'] < 4):#mod da <3 a <4
	# 			pathogenic_subscore = 'Likely Pathogenic'
	# 		elif (pat_intensities['Moderate'] == 2) & (pat_intensities['Supporting'] >= 2):
	# 			pathogenic_subscore = 'Pathogenic'
	# 		elif (pat_intensities['Moderate'] == 2) & (pat_intensities['Supporting'] < 2):
	# 			pathogenic_subscore = 'Likely Pathogenic'
	# 		elif pat_intensities['Moderate'] >= 3:
	# 			pathogenic_subscore = 'Pathogenic'
	#
	# 	if ((pat_intensities['Moderate'] == 1) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0)):
	# 		if pat_intensities['Supporting'] >= 4: # 25022022 modificato per nuovo algoritmo #sul web 4
	# 			pathogenic_subscore = 'Likely Pathogenic'
	# 		elif pat_intensities['Supporting'] < 4: # 25022022 modificato per nuovo algoritmo #sul web 4
	# 			pathogenic_subscore = 'Uncertain Significance'
	# 	elif (pat_intensities['Moderate'] >= 3):
	# 		pathogenic_subscore =  'Likely Pathogenic'
	# 	elif ((pat_intensities['Moderate'] >= 2) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0)):
	# 		if pat_intensities['Supporting'] == 1:
	# 			pathogenic_subscore = 'Uncertain Significance'
	# 		elif (pat_intensities['Supporting'] == 2):
	# 			pathogenic_subscore =  'Likely Pathogenic'
	# 		elif pat_intensities['Supporting'] >= 3:
	# 			pathogenic_subscore =  'Likely Pathogenic'
	# if benign_subscore != 'UNKNOWN':
	# 	if ben_intensities['Stand Alone'] >= 1:
	# 		benign_subscore = 'Benign'
	# 	elif ben_intensities['Strong'] >= 2:
	# 		benign_subscore = 'Benign'
	# 	elif ben_intensities['Strong'] == 1:
	# 		benign_subscore = 'Likely Benign'
	# 	elif ben_intensities['Supporting'] >= 2:
	# 		benign_subscore = 'Likely Benign'
	# #############################################################################
	# if (benign_subscore == 'Uncertain Significance') & (pathogenic_subscore != 'Uncertain Significance'):
	# 	verdict = pathogenic_subscore
	# elif (benign_subscore != 'Uncertain Significance'):# & (pathogenic_subscore == 'Uncertain Significance'): #questo serve per gestire quando ho 1 strong benigno e dei punti di patogenicità attivi
	# 	if (ben_intensities['Strong'] == 1) & (ben_intensities['Stand Alone'] == 0) & (ben_intensities['Supporting'] == 0):
	# 		if ((pat_intensities['Very Strong'] == 0) & (pat_intensities['Strong'] == 0) & (pat_intensities['Moderate'] ==0) & (pat_intensities['Supporting'] == 0)):
	# 			verdict = benign_subscore
	# 		else:
	# 			verdict = pathogenic_subscore
	# 	else:
	# 		if (pathogenic_subscore == 'Uncertain Significance'):
	# 			verdict = benign_subscore
	#
    # # Inseire Algoritmo per Intensità VUS
	# if verdict == 'Uncertain Significance':
	# 	vusstatus = 'Unkown'
	# 	#if (pat_intensities['Very Strong'] == 1) & (pat_intensities['Strong'] == 0) & (pat_intensities['Supporting'] == 0) & (pat_intensities['Moderate'] == 0):
	# 	if (pat_intensities['Very Strong'] == 1) & (pat_intensities['Strong'] == 0):
	# 		if (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 0):#@
	# 			vusstatus = 'Cold'
	# 		elif ((ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1)) | ((ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0)) | ((ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2)):#  | @ |  @
	# 			vusstatus = 'Middle'
	# 		elif (ben_intensities['Supporting'] == 1) & (ben_intensities['Strong'] == 0):#@
	# 			vusstatus = 'Hot'#@
	# 		elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0):#@
	# 			vusstatus = 'Hot'#@
	# 		elif (pat_intensities['Moderate'] == 1) & (ben_intensities['Supporting'] == 1):#@
	# 			vusstatus = 'Hot'#@
	# 		elif (pat_intensities['Supporting'] == 1) & (ben_intensities['Supporting'] == 1):#@
	# 			vusstatus = 'Middle'#@
	# 		# elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2) & (pat_intensities['Strong'] == 0) & (pat_intensities['Supporting'] == 0) & (pat_intensities['Moderate'] == 0):
	# 		# 	vusstatus = 'Middle'
	# 	elif (pat_intensities['Strong'] == 1) & (pat_intensities['Very Strong'] == 0):
	# 		if (ben_intensities['Strong'] == 2):#@@
	# 			vusstatus = 'Cold'
	# 		elif (ben_intensities['Strong'] == 1): #@@@@
	# 			vusstatus = 'Middle'
	# 		elif (ben_intensities['Supporting'] == 2): #@@
	# 			vusstatus = 'Middle'
	# 		elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Supporting'] == 0):#@
	# 			vusstatus = 'Hot'
	# 		elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Supporting'] == 0):#@
	# 			vusstatus = 'Hot'
	# 		elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Supporting'] == 1):#@
	# 			vusstatus = 'Hot'
	# 		elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Supporting'] == 1):#@
	# 			vusstatus = 'Hot'
	# 	elif (pat_intensities['Moderate'] == 1) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
	# 		if (ben_intensities['Strong'] == 2):#@@@@
	# 			vusstatus = 'Cold'
	# 		elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2) & (pat_intensities['Supporting'] == 1):#@
	# 			vusstatus = 'Cold'#
	# 		elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 0):#@
	# 			vusstatus = 'Cold'
	# 		elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 1):#@
	# 			vusstatus = 'Cold'
	# 		elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 2):#@
	# 			vusstatus = 'Cold'
	# 		elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2) & (pat_intensities['Supporting'] == 0):#@
	# 			vusstatus = 'Cold'
	# 		elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 1):#@
	# 			vusstatus = 'Middle'#c
	# 		elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2) & (pat_intensities['Supporting'] == 2):#@
	# 			vusstatus = 'Cold'
	# 		elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 0):#@
	# 			vusstatus = 'Cold'
	# 		elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 1):#@
	# 			vusstatus = 'Cold'
	# 		elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 2):#@
	# 			vusstatus = 'Cold'
	# 		elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 0):#@
	# 			vusstatus = 'Cold'
	# 		elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 0):#@
	# 			vusstatus = 'Cold'
	# 		elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 3):#@
	# 			vusstatus = 'Middle'
	# 		elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2) & (pat_intensities['Supporting'] == 3):#@
	# 			vusstatus = 'Middle'
	# 		elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 3):#@
	# 			vusstatus = 'Hot' #m
	# 		elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 3):#@
	# 			vusstatus = 'Middle'
	# 		elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 2):#@
	# 			vusstatus = 'Middle'
	# 		elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 2):#@
	# 			vusstatus = 'Middle'
	# 		elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 1):#@
	# 			vusstatus = 'Middle'
	# 		elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 3):#@
	# 			vusstatus = 'Hot'
	# 		elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 3):#@
	# 			vusstatus = 'Hot'
	# 	elif (pat_intensities['Moderate'] == 2) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
	# 		if (ben_intensities['Strong'] == 2):#@@
	# 			vusstatus = 'Cold'
	# 		elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 0):#@
	# 			vusstatus = 'Cold'
	# 		elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2) & (pat_intensities['Supporting'] == 0):#@
	# 			vusstatus = 'Cold'
	# 		elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 0):#@
	# 			vusstatus = 'Cold'
	# 		elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 0):#@
	# 			vusstatus = 'Middle'
	# 		elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 0):#@
	# 			vusstatus = 'Middle'
	# 		elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 1):#@
	# 			vusstatus = 'Middle'
	# 		elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2) & (pat_intensities['Supporting'] == 1):#@
	# 			vusstatus = 'Middle'
	# 		elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 1):#@
	# 			vusstatus = 'Middle'
	# 		elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 1):#@
	# 			vusstatus = 'Hot'
	# 		elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 1):#@
	# 			vusstatus = 'Hot'
	# 	elif (pat_intensities['Moderate'] == 3) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
	# 		if (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2):
	# 			vusstatus = 'Hot'
	# 		elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1):
	# 			vusstatus = 'Hot'
	# 		elif (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 0):
	# 			vusstatus = 'Hot'
	# 	elif (pat_intensities['Supporting'] == 4) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
	# 		if (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0):#@
	# 			vusstatus = 'Middle'
	# 		elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1):#@
	# 			vusstatus = 'Middle'
	# 		elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0):#@
	# 			vusstatus = 'Cold'
	# 		elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1):#@
	# 			vusstatus = 'Cold'
	# 		elif (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 0):#@
	# 			vusstatus = 'Cold'
	# 		elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2):#@
	# 			vusstatus = 'Cold'
	# 	elif (pat_intensities['Supporting'] == 3) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
	# 		if (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0):#@
	# 			vusstatus = 'Cold'
	# 		elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1):#@
	# 			vusstatus = 'Cold'
	# 		elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0):#@
	# 			vusstatus = 'Cold'
	# 		elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1):#@
	# 			vusstatus = 'Cold'
	# 		elif (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 0):#@
	# 			vusstatus = 'Cold'
	# 		elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2):#@
	# 			vusstatus = 'Cold'
	# 	elif (pat_intensities['Supporting'] == 2) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
	# 		if (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0):#@
	# 			vusstatus = 'Cold'
	# 		elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1):#@
	# 			vusstatus = 'Cold'
	# 		elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0):#@
	# 			vusstatus = 'Cold'
	# 		elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1):#@
	# 			vusstatus = 'Cold'
	# 		elif (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 0):#@
	# 			vusstatus = 'Cold'
	# 		elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2):#@
	# 			vusstatus = 'Cold'
	# 	elif (pat_intensities['Supporting'] == 1) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
	# 		if (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0):#@
	# 			vusstatus = 'Cold'
	# 		elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1):#@
	# 			vusstatus = 'Cold'
	# 		elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0):#@
	# 			vusstatus = 'Cold'
	# 		elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1):#@
	# 			vusstatus = 'Cold'
	# 		elif (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 0):#@
	# 			vusstatus = 'Cold'
	# 		elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2):#@
	# 			vusstatus = 'Cold'
	# 	elif (pat_intensities['Supporting'] == 0) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
	# 		if (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0):
	# 			vusstatus = 'Cold'
	# 		elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1):
	# 			vusstatus = 'Cold'
	# 	elif (pat_intensities['Supporting'] > 0) | (pat_intensities['Moderate'] > 0) | (pat_intensities['Strong'] > 0) | (pat_intensities['Very Strong'] > 0):
	# 		if (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 3):
	# 			vusstatus = 'Cold'
	# 		elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 2):
	# 			vusstatus = 'Cold'
	# 		elif (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 1):
	# 			vusstatus = 'Cold'
	# 		elif (ben_intensities['Strong'] == 3) & (ben_intensities['Supporting'] == 0):
	# 			vusstatus = 'Cold'
	# 	#vusstatus = 'Hot' #cold, middle
	# else:  vusstatus = 'Unkown'
	#
	# return verdict, vusstatus, variant.loc[index,'PMPOT_final'],variant.loc[index, 'PMPOT_intensita_final'],variant.loc[index,'PMPOT_cause_final'], variant.loc[index,'PPPOT_final'], variant.loc[index,'PPPOT_intensita_final'], variant.loc[index,'PPPOT_cause_final']









# def variant_interpretation(variant):
# 	# print('variant_interpretation')
# 	variant['BA1_intensita_final'].fillna(strength_dict['BA1'],inplace=True)
# 	variant['BS1_intensita_final'].fillna(strength_dict['BS1'],inplace=True)
# 	variant['BS2_intensita_final'].fillna(strength_dict['BS2'],inplace=True)
# 	variant['BS3_intensita_final'].fillna(strength_dict['BS3'],inplace=True)
# 	variant['BS4_intensita_final'].fillna(strength_dict['BS4'],inplace=True)
# 	variant['BP1_intensita_final'].fillna(strength_dict['BP1'],inplace=True)
# 	variant['BP2_intensita_final'].fillna(strength_dict['BP2'],inplace=True)
# 	variant['BP3_intensita_final'].fillna(strength_dict['BP3'],inplace=True)
# 	variant['BP4_intensita_final'].fillna(strength_dict['BP4'],inplace=True)
# 	variant['BP5_intensita_final'].fillna(strength_dict['BP5'],inplace=True)
# 	variant['BP6_intensita_final'].fillna(strength_dict['BP6'],inplace=True)
# 	variant['BP7_intensita_final'].fillna(strength_dict['BP7'],inplace=True)
# 	variant['PVS1_intensita_final'].fillna(strength_dict['PVS1'],inplace=True)
# 	variant['PS1_intensita_final'].fillna(strength_dict['PS1'],inplace=True)
# 	variant['PS2_intensita_final'].fillna(strength_dict['PS2'],inplace=True)
# 	variant['PS3_intensita_final'].fillna(strength_dict['PS3'],inplace=True)
# 	variant['PS4_intensita_final'].fillna(strength_dict['PS4'],inplace=True)
# 	variant['PM1_intensita_final'].fillna(strength_dict['PM1'],inplace=True)
# 	variant['PM2_intensita_final'].fillna(strength_dict['PM2'],inplace=True)
# 	variant['PM3_intensita_final'].fillna(strength_dict['PM3'],inplace=True)
# 	variant['PM4_intensita_final'].fillna(strength_dict['PM4'],inplace=True)
# 	variant['PM5_intensita_final'].fillna(strength_dict['PM5'],inplace=True)
# 	variant['PM6_intensita_final'].fillna(strength_dict['PM6'],inplace=True)
# 	variant['PP1_intensita_final'].fillna(strength_dict['PP1'],inplace=True)
# 	variant['PP2_intensita_final'].fillna(strength_dict['PP2'],inplace=True)
# 	variant['PP3_intensita_final'].fillna(strength_dict['PP3'],inplace=True)
# 	variant['PP4_intensita_final'].fillna(strength_dict['PP4'],inplace=True)
# 	variant['PP5_intensita_final'].fillna(strength_dict['PP5'],inplace=True)
# 	variant['PMPOT_intensita_final'].fillna(strength_dict['PMPOT'],inplace=True)
# 	variant['PPPOT_intensita_final'].fillna(strength_dict['PPPOT'],inplace=True)
#
# 	variant['BA1_final'].fillna(0,inplace=True)
# 	variant['BS1_final'].fillna(0,inplace=True)
# 	variant['BS2_final'].fillna(0,inplace=True)
# 	variant['BS3_final'].fillna(0,inplace=True)
# 	variant['BS4_final'].fillna(0,inplace=True)
# 	variant['BP1_final'].fillna(0,inplace=True)
# 	variant['BP2_final'].fillna(0,inplace=True)
# 	variant['BP3_final'].fillna(0,inplace=True)
# 	variant['BP4_final'].fillna(0,inplace=True)
# 	variant['BP5_final'].fillna(0,inplace=True)
# 	variant['BP6_final'].fillna(0,inplace=True)
# 	variant['BP7_final'].fillna(0,inplace=True)
# 	variant['PVS1_final'].fillna(0,inplace=True)
# 	variant['PS2_final'].fillna(0,inplace=True)
# 	variant['PS3_final'].fillna(0,inplace=True)
# 	variant['PS1_final'].fillna(0,inplace=True)
# 	variant['PS4_final'].fillna(0,inplace=True)
# 	variant['PM1_final'].fillna(0,inplace=True)
# 	variant['PM2_final'].fillna(0,inplace=True)
# 	variant['PM3_final'].fillna(0,inplace=True)
# 	variant['PM4_final'].fillna(0,inplace=True)
# 	variant['PM5_final'].fillna(0,inplace=True)
# 	variant['PM6_final'].fillna(0,inplace=True)
# 	variant['PP1_final'].fillna(0,inplace=True)
# 	variant['PP2_final'].fillna(0,inplace=True)
# 	variant['PP3_final'].fillna(0,inplace=True)
# 	variant['PP4_final'].fillna(0,inplace=True)
# 	variant['PP5_final'].fillna(0,inplace=True)
# 	variant['PMPOT_final'].fillna(0,inplace=True)
# 	variant['PPPOT_final'].fillna(0,inplace=True)
#
# 	benign_subscore = 'Uncertain Significance'
# 	pathogenic_subscore = 'Uncertain Significance'
# 	verdict = 'Uncertain Significance'
# 	punti_pat = ['PVS1', 'PS1', 'PS2', 'PS3', 'PS4', 'PM1', 'PM2', 'PM3', 'PM4', 'PM5', 'PM6', 'PP1', 'PP2', 'PP3', 'PP4', 'PP5', 'PMPOT', 'PPPOT']
# 	punti_ben = ['BA1', 'BS1', 'BS2', 'BS3', 'BS4', 'BP1', 'BP2', 'BP3', 'BP4', 'BP5', 'BP6', 'BP7']
#
# 	n_PM = 0
# 	n_PP = 0
# 	n_BP = 0
# 	pat_model = {}
# 	index = variant.index[0]
#
# 	for p in punti_pat:
# 		# print(p)
# 		# print(variant.loc[index, p+'_final'])
# 		if (variant.loc[index, p+'_intensita_final'] == 'Moderate') & (variant.loc[index, p+'_final'] == 1) & ((p == 'PM1') | (p == 'PM3') | (p == 'PM4') | (p == 'PM5') | (p == 'PM6')):
# 			n_PM = n_PM + 1
# 			# print(n_PM,'in n_PM')
# 		if (variant.loc[index, p+'_intensita_final'] == 'Supporting') & (variant.loc[index, p+'_final'] == 1) & ((p == 'PP1') | (p == 'PP2') | (p == 'PP4') | (p == 'PP5') | (p == 'PP6')):
# 			n_PP = n_PP + 1
# 			# print(n_PP, 'in n_PP')
# 		pat_model[p] = {'Presenza': variant.loc[index, p+'_final'], 'Intensita': variant.loc[index, p+'_intensita_final'], 'Causa': variant.loc[index, p+'_cause_final']}
# 	pat_model = pd.DataFrame.from_dict(pat_model, orient='index')
#
# 	if len(pat_model.loc[(pat_model['Intensita'].astype(str) != 'Very Strong') & (pat_model['Intensita'].astype(str) != 'Strong') & (pat_model['Intensita'].astype(str) != 'Moderate') & (pat_model['Intensita'].astype(str) != 'Supporting')]) != 0:
# 		benign_subscore = 'UNKNOWN'
# 		pathogenic_subscore = 'UNKNOWN'
# 		verdict = 'UNKNOWN'
# 	ben_model = {}
# 	for p in punti_ben:
# 		if (variant.loc[index, p+'_intensita_final'] == 'Supporting') &  (variant.loc[index, p+'_final'] == 1) & ((p == 'BP1') | (p == 'BP2') | (p == 'BP3') | (p == 'BP5') | (p == 'BP6') | (p == 'BP6')):
# 			n_BP = n_BP + 1
# 		#gestisco i diversi modi di scrivere Stand Alone
# 		if (p == 'BA1') & ((variant.loc[index, p+'_intensita_final'] == 'Stand-Alone') | (variant.loc[index, p+'_intensita_final'] == 'Stand Alone') | (variant.loc[index, p+'_intensita_final'] == 'Stand-alone') | (variant.loc[index, p+'_intensita_final'] == 'Stand alone')):
# 			ben_model[p] = {'Presenza': variant.loc[index, p+'_final'], 'Intensita': 'Stand Alone', 'Causa': variant.loc[index, p+'_cause_final']}
# 		else:
# 			ben_model[p] = {'Presenza': variant.loc[index, p+'_final'], 'Intensita': variant.loc[index, p+'_intensita_final'], 'Causa': variant.loc[index, p+'_cause_final']}
# 	ben_model = pd.DataFrame.from_dict(ben_model, orient='index')
# 	# print(ben_model)
#
# 	if len(ben_model.loc[((ben_model['Intensita'].astype(str) != 'Stand Alone')) & (ben_model['Intensita'].astype(str) != 'Strong') & (ben_model['Intensita'].astype(str) != 'Moderate') & (ben_model['Intensita'].astype(str) != 'Supporting')]) != 0:
# 		benign_subscore = 'UNKNOWN'
# 		pathogenic_subscore = 'UNKNOWN'
# 		verdict = 'UNKNOWN'
#
# 	if (variant.loc[index,'PM2_final'] == 1) & (n_PM != 0) &(n_BP <=1):
# 		# print('in pppot 1')
# 		variant.loc[index,'PPPOT_final'] = 1
# 		pat_model.loc['PPPOT','Presenza'] = 1
# 		variant.loc[index,'PPPOT_intensita_final'] = 'Supporting'
# 		if str(variant.loc[index,'PPPOT_cause_final']) != 'nan':
# 			variant.loc[index,'PPPOT_cause_final'] = variant.loc[index,'PPPOT_cause_final'] + 'Verifica segregazione in ulteriori affetti (PP1) o compatibilità di fenotipo (PP4). '
# 			pat_model.loc['PPPOT','Causa'] = pat_model.loc['PPPOT','Causa'] + 'Verifica segregazione in ulteriori affetti (PP1) o compatibilità di fenotipo (PP4). '
# 		else:
# 			variant.loc[index,'PPPOT_cause_final'] = 'Verifica segregazione in ulteriori affetti (PP1) o compatibilità di fenotipo (PP4). '
# 			pat_model.loc['PPPOT','Causa'] = 'Verifica segregazione in ulteriori affetti (PP1) o compatibilità di fenotipo (PP4). '
#
# 	if (variant.loc[index,'PM2_final'] == 1) & (n_PP == 2) &(n_BP <=1):
# 		variant.loc[index,'PPPOT_final'] = 1
# 		variant.loc[index,'PPPOT_intensita_final'] = 'Supporting'
# 		if str(variant.loc[index,'PPPOT_cause_final']) != 'nan':
# 			variant.loc[index,'PPPOT_cause_final'] = variant.loc[index,'PPPOT_cause_final'] + 'Verifica segregazione in ulteriori affetti (PP1) o compatibilità di fenotipo (PP4). '
# 		else:
# 			variant.loc[index,'PPPOT_cause_final'] = 'Verifica segregazione in ulteriori affetti (PP1) o compatibilità di fenotipo (PP4). '
#
# 	if variant.loc[index,'var_on_gene']>=2:
# 		# print('1')
# 		variant.loc[index,'PMPOT_final'] = 1
# 		variant.loc[index,'PMPOT_cause_final'] = ' Potenziale PM3 per eterozigosita\' composta'
# 	elif variant.loc[index,'zigosita']=='homo':
# 		# print('2')
# 		variant.loc[index,'PMPOT_final'] = 1
# 		variant.loc[index,'PMPOT_intensita_final'] = 'Supporting'
# 		variant.loc[index,'PMPOT_cause_final'] = 'Potenziale PM3 supporting per variante in omozigosi'
# 	elif variant.loc[index,'allinheritance'] == 'AD':
# 		# print('3')
# 		variant.loc[index,'PMPOT_final'] = 1
# 		variant.loc[index,'PMPOT_cause_final'] = 'Potenziale PM6 per variante de novo'
# 	if (variant.loc[index,'PM2_final'] == 1) & (variant.loc[index,'PP3_final'] == 1) & (('intron_variant' in variant.loc[index,'consequence']) | ('missense' in variant.loc[index,'consequence']) | ('synonimous' in variant.loc[index,'consequence'])) & (n_BP ==0):
# 		# print('in pmpot 1')
# 		variant.loc[index,'PMPOT_final'] = 1
# 		variant.loc[index,'PMPOT_intensita_final'] = 'Moderate'
# 		# print(variant.loc[index,'PMPOT_cause_final'])
# 		if str(variant.loc[index,'PMPOT_cause_final']) != 'nan':
# 			variant.loc[index,'PMPOT_cause_final'] = variant.loc[index,'PMPOT_cause_final'] + 'Verifica stato allelico (PM3) o insorgenza de novo (PM6). '
# 		else:
# 			variant.loc[index,'PMPOT_cause_final'] = 'Verifica stato allelico (PM3) o insorgenza de novo (PM6). '
#
# 	present_pat = pat_model[pat_model['Presenza']==1]
# 	pat_intensities = present_pat['Intensita'].value_counts()
# 	if 'Very Strong' not in pat_intensities:
# 		pat_intensities['Very Strong'] = 0
# 	if 'Strong' not in pat_intensities:
# 		pat_intensities['Strong'] = 0
# 	if 'Moderate' not in pat_intensities:
# 		pat_intensities['Moderate'] = 0
# 	if 'Supporting' not in pat_intensities:
# 		pat_intensities['Supporting'] = 0
#
# 	present_ben = ben_model[ben_model['Presenza']==1]
# 	ben_intensities = present_ben['Intensita'].value_counts()
# 	if 'Stand Alone' not in ben_intensities:
# 		ben_intensities['Stand Alone'] = 0
# 	if 'Strong' not in ben_intensities:
# 		ben_intensities['Strong'] = 0
# 	if 'Supporting' not in ben_intensities:
# 		ben_intensities['Supporting'] = 0
#
# 	###############Algorithm Varsome Interpratation#############################
# 	if pathogenic_subscore != 'UNKNOWN':
# 		if (pat_intensities['Very Strong'] >= 2):
# 			pathogenic_subscore = 'Pathogenic'
# 		elif (pat_intensities['Very Strong'] >= 1):
# 			if pat_intensities['Strong'] >= 1:
# 				pathogenic_subscore = 'Pathogenic'
# 			elif pat_intensities['Moderate'] >= 2:
# 				pathogenic_subscore = 'Pathogenic'
# 			elif (pat_intensities['Moderate'] ==1) & (pat_intensities['Supporting'] >=1):
# 				pathogenic_subscore = 'Pathogenic'
# 			elif pat_intensities['Supporting'] >=2:
# 				pathogenic_subscore = 'Pathogenic'
# 			elif pat_intensities['Moderate'] ==1:
# 				pathogenic_subscore = 'Likely Pathogenic'
#
# 		if ((pat_intensities['Strong'] >= 2) & (pat_intensities['Very Strong'] == 0)):
# 			pathogenic_subscore = 'Pathogenic'
# 		elif ((pat_intensities['Strong'] == 1) & (pat_intensities['Very Strong'] == 0)):
# 			if (pat_intensities['Moderate'] == 0) & (pat_intensities['Supporting'] >= 2): #modificato da >=1 supporting a >=2
# 				pathogenic_subscore = 'Likely Pathogenic'
# 			elif (((pat_intensities['Moderate'] == 1) | (pat_intensities['Moderate'] == 2)) & (pat_intensities['Supporting'] == 0)):
# 				pathogenic_subscore = 'Likely Pathogenic'
# 			elif (pat_intensities['Moderate'] == 1) & (pat_intensities['Supporting'] >= 4):#modificato da >=3 supporting a >=4
# 				pathogenic_subscore = 'Pathogenic'
# 			elif (pat_intensities['Moderate'] == 1) & (pat_intensities['Supporting'] < 4):#mod da <3 a <4
# 				pathogenic_subscore = 'Likely Pathogenic'
# 			elif (pat_intensities['Moderate'] == 2) & (pat_intensities['Supporting'] >= 2):
# 				pathogenic_subscore = 'Pathogenic'
# 			elif (pat_intensities['Moderate'] == 2) & (pat_intensities['Supporting'] < 2):
# 				pathogenic_subscore = 'Likely Pathogenic'
# 			elif pat_intensities['Moderate'] >= 3:
# 				pathogenic_subscore = 'Pathogenic'
#
# 		if ((pat_intensities['Moderate'] == 1) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0)):
# 			if pat_intensities['Supporting'] >= 4: # 25022022 modificato per nuovo algoritmo #sul web 4
# 				pathogenic_subscore = 'Likely Pathogenic'
# 			elif pat_intensities['Supporting'] < 4: # 25022022 modificato per nuovo algoritmo #sul web 4
# 				pathogenic_subscore = 'Uncertain Significance'
# 		elif (pat_intensities['Moderate'] >= 3):
# 			pathogenic_subscore =  'Likely Pathogenic'
# 		elif ((pat_intensities['Moderate'] >= 2) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0)):
# 			if pat_intensities['Supporting'] == 1:
# 				pathogenic_subscore = 'Uncertain Significance'
# 			elif (pat_intensities['Supporting'] == 2):
# 				pathogenic_subscore =  'Likely Pathogenic'
# 			elif pat_intensities['Supporting'] >= 3:
# 				pathogenic_subscore =  'Likely Pathogenic'
# 	if benign_subscore != 'UNKNOWN':
# 		if ben_intensities['Stand Alone'] >= 1:
# 			benign_subscore = 'Benign'
# 		elif ben_intensities['Strong'] >= 2:
# 			benign_subscore = 'Benign'
# 		elif ben_intensities['Strong'] == 1:
# 			benign_subscore = 'Likely Benign'
# 		elif ben_intensities['Supporting'] >= 2:
# 			benign_subscore = 'Likely Benign'
# 	#############################################################################
# 	if (benign_subscore == 'Uncertain Significance') & (pathogenic_subscore != 'Uncertain Significance'):
# 		verdict = pathogenic_subscore
# 	elif (benign_subscore != 'Uncertain Significance'):# & (pathogenic_subscore == 'Uncertain Significance'): #questo serve per gestire quando ho 1 strong benigno e dei punti di patogenicità attivi
# 		if (ben_intensities['Strong'] == 1) & (ben_intensities['Stand Alone'] == 0) & (ben_intensities['Supporting'] == 0):
# 			if ((pat_intensities['Very Strong'] == 0) & (pat_intensities['Strong'] == 0) & (pat_intensities['Moderate'] ==0) & (pat_intensities['Supporting'] == 0)):
# 				verdict = benign_subscore
# 			else:
# 				verdict = pathogenic_subscore
# 		else:
# 			if (pathogenic_subscore == 'Uncertain Significance'):
# 				verdict = benign_subscore
#
#     # Inseire Algoritmo per Intensità VUS
# 	if verdict == 'Uncertain Significance':
# 		vusstatus = 'Unkown'
# 		#if (pat_intensities['Very Strong'] == 1) & (pat_intensities['Strong'] == 0) & (pat_intensities['Supporting'] == 0) & (pat_intensities['Moderate'] == 0):
# 		if (pat_intensities['Very Strong'] == 1) & (pat_intensities['Strong'] == 0):
# 			if (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 0):#@
# 				vusstatus = 'Cold'
# 			elif ((ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1)) | ((ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0)) | ((ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2)):#  | @ |  @
# 				vusstatus = 'Middle'
# 			elif (ben_intensities['Supporting'] == 1) & (ben_intensities['Strong'] == 0):#@
# 				vusstatus = 'Hot'#@
# 			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0):#@
# 				vusstatus = 'Hot'#@
# 			elif (pat_intensities['Moderate'] == 1) & (ben_intensities['Supporting'] == 1):#@
# 				vusstatus = 'Hot'#@
# 			elif (pat_intensities['Supporting'] == 1) & (ben_intensities['Supporting'] == 1):#@
# 				vusstatus = 'Middle'#@
# 			# elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2) & (pat_intensities['Strong'] == 0) & (pat_intensities['Supporting'] == 0) & (pat_intensities['Moderate'] == 0):
# 			# 	vusstatus = 'Middle'
# 		elif (pat_intensities['Strong'] == 1) & (pat_intensities['Very Strong'] == 0):
# 			if (ben_intensities['Strong'] == 2):#@@
# 				vusstatus = 'Cold'
# 			elif (ben_intensities['Strong'] == 1): #@@@@
# 				vusstatus = 'Middle'
# 			elif (ben_intensities['Supporting'] == 2): #@@
# 				vusstatus = 'Middle'
# 			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Supporting'] == 0):#@
# 				vusstatus = 'Hot'
# 			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Supporting'] == 0):#@
# 				vusstatus = 'Hot'
# 			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Supporting'] == 1):#@
# 				vusstatus = 'Hot'
# 			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Supporting'] == 1):#@
# 				vusstatus = 'Hot'
# 		elif (pat_intensities['Moderate'] == 1) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
# 			if (ben_intensities['Strong'] == 2):#@@@@
# 				vusstatus = 'Cold'
# 			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2) & (pat_intensities['Supporting'] == 1):#@
# 				vusstatus = 'Cold'#
# 			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 0):#@
# 				vusstatus = 'Cold'
# 			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 1):#@
# 				vusstatus = 'Cold'
# 			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 2):#@
# 				vusstatus = 'Cold'
# 			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2) & (pat_intensities['Supporting'] == 0):#@
# 				vusstatus = 'Cold'
# 			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 1):#@
# 				vusstatus = 'Middle'#c
# 			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2) & (pat_intensities['Supporting'] == 2):#@
# 				vusstatus = 'Cold'
# 			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 0):#@
# 				vusstatus = 'Cold'
# 			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 1):#@
# 				vusstatus = 'Cold'
# 			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 2):#@
# 				vusstatus = 'Cold'
# 			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 0):#@
# 				vusstatus = 'Cold'
# 			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 0):#@
# 				vusstatus = 'Cold'
# 			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 3):#@
# 				vusstatus = 'Middle'
# 			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2) & (pat_intensities['Supporting'] == 3):#@
# 				vusstatus = 'Middle'
# 			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 3):#@
# 				vusstatus = 'Hot' #m
# 			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 3):#@
# 				vusstatus = 'Middle'
# 			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 2):#@
# 				vusstatus = 'Middle'
# 			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 2):#@
# 				vusstatus = 'Middle'
# 			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 1):#@
# 				vusstatus = 'Middle'
# 			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 3):#@
# 				vusstatus = 'Hot'
# 			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 3):#@
# 				vusstatus = 'Hot'
# 		elif (pat_intensities['Moderate'] == 2) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
# 			if (ben_intensities['Strong'] == 2):#@@
# 				vusstatus = 'Cold'
# 			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 0):#@
# 				vusstatus = 'Cold'
# 			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2) & (pat_intensities['Supporting'] == 0):#@
# 				vusstatus = 'Cold'
# 			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 0):#@
# 				vusstatus = 'Cold'
# 			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 0):#@
# 				vusstatus = 'Middle'
# 			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 0):#@
# 				vusstatus = 'Middle'
# 			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 1):#@
# 				vusstatus = 'Middle'
# 			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2) & (pat_intensities['Supporting'] == 1):#@
# 				vusstatus = 'Middle'
# 			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 1):#@
# 				vusstatus = 'Middle'
# 			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0) & (pat_intensities['Supporting'] == 1):#@
# 				vusstatus = 'Hot'
# 			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1) & (pat_intensities['Supporting'] == 1):#@
# 				vusstatus = 'Hot'
# 		elif (pat_intensities['Moderate'] == 3) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
# 			if (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2):
# 				vusstatus = 'Hot'
# 			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1):
# 				vusstatus = 'Hot'
# 			elif (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 0):
# 				vusstatus = 'Hot'
# 		elif (pat_intensities['Supporting'] == 4) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
# 			if (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0):#@
# 				vusstatus = 'Middle'
# 			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1):#@
# 				vusstatus = 'Middle'
# 			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0):#@
# 				vusstatus = 'Cold'
# 			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1):#@
# 				vusstatus = 'Cold'
# 			elif (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 0):#@
# 				vusstatus = 'Cold'
# 			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2):#@
# 				vusstatus = 'Cold'
# 		elif (pat_intensities['Supporting'] == 3) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
# 			if (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0):#@
# 				vusstatus = 'Cold'
# 			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1):#@
# 				vusstatus = 'Cold'
# 			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0):#@
# 				vusstatus = 'Cold'
# 			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1):#@
# 				vusstatus = 'Cold'
# 			elif (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 0):#@
# 				vusstatus = 'Cold'
# 			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2):#@
# 				vusstatus = 'Cold'
# 		elif (pat_intensities['Supporting'] == 2) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
# 			if (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0):#@
# 				vusstatus = 'Cold'
# 			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1):#@
# 				vusstatus = 'Cold'
# 			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0):#@
# 				vusstatus = 'Cold'
# 			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1):#@
# 				vusstatus = 'Cold'
# 			elif (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 0):#@
# 				vusstatus = 'Cold'
# 			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2):#@
# 				vusstatus = 'Cold'
# 		elif (pat_intensities['Supporting'] == 1) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
# 			if (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 0):#@
# 				vusstatus = 'Cold'
# 			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1):#@
# 				vusstatus = 'Cold'
# 			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0):#@
# 				vusstatus = 'Cold'
# 			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 1):#@
# 				vusstatus = 'Cold'
# 			elif (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 0):#@
# 				vusstatus = 'Cold'
# 			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 2):#@
# 				vusstatus = 'Cold'
# 		elif (pat_intensities['Supporting'] == 0) & (pat_intensities['Moderate'] == 0) & (pat_intensities['Strong'] == 0) & (pat_intensities['Very Strong'] == 0):
# 			if (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 0):
# 				vusstatus = 'Cold'
# 			elif (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 1):
# 				vusstatus = 'Cold'
# 		elif (pat_intensities['Supporting'] > 0) | (pat_intensities['Moderate'] > 0) | (pat_intensities['Strong'] > 0) | (pat_intensities['Very Strong'] > 0):
# 			if (ben_intensities['Strong'] == 0) & (ben_intensities['Supporting'] == 3):
# 				vusstatus = 'Cold'
# 			elif (ben_intensities['Strong'] == 1) & (ben_intensities['Supporting'] == 2):
# 				vusstatus = 'Cold'
# 			elif (ben_intensities['Strong'] == 2) & (ben_intensities['Supporting'] == 1):
# 				vusstatus = 'Cold'
# 			elif (ben_intensities['Strong'] == 3) & (ben_intensities['Supporting'] == 0):
# 				vusstatus = 'Cold'
# 		#vusstatus = 'Hot' #cold, middle
# 	else:  vusstatus = 'Unkown'
#
# 	return verdict, vusstatus, variant.loc[index,'PMPOT_final'],variant.loc[index, 'PMPOT_intensita_final'],variant.loc[index,'PMPOT_cause_final'], variant.loc[index,'PPPOT_final'], variant.loc[index,'PPPOT_intensita_final'], variant.loc[index,'PPPOT_cause_final']
