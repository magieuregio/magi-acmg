#!/usr/bin/env bash
#############  Author:   Ing. MUHARREM DAJA   - 05/11/2020
NAME=${1?Error: no name given. Scrivi il nome del file che vuoi mettere su GITLAB }

git add "$NAME"
git commit -m "Last commit- Data: $(date +'%d/%m/%Y')"
git push -u origin master

#echo "Hello! $NAME and $(date +'%d/%m/%Y')"
